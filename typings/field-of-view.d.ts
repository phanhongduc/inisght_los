declare module "field-of-view" {
  export const fieldOfView: {
    fromFeature(a: any, opt: any): IFovGeo;
  };

  export interface IFovGeo {
    type: "Feature";
    properties: {
      angle: number;
      bearing: number;
      distance: number;
    };
    geometry: {
      type: "GeometryCollection";
      geometries: [
        {
          type: "Point";
          coordinates: [number, number];
        },
        {
          type: "LineString";
          coordinates: [[number, number], [number, number]];
        }
      ];
    };
  }
}
