declare module "react-file-reader" {
  import * as React from "react";

  interface IProp {
    fileTypes?: string[];
    base64?: boolean;
    multipleFiles?: boolean;
    handleFiles?: (files: FileList) => void;
  }
  export default class ReactFileReader extends React.Component<IProp> {}
}
