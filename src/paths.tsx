export default {
  home: "/",
  board: "/board",
  boardSiteDetail: "/board/site/:id",
  boardCameraView: "/board/camera-view/:id",
  forgot: "/auth/forgot",
  resetPassword: "/auth/resetpassword/:id/:token",
  profile: "/profile",
  lib: "/libs",
  callback: "/callback",
  libSelect: "/libs/select",
  libImport: "/libs/import",
  jobDetail: "/board/job/:id",
  users: "/users",
  usersPending: "/users/pending",
  invitationAccept: "/user/accept/:id",
  invitation: "/user/accept",
  siteManagement: "/site",
  report: "/report",
  template: "/setting/template"
};
// "oidc.14f60d4ede3545ffa3f1555a075ef1d1"
// "{"id":"14f60d4ede3545ffa3f1555a075ef1d1","created":1533104328,"nonce":"8045d61dda2d439194a7ea3de68a8ef9","authority":"http://shop.ins8.us/","client_id":"los_react_local"}"
