export const REACT_APP_API_URL: string = process.env.REACT_APP_API_URL || "";
export const REACT_APP_ROUTE_HASH: boolean =
  process.env.REACT_APP_ROUTE_HASH === "true";
export const REACT_APP_SOCKET_IO: string =
  process.env.REACT_APP_SOCKET_IO || "http://localhost:8000";
export const REACT_APP_API_URL_LOGOUT: string =
  process.env.REACT_APP_API_URL_LOGOUT || "";
export const REACT_APP_REDIRECT_URL: string =
  process.env.REACT_APP_REDIRECT_URL || "";
export const REACT_APP_CLIENT_ID: string =
  process.env.REACT_APP_CLIENT_ID || "";
