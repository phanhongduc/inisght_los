import { RouterState } from "react-router-redux";

import RouteUri from "../helpers/routeUri";
import IAuth from "./Auth";
import { IFormLogin, IErrorLogin } from "../scenes/auth/components/Signin";
import IProfileMenu from "./ProfileMenu";
import ICommon from "./Common";
import INetwork from "../scenes/network/types";
import ITabRoute from "../components/TabRoute/types";
import {
  IFormForgotPassword,
  IErrorForgotPassword
} from "../scenes/auth/components/ForgotPassword";
import { Feature } from "geojson";
import { ILibsStore } from "../scenes/lib/types/libs";
import { IStoreJobs } from "../scenes/network/types/Job";
import { IUsersManagement } from "../scenes/user-mgnt/types";
import { ISitesManagement } from "../scenes/site-mgnt/types";
import IGuideTour from "../scenes/guide-tour/types";
import IReport from "../scenes/report/types";

export interface IStoreState {
  common: ICommon;
  tabRoute: ITabRoute;
  helloState: { width: number; height: number };
  routing: RouterState;
  isOpenDrawer: boolean;
  mainMap: INetwork;
  currentUri: RouteUri;
  auth: IAuth;
  login: {
    form: IFormLogin;
    error: IErrorLogin;
  };
  forgot: {
    form: IFormForgotPassword;
    error: IErrorForgotPassword;
  };
  profileMenu: IProfileMenu;
  tabBoardManagement: {
    tabId: string;
  };
  site: {
    listSite: Feature[];
    siteAddress: {
      [k: string]: string;
    };
  };
  libs: ILibsStore;
  jobs: IStoreJobs;

  usersManagement: IUsersManagement;
  sitesManagement: ISitesManagement;
  guideTour: IGuideTour;
  report: IReport;
}
