import { JobStatus } from "../scenes/lib/types/libs";

export enum RealtimeEvent {
  DATA_STATUS_LIBRARY_CHANGE = "data_status_of_library_change",
  SITE_DATA_LIBRARY_CHANGE = "site_data_of_library_change",
  MEDIA_PROCESSED_SUCCESS = "media_has_been_processed_success",
  NEW_JOB_UPDATED = "New_job_has_been_updated",
  JOIN_ROOM = "JOIN_ROOM",
  JOIN = "join",
  JOB_STATUS_CHANGED = "job_status_changed",
  LIBRARY_STATUS_CHANGED = "library_status_changed"
}

export default interface IRealtimeData<T extends RealtimeEvent> {
  action: T;
  libraryId: string;
}

export interface IRealtimeDataMedia
  extends IRealtimeData<RealtimeEvent.MEDIA_PROCESSED_SUCCESS> {
  jobId: string;
  mediaId: string;
  status: string;
}

export interface IRealtimeActiveChange
  extends IRealtimeData<RealtimeEvent.MEDIA_PROCESSED_SUCCESS> {
  active: boolean;
  libraryId: string;
}

export interface IRealtimeDataLibrary
  extends IRealtimeData<RealtimeEvent.DATA_STATUS_LIBRARY_CHANGE> {
  status: string;
}

export interface IRealTimeJobStatus {
  action: RealtimeEvent.JOB_STATUS_CHANGED;
  libraryId: string;
  jobId: string;
  status: JobStatus;
}
