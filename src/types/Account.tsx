export default interface IAccount {
  id_token: string;
  session_state: string;
  access_token: string;
  token_type: string;
  scope: string;
  profile: IProfile;
  expires_at: number;
}

export interface IProfile {
  sid: string;
  sub: string;
  auth_time: number;
  idp: string;
  amr: string[];
  preferred_username: string;
  name: string;
  given_name: string;
  role: string;
  email: string;
}

export interface IAccountLib {
  id: string;
  name: string;
  ownerId: string;
  status: number;
}

// interface IOperator {
//   band: string[];
//   canUpdate: boolean;
//   currency: string;
//   dataImportStatus: number;
//   freeJobRemain: number;
//   isEnterprise: boolean;
//   isExpired: boolean;
//   isTrial: boolean;
//   isWaitingApproveEnterprise: boolean;
//   operatorId: string;
//   operatorName: string;
//   startTrialDate: string;
//   subscriptionType: number;
// }

// interface ILocation {
//   lat: number;
//   lon: number;
// }

// interface IUserInfo {
//   avatarUrl: string;
//   canCreateLibrary: string;
//   claimLinkedInReward: string;
//   currentOperator: IOperator;
//   defaultLocation: ILocation;
//   email: string;
//   firstName: string;
//   hasLinkedInProfile: boolean;
//   id: string;
//   isInsightusSuperAdmin: boolean;
//   lastName: string;
//   phoneNumber: string;
//   roleInOperation: string[];
//   showTutorial: boolean;
//   userName: string;
// }
