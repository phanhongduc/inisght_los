import { USERS_GET_LIST, USERS_UPDATE_LIST } from "../constants/UserList";
import { Action, Dispatch } from "redux";
import axios, { AxiosResponse } from "axios";
import { REACT_APP_API_URL } from "../environment";
import IUser from "../types/User";

export interface IUserGetList extends Action<USERS_GET_LIST> {}
export interface IUserUpdateList extends Action<USERS_UPDATE_LIST> {
  users: IUser[];
}

export const getListUser = (
  dispatch: Dispatch<IUserGetList | IUserUpdateList>
) => () => {
  dispatch({
    type: USERS_GET_LIST
  });

  axios
    .get(`${REACT_APP_API_URL}/api/v2/incident/users`)
    .then((resp: AxiosResponse<IUser[]>) => {
      dispatch({
        type: USERS_UPDATE_LIST,
        users: resp.data
      });
    });
};
