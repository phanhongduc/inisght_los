import { Dispatch } from "redux";
import { Store } from "react-redux";
import { IStoreState } from "../types";
import { SETUP_AUTHENTICATION } from "src/constants";
import { ISetupAuthentication } from "../scenes/auth/actions";
import { VideoUtils } from "../helpers/videoUrlCreator";

export const initVideoUtils = (store: Store<IStoreState>) => (
  next: Dispatch<ISetupAuthentication>
) => (action: ISetupAuthentication) => {
  switch (action.type) {
    case SETUP_AUTHENTICATION:
      {
        if (action.payload) {
          VideoUtils.updateUser(action.payload);
        }
      }
      break;
  }

  return next(action);
};
