import { Store, Dispatch } from "react-redux";
import { IStoreState } from "../types";
import { REACT_APP_SOCKET_IO } from "../environment";
import * as io from "socket.io-client";
import { ISetupAuthentication } from "../scenes/auth/actions";
import { SETUP_AUTHENTICATION } from "../constants/index";
import { SET_LIST_LIBS } from "../scenes/lib/constants/libs";
import {
  getListLibs,
  getLibVersion,
  setListJob,
  setListLibs,
  ISetListLibs
} from "../scenes/lib/actions/libs";
import {
  RealtimeEvent,
  IRealtimeDataMedia,
  IRealtimeDataLibrary,
  IRealTimeJobStatus,
  IRealtimeActiveChange
} from "../types/RealtimeEvent";
import { getJobDetail, setJobDetail } from "../scenes/network/actions/Job";
import { fireNotification } from "../components/notification/action";
import Variant from "src/components/notification/types/variant";
import cloneDeep from "lodash-es/cloneDeep";
import { JobStatus, ILib } from "../scenes/lib/types/libs";

let socket: SocketIOClient.Socket;
let listLibs: ILib[];
export const iniSocket = (store: Store<IStoreState>) => (
  next: Dispatch<ISetupAuthentication | ISetListLibs>
) => (action: ISetupAuthentication | ISetListLibs) => {
  switch (action.type) {
    case SETUP_AUTHENTICATION:
      {
        const account = action.payload;
        if (!account || !account.profile) {
          if (socket && socket.connected) {
            socket.disconnect();
            socket.close();
          }
          break;
        }
        console.log("connect");
        socket = io.connect(
          `${REACT_APP_SOCKET_IO}/los?token=${account.access_token}`
        );

        socket.on("connect", (data: any) => {
          console.log("connected", data);
          if (listLibs) {
            listLibs.forEach(el => {
              socket.emit(RealtimeEvent.JOIN, el.id);
            });
          }
        });

        socket.on(
          RealtimeEvent.LIBRARY_STATUS_CHANGED,
          updateLibrary(store.dispatch, store.getState())
        );
        socket.on(
          RealtimeEvent.DATA_STATUS_LIBRARY_CHANGE,
          processLibraries(store.dispatch)
        );
        socket.on(
          RealtimeEvent.JOB_STATUS_CHANGED,
          (data: IRealTimeJobStatus) => {
            console.log("Job status changed");

            const currentState = store.getState();

            if (currentState.libs.selectedLib === data.libraryId) {
              if (
                currentState.jobs.listJob[data.jobId] &&
                currentState.jobs.listJob[data.jobId].jobStatus !== data.status
              ) {
                const newData = cloneDeep(
                  currentState.jobs.listJob[data.jobId]
                );
                newData.jobStatus = JobStatus.Finished;
                store.dispatch(setJobDetail(newData));
              }

              const jobInResult = currentState.libs.jobSearchResult.jobs.find(
                job => job.id === data.jobId
              );
              if (jobInResult && jobInResult.jobStatus !== data.status) {
                store.dispatch(
                  setListJob({
                    total: currentState.libs.jobSearchResult.total,
                    jobs: currentState.libs.jobSearchResult.jobs.map(j => {
                      if (j.id === data.jobId) {
                        j.jobStatus = JobStatus.Finished;
                      }
                      return j;
                    })
                  })
                );
              }
            }
          }
        );
        socket.on(RealtimeEvent.SITE_DATA_LIBRARY_CHANGE, (data: any) => {
          console.log("SITE_DATA_LIBRARY_CHANGE", data);
        });
        socket.on(
          RealtimeEvent.MEDIA_PROCESSED_SUCCESS,
          processMedia(store.dispatch)
        );
        socket.on(RealtimeEvent.NEW_JOB_UPDATED, (data: any) => {
          console.log("NEW_JOB_UPDATED", data);
        });
        socket.on(RealtimeEvent.JOIN_ROOM, (data: any) => {
          console.log("Join Room", data);
        });
      }
      break;
    // case SELECT_LIB: {
    //   libraryId = action.payload;
    //   if (socket) {
    //     socket.emit(RealtimeEvent.JOIN, action.payload);
    //   }
    // }

    case SET_LIST_LIBS: {
      listLibs = action.payload;
      if (socket) {
        listLibs.forEach(el => {
          socket.emit(RealtimeEvent.JOIN, el.id);
        });
      }
    }
  }

  return next(action);
};

const processMedia = (dispatch: Dispatch) => (data: IRealtimeDataMedia) => {
  console.log("Media process success", data);
  fireNotification(dispatch)({
    variant: Variant.SUCCESS,
    message: `Media ${data.mediaId} is processed ${data.status}`
  });
  dispatch(getJobDetail(data.jobId));
};

const processLibraries = (dispatch: Dispatch) => (
  data: IRealtimeDataLibrary
) => {
  console.log("Library process success", data);
  fireNotification(dispatch)({
    variant: Variant.SUCCESS,
    message: `Library ${data.libraryId} is processed ${data.status}`
  });
  dispatch(getListLibs());
  dispatch(getLibVersion());
};

const updateLibrary = (dispatch: Dispatch, state: IStoreState) => (
  data: IRealtimeActiveChange
) => {
  const newListLibs = listLibs.map(el => {
    if (el.id === data.libraryId) {
      el.active = data.active;
    }
    return el;
  });
  dispatch(setListLibs(newListLibs));
};
