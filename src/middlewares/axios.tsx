import axios, { AxiosResponse, AxiosError } from "axios";
import { Dispatch } from "redux";
import { common, IApplicationInit } from "../actions";
import { IUpdateNotification } from "../components/notification/action";
import Variant from "../components/notification/types/variant";
import { Store } from "react-redux";
import { IStoreState } from "../types";
import { APPLICATION_INIT, SETUP_AUTHENTICATION } from "src/constants";
import {
  ILogout,
  ISetupAuthentication,
  logout,
  setupAuthentication
} from "../scenes/auth/actions";
// import IAccount from "../types/Account";
import { User } from "oidc-client";
import {
  ISetApiLoading,
  IUnsetApiLoading,
  setApiLoading,
  unsetApiLoading,
  setLibActive,
  ISetLibActive
} from "../actions/loading";
// import { UNIQUE_ID } from "../helpers/sessionId";
enum ERROR_MESSAGE {
  LIBRARY_NOT_INACTIVE = "library_is_not_inactive"
}
const initAxiosInterceptor = (
  dispatch: Dispatch<
    | IUpdateNotification
    | ISetApiLoading
    | IUnsetApiLoading
    | ISetupAuthentication
    | ILogout
    | ISetLibActive
  >,
  state: IStoreState
) => {
  axios.interceptors.request.use(
    config => {
      if (config.url) {
        dispatch(setApiLoading(config.url));
      }
      if (
        config.url &&
        config.url.indexOf(
          "http://elevation.arcgis.com/arcgis/rest/services/Tools/ElevationSync/GPServer/Profile/execute"
        ) !== -1
      ) {
        delete config.headers.authorization;
      }
      return config;
    },
    error => Promise.reject(error)
  );
  axios.interceptors.response.use(
    (response: AxiosResponse<any>) => {
      if (response.config.url) {
        dispatch(unsetApiLoading(response.config.url));
      }
      return response;
    },
    (error: AxiosError) => {
      if (error.config.url) {
        dispatch(unsetApiLoading(error.config.url));
      }
      if (!axios.isCancel(error)) {
        console.log({ ...error });
        switch (error.response ? error.response.status : -1) {
          case 0: {
            common.fireNotification(dispatch)({
              message: `Fail to request server`,
              variant: Variant.ERROR
            });
            break;
          }
          case 400: {
            if (
              error.response &&
              error.response.data === ERROR_MESSAGE.LIBRARY_NOT_INACTIVE
            ) {
              dispatch(setLibActive(false));
            }
            break;
          }
          case 401: {
            common.fireNotification(dispatch)({
              message: `${error.request.responseURL}: Authentication Fail`,
              variant: Variant.ERROR
            });
            dispatch(logout());
            dispatch(setupAuthentication());
            break;
          }
          case 500: {
            common.fireNotification(dispatch)({
              message: `${error.request.responseURL}: Internal Server Error`,
              variant: Variant.ERROR
            });
            break;
          }
        }
      }
      return Promise.reject(error);
    }
  );
};

let interceptorsRquestId = 0;

const setupAuthenforAxios = (account: User) => {
  if (interceptorsRquestId) {
    axios.interceptors.request.eject(interceptorsRquestId);
  }
  interceptorsRquestId = axios.interceptors.request.use(
    config => {
      config.headers.authorization = `${account.token_type} ${
        account.access_token
      }`;
      return config;
    },
    error => Promise.reject(error)
  );
};

export const initAxios = (store: Store<IStoreState>) => (
  next: Dispatch<IApplicationInit | ISetupAuthentication>
) => (action: IApplicationInit | ISetupAuthentication) => {
  switch (action.type) {
    case APPLICATION_INIT:
      {
        initAxiosInterceptor(store.dispatch, store.getState());
      }
      break;
    case SETUP_AUTHENTICATION:
      {
        if (action.payload) {
          setupAuthenforAxios(action.payload);
        }
      }
      break;
  }

  return next(action);
};
