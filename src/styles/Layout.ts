import { Theme, createStyles } from "@material-ui/core/styles";
import green from "@material-ui/core/colors/green";
import red from "@material-ui/core/colors/red";
import amber from "@material-ui/core/colors/amber";
import { IMyMixisOptions } from "../withRoot";

const drawerWidth = 240;
const drawerMinWidth = 60;

const styles = (theme: Theme) =>
  createStyles({
    root: {
      flexGrow: 1
    },
    rootFull: {
      flexGrow: 1,
      height: "100vh"
    },
    appFrame: {
      height: "100%",
      zIndex: 1,
      overflow: "hidden",
      position: "relative",
      display: "flex",
      width: "100%"
    },
    appBar: {
      backgroundColor: "#191c21",
      position: "absolute",
      marginLeft: drawerMinWidth,
      width: `calc(100% - ${drawerMinWidth}px)`,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    appBarNoDrawer: {
      backgroundColor: "#191c21"
    },
    appBarShiftNoDrawer: {
      width: "100%"
    },
    appBarShift: {
      width: `calc(100% - ${drawerWidth}px)`,
      transition: theme.transitions.create(["margin", "width"], {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    "appBarShift-left": {
      marginLeft: drawerWidth
    },
    "appBarShift-right": {
      marginRight: drawerWidth
    },
    menuButton: {
      marginLeft: 12,
      marginRight: 20,
      color: theme.palette.common.white
    },
    hide: {
      display: "none"
    },
    drawerPaper: {
      backgroundColor: "#191c21",
      color: "#fff",
      position: "relative",
      width: drawerWidth,
      transition: theme.transitions.create("width", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    drawerPaperMin: {
      width: drawerMinWidth
    },
    drawerHeader: {
      display: "flex",
      alignItems: "center",
      justifyContent: "flex-end",
      padding: `0 ${theme.spacing.unit * 2}px`,
      ...theme.mixins.toolbar
    },
    content: {
      overflow: "hidden",
      width: "100%",
      flexGrow: 1,
      backgroundColor: theme.palette.background.default,
      // padding: theme.spacing.unit ,
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.sharp,
        duration: theme.transitions.duration.leavingScreen
      })
    },
    drawerContent: {
      height:
        (theme.mixins as IMyMixisOptions).windowHeight - theme.spacing.unit * 8,
      overflow: "auto"
    },
    "content-left": {
      // marginLeft: -(drawerWidth - drawerMinWidth)
    },
    "content-right": {
      marginRight: -(drawerWidth - drawerMinWidth)
    },
    contentShift: {
      transition: theme.transitions.create("margin", {
        easing: theme.transitions.easing.easeOut,
        duration: theme.transitions.duration.enteringScreen
      })
    },
    "contentShift-left": {
      marginLeft: 0
    },
    "contentShift-right": {
      marginRight: 0
    },
    title: {
      flex: 1
    },
    libStatus: {
      fontSize: "12px",
      padding: "5px",
      borderRadius: "5px",
      color: theme.palette.common.white,
      lineHeight: "1.1875em",
      marginLeft: theme.spacing.unit
    },
    libStatusItem: {
      fontSize: "12px",
      padding: "5px",
      borderRadius: "5px",
      color: theme.palette.common.white,
      lineHeight: "1.1875em",
      marginLeft: "auto"
    },
    activeItem: {
      fontSize: "12px",
      padding: "5px",
      borderRadius: "5px",
      color: green[400],
      lineHeight: "1.1875em"
    },
    inactiveItem: {
      fontSize: "12px",
      padding: "5px",
      borderRadius: "5px",
      color: red[400],
      lineHeight: "1.1875em"
    },
    libStatusImported: {
      backgroundColor: green[500]
    },
    libStatusNotImported: {
      backgroundColor: amber[500]
    },
    libStatusFailed: {
      backgroundColor: red[500]
    },
    selectLib: {
      display: "flex",
      alignItems: " center",
      color: theme.palette.common.white,
      lineHeight: 1,

      "& > span:first-child": {
        marginRight: theme.spacing.unit
      }
    },
    selectLibItem: {
      minWidth: 200,
      display: "flex",

      "& a": {
        color: "inherit",
        textDecoration: "none"
      }
    },
    selectLibResult: {
      "&:focus": {
        backgroundColor: "transparent"
      }
    },
    logoToolbar: {
      alignItems: "center",
      justifyContent: "flex-end",
      display: "flex",
      padding: "0 16px",
      height: "64px",
      backgroundColor: "#191c21",
      marginRight: theme.spacing.unit * 2
    },
    selectIcon: {
      color: theme.palette.common.white
    },
    bottomMenu: {
      position: "absolute",
      bottom: 0
    },
    libType: {
      color: "#ff8c00",
      fontWeight: 700,
      marginLeft: theme.spacing.unit * 2
    },
    menuSideBar: {
      height: "calc(100vh - 130px)",
      overflow: "auto"
    },
    upgradePlan: {
      marginLeft: 16
    },
    active: {
      position: "absolute",
      fontSize: "12px",
      left: "0",
      bottom: -8,
      display: "flex",
      alignItems: "center"
    },
    activeIcon: {
      marginRight: 4,
      display: "block",
      height: "10px",
      width: "10px",
      borderRadius: "50%"
    },
    activeIconActive: {
      backgroundColor: green[500]
    },
    activeIconInactive: {
      backgroundColor: red[500]
    },
    selectLibStatus: {
      position: "relative"
    }
  });

export default styles;
