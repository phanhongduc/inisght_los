import createMuiTheme from "@material-ui/core/styles/createMuiTheme";

const muiTheme = createMuiTheme({
  direction: "rtl"
});

export default muiTheme;
