import * as React from "react";
import FileDownloadIcon from "@material-ui/icons/FileDownload";
import IconButton from "@material-ui/core/IconButton";
import { createStyles, Theme } from "@material-ui/core";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import IStyleProps from "../../styles/utils";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    btn: {
      color: "#CBCFD3",
      "&:hover": {
        color: theme.palette.common.white
      }
    }
  });

interface IProps {
  url: string;
}

class DownloadFile extends React.Component<IStyleProps & IProps> {
  public render() {
    const { classes, url } = this.props;
    return (
      <a target="_blank" download={true} href={url}>
        <IconButton className={classes.btn} color="inherit">
          <FileDownloadIcon />
        </IconButton>
      </a>
    );
  }
}

export default withStyles(styles)(DownloadFile);
