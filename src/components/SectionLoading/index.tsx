import * as React from "react";
import LinearProgress from "@material-ui/core/LinearProgress";

const SectionLoading: React.SFC = () => <LinearProgress />;

export default SectionLoading;
