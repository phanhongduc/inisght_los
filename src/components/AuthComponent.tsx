import * as React from "react";
import { Redirect } from "react-router";
import paths from "src/paths";
import IAuth from "../types/Auth";
import { ILib, LibStatus } from "../scenes/lib/types/libs";
import { mgr } from "../scenes/auth/middlewares/userManager";
import Redirecting from "./Redirecting";
// import { mgr } from "src/scenes/auth/middlewares/userManager";
export enum ProtectLevel {
  all,
  private,
  public
}

export interface IRouteProps {
  protect?: ProtectLevel;
  auth: IAuth;
  selectLib: string;
  libs: ILib[];
  children: React.ReactElement<{}> | null;
  routePath: string;
  invitationId: string;
}

// const alow = (protect: ProtectLevel, isAuth: boolean): boolean => {
//   switch (protect) {
//     case ProtectLevel.private: {
//       return isAuth;
//     }
//     case ProtectLevel.public: {
//       return !isAuth;
//     }
//   }
//   return true;
// };

const AuthComponent = ({
  protect,
  auth,
  children,
  routePath,
  selectLib,
  libs,
  invitationId
}: IRouteProps) => {
  protect = protect || ProtectLevel.all;
  const isAuth = auth.isAuth;
  if (auth.isChecking) {
    return children;
  }
  if (isAuth && invitationId) {
    if (routePath !== paths.invitation) {
      return <Redirect to={paths.invitation} />;
    } else {
      return children;
    }
  }
  if (isAuth && !invitationId && routePath === paths.invitation) {
    return <Redirect to={paths.home} />;
  }
  if (isAuth && paths.home === routePath) {
    return <Redirect to={paths.board} />;
  }
  if (protect === ProtectLevel.public && isAuth) {
    return <Redirect to={paths.board} />;
  } else if (protect === ProtectLevel.private && !isAuth) {
    mgr.signinRedirect();
    return <Redirecting />;
  }

  const selectedLib = libs.find(lib => lib.id === selectLib);
  if (auth.isAuth && !selectedLib && routePath !== paths.libSelect) {
    return <Redirect to={paths.libSelect} />;
  }

  if (
    auth.isAuth &&
    selectedLib &&
    selectedLib.status === LibStatus.NOT_IMPORT &&
    routePath !== paths.libImport
  ) {
    return <Redirect to={paths.libImport} />;
  }
  return children;
};

export default AuthComponent;
