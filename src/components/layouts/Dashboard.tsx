import * as React from "react";
import * as classnames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Drawer from "@material-ui/core/Drawer";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import IconButton from "@material-ui/core/IconButton";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import ChevronRight from "@material-ui/icons/ChevronRight";
import ChevronLeft from "@material-ui/icons/ChevronLeft";
import Menu from "@material-ui/icons/Menu";
import * as classNames from "classnames";
import styles from "../../styles/Layout";
import LeftMenu from "./../../containers/LeftMenu";
import Logo from "../Logo";
// import IAccount from "../../types/Account";
import ProfileMenu from "../../containers/layouts/ProfileMenu";
import { IStyleTypeProps } from "src/styles/utils";
import { User } from "oidc-client";
import Button from "@material-ui/core/Button";
import {
  ILib,
  LibStatus,
  LibStatusText,
  LibType,
  LibTypeText,
  LibActiveStatusText
} from "../../scenes/lib/types/libs";
import { LeftBottomMenu } from "./items";
import links from "../../links";

function onChangeLib(cb: (id: string) => void) {
  return (e: React.SyntheticEvent) => {
    const target = e.target as HTMLSelectElement;
    console.log(target.value);
    cb(target.value);
  };
}

export interface IPersistentDrawerProps {
  isOpenDrawer: boolean;
  handleDrawerOpen: () => void;
  handleDrawerClose: () => void;
  children: React.ReactElement<{}> | Array<React.ReactElement<{}>>;
  account: User;
  libs: ILib[];
  selectedLib: string;
  changeLib: (id: string) => void;
}

const PersistentDrawer = ({
  libs,
  selectedLib,
  isOpenDrawer,
  handleDrawerOpen,
  handleDrawerClose,
  classes,
  theme,
  children,
  account,
  changeLib
}: Required<IStyleTypeProps<typeof styles>> & IPersistentDrawerProps) => {
  const open = isOpenDrawer;
  const anchor = "left";
  const drawer = (
    <Drawer
      variant="persistent"
      open={true}
      classes={{
        paper: classNames(classes.drawerPaper, !open && classes.drawerPaperMin)
      }}
    >
      <div className={classes.drawerHeader}>
        <Logo open={open} />
      </div>
      <Divider />
      <List className={classes.menuSideBar}>
        <LeftMenu />
      </List>
      <div className={classes.bottomMenu}>
        <Divider />
        <LeftBottomMenu />
      </div>
    </Drawer>
  );

  let before = null;
  before = drawer;

  const currentLib = libs.find(f => f.id === selectedLib);

  return (
    <div className={classes.root}>
      <div className={classes.appFrame}>
        <AppBar
          className={classNames(classes.appBar, {
            [classes.appBarShift]: open,
            [classes[`appBarShift-${anchor}`]]: open
          })}
        >
          <Toolbar disableGutters={true}>
            <IconButton
              onClick={handleDrawerClose}
              className={classNames(classes.menuButton, !open && classes.hide)}
            >
              {theme.direction === "rtl" ? <ChevronRight /> : <ChevronLeft />}
            </IconButton>
            <IconButton
              color="inherit"
              aria-label="open drawer"
              onClick={handleDrawerOpen}
              className={classNames(classes.menuButton, open && classes.hide)}
            >
              <Menu />
            </IconButton>
            <div className={classes.selectLibStatus}>
              <Select
                onChange={onChangeLib(changeLib)}
                disableUnderline={true}
                value={selectedLib}
                classes={{
                  icon: classes.selectIcon,
                  selectMenu: classes.selectLib
                }}
              >
                {libs.map(lib => (
                  <MenuItem
                    className={classes.selectLibItem}
                    key={lib.id}
                    value={lib.id}
                  >
                    <span>{lib.name} </span>
                    <span
                      className={classnames(
                        classes.libStatusItem,
                        {
                          [classes.libStatusImported]:
                            lib.status === LibStatus.SUCCESS
                        },
                        {
                          [classes.libStatusNotImported]:
                            lib.status === LibStatus.NOT_IMPORT ||
                            lib.status === LibStatus.PROCESSING ||
                            lib.status === LibStatus.IMPORTING
                        },
                        {
                          [classes.libStatusFailed]:
                            lib.status === LibStatus.FAILED
                        }
                      )}
                    >
                      {LibStatusText[LibStatus[lib.status]]}
                    </span>
                  </MenuItem>
                ))}
                <MenuItem className={classes.selectLibItem}>
                  <a href={links.plan}>Create new library</a>
                </MenuItem>
              </Select>
              {currentLib && (
                <div className={classes.active}>
                  <span
                    className={`${classes.activeIcon} ${
                      currentLib.active
                        ? classes.activeIconActive
                        : classes.activeIconInactive
                    }`}
                  />
                  {currentLib.active
                    ? LibActiveStatusText.ACTIVE
                    : LibActiveStatusText.INACTIVE}
                </div>
              )}
            </div>
            {currentLib && (
              <Typography className={classes.libType}>
                {LibTypeText[LibType[currentLib.type]]}
              </Typography>
            )}
            {currentLib &&
              currentLib.type === LibType.FreeTrial && (
                <Button
                  className={classes.upgradePlan}
                  href={links.upgradePlan}
                  variant="raised"
                  color="primary"
                >
                  Upgrade plan
                </Button>
              )}
            <Typography
              variant="title"
              color="inherit"
              noWrap={true}
              className={classes.title}
            >
              {/* Persistent drawer */}
            </Typography>
            <ProfileMenu account={account} />
          </Toolbar>
        </AppBar>
        {before}

        <main
          className={classNames(classes.content, classes[`content-${anchor}`], {
            [classes.contentShift]: open,
            [classes[`contentShift-${anchor}`]]: open
          })}
        >
          <div className={classes.drawerHeader} />
          <div className={classes.drawerContent}>{children}</div>
        </main>
      </div>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(PersistentDrawer);
