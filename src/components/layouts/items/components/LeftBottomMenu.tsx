import * as React from "react";
import HelpOutline from "@material-ui/icons/HelpOutline";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import { Theme } from "@material-ui/core";
import MenuList from "@material-ui/core/MenuList";
import IStyleProps from "src/styles/utils";
import CMenuItem from "src/containers/CMenuItem";

export interface IRouteProps {
  handleHelp: () => void;
}

const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  wrapper: {
    display: "inline"
  }
});
class LeftMenu extends React.Component<IStyleProps & IRouteProps> {
  constructor(props: IStyleProps & IRouteProps) {
    super(props);
  }
  public render() {
    return (
      <MenuList>
        <CMenuItem
          text="Guide Tour"
          icon={<HelpOutline />}
          onForceClick={this.props.handleHelp}
        />
      </MenuList>
    );
  }
}

export default withStyles(styles)(LeftMenu);
