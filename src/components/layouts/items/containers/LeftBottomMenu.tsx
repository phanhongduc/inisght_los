import { connect, Dispatch } from "react-redux";
import {
  guideTourDialogControl,
  IGuideTourDialogControl
} from "src/scenes/guide-tour/actions";
import { IStoreState } from "src/types";
import LeftBottomMenu from "../components/LeftBottomMenu";

export function mapStateToProps({  }: IStoreState) {
  return {};
}

export function mapDispatchToProps(
  dispatch: Dispatch<IGuideTourDialogControl>
) {
  return {
    handleHelp: () => {
      guideTourDialogControl(dispatch)(true);
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeftBottomMenu);
