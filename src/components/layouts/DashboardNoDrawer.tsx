import * as React from "react";
import * as classnames from "classnames";
import { withStyles } from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import * as classNames from "classnames";
import styles from "../../styles/Layout";
import Logo from "../Logo";
import ProfileMenu from "../../containers/layouts/ProfileMenu";
import { IStyleTypeProps } from "src/styles/utils";
import { User } from "oidc-client";
import {
  ILib,
  LibStatus,
  LibStatusText,
  LibActiveStatusText
} from "../../scenes/lib/types/libs";

function onChangeLib(cb: (id: string) => void) {
  return (e: React.SyntheticEvent) => {
    const target = e.target as HTMLSelectElement;
    cb(target.value);
  };
}

export interface IPersistentDrawerProps {
  children: React.ReactElement<{}> | Array<React.ReactElement<{}>>;
  account: User;
  libs: ILib[];
  selectedLib: string;
  changeLib: (id: string) => void;
}

const PersistentDrawer = ({
  libs,
  selectedLib,
  classes,
  theme,
  children,
  account,
  changeLib
}: Required<IStyleTypeProps<typeof styles>> & IPersistentDrawerProps) => {
  const anchor = "left";

  return (
    <div className={classes.root}>
      <div className={classes.appFrame}>
        <AppBar
          className={classNames(
            classes.appBar,
            classes.appBarShiftNoDrawer,
            classes.appBarNoDrawer
          )}
        >
          <Toolbar disableGutters={true}>
            <div className={classes.logoToolbar}>
              <Logo open={true} />
            </div>
            {selectedLib && (
              <Select
                onChange={onChangeLib(changeLib)}
                disableUnderline={true}
                value={selectedLib}
                classes={{
                  select: classes.selectLibResult,
                  selectMenu: classes.selectLib,
                  icon: classes.selectIcon
                }}
              >
                {libs.map(lib => (
                  <MenuItem key={lib.id} value={lib.id}>
                    {lib.name}{" "}
                    <span
                      className={classnames(
                        {
                          [classes.activeItem]: lib.active === true
                        },
                        {
                          [classes.inactiveItem]: lib.active === false
                        }
                      )}
                    >
                      {lib.active && LibActiveStatusText.ACTIVE}
                      {!lib.active && LibActiveStatusText.INACTIVE}
                    </span>
                    <span
                      className={classnames(
                        classes.libStatus,
                        {
                          [classes.libStatusImported]:
                            lib.status === LibStatus.SUCCESS
                        },
                        {
                          [classes.libStatusNotImported]:
                            lib.status === LibStatus.NOT_IMPORT ||
                            lib.status === LibStatus.IMPORTING
                        },
                        {
                          [classes.libStatusFailed]:
                            lib.status === LibStatus.FAILED
                        }
                      )}
                    >
                      {LibStatusText[LibStatus[lib.status]]}
                    </span>
                  </MenuItem>
                ))}
              </Select>
            )}

            <Typography
              variant="title"
              color="inherit"
              noWrap={true}
              className={classes.title}
            >
              {/* Persistent drawer */}
            </Typography>
            <ProfileMenu account={account} />
          </Toolbar>
        </AppBar>

        <main
          className={classNames(classes.content, classes[`content-${anchor}`], {
            [classes.contentShift]: !!open,
            [classes[`contentShift-${anchor}`]]: !!open
          })}
        >
          <div className={classes.drawerHeader} />
          <div className={classes.drawerContent}>{children}</div>
        </main>
      </div>
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(PersistentDrawer);
