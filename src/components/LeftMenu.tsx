import * as React from "react";
import Timeline from "@material-ui/icons/Timeline";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import { Theme } from "@material-ui/core";
import Person from "@material-ui/icons/Person";
import TrendingDown from "@material-ui/icons/TrendingDown";
import DriveFile from "@material-ui/icons/InsertDriveFile";
import Settings from "@material-ui/icons/Settings";
import Place from "@material-ui/icons/Place";
import PinDrop from "@material-ui/icons/PinDrop";
import DateRange from "@material-ui/icons/DateRange";
import MenuList from "@material-ui/core/MenuList";
// import Collapse from "@material-ui/core/Collapse";
import CMenuItem from "../containers/CMenuItem";
import paths from "../paths";
import RouteUri from "../helpers/routeUri";
import IAuth from "../types/Auth";
import IStyleProps from "../styles/utils";
import { allow, UserLibRole } from "src/helpers/permission";
import { IJobInfo } from "../scenes/network/types/Job";
import IMapState from "../scenes/network/types/MapState";

export interface IRouteProps {
  auth: IAuth;
  currentRole: UserLibRole;
  recentJobs: IJobInfo[];
  handleMainMapState: (mapState: IMapState) => void;
  mapState: IMapState;
}

export interface IState {
  openRecent: boolean;
}

const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  wrapper: {
    display: "inline"
  }
});
class LeftMenu extends React.Component<IStyleProps & IRouteProps, IState> {
  constructor(props: IStyleProps & IRouteProps) {
    super(props);
    this.state = {
      openRecent: false
    };
  }
  public handleClickRecentJobs = () => {
    this.setState({
      openRecent: !this.state.openRecent
    });
  };

  public selectJob = (job: IJobInfo) => {
    const { handleMainMapState, mapState } = this.props;
    handleMainMapState({
      lat: job.lat,
      lng: job.lon,
      zoom: mapState.zoom
    });
  };
  public render() {
    const { currentRole, recentJobs } = this.props;
    return (
      <MenuList>
        <CMenuItem
          text="Network Map"
          icon={<Timeline />}
          uri={new RouteUri(paths.board)}
        />
        {allow("userList", currentRole) && (
          <CMenuItem
            text="User management"
            icon={<Person />}
            uri={new RouteUri(paths.users)}
          />
        )}
        <CMenuItem
          text="Site management"
          icon={<Place />}
          uri={new RouteUri(paths.siteManagement)}
        />
        <CMenuItem
          text="Reports"
          icon={<TrendingDown />}
          uri={new RouteUri(paths.report)}
        />
        <CMenuItem text="Settings" icon={<Settings />} />
        <MenuList>
          <CMenuItem
            icon={<DriveFile />}
            uri={new RouteUri(paths.template)}
            text="Template"
            isSub={true}
          />
        </MenuList>
        <CMenuItem
          text="Recent Jobs"
          icon={<DateRange />}
          // onSelect={this.handleClickRecentJobs}
        />
        {/* <Collapse
          in={this.state.openRecent}
          timeout="auto"
          unmountOnExit={true}
        > */}
        <MenuList>
          {recentJobs.map(job => (
            <CMenuItem
              key={job.id}
              uri={new RouteUri(paths.jobDetail.replace(":id", job.id))}
              text={job.name}
              icon={<PinDrop />}
              isSub={true}
              onSelect={this.selectJob.bind(this, job)}
            />
          ))}
        </MenuList>
        {/* </Collapse> */}
      </MenuList>
    );
  }
}

export default withStyles(styles)(LeftMenu);
