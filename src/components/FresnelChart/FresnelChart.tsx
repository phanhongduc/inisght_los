import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import * as classNames from "classnames";
import IStyleProps from "src/styles/utils";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { createStyles, Theme } from "@material-ui/core/styles";
import IconButton from "@material-ui/core/IconButton";
import Autorenew from "@material-ui/icons/Autorenew";
import { Line, ChartData } from "react-chartjs-2";
import * as uuid from "uuid";
import axios, { AxiosResponse } from "axios";
import CircularProgress from "@material-ui/core/CircularProgress";
import lineIntersect from "@turf/line-intersect";
import { lineString } from "@turf/helpers";

const CancelToken = axios.CancelToken;

interface IState {
  groundHeightOfEnd: number;
  groundHeightOfStart: number;
  arrayOfDistanceAndHeight: ITerrainData[];
  heightOfEnd: number;
  isComplete: boolean;
  uuid: string;
}

export interface ITerrainDataChange {
  groundHeightOfEnd: number;
  groundHeightOfStart: number;
  arrayOfDistanceAndHeight: ITerrainData[];
}

interface IProps {
  terrainData?: string;
  onChangeMidPointHeight?: (height: number) => void;
  distanceMidPoint?: number;
  classesOverride?: {
    progress?: string;
    chart?: string;
  };
  id: string;
  frequency: number;
  heightOfStart: number;
  heightOfEnd?: number;
  aEndLat: number;
  aEndLon: number;
  bEndLat: number;
  bEndLon: number;
  distanceFromAendToBend: number;
  onComplete?: (isComplete: boolean) => void;
  onChangeTerrain?: (data: ITerrainDataChange) => void;
}

// interface IData {
//   labels: number[],
//   datasets: IDataSet[],
// }

interface ITerrainData {
  distance: number;
  height: number;
}

const plugins = [
  {
    getLineCoor(chart: any) {
      const meta = chart.getDatasetMeta(0);
      const dataS = meta.data;

      return dataS.length > 0
        ? [
            {
              x: dataS[0]._model.x,
              y: dataS[0]._model.y
            },
            {
              x: dataS[dataS.length - 1]._model.x,
              y: dataS[dataS.length - 1]._model.y
            }
          ]
        : [];
    },

    getPointY(
      a: { x: number; y: number },
      b: { x: number; y: number },
      x: number
    ) {
      return ((x - a.x) / (b.x - a.x)) * (b.y - a.y) + a.y;
    },

    renderLine(chartInstance: any) {
      const scaleY = chartInstance.scales["y-axis-0"];
      const scaleX = chartInstance.scales["x-axis-0"];
      const context = chartInstance.chart.ctx;

      const firstStep = parseFloat(scaleX.getLabelForIndex(1));

      const pointLine = this.getLineCoor(chartInstance);

      const distanceMidPoint = chartInstance.config.data.distanceMidPoint;

      if (pointLine.length > 0 && firstStep !== 0) {
        const a = chartInstance.getDatasetMeta(3);
        const lineTerrain = a.data.map((l: any) => {
          return [l._model.x, l._model.y];
        });
        const lineStringTerrain = lineString(lineTerrain);

        const pixelXToMidPoint =
          distanceMidPoint / (scaleX.max / scaleX.maxWidth) + scaleX.left;

        const lineStringMid = lineString([
          [
            pixelXToMidPoint,
            this.getPointY(pointLine[0], pointLine[1], pixelXToMidPoint)
          ],
          [pixelXToMidPoint, scaleY.bottom]
        ]);

        const pointIntersectMidPoint = lineIntersect(
          lineStringTerrain,
          lineStringMid
        );

        if (pointIntersectMidPoint.features.length > 0) {
          const midPointCoorTop = [
            pixelXToMidPoint,
            this.getPointY(pointLine[0], pointLine[1], pixelXToMidPoint)
          ];

          const midPointCoorBottom = [
            pixelXToMidPoint,
            pointIntersectMidPoint.features[0].geometry.coordinates[1]
          ];

          // draw mid
          context.beginPath();

          context.strokeStyle = "red";

          context.moveTo(midPointCoorTop[0], midPointCoorTop[1]);
          context.lineTo(midPointCoorBottom[0], midPointCoorBottom[1]);

          context.stroke();

          context.closePath();

          context.beginPath();

          context.arc(
            midPointCoorBottom[0],
            midPointCoorBottom[1],
            5,
            0,
            2 * Math.PI,
            false
          );

          context.fillStyle = "red";
          context.fill();

          context.closePath();

          context.beginPath();

          context.arc(
            midPointCoorTop[0],
            midPointCoorTop[1],
            5,
            0,
            2 * Math.PI,
            false
          );

          context.fillStyle = "red";
          context.fill();

          context.closePath();

          // draw begin
          const beginColor = "yellow";

          context.beginPath();

          context.strokeStyle = beginColor;

          context.moveTo(pointLine[0].x, pointLine[0].y);
          context.lineTo(
            pointLine[0].x,
            scaleY.getPixelForValue(
              chartInstance.config.data.groundHeightOfStart
            )
          );

          context.stroke();

          context.closePath();

          context.beginPath();

          context.arc(
            pointLine[0].x,
            scaleY.getPixelForValue(
              chartInstance.config.data.groundHeightOfStart
            ),
            5,
            0,
            2 * Math.PI,
            false
          );

          context.fillStyle = beginColor;
          context.fill();

          context.closePath();

          context.beginPath();

          context.arc(pointLine[0].x, pointLine[0].y, 5, 0, 2 * Math.PI, false);

          context.fillStyle = beginColor;
          context.fill();

          context.closePath();

          // draw end
          const endColor = "#51bbd6";

          context.beginPath();

          context.strokeStyle = endColor;

          context.moveTo(pointLine[1].x, pointLine[1].y);
          context.lineTo(
            pointLine[1].x,
            scaleY.getPixelForValue(chartInstance.config.data.groundHeightOfEnd)
          );

          context.stroke();

          context.closePath();

          context.beginPath();

          context.arc(
            pointLine[1].x,
            scaleY.getPixelForValue(
              chartInstance.config.data.groundHeightOfEnd
            ),
            5,
            0,
            2 * Math.PI,
            false
          );

          context.fillStyle = endColor;
          context.fill();

          context.closePath();

          context.beginPath();

          context.arc(pointLine[1].x, pointLine[1].y, 5, 0, 2 * Math.PI, false);

          context.fillStyle = endColor;
          context.fill();

          context.closePath();

          if (chartInstance.config.data.onChangeMidPointHeight) {
            chartInstance.config.data.onChangeMidPointHeight(
              scaleY.getValueForPixel(midPointCoorTop[1]) -
                scaleY.getValueForPixel(midPointCoorBottom[1])
            );
          }
        }
      }
    },

    afterDatasetsDraw(chart: any, easing: any) {
      if (chart.config.data && chart.config.data.distanceMidPoint) {
        this.renderLine(chart);
      }
    }
  }
];

const option = {
  elements: { point: { radius: 0 } },
  layout: {
    padding: {
      right: 10
    }
  },
  responsive: true,
  tooltips: {
    mode: "index",
    intersect: false
  },
  hover: {
    mode: "nearest",
    intersect: true
  },
  legend: {
    display: false
  },
  scales: {
    xAxes: [
      {
        display: false,
        scaleLabel: {
          display: false,
          labelString: "Distance"
        }
      }
    ],
    yAxes: [
      {
        display: true,
        scaleLabel: {
          display: true,
          labelString: "Height"
        }
      }
    ]
  }
};
const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrapper: {
      display: "block",
      height: "50px",
      "padding-left": "10px"
    },
    button: {
      float: "right"
    },
    job: {
      display: "inline-block",
      width: "50%",
      "padding-top": "20px"
    },
    target: {
      display: "inline-block",
      width: "50%"
    },
    inputWrapper: {
      display: "inline-block",
      width: "50%"
    },
    input: {
      "margin-top": 0
    },
    progress: {
      position: "relative",
      left: "calc(50% - 25px)"
    },
    refreshButton: {
      position: "relative",
      marginLeft: "100%",
      right: "35px"
    },
    chart: {
      // "margin-right": "10px"
    }
  });

class FresnelChart extends React.Component<IStyleProps & IProps, IState> {
  private cancel: any = null;

  public constructor(props: IStyleProps & IProps) {
    super(props);
    this.state = {
      groundHeightOfEnd: 0,
      groundHeightOfStart: 0,
      arrayOfDistanceAndHeight: [],
      heightOfEnd:
        this.props.heightOfEnd || this.props.heightOfEnd === 0
          ? this.props.heightOfEnd
          : 20,
      isComplete: false,
      uuid: uuid()
    };
  }

  private getUpperFresnel() {
    const { distanceFromAendToBend, heightOfStart, frequency } = this.props;
    const { groundHeightOfStart } = this.state;
    const gradient = this.getGradient();
    const k = 300000000 / frequency;
    return this.state.arrayOfDistanceAndHeight.map((x: any) => {
      const rValue =
        k *
        Math.sqrt(
          x.distance - (x.distance * x.distance) / distanceFromAendToBend
        );
      return (
        x.distance * gradient + rValue + groundHeightOfStart + heightOfStart
      );
    });
  }

  private getLowerFresnel() {
    const { distanceFromAendToBend, heightOfStart, frequency } = this.props;
    const { groundHeightOfStart } = this.state;
    const gradient = this.getGradient();
    const k = 300000000 / frequency;
    return this.state.arrayOfDistanceAndHeight.map((x: any) => {
      const rValue =
        k *
        Math.sqrt(
          x.distance - (x.distance * x.distance) / distanceFromAendToBend
        );
      return (
        x.distance * gradient - rValue + groundHeightOfStart + heightOfStart
      );
    });
  }

  private getGradient() {
    const { groundHeightOfEnd, groundHeightOfStart, heightOfEnd } = this.state;
    const { heightOfStart, distanceFromAendToBend } = this.props;
    const endHeight = this.props.heightOfEnd || heightOfEnd;
    return (
      (groundHeightOfEnd + endHeight - groundHeightOfStart - heightOfStart) /
      distanceFromAendToBend
    );
  }

  private loading() {
    if (this.cancel) {
      this.cancel();
    }
    const {
      aEndLat,
      aEndLon,
      bEndLat,
      bEndLon,
      distanceFromAendToBend
    } = this.props;
    this.getTerrain(
      aEndLat,
      aEndLon,
      bEndLat,
      bEndLon,
      (terrainDatas: number[][]) => {
        if (terrainDatas.length > 0) {
          const arrayOfDistanceAndHeight = terrainDatas.map(x => ({
            distance: x[3],
            height: x[2]
          }));
          const groundHeightOfStart: number =
            arrayOfDistanceAndHeight[0].height;
          this.getTerrain(
            bEndLat,
            bEndLon,
            aEndLat,
            aEndLon,
            (terrainFromBendToLastPoint: number[][]) => {
              if (terrainFromBendToLastPoint.length > 0) {
                const groundHeightOfEnd = terrainFromBendToLastPoint[0][2];
                if (
                  arrayOfDistanceAndHeight[arrayOfDistanceAndHeight.length - 1]
                    .distance < distanceFromAendToBend
                ) {
                  arrayOfDistanceAndHeight.push({
                    distance: distanceFromAendToBend,
                    height: groundHeightOfEnd
                  });
                } else {
                  arrayOfDistanceAndHeight[
                    arrayOfDistanceAndHeight.length - 1
                  ] = {
                    distance: distanceFromAendToBend,
                    height: groundHeightOfEnd
                  };
                }
                this.setState({
                  groundHeightOfStart,
                  groundHeightOfEnd,
                  arrayOfDistanceAndHeight,
                  isComplete: true,
                  uuid: uuid()
                });

                if (this.props.onChangeTerrain) {
                  this.props.onChangeTerrain({
                    groundHeightOfStart,
                    groundHeightOfEnd,
                    arrayOfDistanceAndHeight
                  });
                }
                this.cancel = null;
              }
            }
          );
        }
      }
    );
  }

  private getTerrain(
    startLat: number,
    startLon: number,
    endLat: number,
    endLon: number,
    callback: any
  ) {
    const inputObj = {
      displayFieldName: "ProfileSet",
      geometryType: "esriGeometryPolyline",
      spatialReference: {
        wkid: 4326
      },
      fields: [
        {
          name: "OID",
          type: "esriFieldTypeOID",
          alias: "OID"
        },
        {
          name: "Shape_Length",
          type: "esriFieldTypeDouble",
          alias: "Shape_Length"
        }
      ],
      features: [
        {
          attributes: {
            OBJECTID: 1,
            Shape_Length: 100
          },
          geometry: {
            paths: [[[startLon, startLat], [endLon, endLat]]],
            spatialReference: {
              wkid: 4326
            }
          }
        }
      ],
      exceededTransferLimit: false
    };

    const dataString =
      "InputLineFeatures=" +
      encodeURIComponent(JSON.stringify(inputObj)) +
      "&ProfileIDField=OID&DEMResolution=FINEST&MaximumSampleDistance=30&MaximumSampleDistanceUnits=METERS&env%3AoutSR=4326&env%3AprocessSR=4326&returnZ=true&returnM=true&f=pjson";
    axios
      .post(
        "http://elevation.arcgis.com/arcgis/rest/services/Tools/ElevationSync/GPServer/Profile/execute?" +
          dataString,
        null,
        {
          cancelToken: new CancelToken(c => {
            this.cancel = c;
          })
        }
      )
      .then((res: AxiosResponse<string>) => {
        const dataRes: any = res.data;
        const pointWithHeights =
          dataRes.results[0].value.features[0].geometry.paths[0];
        callback(pointWithHeights);
      })
      .catch(e => {
        if (axios.isCancel(e)) {
          console.log("Request canceled", e.message);
        }
        callback([]);
      });
  }

  public componentDidUpdate(prevProps: IProps, prevStates: IState) {
    const { id: prevId } = prevProps;
    const { id } = this.props;
    if (id !== prevId) {
      this.setState({
        isComplete: false
      });
      this.loading();
    }

    if (
      this.state.isComplete !== prevStates.isComplete &&
      this.props.onComplete
    ) {
      this.props.onComplete(this.state.isComplete);
    }
  }

  public static getDerivedStateFromProps(props: IStyleProps & IProps) {
    return {
      heightOfEnd:
        props.heightOfEnd || props.heightOfEnd === 0 ? props.heightOfEnd : 20
    };
  }

  public componentDidMount() {
    if (this.props.terrainData) {
      const data = JSON.parse(this.props.terrainData);
      this.setState({
        groundHeightOfEnd: data.groundHeightOfEnd,
        groundHeightOfStart: data.groundHeightOfStart,
        arrayOfDistanceAndHeight: data.arrayOfDistanceAndHeight,
        isComplete: true,
        uuid: uuid()
      });
    } else {
      this.loading();
    }
  }

  // private heightChange = (event: any) => {
  //   this.setState({
  //     heightOfEnd: parseInt(event.target.value, 0)
  //   });
  // };

  // private refresh = () => {
  //   this.setState({
  //     isComplete: false
  //   });
  //   this.loading();
  // };
  public handleRefresh = () => {
    this.setState({
      isComplete: false
    });
    this.loading();
  };
  public render() {
    const data: ChartData<any> = {
      labels: [],
      datasets: [
        {
          label: "upper fresnel",
          fill: false,
          backgroundColor: "rgb(255, 159, 64)",
          borderColor: "rgb(255, 159, 64)",
          borderDash: [5, 5],
          data: []
        },
        {
          label: "laser",
          fill: false,
          backgroundColor: "rgb(54, 162, 235)",
          borderColor: "rgb(54, 162, 235)",
          data: []
        },
        {
          label: "lower fresnel",
          fill: false,
          backgroundColor: "rgb(75, 192, 192)",
          borderColor: "rgb(75, 192, 192)",
          borderDash: [5, 5],
          data: []
        },
        {
          label: "terrain",
          backgroundColor: "rgb(255, 99, 132)",
          borderColor: "rgb(255, 99, 132)",
          data: [],
          fill: true
        }
      ]
    };

    const { isComplete } = this.state;
    const {
      classes,
      classesOverride,
      distanceMidPoint,
      onChangeMidPointHeight
    } = this.props;
    const { heightOfStart } = this.props;
    const {
      arrayOfDistanceAndHeight,
      groundHeightOfStart,
      groundHeightOfEnd
    } = this.state;
    if (isComplete) {
      const gradient = this.getGradient();

      data.labels = arrayOfDistanceAndHeight.map((x: any) => x.distance);
      data.datasets[1].data = arrayOfDistanceAndHeight.map(
        (x: any) => x.distance * gradient + groundHeightOfStart + heightOfStart
      );
      data.datasets[0].data = this.getUpperFresnel();
      data.datasets[2].data = this.getLowerFresnel();
      data.datasets[3].data = arrayOfDistanceAndHeight.map(
        (x: any) => x.height
      );

      if (distanceMidPoint) {
        data.distanceMidPoint = distanceMidPoint;
        data.heightOfStart = this.props.heightOfStart;
        data.heightOfEnd = this.props.heightOfEnd || this.state.heightOfEnd;
        data.groundHeightOfStart = groundHeightOfStart;
        data.groundHeightOfEnd = groundHeightOfEnd;

        if (onChangeMidPointHeight) {
          data.onChangeMidPointHeight = onChangeMidPointHeight;
        }
      }
    }

    return (
      <div>
        {isComplete && (
          <div
            className={classNames(
              classes.chart,
              classesOverride && classesOverride.progress
                ? classesOverride.chart
                : ""
            )}
          >
            <IconButton
              className={classes.refreshButton}
              onClick={this.handleRefresh}
            >
              <Autorenew />
            </IconButton>
            <Line data={data} options={option} plugins={plugins} />
          </div>
        )}
        {!isComplete && (
          <CircularProgress
            className={classNames(
              classes.progress,
              classesOverride && classesOverride.progress
                ? classesOverride.progress
                : ""
            )}
            size={50}
          />
        )}
      </div>
    );
  }
}
export default withStyles(styles)(FresnelChart);
