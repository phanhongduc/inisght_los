import * as React from "react";
import * as classNames from "classnames";
import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import SidebarCore from "../../scenes/network/components/MapSidebar/SidebarCore";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import IStyleProps from "src/styles/utils";
import FresnelChart from "./FresnelChart";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    chart: {
      position: "absolute",
      left: -400,
      height: 310,
      width: 400,
      bottom: 10,
      overflow: "visible"
    },
    chartSidebarOpen: {
      zIndex: 1,
      left: 10
    }
  });

interface IProps {
  isOpen: boolean;
  id: string;
  aEndLat: number;
  aEndLon: number;
  bEndLat: number;
  bEndLon: number;
  distanceFromAendToBend: number;
}

const Sidebar = ({
  classes,
  isOpen,
  id,
  aEndLat,
  aEndLon,
  bEndLat,
  bEndLon,
  distanceFromAendToBend
}: IStyleProps & IProps) => (
  <div>
    <SidebarCore
      className={classNames(classes.chart, {
        [classes.chartSidebarOpen]: isOpen
      })}
    >
      <FresnelChart
        id={id}
        frequency={500000000}
        heightOfStart={20}
        aEndLat={aEndLat}
        aEndLon={aEndLon}
        bEndLat={bEndLat}
        bEndLon={bEndLon}
        distanceFromAendToBend={distanceFromAendToBend}
      />
    </SidebarCore>
  </div>
);

export default withStyles(styles)(Sidebar);
