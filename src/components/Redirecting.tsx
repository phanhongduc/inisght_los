import * as React from "react";
import Loading from "@material-ui/core/CircularProgress";
import { Theme, withStyles } from "@material-ui/core/styles";
import IStyleProps from "../styles/utils";
import { CSSProperties } from "@material-ui/core/styles/withStyles";

const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  wrapper: {
    marginTop: theme.spacing.unit * 2,
    textAlign: "center"
  }
});

const Redirecting = ({ classes }: IStyleProps) => {
  return (
    <div className={classes.wrapper}>
      <Loading />
    </div>
  );
};

export default withStyles(styles)(Redirecting);
