import * as React from "react";
import "mapbox-gl/dist/mapbox-gl.css";
import * as mapboxgl from "mapbox-gl";
import "@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css";
import { Geometry } from "geojson";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import { Theme } from "@material-ui/core";
import IStyleProps from "src/styles/utils";
import * as classNames from "classnames";
import {
  LngLat,
  SymbolPaint,
  LinePaint,
  FillPaint,
  CirclePaint
} from "mapbox-gl";
import { IPointGeoJsonProperties } from "src/scenes/network/types/GeoProperties";
import * as turf from "@turf/turf";

const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  mapInner: {
    position: "absolute"
  }
});

const GEO_EMPTY_FEATURE: GeoJSON.FeatureCollection<
  Geometry,
  IPointGeoJsonProperties
> = {
  type: "FeatureCollection",
  features: []
};

interface IState {
  lat: number;
  lng: number;
  zoom: number;
}

export interface IMapboxLocation {
  lng: number;
  lat: number;
}

// type IdType<T extends IMapboxData> = IMapboxLayer<T>["source"] extends undefined ? keyof T : string;
type SourceTypeIMap<
  T extends IMapboxData,
  K extends IMapboxLayerNotSource<T> | IMapboxLayerHasSource<T>
> = K extends IMapboxLayerNotSource<T>
  ? K["id"]
  : (K extends IMapboxLayerHasSource<T> ? K["source"] : string);
// type SourceType<T extends IMapboxData> = SourceTypeIMap<T, IMapboxLayer<T>>;

type PaintType<T extends LayerType> = T extends "symbol"
  ? SymbolPaint
  : T extends "line"
    ? LinePaint
    : T extends "fill"
      ? FillPaint
      : T extends "circle" ? CirclePaint : undefined;

export type LayerType = "symbol" | "fill" | "line" | "circle";

interface IMapLayerOriginNoPain {
  type: LayerType;
  layout?: {
    [key: string]:
      | string
      | number
      | boolean
      | {
          type: "identity";
          property: string;
        }
      | string[];
  };
}

type IMapLayerOrigin<
  T extends IMapLayerOriginNoPain = IMapLayerOriginNoPain
> = T & {
  paint?: PaintType<T["type"]>;
};

interface IMapboxLayerNotSource<T extends IMapboxData> {
  id: keyof T;
}
interface IMapboxLayerHasSource<T extends IMapboxData> {
  id: string;
  source: keyof T;
}

type IMapboxLayer<T extends IMapboxData> = (
  | IMapboxLayerNotSource<T>
  | IMapboxLayerHasSource<T>) &
  IMapLayerOrigin;

export type IMapboxLayerInterval<
  T extends IMapboxData,
  U extends IMapboxLayer<T> = IMapboxLayer<T>
> = U & {
  interval?: IIntervalStyle<T, SourceTypeIMap<T, U>>;
};

export interface IIntervalStyle<T extends IMapboxData, K extends keyof T> {
  paint?: IPaintIntervalStyle<T, K>;
}

interface IPaintIntervalStyle<T extends IMapboxData, K extends keyof T> {
  [key: string]: (param: T[K]) => string | number;
}

export interface IMapboxAsset {
  name: string;
  value: any;
}

export interface IMapboxData {
  [key: string]: GeoJSON.FeatureCollection;
}

export interface IMapBoxOptions<T extends IMapboxData = IMapboxData> {
  images?: IMapboxAsset[];
  center: IMapboxLocation;
  zoom: number;
  bbox?: turf.helpers.BBox;
  layers?: Array<IMapboxLayerInterval<T>>;
  data: T;
  children?: React.ReactElement<{}> | Array<React.ReactElement<{}>>;
  intervalTime?: number;
}

class MapBox<T extends IMapboxData = IMapboxData> extends React.Component<
  IMapBoxOptions<T> & IStyleProps,
  IState
> {
  private mapContainer: any;
  private map: mapboxgl.Map;

  constructor(props: IMapBoxOptions<T> & IStyleProps) {
    super(props);
    this.mapContainer = React.createRef();
    this.state = {
      lng: props.center.lng,
      lat: props.center.lat,
      zoom: props.zoom
    };
  }

  public componentDidMount() {
    const { lng, lat, zoom } = this.state;

    this.map = new mapboxgl.Map({
      container: this.mapContainer.current,
      style: "/3d_building_style.json",
      center: [lng, lat],
      zoom
    });

    this.map.addControl(new mapboxgl.NavigationControl(), "top-left");

    this.map.on("load", () => {
      if (this.props.images) {
        Promise.all(this.props.images.map(asset => this.loadAsset(asset))).then(
          () => this.loadLayer()
        );
      } else {
        this.loadLayer();
      }
    });
  }

  private loadAsset(asset: IMapboxAsset) {
    return new Promise((resolve, reject) =>
      this.map.loadImage(asset.value, (error: any, image: HTMLImageElement) => {
        if (error) {
          reject(error);
        } else {
          this.map.addImage(asset.name, image);
          resolve(image);
        }
      })
    );
  }

  private loadLayer() {
    (this.props.layers || []).forEach(layer => {
      const mapLayer = {
        ...layer
      } as mapboxgl.Layer;
      this.map.addSource(mapLayer.id, {
        type: "geojson",
        data: GEO_EMPTY_FEATURE
      });
      this.map.addLayer({
        ...mapLayer,
        source: mapLayer.source || mapLayer.id
      });

      const intervalStyle = layer.interval;
      if (intervalStyle) {
        let source: keyof T;
        if (Object.keys(layer).find(key => key === "source")) {
          source = (layer as IMapboxLayerHasSource<T>).source;
        } else {
          source = layer.id;
        }
        this.setupIntervalStyle(intervalStyle, layer.id as string, source);
      }

      this.map.on("click", mapLayer.id, (e: any) => {
        console.log(e);
        const feats = this.map.queryRenderedFeatures(e.point, {
          layers: [mapLayer.id]
        });
        console.log(feats);
      });

      this.updateData();
    });
  }

  private updateData() {
    Object.keys(this.props.data).forEach(dataKey => {
      const source = this.map.getSource(dataKey) as mapboxgl.GeoJSONSource;
      if (source) {
        source.setData(this.props.data[dataKey]);
      }
    });

    const { bbox, center } = this.props;

    if (bbox) {
      const PADDING = 50;
      this.map.fitBounds([[bbox[0], bbox[1]], [bbox[2], bbox[3]]], {
        padding: {
          top: PADDING,
          bottom: PADDING,
          left: PADDING,
          right: PADDING
        }
      });
    } else {
      this.map.setCenter(new LngLat(center.lng, center.lat));
    }
  }

  private setupIntervalStyle(
    intervalStyle: IIntervalStyle<T, keyof T>,
    layerId: string,
    source: keyof T
  ) {
    if (intervalStyle.paint) {
      this.setupIntervalStylePaint(intervalStyle.paint, layerId, source);
    }
  }

  private setupIntervalStylePaint(
    paintStyles: IPaintIntervalStyle<T, keyof T>,
    layerId: string,
    source: keyof T
  ) {
    setInterval(() => {
      Object.keys(paintStyles).forEach(key => {
        const value = paintStyles[key](this.props.data[source]);
        this.map.setPaintProperty(layerId, key, value);
      });
    }, this.props.intervalTime || 1000);
  }

  public componentDidUpdate(prevProps: IMapBoxOptions<T>, prevState: IState) {
    this.updateData();
  }

  public render() {
    return (
      <div className="map">
        <div
          ref={this.mapContainer}
          className={classNames(this.props.classes.mapInner, "map-inner")}
        />
        {this.props.children && this.props.children}
      </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(MapBox);
