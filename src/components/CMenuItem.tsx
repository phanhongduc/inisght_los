import * as React from "react";
import ListItemText from "@material-ui/core/ListItemText";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import MenuItem from "@material-ui/core/MenuItem";
import { withRouter, RouteComponentProps } from "react-router-dom";
import RouteUri from "src/helpers/routeUri";
import { withStyles } from "@material-ui/core/styles";
import IStyleProps from "../styles/utils";

const styles = {
  item: {
    color: "#fff"
  },
  itemSelected: {
    color: "#13bcff"
  },
  nested: {
    paddingLeft: "80px"
  }
};

export interface IMenuItemProps {
  text: string;
  uri?: RouteUri;
  selected?: boolean;
  disabled?: boolean;
  isSub?: boolean;
  icon?: React.ReactElement<{}>;
  onClick?: (uri: RouteUri) => void;
  onSelect?: () => void;
  onForceClick?: () => void;
}

export interface IRouteMenuProps
  extends RouteComponentProps<any>,
    IMenuItemProps {}

export const CMenuItem = ({
  text,
  selected = false,
  disabled = false,
  isSub = false,
  icon,
  uri,
  onClick,
  history,
  classes,
  onSelect,
  onForceClick
}: IRouteMenuProps & IStyleProps) => {
  const navigateUrl = () => {
    if (uri && onClick) {
      // onClick(uri);
      if (onSelect) {
        onSelect();
      }
      if (history.location.pathname !== uri.value) {
        history.push(uri.value);
      }
      // navigateToUri(uri);
    }
  };

  let icons;
  if (icon) {
    icons = (
      <ListItemIcon
        classes={{
          root: selected && !isSub ? classes.itemSelected : classes.item
        }}
      >
        {icon}
      </ListItemIcon>
    );
  }
  return (
    <MenuItem
      button={true}
      disabled={disabled}
      selected={selected}
      onClick={onForceClick || navigateUrl}
      className={isSub ? classes.nested : ""}
    >
      {icons}
      <ListItemText
        classes={{
          primary: selected && !isSub ? classes.itemSelected : classes.item
        }}
        primary={text}
      />
    </MenuItem>
  );
};

export default withRouter<IRouteMenuProps>(withStyles(styles)(CMenuItem));
