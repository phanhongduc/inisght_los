export const API_LOADING_SET = "API_LOADING_SET";
export type API_LOADING_SET = typeof API_LOADING_SET;

export const API_LOADING_UNSET = "API_LOADING_UNSET";
export type API_LOADING_UNSET = typeof API_LOADING_UNSET;

export const LIB_ACTIVE = "LIB_ACTIVE";
export type LIB_ACTIVE = typeof LIB_ACTIVE;
