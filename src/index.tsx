// Make sure css-loader ready before async loading css
// https://github.com/webpack-contrib/extract-text-webpack-plugin/issues/456
import "style-loader/lib/addStyles";
import "css-loader/lib/css-base";
import * as React from "react";
import * as ReactDOM from "react-dom";
import "./index.css";
import { createStore, combineReducers, applyMiddleware } from "redux";
import * as enthusiasms from "./reducers/index";
import { IStoreState } from "./types/index";
// import Hello from './containers/Hello';
import { Provider } from "react-redux";
import { DeepPartial } from "redux";
import { IHelloAction } from "./actions";

import createHistory from "history/createBrowserHistory";
// import { Route } from 'react-router';
import {
  routerReducer,
  routerMiddleware,
  ConnectedRouter
} from "react-router-redux";
// import { Link } from 'react-router-dom';
import App from "./App";

import * as actions from "./actions/index";
import * as middlewares from "./middlewares/index";
import MuiThemeProvider from "@material-ui/core/styles/MuiThemeProvider";
import muiTheme from "./styles/theme";
import MuiPickersUtilsProvider from "material-ui-pickers/utils/MuiPickersUtilsProvider";
import MomentUtils from "material-ui-pickers/utils/moment-utils";

const history = createHistory();
const middleware = routerMiddleware(history);

const reducers = combineReducers<IStoreState, IHelloAction>({
  ...enthusiasms,
  routing: routerReducer
});

interface IWindowReduxDevTool extends Window {
  __REDUX_DEVTOOLS_EXTENSION__(): DeepPartial<any>;
}

declare var window: IWindowReduxDevTool;

const midws = Object.keys(middlewares).map(key => middlewares[key]);

export const store = createStore<IStoreState, IHelloAction, {}, {}>(
  reducers,
  window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__(),
  applyMiddleware(middleware, ...midws)
);

store.dispatch(actions.applicationInit());

ReactDOM.render(
  <Provider store={store}>
    <ConnectedRouter history={history}>
      <MuiThemeProvider theme={muiTheme}>
        <MuiPickersUtilsProvider utils={MomentUtils}>
          <App />
        </MuiPickersUtilsProvider>
      </MuiThemeProvider>
    </ConnectedRouter>
  </Provider>,
  document.getElementById("root") as HTMLElement
);
