import { connect, Dispatch } from "react-redux";
import * as actions from "../../actions/index";
import { IStoreState } from "../../types/index";
import Layout from "../../components/layouts/DashboardNoDrawer";
import { User } from "oidc-client";
import { ILib } from "../../scenes/lib/types/libs";
import { ISelectLib, selectLib } from "../../scenes/lib/actions/libs";

interface IStateToProps {
  libs: ILib[];
  selectedLib: string;
}

interface IDispatchToProps {
  changeLib: (id: string) => void;
}

export function mapStateToProps({ libs }: IStoreState): IStateToProps {
  return {
    libs: libs.list,
    selectedLib: libs.selectedLib
  };
}

export function mapDispatchToProps(
  dispatch: Dispatch<actions.IHelloAction | ISelectLib>
): IDispatchToProps {
  return {
    changeLib: (id: string) => {
      dispatch(selectLib(id));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)<{
  account: User;
  children: React.ReactElement<{}> | Array<React.ReactElement<{}>>;
}>(Layout);
