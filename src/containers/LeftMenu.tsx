import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../types/index";
import { RouterAction } from "react-router-redux";
import LeftMenu from "../components/LeftMenu";
import { handleMainMapState, IHandleMainMapState } from "../actions";
import IMapState from "../scenes/network/types/MapState";

export function mapStateToProps({
  auth,
  libs: { currentRole },
  jobs: { recentJobs },
  mainMap: { mapState }
}: IStoreState) {
  return {
    auth,
    currentRole,
    recentJobs,
    mapState
  };
}

export function mapDispatchToProps(
  dispatch: Dispatch<RouterAction | IHandleMainMapState>
) {
  return {
    handleMainMapState: (mapState: IMapState) => {
      handleMainMapState(dispatch)(mapState);
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LeftMenu);
