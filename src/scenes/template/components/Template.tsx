import * as React from "react";
import { Theme } from "@material-ui/core/styles";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import IStyleProps from "../../../styles/utils";
import FileDownload from "@material-ui/icons/FileDownload";
import Upload from "@material-ui/icons/FileUpload";
import Typography from "@material-ui/core/Typography";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import Paper from "@material-ui/core/Paper";
import { Component } from "react";
import green from "@material-ui/core/colors/green";
import blue from "@material-ui/core/colors/blue";
import ReactFileReader from "react-file-reader";

const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  wrap: {
    padding: theme.spacing.unit * 2
  },
  title: {
    backgroundColor: "#28b1fc",
    paddingLeft: theme.spacing.unit * 2,
    paddingRight: theme.spacing.unit * 2,
    paddingTop: theme.spacing.unit * 3,
    paddingBottom: theme.spacing.unit * 3,

    "& > *": {
      color: theme.palette.common.white
    }
  },
  container: {
    padding: theme.spacing.unit * 2
  },
  download: {
    color: theme.palette.common.white,
    backgroundColor: green[500],

    "&:hover, &:focus": {
      color: theme.palette.common.white,
      backgroundColor: green[700]
    }
  },
  upload: {
    color: theme.palette.common.white,
    backgroundColor: blue[500],

    "&:hover, &:focus": {
      color: theme.palette.common.white,
      backgroundColor: blue[700]
    }
  },
  uploadInput: {
    display: "none"
  },
  actions: {
    marginTop: theme.spacing.unit,

    "& a": {
      textDecoration: "none"
    }
  },
  doc: {
    width: "100%",
    minHeight: 400,
    height: "calc(100vh - 290px)",
    marginTop: theme.spacing.unit * 2
  },
  docLoad: {
    width: "100%",
    height: "100%"
  },
  right: {
    display: "flex",
    justifyContent: "flex-end"
  },
  view: {
    marginTop: 20
  },
  listToken: {
    marginTop: 5,
    fontSize: 16
  }
});

interface IStates {
  fileName: string;
  isHandle: boolean;
}

interface IProps {
  doc: string;
  uploadDoc: (files: FileList, done: () => void) => void;
}

class Template extends Component<IStyleProps & IProps, IStates> {
  public state = {
    isHandle: false,
    fileName: ""
  };

  private handleFile = (files: any) => {
    this.setState(
      {
        isHandle: true,
        fileName: files[0].name
      },
      () => {
        this.props.uploadDoc(files, () => {
          this.setState({
            isHandle: false
          });
        });
      }
    );
  };

  public render() {
    const { classes, doc } = this.props;
    return (
      <div className={classes.wrap}>
        <Paper>
          <div className={classes.title}>
            <Typography variant="title">Template Management</Typography>
          </div>
          <div className={classes.container}>
            <Typography>
              Use Template Management to create/customise and load reports that
              can be used for management or clients.
            </Typography>

            <Grid container={true} spacing={8} className={classes.actions}>
              <Grid item={true} md={6}>
                {doc && (
                  <a href={doc}>
                    <Button
                      className={classes.download}
                      variant={"raised"}
                      disabled={this.state.isHandle}
                    >
                      <FileDownload />
                      <span>Download example template</span>
                    </Button>
                  </a>
                )}
              </Grid>

              <Grid item={true} md={6}>
                <ReactFileReader
                  fileTypes={[".xlsx", ".docx"]}
                  handleFiles={this.handleFile}
                >
                  <div className={classes.right}>
                    <Button
                      className={classes.upload}
                      variant={"raised"}
                      disabled={this.state.isHandle}
                    >
                      <Upload />
                      <span>Upload template</span>
                    </Button>
                  </div>
                </ReactFileReader>
              </Grid>
            </Grid>
            <Grid container={true} spacing={8} className={classes.view}>
              <Grid item={true} md={6}>
                <Typography>List available token:</Typography>
                <ul className={classes.listToken}>
                  <li>
                    <b>#JobName#</b>: Current job name
                  </li>
                  <li>
                    <b>#Address#</b>: Address of the job
                  </li>
                  <li>
                    <b>#Date#</b>: Current date when export
                  </li>
                  <li>
                    <b>#Section-Candidates#</b>: Mark begin section candidates
                  </li>
                  <li>
                    <b>#/Section-Candidates#</b>: Mark end section candidates
                  </li>
                  <li>
                    <b>#SiteName#</b>: Site name (Use inside Section-Candidates
                    only){" "}
                  </li>
                  <li>
                    <b>#SiteAddress#</b>: Site address (Use inside
                    Section-Candidates only)
                  </li>
                  <li>
                    <b>#Section-Photos#</b>: Mark begin section photos of site
                    (Use inside Section-Candidates only)
                  </li>
                  <li>
                    <b>#/Section-Photos#</b>: Mark end section photos of site
                    (Use inside Section-Candidates only)
                  </li>
                  <li>
                    <b>#Height#</b>: The current height that taken photos (Use
                    inside Section-Photos only)
                  </li>
                  <li>
                    <b>#Photo#</b>: The current photo taken at height (Use
                    inside Section-Photos only)
                  </li>
                  <li>
                    <b>#Mid-Point-Photos#</b>: List mid point photos (Use inside
                    Section-Candidates only)
                  </li>
                </ul>
              </Grid>
              <Grid item={true} md={6}>
                {doc && (
                  <div className={classes.doc}>
                    <iframe
                      className={classes.docLoad}
                      src={`https://docs.google.com/gview?url=${doc}&embedded=true`}
                    />
                  </div>
                )}
              </Grid>
            </Grid>
          </div>
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(Template);
