import { connect, Dispatch } from "react-redux";
import Template from "../components/Template";
import axios, { AxiosError, AxiosResponse } from "axios";
import { REACT_APP_API_URL } from "../../../environment";
import { store } from "../../../index";
import { common } from "../../../actions";
import Variant from "../../../components/notification/types/variant";
import getErrorMessage from "../../../helpers/getErrorMessage";
import { IStoreState } from "../../../types";
import { setLibTemplate } from "../../lib/actions/libs";

function mapStateToProps(state: IStoreState) {
  return {
    doc: state.libs.template
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    uploadDoc: (files: FileList, done: () => void) => {
      const currentState = store.getState();

      const formData = new FormData();

      formData.append("file", files[0]);

      axios
        .post(`${REACT_APP_API_URL}/api/Misc/docs`, formData, {
          headers: {
            "Content-Type": "multipart/form-data"
          }
        })
        .then((data: AxiosResponse<string>) => {
          axios
            .post(
              `${REACT_APP_API_URL}/api/Misc/${
                currentState.libs.selectedLib
              }/template`,
              {
                templateUrl: data.data
              }
            )
            .then(() => {
              common.fireNotification(dispatch)({
                message: "DownloadIcon success.",
                variant: Variant.SUCCESS
              });
              dispatch(setLibTemplate(data.data));
              done();
            })
            .catch((e: AxiosError) => {
              console.error(e);
              const response = e.response as AxiosResponse;
              common.fireNotification(dispatch)({
                message:
                  response && response.data
                    ? getErrorMessage(response.data, "Failed upload")
                    : "Failed upload",
                variant: Variant.ERROR
              });
              done();
            });
        })
        .catch((e: AxiosError) => {
          console.error(e);
          const response = e.response as AxiosResponse;
          common.fireNotification(dispatch)({
            message:
              response && response.data
                ? getErrorMessage(response.data, "Failed upload")
                : "Failed upload",
            variant: Variant.ERROR
          });
          done();
        });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Template);
