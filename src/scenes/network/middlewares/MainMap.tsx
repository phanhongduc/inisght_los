import { Store } from "react-redux";
import { IStoreState } from "src/types";
import { Dispatch } from "redux";
import { SETUP_AUTHENTICATION } from "src/constants";
import * as actions from "../actions";
import { IHandleMainMapState } from "../actions/MainMap";
import { HANDLE_MAIN_MAP_STATE } from "../constants";
import clientStorage, { ClientStorageKey } from "src/helpers/clientStorage";
import { ISetupAuthentication } from "src/scenes/auth/actions";

export const saveMainMapState2LocalStorage = (store: Store<IStoreState>) => (
  next: Dispatch<IHandleMainMapState>
) => (action: IHandleMainMapState) => {
  const currentState = store.getState();
  switch (action.type) {
    case HANDLE_MAIN_MAP_STATE: {
      clientStorage.setItem(
        ClientStorageKey.MAIN_MAP_STATE,
        JSON.stringify(action.payload || {}),
        currentState.auth.account
      );
    }
  }

  return next(action);
};

export const exportMainMapStateFromLocalStorage = (
  store: Store<IStoreState>
) => (next: Dispatch<ISetupAuthentication>) => (
  action: ISetupAuthentication
) => {
  switch (action.type) {
    case SETUP_AUTHENTICATION: {
      if (!action.payload) {
        break;
      }
      const stateStr = clientStorage.getItem(
        ClientStorageKey.MAIN_MAP_STATE,
        action.payload
      );

      if (stateStr) {
        const mapState = stateStr ? JSON.parse(stateStr) : {};
        if (Object.keys(mapState).length) {
          actions.handleMainMapState(store.dispatch)(mapState);
        }
      }
    }
  }

  return next(action);
};
