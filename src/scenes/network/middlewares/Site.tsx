import { Store, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import { IGetSiteAddress, setSiteAddress } from "../actions/Site";
import { GET_SITE_ADDRESS } from "../../../constants";
import { GeoCodingService } from "../../../helpers/GeoCodingService";

export const siteMiddlewares = (store: Store<IStoreState>) => (
  next: Dispatch<IGetSiteAddress>
) => (action: IGetSiteAddress) => {
  // const currentState = store.getState();
  switch (action.type) {
    case GET_SITE_ADDRESS: {
      const geoCoder = new GeoCodingService();
      geoCoder
        .reverseGeocode({
          lat: action.payload.lat,
          lng: action.payload.lon
        })
        .then(([result]) => {
          store.dispatch(
            setSiteAddress({
              id: action.payload.id,
              address: result.formatted_address
            })
          );
        });

      break;
    }
  }

  return next(action);
};
