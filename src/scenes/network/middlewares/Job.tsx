import { Dispatch, Store } from "react-redux";
import { IStoreState } from "../../../types";
import axios, { AxiosError, AxiosResponse } from "axios";
import cloneDeep from "lodash-es/cloneDeep";
import {
  DeleteJobDetailBEndType,
  getJobDetail,
  getRecentJobs,
  IChangeJobMediaStatus,
  ICreateMidPoint,
  IDeleteJobDetailBEnd,
  IDeleteMidPointMedia,
  IEditJobDetail,
  IExportJob,
  IFinishJob,
  IGetJobDetail,
  IGetRecentJobs,
  setJobDetail,
  setJobDetailBend,
  setJobDetailMidPoint,
  setJobDetailState,
  setRecentJobs
} from "../actions/Job";
import {
  CHANGE_JOB_MEDIA_STATUS,
  CREATE_JOB_MID_POINT,
  DELETE_JOB_DETAIL_BEND,
  DELETE_JOB_MID_POINT_MEDIA,
  EDIT_JOB_DETAIL,
  EXPORT_JOB,
  FINISH_JOB,
  GET_JOB_DETAIL,
  GET_RECENT_JOBS
} from "../constants/Job";
import { REACT_APP_API_URL } from "../../../environment";
import {
  IJobDetail,
  IJobDetailBEnd,
  IJobInfo,
  IMidPoint,
  MediaStatus
} from "../types/Job";
import { updateTab } from "../../../components/TabRoute/actions";
import paths from "../../../paths";
import { common, IApplicationInit } from "../../../actions";
import Variant from "../../../components/notification/types/variant";
import { APPLICATION_INIT } from "../../../constants";
import { JobStatus } from "../../lib/types/libs";
import { setListJob } from "../../lib/actions/libs";
import getErrorMessage from "../../../helpers/getErrorMessage";

export const jobsMiddleware = (store: Store<IStoreState>) => (
  next: Dispatch<
    | IGetJobDetail
    | IEditJobDetail
    | IDeleteJobDetailBEnd
    | IApplicationInit
    | IExportJob
    | IFinishJob
    | IGetJobDetail
    | IEditJobDetail
    | IDeleteJobDetailBEnd
    | IApplicationInit
    | IGetJobDetail
    | IEditJobDetail
    | IDeleteJobDetailBEnd
    | IApplicationInit
    | IGetRecentJobs
    | IChangeJobMediaStatus
  >
) => (
  action:
    | IGetJobDetail
    | IGetRecentJobs
    | IEditJobDetail
    | IDeleteJobDetailBEnd
    | IApplicationInit
    | IExportJob
    | IFinishJob
    | IChangeJobMediaStatus
) => {
  const currentState = store.getState();
  switch (action.type) {
    case APPLICATION_INIT: {
      if (currentState.libs.selectedLib) {
        store.dispatch(getRecentJobs(currentState.libs.selectedLib));
      }
      break;
    }
    case GET_RECENT_JOBS: {
      axios
        .get(`${REACT_APP_API_URL}/api/Job/${action.payload}/last5`)
        .then((data: AxiosResponse<IJobInfo[]>) => {
          store.dispatch(setRecentJobs(data.data));
        });
      break;
    }
    case GET_JOB_DETAIL: {
      store.dispatch(
        setJobDetailState(action.payload, {
          isFetching: true,
          isFailed: false
        })
      );

      axios
        .get(
          `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/detail/${action.payload}`
        )
        .then((data: AxiosResponse<IJobDetail>) => {
          updateTab(store.dispatch)("networkRoutes", {
            id: paths.jobDetail.replace(":id", data.data.jobId),
            name: `Job ${data.data.jobName}`,
            isAlways: false
          });
          store.dispatch(
            setJobDetailState(action.payload, {
              isFetching: false,
              isFailed: false
            })
          );
          store.dispatch(setJobDetail(data.data));
        });
      axios
        .all([
          axios.get(
            `${REACT_APP_API_URL}/api/Job/${
              currentState.libs.selectedLib
            }/detail/${action.payload}`
          ),
          axios.get(
            `${REACT_APP_API_URL}/api/Job/${
              currentState.libs.selectedLib
            }/media/${action.payload}`
          ),
          axios.get(
            `${REACT_APP_API_URL}/api/Job/${
              currentState.libs.selectedLib
            }/midpoint/${action.payload}`
          )
        ])
        .then(
          axios.spread(
            (
              dataDetail: AxiosResponse,
              dataBend: AxiosResponse,
              dataMidPoint: AxiosResponse
            ) => {
              const detail = dataDetail.data as IJobDetail;
              const bend = dataBend.data as IJobDetailBEnd[];
              const midPoint = dataMidPoint.data as IMidPoint[];
              updateTab(store.dispatch)("networkRoutes", {
                id: paths.jobDetail.replace(":id", detail.jobId),
                name: `Job ${detail.jobName}`,
                isAlways: false
              });
              setJobDetailState(action.payload, {
                isFetching: false,
                isFailed: false
              });
              store.dispatch(setJobDetail(detail));
              store.dispatch(setJobDetailBend(detail.jobId, bend));
              store.dispatch(setJobDetailMidPoint(detail.jobId, midPoint));
            }
          )
        )
        .catch((e: AxiosError) => {
          setJobDetailState(action.payload, {
            isFetching: false,
            isFailed: true
          });
          console.error(e);
        });
      break;
    }
    case EDIT_JOB_DETAIL: {
      const data = action.payload.data;
      const currentBend =
        currentState.jobs.listJob[action.payload.data.id].bends;

      const deleteBends =
        action.payload.candidates.length > 0
          ? currentBend.filter(b => {
              return Boolean(
                !action.payload.candidates.find(c => c.bendId === b.bendId)
              );
            })
          : [];
      const pushBends =
        action.payload.candidates.length > 0
          ? action.payload.candidates.filter(b => {
              return Boolean(!currentBend.find(c => c.bendId === b.bendId));
            })
          : [];

      const pushBendAxios = pushBends.map(bend => {
        return axios.post(
          `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/update/${action.payload.data.id}/bend/`,
          {
            bendId: bend.bendId,
            bendSiteName: bend.bendSiteName,
            lon: bend.lon,
            lat: bend.lat,
            displayOrder: 0,
            bendHeight: 20
          }
        );
      });

      const deleteBendAxios = deleteBends.map(bend => {
        return axios.delete(
          `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/update/${action.payload.data.id}/bend/${bend.bendId}`
        );
      });

      const operation = [];

      if (pushBendAxios.length > 0) {
        operation.push(
          new Promise((resolve, reject) => {
            axios
              .all(pushBendAxios)
              .then(
                axios.spread((...args) => {
                  resolve(args);
                })
              )
              .catch(e => {
                reject(e);
              });
          })
        );
      }

      if (deleteBendAxios.length > 0) {
        operation.push(
          new Promise((resolve, reject) => {
            axios
              .all(deleteBendAxios)
              .then(
                axios.spread((...args) => {
                  resolve(args);
                })
              )
              .catch(e => {
                reject(e);
              });
          })
        );
      }

      const newData = {
        jobName: data.jobName,
        recommendHeight: data.recommendHeight,
        frequency: data.frequency,
        engineerId: data.engineerId
      };

      operation.push(
        new Promise((resolve, reject) => {
          axios
            .put(
              `${REACT_APP_API_URL}/api/Job/${
                currentState.libs.selectedLib
              }/update/${action.payload.data.id}`,
              newData
            )
            .then((res: AxiosResponse) => {
              resolve(res);
            })
            .catch((e: AxiosError) => {
              reject(e);
            });
        })
      );

      Promise.all(operation)
        .then(() => {
          store.dispatch(getJobDetail(action.payload.data.id));
          common.fireNotification(store.dispatch)({
            message: "Edit job success.",
            variant: Variant.SUCCESS
          });
          action.meta.done();
        })
        .catch(e => {
          common.fireNotification(store.dispatch)({
            message: "Edit job failed.",
            variant: Variant.ERROR
          });
          action.meta.done();
        });

      break;
    }

    case DELETE_JOB_DETAIL_BEND: {
      let url: string = "";
      switch (action.payload.type) {
        case DeleteJobDetailBEndType.Media: {
          url = `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/media/${action.payload.jobId}/delete/${action.payload.id}`;
          break;
        }
        case DeleteJobDetailBEndType.Height: {
          url = `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/media/${action.payload.jobId}/deletesetbyheight/${
            action.payload.bendId
          }/${action.payload.height}`;
          break;
        }
      }

      if (url) {
        axios
          .delete(url)
          .then(() => {
            store.dispatch(getJobDetail(action.payload.jobId));

            common.fireNotification(store.dispatch)({
              message: "Edit job success.",
              variant: Variant.SUCCESS
            });
            if (action.meta && action.meta.done) {
              action.meta.done();
            }
          })
          .catch(e => {
            common.fireNotification(store.dispatch)({
              message: "Edit job failed.",
              variant: Variant.ERROR
            });
            if (action.meta && action.meta.done) {
              action.meta.done();
            }
          });
      }

      break;
    }

    case EXPORT_JOB: {
      axios
        .post(
          `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/export/${action.payload.id}`
        )
        .then(data => {
          common.fireNotification(store.dispatch)({
            message: "Export document success",
            variant: Variant.SUCCESS
          });
          if (action.meta && action.meta.done) {
            action.meta.done(true, data.data);
          }
        })
        .catch(e => {
          const response = e.response as AxiosResponse;
          common.fireNotification(store.dispatch)({
            message:
              response && response.data
                ? getErrorMessage(response.data, "Export document failed")
                : "Export document failed",
            variant: Variant.ERROR
          });
          if (action.meta && action.meta.done) {
            action.meta.done(false, "");
          }
        });
      break;
    }

    case FINISH_JOB: {
      axios
        .put(
          `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/update/${action.payload}/status`,
          {
            status: JobStatus.Finished
          }
        )
        .then(() => {
          common.fireNotification(store.dispatch)({
            message: "Job is finished",
            variant: Variant.SUCCESS
          });
          const newData = cloneDeep(currentState.jobs.listJob[action.payload]);
          newData.jobStatus = JobStatus.Finished;
          store.dispatch(setJobDetail(newData));

          if (
            currentState.libs.jobSearchResult.jobs.find(
              job => job.id === action.payload
            )
          ) {
            store.dispatch(
              setListJob({
                total: currentState.libs.jobSearchResult.total,
                jobs: currentState.libs.jobSearchResult.jobs.map(j => {
                  if (j.id === action.payload) {
                    j.jobStatus = JobStatus.Finished;
                  }
                  return j;
                })
              })
            );
          }

          if (action.meta && action.meta.done) {
            action.meta.done(true);
          }
        })
        .catch(e => {
          common.fireNotification(store.dispatch)({
            message: "Change job status failed",
            variant: Variant.ERROR
          });
          if (action.meta && action.meta.done) {
            action.meta.done(false);
          }
        });
      break;
    }

    case CHANGE_JOB_MEDIA_STATUS: {
      axios
        .put(
          `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/media/${action.payload.id}/status`,
          {
            released: action.payload.status === MediaStatus.Released
          }
        )
        .then(data => {
          common.fireNotification(store.dispatch)({
            message: "Change media status success.",
            variant: Variant.SUCCESS
          });

          const newBendStates = cloneDeep(
            currentState.jobs.jobBend[action.payload.jobId]
          ).reduce(
            (b, current) => {
              current.media.forEach((ms, ih) => {
                ms.media.forEach((m, i) => {
                  if (m.mediaId === action.payload.id) {
                    m.mediaStatus = action.payload.status;
                  }
                });
              });

              b.push(current);

              return b;
            },
            [] as IJobDetailBEnd[]
          );

          store.dispatch(setJobDetailBend(action.payload.jobId, newBendStates));
          if (action.meta && action.meta.done) {
            action.meta.done(true);
          }
        })
        .catch(e => {
          common.fireNotification(store.dispatch)({
            message: "Change media status failed.",
            variant: Variant.ERROR
          });
          if (action.meta && action.meta.done) {
            action.meta.done(false);
          }
        });
      break;
    }
  }
  return next(action);
};

export * from "../components/Job/VirtualLOS/middlewares";

export const jobsMidPointMiddleware = (store: Store<IStoreState>) => (
  next: Dispatch<ICreateMidPoint | IDeleteMidPointMedia>
) => (action: ICreateMidPoint | IDeleteMidPointMedia) => {
  const currentState = store.getState();

  switch (action.type) {
    case CREATE_JOB_MID_POINT: {
      axios
        .post(
          `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/midpoint/${action.payload.aendSiteId}/new`,
          action.payload
        )
        .then((res: AxiosResponse) => {
          store.dispatch(getJobDetail(action.payload.aendSiteId));

          common.fireNotification(store.dispatch)({
            message: "Create midpoint success.",
            variant: Variant.SUCCESS
          });
          if (action.meta && action.meta.done) {
            action.meta.done(true);
          }
        })
        .catch((e: AxiosError) => {
          common.fireNotification(store.dispatch)({
            message: "Create job midpoint failed.",
            variant: Variant.ERROR
          });
        });

      if (action.meta && action.meta.done) {
        action.meta.done(false);
      }

      break;
    }

    case DELETE_JOB_MID_POINT_MEDIA: {
      axios
        .delete(
          `${REACT_APP_API_URL}/api/Job/${
            currentState.libs.selectedLib
          }/midpoint/${action.payload.jobId}/deletemedia/${
            action.payload.mediaId
          }`
        )
        .then(() => {
          common.fireNotification(store.dispatch)({
            message: "Delete media success.",
            variant: Variant.SUCCESS
          });
          if (action.meta && action.meta.done) {
            action.meta.done(true);
          }
          store.dispatch(getJobDetail(action.payload.jobId));
        })
        .catch((e: AxiosError) => {
          common.fireNotification(store.dispatch)({
            message: "Delete media failed",
            variant: Variant.ERROR
          });
          if (action.meta && action.meta.done) {
            action.meta.done(false);
          }
        });
    }
  }

  return next(action);
};
