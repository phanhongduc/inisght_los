import { Store, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import { ILoadListSite, setListSite } from "../actions/Site";
import { LOAD_LIST_SITE } from "../../../constants";
// import axios, { AxiosResponse } from "axios";
import { FeatureCollection, Feature, Point } from "geojson";
import { IProperties } from "../types/Site";
import { getLibVersion, ISetLibVersion } from "../../lib/actions/libs";
import { SET_LIB_VERSION } from "../../lib/constants/libs";
import axios, { AxiosError, AxiosResponse } from "axios";
import { LibStatus } from "../../lib/types/libs";

export const loadListSite = (store: Store<IStoreState>) => (
  next: Dispatch<ILoadListSite | ISetLibVersion>
) => (action: ILoadListSite | ISetLibVersion) => {
  const currentState = store.getState();
  switch (action.type) {
    case LOAD_LIST_SITE: {
      if (currentState.libs.selectedLib) {
        const selected = currentState.libs.list.find(
          lib => lib.id === currentState.libs.selectedLib
        );
        if (selected && selected.status === LibStatus.SUCCESS) {
          store.dispatch(getLibVersion());
        }
      }
      break;
    }

    case SET_LIB_VERSION: {
      axios
        .get(action.payload.siteJsonUrl)
        .then((data: AxiosResponse<FeatureCollection>) => {
          const listSite = data.data.features as Array<
            Feature<Point, IProperties>
          >;

          store.dispatch(setListSite(listSite));
        })
        .catch((e: AxiosError) => {
          console.error(e);
        });
      break;
    }
  }

  return next(action);
};
