import { Dispatch, Store } from "react-redux";
import { IStoreState } from "../../../types";
import {
  IGetJobLog,
  IGetJobLogFile,
  setJobLog,
  setJobLogFile
} from "../actions/Job";
import axios, { AxiosError, AxiosResponse } from "axios";
import { common } from "../../../actions";
import Variant from "../../../components/notification/types/variant";
import { REACT_APP_API_URL } from "../../../environment";
import { IJobLog } from "../types/Job";
import getErrorMessage from "../../../helpers/getErrorMessage";

export const jobLogMiddlewares = (store: Store<IStoreState>) => (
  next: Dispatch<IGetJobLog | IGetJobLogFile>
) => (action: IGetJobLog | IGetJobLogFile) => {
  switch (action.type) {
    case "GET_JOB_LOG": {
      store.dispatch(
        setJobLog(action.payload.id, {
          isFetching: true,
          isFailed: false
        })
      );

      axios
        .get(
          `${REACT_APP_API_URL}/api/Job/${action.payload.libraryId}/flightlog/${
            action.payload.id
          }`
        )
        .then((res: AxiosResponse<IJobLog[]>) => {
          store.dispatch(
            setJobLog(action.payload.id, {
              lastModified: new Date().getTime(),
              isFetching: false,
              isFailed: false,
              data: res.data
            })
          );

          if (action.meta.done) {
            action.meta.done(true);
          }
        })
        .catch((e: AxiosError) => {
          const response = e.response as AxiosResponse;
          common.fireNotification(store.dispatch)({
            message:
              response && response.data
                ? getErrorMessage(response.data, "Get list log failed.")
                : "Get list log failed.",
            variant: Variant.ERROR
          });

          store.dispatch(
            setJobLog(action.payload.id, {
              isFetching: false,
              isFailed: true
            })
          );

          if (action.meta.done) {
            action.meta.done(false);
          }
        });

      break;
    }

    case "GET_JOB_LOG_FILE": {
      store.dispatch(
        setJobLogFile(action.payload.id, {
          isFetching: true,
          isFailed: true,
          url: action.payload.url
        })
      );

      axios
        .get(action.payload.url)
        // .get(`https://cors-anywhere.herokuapp.com/${action.payload.url}`)
        .then((res: AxiosResponse<string>) => {
          const lines = res.data.split("\n");

          store.dispatch(
            setJobLogFile(action.payload.id, {
              lastModified: new Date().getTime(),
              isFetching: false,
              isFailed: false,
              data: lines.reduce(
                (m, l) => {
                  if (l) {
                    const final = m;
                    final.push(l.split("|"));
                    return final;
                  }
                  return m;
                },
                [] as string[][]
              )
            })
          );

          if (action.meta.done) {
            action.meta.done(true);
          }
        })
        .catch((e: AxiosError) => {
          common.fireNotification(store.dispatch)({
            message: "Get log failed.",
            variant: Variant.ERROR
          });
          store.dispatch(
            setJobLogFile(action.payload.id, {
              isFetching: false,
              isFailed: true
            })
          );
          if (action.meta.done) {
            action.meta.done(false);
          }
        });

      break;
    }
  }

  return next(action);
};
