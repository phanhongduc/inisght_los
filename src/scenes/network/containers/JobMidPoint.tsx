import { connect, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import JobMidPoint from "../components/Job/JobMidPoint";
import { getSiteAddress, IGetSiteAddressData } from "../actions/Site";
import { virtualLosDialogOpen } from "../components/Job/VirtualLOS/actions";
import { deleteMidPointMedia } from "../actions/Job";

function mapStateToProps({ site }: IStoreState) {
  return {
    siteAddress: site.siteAddress
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    opVlos: (jobId: string, mediaId: string) => {
      virtualLosDialogOpen(dispatch)(jobId, mediaId);
    },
    getSiteAddress: (data: IGetSiteAddressData) => {
      dispatch(getSiteAddress(data));
    },
    deleteMidPointMedia: (
      mediaId: string,
      jobId: string,
      done?: (success: boolean) => void
    ) => {
      dispatch(
        deleteMidPointMedia(
          {
            jobId,
            mediaId
          },
          done
        )
      );
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JobMidPoint);
