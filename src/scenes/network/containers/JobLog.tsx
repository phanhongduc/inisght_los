import { connect, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import JobLog, {
  IJobLogDispatchProps,
  IJobLogOwnProps,
  IJobLogStateProps
} from "../components/Job/JobLog";
import { getJobLog, getJobLogFile } from "../actions/Job";
import { IJobLogFile } from "../types/Job";

function mapStateToProps({ jobs }: IStoreState, props: IJobLogOwnProps) {
  const list: {
    [k: string]: IJobLogFile;
  } = {};

  if (jobs.jobLog[props.id] && jobs.jobLog[props.id].data) {
    jobs.jobLog[props.id].data.forEach(d => {
      if (jobs.logs[d.id]) {
        list[d.id] = jobs.logs[d.id];
      }
    });
  }

  return {
    data: jobs.jobLog[props.id] || null,
    logs: list
  };
}

function mapDispatchToProps(dispatch: Dispatch, props: IJobLogOwnProps) {
  return {
    getLog: () => {
      dispatch(getJobLog(props.id, props.selectedLib));
    },
    getLogFile: (id: string, url: string) => {
      dispatch(getJobLogFile(id, url));
    }
  };
}

export default connect<
  IJobLogStateProps,
  IJobLogDispatchProps,
  IJobLogOwnProps
>(
  mapStateToProps,
  mapDispatchToProps
)(JobLog);
