import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import JobDetail from "../components/Job/JobDetail";
import {
  createMidPoint,
  deleteJobDetailBEnd,
  editJobDetail,
  exportJob,
  finishJob,
  getJobDetail,
  ICreateMidPointData,
  IDeleteJobDetailBEndData,
  IEditJobData
} from "../actions/Job";
import { IJobCandidate } from "../../lib/types/libs";
import Variant from "../../../components/notification/types/variant";
import { fireNotification } from "../../../components/notification/action";

function mapStateToProps({
  libs: { selectedLib, users },
  jobs,
  currentUri
}: IStoreState) {
  const paths = currentUri.value.split("/");

  const job = jobs.listJob[paths[3]];

  return {
    engineers: users,
    bends: job ? jobs.jobBend[paths[3]] : [],
    midPoints: job ? jobs.jobMidPoint[paths[3]] : [],
    selectedLib,
    isLoading: job ? jobs.jobState[paths[3]].isFetching : false,
    data: job ? job : undefined
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    deleteJobDetailBEnd: (data: IDeleteJobDetailBEndData, cb: () => void) => {
      dispatch(deleteJobDetailBEnd(data, cb));
    },
    getJobDetail: (id: string) => {
      dispatch(getJobDetail(id));
    },
    editJobDetail: (
      data: IEditJobData,
      candidates: IJobCandidate[],
      done: (reset: boolean) => void
    ) => {
      dispatch(editJobDetail(data, candidates, done));
    },
    createMidPoint: (data: ICreateMidPointData, done: () => void) => {
      dispatch(createMidPoint(data, done));
    },
    exportJob: (id: string, cb?: (success: boolean, url: string) => void) => {
      dispatch(exportJob(id, cb));
    },
    finishJob: (id: string, cb?: () => void) => {
      dispatch(finishJob(id, cb));
    },
    notify: (variant: Variant, message: string) => {
      fireNotification(dispatch)({
        message,
        variant
      });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JobDetail);
