import { connect, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import JobBEnds from "../components/Job/JobBEnds";
import { virtualLosDialogOpen } from "../components/Job/VirtualLOS/actions";
import { getSiteAddress, IGetSiteAddressData } from "../actions/Site";
import { MediaStatus } from "../types/Job";
import { changeJobMediaStatus } from "../actions/Job";

function mapStateToProps({ site }: IStoreState) {
  return {
    siteAddress: site.siteAddress
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    opVlos: (jobId: string, mediaId: string) => {
      virtualLosDialogOpen(dispatch)(jobId, mediaId);
    },
    getSiteAddress: (data: IGetSiteAddressData) => {
      dispatch(getSiteAddress(data));
    },
    changeJobMediaStatus: (
      id: string,
      jobId: string,
      status: MediaStatus,
      done?: (success: boolean) => void
    ) => {
      dispatch(changeJobMediaStatus(id, jobId, status, done));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(JobBEnds);
