import { connect, Dispatch } from "react-redux";
import { RouterAction } from "react-router-redux";
import * as actions from "../../../actions";
import { ISetTabId } from "../../../actions/TabBoard";
import { IStoreState } from "../../../types";
import NetworkBoard from "../components/NetworkBoard";

export function mapStateToProps({
  mainMap: {},
  common: { loading }
}: IStoreState) {
  return {
    isLoading: loading.length > 0
  };
}

export function mapDispatchToProps(
  dispatch: Dispatch<actions.IHelloAction | ISetTabId | RouterAction>
) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NetworkBoard);
