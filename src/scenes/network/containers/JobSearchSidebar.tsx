import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import SearchSidebar from "../components/MapSidebar/JobSearchSidebar";
import { getJob, setJobQuery } from "../../lib/actions/libs";
import { ILibQueryPoint } from "../../lib/types/libs";

function mapStateToProps({
  libs: { jobSearchResult, jobQuery, users }
}: IStoreState) {
  return {
    result: jobSearchResult,
    query: jobQuery,
    users
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onChangeSkip: (skip: number) => {
      dispatch(
        setJobQuery({
          skip
        })
      );
      dispatch(getJob());
    },
    onChangeUser: (id: string) => {
      dispatch(
        setJobQuery({
          assigneeId: id
        })
      );
      dispatch(getJob());
    },
    onChangeStatus: (status: number) => {
      dispatch(
        setJobQuery({
          jobStatus: status
        })
      );
      dispatch(getJob());
    },
    onChangeSearchInMap: (data: ILibQueryPoint[]) => {
      dispatch(
        setJobQuery({
          polygonCoordinates: data
        })
      );
      dispatch(getJob());
    },
    onChangeKeyword: (keyword: string) => {
      dispatch(
        setJobQuery({
          keyword,
          take: 10,
          skip: 0
        })
      );
      dispatch(getJob());
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps,
  null,
  { withRef: true }
)(SearchSidebar);
