import { connect, Dispatch } from "react-redux";

import MapBox from "../components/MapBox";
import { IStoreState } from "../../../types";
import { loadListSite } from "../actions/Site";
import { common, handleMainMapState } from "../../../actions";
import Variant from "../../../components/notification/types/variant";
import { getJob, saveJob, setJobQuery } from "../../lib/actions/libs";
import {
  IJobCandidate,
  ILibQueryPoint,
  ISaveJobData
} from "../../lib/types/libs";
import { editJobDetail, IEditJobData } from "../actions/Job";
import IMapState from "../types/MapState";

export function mapStateToProps({
  mainMap: { jsonVersion, mapState },
  site: { listSite },
  libs: { users, jobSearchResult, jobQuery, list, selectedLib }
}: IStoreState) {
  return {
    currentLib: list.find(l => l.id === selectedLib),
    libUsers: users,
    jsonVersion,
    listSite,
    jobQuery,
    jobSearchResult,
    mapState
  };
}
export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    searchJobWithNewBound: (data: ILibQueryPoint[]) => {
      dispatch(
        setJobQuery({
          polygonCoordinates: data
        })
      );
      dispatch(getJob());
    },
    loadListSite: () => {
      dispatch(loadListSite());
    },
    getJob: () => {
      dispatch(getJob());
    },
    saveJob: (data: ISaveJobData, done: (reset: boolean) => void) => {
      dispatch(saveJob(data, done));
    },
    editJobDetail: (
      data: IEditJobData,
      candidates: IJobCandidate[],
      done: (reset: boolean) => void
    ) => {
      dispatch(editJobDetail(data, candidates, done));
    },
    fireError: (msg: string) => {
      common.fireNotification(dispatch)({
        message: msg,
        variant: Variant.ERROR
      });
    },
    handleMainMapState: (mapState: IMapState) => {
      handleMainMapState(dispatch)(mapState);
    }
  };
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(MapBox);
