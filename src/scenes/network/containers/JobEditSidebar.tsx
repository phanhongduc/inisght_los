import { connect, Dispatch } from "react-redux";
import { Omit } from "utility-types";
import { IStoreState } from "../../../types";
import JobEditSidebar, {
  IProps
} from "../components/MapSidebar/JobSidebar/JobEditSidebar";
import { getJobDetail } from "../actions/Job";

function mapStateToProps(
  { jobs }: IStoreState,
  ownProps: Exclude<IProps, "isLoading" | "data">
) {
  const job = jobs.listJob[ownProps.id];
  return {
    isLoading: job ? jobs.jobState[ownProps.id].isFetching : false,
    data: job ? job : undefined
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    getJobDetail: (id: string) => {
      dispatch(getJobDetail(id));
    }
  };
}

export default connect<
  Pick<IProps, "isLoading" | "data">,
  Pick<IProps, "getJobDetail">,
  Omit<IProps, "isLoading" | "data" | "getJobDetail">
>(
  mapStateToProps,
  mapDispatchToProps
)(JobEditSidebar);
