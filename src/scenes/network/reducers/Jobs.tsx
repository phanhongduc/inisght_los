import { combineReducers } from "redux";
import {
  IJobDetail,
  IJobDetailBEnd,
  IJobDetailState,
  IJobInfo,
  IJobLogFile,
  IJobLogResult,
  IMidPoint
} from "../types/Job";
import {
  ISetJobDetail,
  ISetJobDetailBEnd,
  ISetJobDetailState,
  ISetRecentJobs,
  ISetJobDetailMidPoint,
  ISetJobLog,
  ISetJobLogFile
} from "../actions/Job";
import {
  SET_JOB_DETAIL,
  SET_JOB_DETAIL_BEND,
  SET_RECENT_JOBS,
  SET_JOB_DETAIL_MID_POINT
} from "../constants/Job";
import { virtualLos } from "../components/Job/VirtualLOS/reducers";

function listJob(
  state: { [k: string]: IJobDetail } = {},
  action: ISetJobDetail
) {
  switch (action.type) {
    case SET_JOB_DETAIL: {
      return {
        ...state,
        [action.payload.jobId]: action.payload
      };
    }
  }
  return state;
}

function jobState(
  state: {
    [k: string]: IJobDetailState;
  } = {},
  action: ISetJobDetailState
) {
  switch (action.type) {
    case "SET_JOB_DETAIL_STATE": {
      return {
        ...state,
        [action.payload.id]: {
          isFailed: false,
          isFetching: false,
          ...state[action.payload.id],
          ...action.payload.state
        }
      };
    }
  }

  return state;
}

function jobBend(
  state: {
    [k: string]: IJobDetailBEnd[];
  } = {},
  action: ISetJobDetailBEnd
) {
  switch (action.type) {
    case SET_JOB_DETAIL_BEND: {
      return {
        ...state,
        [action.payload.id]: action.payload.data
      };
    }
  }

  return state;
}

function recentJobs(state: IJobInfo[] = [], action: ISetRecentJobs) {
  switch (action.type) {
    case SET_RECENT_JOBS: {
      return action.payload.data;
    }
  }

  return state;
}

function jobMidPoint(
  state: {
    [k: string]: IMidPoint[];
  } = {},
  action: ISetJobDetailMidPoint
) {
  switch (action.type) {
    case SET_JOB_DETAIL_MID_POINT: {
      return {
        ...state,
        [action.payload.id]: action.payload.data
      };
    }
  }
  return state;
}

const defaultJobLogResult: IJobLogResult = {
  lastModified: new Date().getTime(),
  data: [],
  isFailed: false,
  isFetching: false
};

function jobLog(
  state: {
    [k: string]: IJobLogResult;
  } = {},
  action: ISetJobLog
) {
  switch (action.type) {
    case "SET_JOB_LOG": {
      if (state[action.payload.id]) {
        return {
          ...state,
          [action.payload.id]: {
            ...state[action.payload.id],
            ...action.payload.data
          }
        };
      } else {
        return {
          ...state,
          [action.payload.id]: {
            ...defaultJobLogResult,
            ...action.payload.data
          }
        };
      }
    }
  }

  return state;
}

const defaultJobLogFile: IJobLogFile = {
  id: "",
  lastModified: new Date().getTime(),
  data: [],
  isFailed: false,
  isFetching: false,
  url: ""
};

function logs(
  state: {
    [k: string]: IJobLogFile;
  } = {},
  action: ISetJobLogFile
) {
  switch (action.type) {
    case "SET_JOB_LOG_FILE": {
      if (state[action.payload.id]) {
        return {
          ...state,
          [action.payload.id]: {
            ...state[action.payload.id],
            ...action.payload.data
          }
        };
      } else {
        return {
          ...state,
          [action.payload.id]: {
            ...defaultJobLogFile,
            ...action.payload.data,
            id: action.payload.id
          }
        };
      }
    }
  }

  return state;
}

export const jobs = combineReducers({
  listJob,
  jobState,
  jobBend,
  jobMidPoint,
  jobLog,
  virtualLos,
  logs,
  recentJobs
});
