import { combineReducers } from "redux";
import { Feature, Point } from "geojson";
import { ISetListSite, ISetSiteAddress } from "../actions/Site";
import { SET_LIST_SITE, SET_SITE_ADDRESS } from "../../../constants";
import { IProperties } from "../types/Site";

function listSite(
  state: Array<Feature<Point, IProperties>> = [],
  action: ISetListSite
): Array<Feature<Point, IProperties>> {
  switch (action.type) {
    case SET_LIST_SITE:
      return action.payload;
  }
  return state;
}

function siteAddress(
  state: {
    [key: string]: string;
  } = {},
  action: ISetSiteAddress
) {
  switch (action.type) {
    case SET_SITE_ADDRESS:
      return {
        ...state,
        [action.payload.id]: action.payload.address
      };
  }
  return state;
}

export const site = combineReducers({
  listSite,
  siteAddress
});
