import {
  IHandleMainMapData,
  IHandleMainMap,
  IMainMapHover,
  IHandleMainMapState
} from "../actions";
import { UPDATE_MAP_DATA, HANDLE_VIEWPORT } from "../../../constants";
import { DeckGLProperties } from "deck.gl";
import { combineReducers } from "redux";
import { ISite } from "../types/Site";
import IGeoProperties from "../types/GeoProperties";
import { MAIN_MAP_HOVER } from "../../../constants";

import IJsonVersion from "../types/JsonVersion";
import { IUpdateJsonVersion } from "../actions/JsonVersion";
import { UPDATE_VERSION_JSON, HANDLE_MAIN_MAP_STATE } from "../constants";
import IMapState from "../types/MapState";

function siteData(state: ISite[] = [], action: IHandleMainMapData): ISite[] {
  switch (action.type) {
    case UPDATE_MAP_DATA:
      return action.payload;
  }
  return state;
}

const viewport = {
  width: 500,
  height: 500,
  longitude: 153.016,
  latitude: -27.7928,
  zoom: 15,
  pitch: 0,
  bearing: 0
};
function viewState(
  state: DeckGLProperties = viewport,
  action: IHandleMainMap
): DeckGLProperties {
  switch (action.type) {
    case HANDLE_VIEWPORT:
      return action.payload;
  }
  return state;
}

function hoverItem(
  state: IGeoProperties | null = null,
  action: IMainMapHover
): IGeoProperties | null {
  switch (action.type) {
    case MAIN_MAP_HOVER:
      return action.payload;
  }
  return state;
}

const jsonDefault = {
  CreatedDateUtc: "2018-06-29T06:46:42.663",
  PointJsonUrl: "/data/points-23.json",
  CoverageJsonUrl: "/data/coverages-23.json",
  VersionId: "b4987d14-e71f-496e-9e3c-4a5e3451f46a"
};

function jsonVersion(
  state: IJsonVersion = jsonDefault,
  action: IUpdateJsonVersion
): IJsonVersion {
  switch (action.type) {
    case UPDATE_VERSION_JSON:
      if (state.VersionId === action.payload.VersionId) {
        break;
      }
      return action.payload;
  }
  return state;
}

const MAP_STATE_DEFAULT = {
  zoom: 16
};

function mapState(
  state: IMapState = MAP_STATE_DEFAULT,
  action: IHandleMainMapState
): IMapState {
  switch (action.type) {
    case HANDLE_MAIN_MAP_STATE:
      return action.payload;
  }
  return state;
}

export const mainMap = combineReducers({
  siteData,
  viewState,
  hoverItem,
  jsonVersion,
  mapState
});
