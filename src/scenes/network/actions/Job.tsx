import { Action } from "redux";

import {
  CHANGE_JOB_MEDIA_STATUS,
  CREATE_JOB_MID_POINT,
  DELETE_JOB_DETAIL_BEND,
  DELETE_JOB_MID_POINT_MEDIA,
  EDIT_JOB_DETAIL,
  EXPORT_JOB,
  FINISH_JOB,
  GET_JOB_DETAIL,
  GET_JOB_LOG,
  GET_JOB_LOG_FILE,
  GET_RECENT_JOBS,
  SET_JOB_DETAIL,
  SET_JOB_DETAIL_BEND,
  SET_JOB_DETAIL_MID_POINT,
  SET_JOB_DETAIL_STATE,
  SET_JOB_LOG,
  SET_JOB_LOG_FILE,
  SET_RECENT_JOBS
} from "../constants/Job";
import {
  IJobDetail,
  IJobDetailState,
  IJobDetailBEnd,
  IMidPoint,
  IJobInfo,
  MediaStatus,
  IJobLogResult,
  IJobLogFile
} from "../types/Job";
import { IJobCandidate } from "../../lib/types/libs";

export interface IGetJobDetail extends Action<GET_JOB_DETAIL> {
  payload: string;
}

export function getJobDetail(id: string): IGetJobDetail {
  return {
    type: GET_JOB_DETAIL,
    payload: id
  };
}

export interface ISetJobDetail extends Action<SET_JOB_DETAIL> {
  payload: IJobDetail;
}

export function setJobDetail(data: IJobDetail): ISetJobDetail {
  return {
    type: SET_JOB_DETAIL,
    payload: data
  };
}

export interface ISetJobDetailBEnd extends Action<SET_JOB_DETAIL_BEND> {
  payload: {
    id: string;
    data: IJobDetailBEnd[];
  };
}

export function setJobDetailBend(
  id: string,
  data: IJobDetailBEnd[]
): ISetJobDetailBEnd {
  return {
    type: SET_JOB_DETAIL_BEND,
    payload: {
      id,
      data
    }
  };
}

export interface ISetRecentJobs extends Action<SET_RECENT_JOBS> {
  payload: {
    data: IJobInfo[];
  };
}

export function setRecentJobs(data: IJobInfo[]): ISetRecentJobs {
  return {
    type: SET_RECENT_JOBS,
    payload: {
      data
    }
  };
}

export interface IGetRecentJobs extends Action<GET_RECENT_JOBS> {
  payload: string;
}

export function getRecentJobs(id: string): IGetRecentJobs {
  return {
    type: GET_RECENT_JOBS,
    payload: id
  };
}

export interface ISetJobDetailMidPoint
  extends Action<SET_JOB_DETAIL_MID_POINT> {
  payload: {
    id: string;
    data: IMidPoint[];
  };
}

export function setJobDetailMidPoint(
  id: string,
  data: IMidPoint[]
): ISetJobDetailMidPoint {
  return {
    type: SET_JOB_DETAIL_MID_POINT,
    payload: {
      id,
      data
    }
  };
}

export interface ISetJobDetailState extends Action<SET_JOB_DETAIL_STATE> {
  payload: {
    id: string;
    state: Partial<IJobDetailState>;
  };
}

export function setJobDetailState(
  id: string,
  state: Partial<IJobDetailState>
): ISetJobDetailState {
  return {
    type: SET_JOB_DETAIL_STATE,
    payload: {
      id,
      state
    }
  };
}

export interface IEditJobDetail extends Action<EDIT_JOB_DETAIL> {
  payload: {
    data: IEditJobData;
    candidates: IJobCandidate[];
  };
  meta: {
    done: (r?: boolean) => void;
  };
}

export interface IEditJobData {
  id: string;
  jobName?: string;
  recommendHeight?: number;
  frequency?: number;
  engineerId?: string;
}

export function editJobDetail(
  data: IEditJobData,
  candidates: IJobCandidate[],
  done: (r?: boolean) => void
): IEditJobDetail {
  return {
    type: EDIT_JOB_DETAIL,
    payload: {
      data,
      candidates
    },
    meta: {
      done
    }
  };
}

export enum DeleteJobDetailBEndType {
  Media = "media",
  Height = "height"
}

export interface IDeleteJobDetailBEndData {
  jobId: string;
  type: DeleteJobDetailBEndType;
  height: number;
  id: string;
  bendId: string;
}

export interface IDeleteJobDetailBEnd extends Action<DELETE_JOB_DETAIL_BEND> {
  payload: IDeleteJobDetailBEndData;
  meta?: {
    done?: (r?: boolean) => void;
  };
}

export function deleteJobDetailBEnd(
  data: IDeleteJobDetailBEndData,
  done?: (r?: boolean) => void
): IDeleteJobDetailBEnd {
  return {
    type: DELETE_JOB_DETAIL_BEND,
    payload: data,
    meta: {
      done
    }
  };
}

export interface ICreateMidPointData {
  aendSiteId: string;
  aendAddress: string;
  aendLon: number;
  aendLat: number;
  aendHeight: number;
  bendSiteId: string;
  bendAddress: string;
  bendLon: number;
  bendLat: number;
  bendName: string;
  bendHeight: number;
  frequency: number;
  lat: number;
  lon: number;
  height: number;
  terrainJson: string;
}

export interface ICreateMidPoint extends Action<CREATE_JOB_MID_POINT> {
  payload: ICreateMidPointData;
  meta: {
    done: (success: boolean) => void;
  };
}

export function createMidPoint(
  data: ICreateMidPointData,
  done: () => void
): ICreateMidPoint {
  return {
    type: CREATE_JOB_MID_POINT,
    payload: data,
    meta: {
      done
    }
  };
}

export interface IDeleteMidPointMedia
  extends Action<DELETE_JOB_MID_POINT_MEDIA> {
  payload: {
    mediaId: string;
    jobId: string;
  };
  meta: {
    done?: (success: boolean) => void;
  };
}

export function deleteMidPointMedia(
  data: {
    mediaId: string;
    jobId: string;
  },
  done?: (success: boolean) => void
): IDeleteMidPointMedia {
  return {
    type: DELETE_JOB_MID_POINT_MEDIA,
    payload: data,
    meta: {
      done
    }
  };
}

export interface IExportJob extends Action<EXPORT_JOB> {
  payload: {
    id: string;
  };
  meta: {
    done?: (success: boolean, url: string) => void;
  };
}

export function exportJob(
  id: string,
  done?: (success: boolean, url: string) => void
): IExportJob {
  return {
    type: EXPORT_JOB,
    payload: {
      id
    },
    meta: {
      done
    }
  };
}

export interface IFinishJob extends Action<FINISH_JOB> {
  payload: string;
  meta: {
    done?: (success: boolean) => void;
  };
}

export function finishJob(
  id: string,
  done?: (success: boolean) => void
): IFinishJob {
  return {
    type: FINISH_JOB,
    payload: id,
    meta: {
      done
    }
  };
}

export interface IChangeJobMediaStatus extends Action<CHANGE_JOB_MEDIA_STATUS> {
  payload: {
    id: string;
    jobId: string;
    status: MediaStatus;
  };
  meta: {
    done?: (success: boolean) => void;
  };
}

export function changeJobMediaStatus(
  id: string,
  jobId: string,
  status: MediaStatus,
  done?: (success: boolean) => void
): IChangeJobMediaStatus {
  return {
    type: CHANGE_JOB_MEDIA_STATUS,
    payload: {
      id,
      jobId,
      status
    },
    meta: {
      done
    }
  };
}

export interface IGetJobLog extends Action<GET_JOB_LOG> {
  payload: {
    id: string;
    libraryId: string;
  };
  meta: {
    done?: (success: boolean) => void;
  };
}

export function getJobLog(
  id: string,
  libraryId: string,
  done?: (success: boolean) => void
): IGetJobLog {
  return {
    type: "GET_JOB_LOG",
    payload: {
      id,
      libraryId
    },
    meta: {
      done
    }
  };
}

export interface ISetJobLog extends Action<SET_JOB_LOG> {
  payload: {
    id: string;
    data: Partial<IJobLogResult>;
  };
}

export function setJobLog(
  id: string,
  data: Partial<IJobLogResult>
): ISetJobLog {
  return {
    type: "SET_JOB_LOG",
    payload: {
      id,
      data
    }
  };
}

export interface IGetJobLogFile extends Action<GET_JOB_LOG_FILE> {
  payload: {
    id: string;
    url: string;
  };
  meta: {
    done?: (success: boolean) => void;
  };
}

export function getJobLogFile(
  id: string,
  url: string,
  done?: (success: boolean) => void
): IGetJobLogFile {
  return {
    type: "GET_JOB_LOG_FILE",
    payload: {
      id,
      url
    },
    meta: {
      done
    }
  };
}

export interface ISetJobLogFile extends Action<SET_JOB_LOG_FILE> {
  payload: {
    id: string;
    data: Partial<IJobLogFile>;
  };
}

export function setJobLogFile(
  id: string,
  data: Partial<IJobLogFile>
): ISetJobLogFile {
  return {
    type: "SET_JOB_LOG_FILE",
    payload: {
      id,
      data
    }
  };
}
