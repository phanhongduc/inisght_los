import * as constants from "../../../constants";
import { Action } from "redux";
import { Feature, Point } from "geojson";
import { IProperties } from "../types/Site";

export interface ILoadListSite extends Action<constants.LOAD_LIST_SITE> {}

export function loadListSite(): ILoadListSite {
  return {
    type: constants.LOAD_LIST_SITE
  };
}

export interface ISetListSite extends Action<constants.SET_LIST_SITE> {
  payload: Array<Feature<Point, IProperties>>;
}

export function setListSite(
  data: Array<Feature<Point, IProperties>>
): ISetListSite {
  return {
    payload: data,
    type: constants.SET_LIST_SITE
  };
}

export interface ISetSiteAddress extends Action<constants.SET_SITE_ADDRESS> {
  payload: ISetSiteAddressData;
}

export interface IGetSiteAddress extends Action<constants.GET_SITE_ADDRESS> {
  payload: IGetSiteAddressData;
}

export interface ISetSiteAddressData {
  id: string;
  address: string;
}

export interface IGetSiteAddressData {
  id: string;
  lat: number;
  lon: number;
}

export function setSiteAddress(data: ISetSiteAddressData): ISetSiteAddress {
  return {
    payload: data,
    type: constants.SET_SITE_ADDRESS
  };
}

export function getSiteAddress(data: IGetSiteAddressData): IGetSiteAddress {
  return {
    payload: data,
    type: constants.GET_SITE_ADDRESS
  };
}
