import * as React from "react";
import * as ReactDOM from "react-dom";
import "../styles/mapbox.css";
import "mapbox-gl/dist/mapbox-gl.css";
import * as mapboxgl from "mapbox-gl";
import { LngLat } from "mapbox-gl";
// import * as turf from "@turf/turf";
import "@mapbox/mapbox-gl-draw/dist/mapbox-gl-draw.css";
import { Feature, Point } from "geojson";
import SearchLocation from "src/components/SearchLocation";
import JobSidebar from "./MapSidebar/JobSidebar";
import JobEditSidebar from "./../containers/JobEditSidebar";
import JobSearchSidebar from "../containers/JobSearchSidebar";
import { IProperties } from "../types/Site";
import * as kdbush from "kdbush";
import * as geokdbush from "geokdbush";
// import IStyleProps from "../../../styles/utils";
import ContextMenu, { ContextMenuList } from "./ContextMenu";
import ContextMenuJob, { ContextMenuListJob } from "./ContextMenuJob";
import {
  IJob,
  IJobCandidate,
  ILib,
  ILibJobQuery,
  ILibQueryPoint,
  ILibUser,
  ISaveJobData,
  ISearchJobResult,
  JobStatus,
  LibType
} from "../../lib/types/libs";
import { RouteComponentProps, withRouter } from "react-router-dom";
import paths from "../../../paths";
import { IEditJobData } from "../actions/Job";
import IMapState from "../types/MapState";
import { GeoCodingService } from "../../../helpers/GeoCodingService";
import ConfirmDialog from "../../../components/Dialog/Confirm";
import ContextMenuSite, { ContextMenuListSite } from "./ContextMenuSite";
import links from "src/links";

// worker.
interface IProps {
  currentLib: ILib;
  editJobDetail: (
    data: IEditJobData,
    candidates: IJobCandidate[],
    done: (reset: boolean) => void
  ) => void;
  searchJobWithNewBound: (data: ILibQueryPoint[]) => void;
  jobQuery: ILibJobQuery;
  jobSearchResult: ISearchJobResult;
  saveJob: (data: ISaveJobData, done: (reset: boolean) => void) => void;
  fireError: (msg: string) => void;
  libUsers: ILibUser[];
  listSite: Array<Feature<Point, IProperties>>;
  loadListSite: () => void;
  getJob: () => void;
  mapState: IMapState;
  handleMainMapState: (mapState: IMapState) => void;
}

interface IPoint {
  lng: number;
  lat: number;
}

interface IJobMap extends IJob {
  inSite: number;
  siteId?: string;
}

interface IState {
  isOpenLimitCreateJob: boolean;
  isOpenDrawResult: boolean;
  isLoadedMap: boolean;
  isLoadedMapDataOnce: boolean;
  isOpenSearchSidebar: boolean;
  isPopupHover: boolean;
  lat: number;
  lng: number;
  zoom: number;
  drawPolygonResults: {
    result: string[];
  } | null;
  step: number;
  contextMenuPointId: string;
  contextMenuSource: string;
  portalContextMenu: HTMLElement | null;
  popupContextMenu: mapboxgl.Popup | null;
  contextMenuCoordinate: number[];
  isOpenCreateJob: boolean;
  jobCreateData: {
    checkedSites: INearestPointInfo[];
    selectedSite: INearestPointInfo | null;
    location: number[];
    address: string;
  };
  isOpenEditJob: boolean;
  jobEditData: {
    id: string;
    selectedSite: INearestPointInfo | null;
    nearbySites: INearestPointInfo[];
  };
  nearbySites: INearestPointInfo[];
}

export interface INearestPointInfo {
  name: string;
  id: string;
  lng: number;
  lat: number;
  distance: number;
}

export interface IRouteProps extends RouteComponentProps<any>, IProps {}

class MapBox extends React.Component<IProps & IRouteProps, IState> {
  private mapContainer: any;
  private ismounted: boolean;
  private map: mapboxgl.Map;
  private popup: mapboxgl.Popup;

  constructor(props: IProps & IRouteProps) {
    super(props);
    const { loadListSite } = this.props;
    this.mapContainer = React.createRef();
    this.ismounted = true;
    this.state = {
      isOpenLimitCreateJob: false,
      isOpenEditJob: false,
      jobEditData: {
        id: "",
        selectedSite: null,
        nearbySites: []
      },
      jobCreateData: {
        selectedSite: null,
        address: "",
        location: [],
        checkedSites: [] as INearestPointInfo[]
      },
      nearbySites: [] as INearestPointInfo[],
      contextMenuCoordinate: [],
      isOpenCreateJob: false,
      contextMenuPointId: "",
      contextMenuSource: "default",
      portalContextMenu: null,
      popupContextMenu: null,
      isPopupHover: false,
      isOpenSearchSidebar: false,
      isOpenDrawResult: false,
      drawPolygonResults: null,
      isLoadedMap: false,
      isLoadedMapDataOnce: false,
      lng: props.mapState.lng ? props.mapState.lng : 153.031904,
      lat: props.mapState.lat ? props.mapState.lat : -27.610364,
      zoom: props.mapState.zoom,
      step: 0
    };
    loadListSite();
  }

  public componentWillUnmount() {
    this.ismounted = false;
    // this.props.setSearchMode({
    //   active: false,
    //   mode: SearchMode.CCTV
    // });
  }

  public componentWillReceiveProps(nextProps: IProps) {
    if (this.state.isLoadedMap && nextProps.listSite.length > 0) {
      (this.map.getSource("sites") as mapboxgl.GeoJSONSource).setData({
        type: "FeatureCollection",
        features: nextProps.listSite
      });
    }
    const { mapState: newMapState } = nextProps;
    const { mapState } = this.props;
    if (
      newMapState &&
      mapState &&
      newMapState.lat !== mapState.lat &&
      newMapState.lng !== mapState.lng
    ) {
      if (newMapState.lat && newMapState.lng) {
        const newPoint = new LngLat(newMapState.lng, newMapState.lat);
        this.map.setCenter(newPoint);
      }
    }
    if (
      this.state.isLoadedMap &&
      nextProps.jobSearchResult.lastUpdated !==
        this.props.jobSearchResult.lastUpdated
    ) {
      const src = this.map.getSource("jobs");
      if (src) {
        (this.map.getSource("jobs-finish") as mapboxgl.GeoJSONSource).setData({
          type: "FeatureCollection",
          features: nextProps.jobSearchResult.jobs
            .filter(j => j.jobStatus === JobStatus.Finished)
            .map(this.getJobSrc)
        });

        (this.map.getSource("jobs-process") as mapboxgl.GeoJSONSource).setData({
          type: "FeatureCollection",
          features: nextProps.jobSearchResult.jobs
            .filter(j => j.jobStatus === JobStatus.Processing)
            .map(this.getJobSrc)
        });

        (this.map.getSource("jobs") as mapboxgl.GeoJSONSource).setData({
          type: "FeatureCollection",
          features: nextProps.jobSearchResult.jobs
            .filter(j => j.jobStatus === JobStatus.Created)
            .map(this.getJobSrc)
        });
      }
    }
  }

  public componentDidMount() {
    const { lng, lat, zoom } = this.state;

    this.popup = new mapboxgl.Popup({
      closeButton: false,
      closeOnClick: false
    });
    this.map = new mapboxgl.Map({
      container: this.mapContainer.current,
      style: "/3d_building_style.json",
      center: [lng, lat],
      zoom
    });

    this.map.addControl(new mapboxgl.NavigationControl(), "top-left");

    this.map.on("load", () => {
      if (this.ismounted) {
        this.setState({
          isLoadedMap: true
        });
      }

      this.map.addSource("measure-lines", {
        type: "geojson",
        data: {
          type: "FeatureCollection",
          features: []
        }
      });

      this.map.addLayer({
        id: "measure-lines",
        type: "line",
        source: "measure-lines",
        layout: {
          "line-cap": "round",
          "line-join": "round"
        },
        paint: {
          "line-color": "#e25144",
          "line-width": 2.5,
          "line-opacity": 0.5
        },
        filter: ["in", "$type", "LineString"]
      });

      this.map.loadImage(
        require("src/assets/images/marker/location.png"),
        (error: any, image: HTMLImageElement) => {
          this.map.addImage("location", image);
          this.map.addSource("location", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: []
            }
          });

          this.map.addLayer({
            id: "location",
            type: "symbol",
            source: "location",
            layout: {
              "icon-image": "location",
              "icon-size": 1,
              "icon-allow-overlap": true,
              "icon-rotation-alignment": "map"
            }
          });
        }
      );

      this.map.loadImage(
        require("src/assets/images/marker/ic_job_process.png"),
        (error: any, image: HTMLImageElement) => {
          this.map.addImage("job-process", image);

          const featuresJobs =
            this.props.jobSearchResult.jobs.length > 0
              ? this.props.jobSearchResult.jobs
                  .filter(j => j.jobStatus === JobStatus.Processing)
                  .map(this.getJobSrc)
              : [];

          this.map.addSource("jobs-process", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: featuresJobs as Array<Feature<Point, IJob>>
            }
          });

          this.map.addLayer({
            id: "jobs-process",
            type: "symbol",
            source: "jobs-process",
            layout: {
              "icon-offset": {
                property: "inSite",
                type: "interval",
                stops: [[0, [0, 0]], [1, [10, 15]]]
              },
              "icon-image": "job-process",
              "icon-size": {
                property: "inSite",
                type: "interval",
                stops: [[0, 1], [1, 3 / 4]]
              },
              "icon-allow-overlap": true,
              "icon-rotation-alignment": "map"
            }
          });

          this.map.on("mouseenter", "jobs-process", this.onHoverJobs);

          this.map.on("mouseleave", "jobs-process", this.onLeavePoints);
        }
      );

      this.map.loadImage(
        require("src/assets/images/marker/ic_job_finish.png"),
        (error: any, image: HTMLImageElement) => {
          this.map.addImage("job-finish", image);

          const featuresJobs =
            this.props.jobSearchResult.jobs.length > 0
              ? this.props.jobSearchResult.jobs
                  .filter(j => j.jobStatus === JobStatus.Finished)
                  .map(this.getJobSrc)
              : [];

          this.map.addSource("jobs-finish", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: featuresJobs as Array<Feature<Point, IJob>>
            }
          });

          this.map.addLayer({
            id: "jobs-finish",
            type: "symbol",
            source: "jobs-finish",
            layout: {
              "icon-offset": {
                property: "inSite",
                type: "interval",
                stops: [[0, [0, 0]], [1, [10, 15]]]
              },
              "icon-image": "job-finish",
              "icon-size": {
                property: "inSite",
                type: "interval",
                stops: [[0, 1], [1, 3 / 4]]
              },
              "icon-allow-overlap": true,
              "icon-rotation-alignment": "map"
            }
          });

          this.map.on("mouseenter", "jobs-finish", this.onHoverJobs);

          this.map.on("mouseleave", "jobs-finish", this.onLeavePoints);
        }
      );

      this.map.loadImage(
        require("src/assets/images/marker/ic_job.png"),
        (error: any, image: HTMLImageElement) => {
          this.map.addImage("job", image);

          this.map.addSource("job", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: []
            }
          });

          this.map.addLayer({
            id: "job",
            type: "symbol",
            source: "job",
            layout: {
              "icon-image": "job",
              "icon-offset": {
                property: "inSite",
                type: "interval",
                stops: [[0, [0, 0]], [1, [10, 15]]]
              },
              "icon-size": {
                property: "inSite",
                type: "interval",
                stops: [[0, 1], [1, 3 / 4]]
              },
              "icon-allow-overlap": true,
              "icon-rotation-alignment": "map"
            }
          });

          const featuresJobs =
            this.props.jobSearchResult.jobs.length > 0
              ? (this.props.jobSearchResult.jobs
                  .filter(j => j.jobStatus === JobStatus.Created)
                  .map(this.getJobSrc) as Array<Feature<Point, IJobMap>>)
              : [];

          this.map.addSource("jobs", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: featuresJobs as Array<Feature<Point, IJobMap>>
            }
          });

          this.map.addLayer({
            id: "jobs",
            type: "symbol",
            source: "jobs",
            layout: {
              "icon-offset": {
                property: "inSite",
                type: "interval",
                stops: [[0, [0, 0]], [1, [10, 15]]]
              },
              "icon-image": "job",
              "icon-size": {
                property: "inSite",
                type: "interval",
                stops: [[0, 1], [1, 3 / 4]]
              },
              "icon-allow-overlap": true,
              "icon-rotation-alignment": "map"
            }
          });
        }
      );

      this.map.addSource("sites", {
        type: "geojson",
        // data:
        //   "https://insightustv.blob.core.windows.net/sitejson/sites-ver-60.json",
        data: {
          type: "FeatureCollection",
          features: this.props.listSite.length > 0 ? this.props.listSite : []
        },
        cluster: true,
        clusterMaxZoom: 14, // Max zoom to cluster points on
        clusterRadius: 50
      });

      this.map.addLayer({
        id: "sites",
        type: "circle",
        source: "sites",
        paint: {
          "circle-color": [
            "step",
            ["get", "point_count"],
            "#51bbd6",
            100,
            "#f1f075",
            750,
            "#f28cb1"
          ],
          "circle-radius": [
            "step",
            ["get", "point_count"],
            20,
            100,
            30,
            750,
            40
          ]
        }
      });

      this.map.addLayer({
        id: "cluster-count",
        type: "symbol",
        source: "sites",
        filter: ["has", "point_count"],
        layout: {
          "text-field": "{point_count_abbreviated}",
          "text-font": ["Klokantech Noto Sans Bold"],
          "text-size": 12
        }
      });

      this.map.addLayer({
        id: "unclustered-point",
        type: "circle",
        source: "sites",
        filter: ["!has", "point_count"],
        paint: {
          "circle-color": "#68ed4b",
          "circle-radius": 15,
          "circle-stroke-color": "#fff"
        }
      });

      this.map.on("contextmenu", this.onContextMenu);

      this.map.on("mouseenter", "unclustered-point", this.onHoverPoints);

      this.map.on("mouseleave", "unclustered-point", this.onLeavePoints);

      this.map.on("mouseenter", "jobs", this.onHoverJobs);

      this.map.on("mouseleave", "jobs", this.onLeavePoints);

      this.map.on("mousedown", "unclustered-point", this.onMouseDownPoints);

      this.map.on("moveend", this.reCheckResultData);
      this.map.on("moveend", this.updateMapState);
    });
  }

  public componentDidUpdate(prevProps: IProps) {
    if (prevProps.currentLib.id !== this.props.currentLib.id) {
      this.props.loadListSite();
    }
  }

  private updateMapState = (
    evt:
      | mapboxgl.MapMouseEvent
      | mapboxgl.MapTouchEvent
      | mapboxgl.MapWheelEvent
  ) => {
    const map: mapboxgl.Map =
      (evt as mapboxgl.MapTouchEvent | mapboxgl.MapWheelEvent).map ||
      (evt as mapboxgl.MapMouseEvent).target;

    const center = map.getCenter();
    const zoom = map.getZoom();

    this.props.handleMainMapState({
      lat: center.lat,
      lng: center.lng,
      zoom
    });
  };

  private getJobSrc = (j: IJob): Feature<Point, IJobMap> => {
    const a = this.props.listSite.find(
      s =>
        s.geometry.coordinates[0] === j.lon &&
        s.geometry.coordinates[1] === j.lat
    );
    return {
      type: "Feature",
      geometry: {
        coordinates: [j.lon, j.lat],
        type: "Point"
      },
      properties: {
        ...j,
        inSite: a ? 1 : 0,
        siteId: a ? a.properties.id : undefined
      }
    };
  };

  private onMouseDownPoints = (e: any) => {
    console.log("aaa");
  };

  public getListPointNearest = (point: IPoint) => {
    return new Promise(resolve => {
      const listSite = this.props.listSite;
      const points: number[][] = listSite.map((el: Feature<Point>) => {
        return el.geometry.coordinates;
      });
      const index = kdbush(points);
      const nearest = geokdbush.around(
        index,
        point.lng,
        point.lat,
        Infinity,
        50
      );
      const result: INearestPointInfo[] = [];
      nearest.forEach((el: any) => {
        console.time("xx");
        const existedSite: INearestPointInfo | undefined = result.find(
          (siteEl: INearestPointInfo) => {
            return siteEl.lng === el[0] && siteEl.lat === el[1];
          }
        );
        const site: Feature<Point, IProperties> | undefined = listSite.find(
          (siteEl: Feature<Point, IProperties>) => {
            if (existedSite) {
              return (
                siteEl.geometry.coordinates[0] === el[0] &&
                siteEl.geometry.coordinates[1] === el[1] &&
                siteEl.properties.id !== existedSite.id
              );
            }
            return (
              siteEl.geometry.coordinates[0] === el[0] &&
              siteEl.geometry.coordinates[1] === el[1]
            );
          }
        );
        if (site) {
          const nearestSite: INearestPointInfo = {
            id: site.properties.id,
            name: site.properties.siteName,
            lng: site.geometry.coordinates[0],
            lat: site.geometry.coordinates[1],
            distance: geokdbush.distance(point.lng, point.lat, el[0], el[1])
          };
          result.push(nearestSite);
        }
      });
      console.timeEnd("xx");
      resolve(result);
    });
  };

  private hoverSite = (
    prop: IProperties,
    location: number[],
    mapLocation: number[],
    jobName?: string
  ) => {
    this.map.getCanvas().style.cursor = "pointer";

    const dest = `
        <table>
            <tr><td>Name</td><td>${prop.siteName}</td><tr>
            <tr><td>Id</td><td>${prop.id}</td><tr>
            ${jobName ? `<tr><td>Job name</td><td>${jobName}</td><tr>` : ""}
            <tr><td>Longitude</td><td>${location[0].toFixed(6)}</td><tr>
            <tr><td>Latitude</td><td>${location[1].toFixed(6)}</td><tr>
        </table>
      `;

    // Ensure that if the map is zoomed out such that multiple
    // copies of the feature are visible, the popup appears
    // over the copy being pointed to.
    while (Math.abs(mapLocation[0] - location[0]) > 180) {
      location[0] += mapLocation[0] > location[0] ? 360 : -360;
    }

    // Populate the popup and set its coordinates
    // based on the feature found.
    this.popup
      .setLngLat(location)
      .setHTML(dest)
      .addTo(this.map);
  };

  private onHoverPoints = (e: any) => {
    if (e.features.length > 0) {
      // Change the cursor style as a UI indicator.
      this.map.getCanvas().style.cursor = "pointer";

      const feature = e.features[0] as Feature<Point>;
      const prop = feature.properties as IProperties;
      const location = e.features[0].geometry.coordinates as number[];

      const job = this.props.jobSearchResult.jobs.find(j => {
        return (
          j.lon === Number(feature.geometry.coordinates[0].toFixed(6)) &&
          j.lat === Number(feature.geometry.coordinates[1].toFixed(6))
        );
      });

      this.hoverSite(
        prop,
        location,
        [e.lngLat.lng, e.lngLat.lat],
        job ? job.name : ""
      );
    }
  };

  private onHoverJobs = (e: any) => {
    if (e.features.length > 0) {
      // Change the cursor style as a UI indicator.
      this.map.getCanvas().style.cursor = "pointer";

      const coordinates = e.features[0].geometry.coordinates.slice();
      const prop = e.features[0].properties as IJobMap;

      if (prop.inSite) {
        const site = this.props.listSite.find(
          s => s.properties.id === prop.siteId
        );
        if (site) {
          this.hoverSite(
            site.properties as IProperties,
            site.geometry.coordinates,
            [e.lngLat.lng, e.lngLat.lat],
            prop.name
          );
        }
      } else {
        const dest = `
        <table>
            <tr><td>${prop.name}</td><tr>
        </table>
      `;

        // Ensure that if the map is zoomed out such that multiple
        // copies of the feature are visible, the popup appears
        // over the copy being pointed to.
        while (Math.abs(e.lngLat.lng - coordinates[0]) > 180) {
          coordinates[0] += e.lngLat.lng > coordinates[0] ? 360 : -360;
        }

        // Populate the popup and set its coordinates
        // based on the feature found.
        this.popup
          .setLngLat(coordinates)
          .setHTML(dest)
          .addTo(this.map);
      }
    }
  };

  private onContextMenu = (e: any) => {
    const features = this.map.queryRenderedFeatures(e.point) as Array<
      Feature<Point, IProperties>
    >;
    const div = window.document.createElement("div");
    this.setState({
      lat: e.lngLat.lat,
      lng: e.lngLat.lng
    });
    div.className = "popup-ul";

    let contextMenuCoordinate = [e.lngLat.lng, e.lngLat.lat];
    let contextMenuPointId = features[0] ? features[0].properties.id : "";
    let contextMenuSource = features[0]
      ? (features[0] as any).source
      : "default";

    if (this.state.popupContextMenu) {
      this.state.popupContextMenu.remove();
    }
    if (features.length === 0 || (features[0] as any).source) {
      if (features.length !== 0 && (features[0] as any).source === "sites") {
        const site = this.props.listSite.find(
          s => s.properties.id === features[0].properties.id
        );

        if (site) {
          contextMenuPointId = site.properties.id;
          contextMenuCoordinate = site.geometry.coordinates;
          const job = this.props.jobSearchResult.jobs.find(
            j =>
              j.lon === Number(contextMenuCoordinate[0].toFixed(6)) &&
              j.lat === Number(contextMenuCoordinate[1].toFixed(6))
          );

          if (job) {
            contextMenuPointId = job.id;
            contextMenuSource = "jobs";
          }
        }
      }
      const popup = new mapboxgl.Popup({
        closeButton: false,
        anchor: "left"
      })
        .setLngLat(e.lngLat)
        .setDOMContent(div)
        .addTo(this.map);

      this.setState({
        contextMenuPointId,
        contextMenuSource,
        portalContextMenu: div,
        popupContextMenu: popup,
        contextMenuCoordinate
      });

      popup.on("close", () => {
        this.setState({
          contextMenuPointId: "",
          portalContextMenu: null,
          popupContextMenu: null,
          contextMenuSource: "default"
        });
      });
    }
  };

  private onClickMenuJob = (menu: ContextMenuListJob) => {
    return () => {
      switch (menu) {
        case ContextMenuListJob.VIEW_JOB: {
          this.props.history.push(
            paths.jobDetail.replace(":id", this.state.contextMenuPointId)
          );

          break;
        }
        case ContextMenuListJob.EDIT_JOB: {
          this.onClickEditJob();
          break;
        }
      }
    };
  };

  private onClickMenuSite = (menu: ContextMenuListSite) => {
    return () => {
      switch (menu) {
        case ContextMenuListSite.CREATE_JOB: {
          this.newJob(this.state.contextMenuPointId);
          break;
        }
      }
      if (this.state.popupContextMenu) {
        this.state.popupContextMenu.remove();
      }
    };
  };

  private onClickEditJob = () => {
    const job = this.props.jobSearchResult.jobs.find(
      j => j.id === this.state.contextMenuPointId
    );
    if (job) {
      this.removeJobOnMap();
      this.removeLineOnMap();

      this.getListPointNearest({
        lng: job.lon,
        lat: job.lat
      }).then((data: INearestPointInfo[]) => {
        if (this.state.popupContextMenu) {
          this.setState(
            {
              isOpenEditJob: true,
              jobEditData: {
                ...this.state.jobEditData,
                nearbySites: data,
                id: this.state.contextMenuPointId
              }
            },
            () => {
              if (this.state.popupContextMenu) {
                this.state.popupContextMenu.remove();
              }
            }
          );
        }
      });
    }
  };

  private newJob = (siteId?: string) => {
    if (
      this.props.currentLib.type === LibType.FreeTrial &&
      this.props.jobSearchResult.total >= 2
    ) {
      this.setState({
        isOpenLimitCreateJob: true
      });
    } else {
      this.removeJobOnMap();
      this.removeLineOnMap();
      this.addJobToMap(
        this.state.contextMenuCoordinate[0],
        this.state.contextMenuCoordinate[1],
        !!siteId
      );
      const geoCoder = new GeoCodingService();
      geoCoder
        .reverseGeocode({
          lat: this.state.contextMenuCoordinate[1],
          lng: this.state.contextMenuCoordinate[0]
        })
        .then(([result]) => {
          if (this.state.isOpenCreateJob) {
            this.setState({
              jobCreateData: {
                ...this.state.jobCreateData,
                address: result.formatted_address,
                checkedSites: [] as INearestPointInfo[]
              }
            });
          }
        });

      this.setState(
        {
          isOpenCreateJob: true,
          jobCreateData: {
            ...this.state.jobCreateData,
            location: [
              this.state.contextMenuCoordinate[0],
              this.state.contextMenuCoordinate[1]
            ],
            selectedSite: null,
            checkedSites: [] as INearestPointInfo[]
          }
        },
        () => {
          this.getListPointNearest({
            lng: this.state.contextMenuCoordinate[0],
            lat: this.state.contextMenuCoordinate[1]
          }).then((data: INearestPointInfo[]) => {
            if (this.state.isOpenCreateJob) {
              this.setState({
                nearbySites: data
              });
            }
          });
        }
      );
    }
  };

  private onClickMenu = (menu: ContextMenuList) => {
    return () => {
      switch (menu) {
        case ContextMenuList.NEW_JOB: {
          this.newJob();
          break;
        }
      }
      if (this.state.popupContextMenu) {
        this.state.popupContextMenu.remove();
      }
    };
  };

  private onLeavePoints = () => {
    this.map.getCanvas().style.cursor = "";
    this.popup.remove();
  };

  private onClickFindPlace = (lng: number, lat: number) => {
    this.map.setCenter([lng, lat]);
    if (this.map.getSource("location") && this.state.isLoadedMap) {
      (this.map.getSource("location") as mapboxgl.GeoJSONSource).setData({
        type: "FeatureCollection",
        features: [
          {
            type: "Feature",
            geometry: {
              type: "Point",
              coordinates: [lng, lat]
            },
            properties: {}
          }
        ]
      });
    }
  };

  private addJobToMap = (lng: number, lat: number, inSite: boolean) => {
    if (this.map.getSource("job") && this.state.isLoadedMap) {
      (this.map.getSource("job") as mapboxgl.GeoJSONSource).setData({
        type: "FeatureCollection",
        features: [
          {
            type: "Feature",
            geometry: {
              type: "Point",
              coordinates: [lng, lat]
            },
            properties: {
              inSite: inSite ? 1 : 0
            }
          }
        ]
      });
    }
  };

  private removeJobOnMap = () => {
    if (this.map.getSource("job") && this.state.isLoadedMap) {
      (this.map.getSource("job") as mapboxgl.GeoJSONSource).setData({
        type: "FeatureCollection",
        features: []
      });
    }
  };

  private addLineToMap = (
    lng1: number,
    lat1: number,
    checkedSitesApply?: INearestPointInfo[]
  ) => {
    if (this.map.getSource("measure-lines") && this.state.isLoadedMap) {
      const sites = checkedSitesApply
        ? checkedSitesApply
        : this.state.jobCreateData.checkedSites;
      const data = sites.reduce(
        (current, site) => {
          current.push({
            type: "Feature",
            geometry: {
              type: "LineString",
              coordinates: [[lng1, lat1], [site.lng, site.lat]]
            },
            properties: {}
          });
          return current;
        },
        [] as Feature[]
      );

      // if (selectedSite) {
      //   const site = selectedSite;
      //   if (site) {
      //     data.push({
      //       type: "Feature",
      //       geometry: {
      //         type: "LineString",
      //         coordinates: [[lng1, lat1], [site.lng, site.lat]]
      //       },
      //       properties: {}
      //     });
      //   }
      // }

      (this.map.getSource("measure-lines") as mapboxgl.GeoJSONSource).setData({
        type: "FeatureCollection",
        features: data
      });
    }
  };

  private removeLineOnMap = () => {
    if (this.map.getSource("measure-lines") && this.state.isLoadedMap) {
      (this.map.getSource("measure-lines") as mapboxgl.GeoJSONSource).setData({
        type: "FeatureCollection",
        features: []
      });
    }
  };

  private onClearPlace = () => {
    if (this.map.getSource("location") && this.state.isLoadedMap) {
      (this.map.getSource("location") as mapboxgl.GeoJSONSource).setData({
        type: "FeatureCollection",
        features: []
      });
    }
  };

  private toggleSearchSidebar = () => {
    if (this.state.isLoadedMap) {
      this.setState({
        isOpenSearchSidebar: !this.state.isOpenSearchSidebar
      });
    }
  };

  private onCloseJobSidebar = () => {
    this.removeJobOnMap();
    this.removeLineOnMap();
    this.setState({
      isOpenCreateJob: false,
      jobCreateData: {
        selectedSite: null,
        address: "",
        location: [],
        checkedSites: [] as INearestPointInfo[]
      },
      nearbySites: [] as INearestPointInfo[]
    });
  };

  private onCloseJobEditSidebar = () => {
    this.removeJobOnMap();
    this.removeLineOnMap();
    this.setState({
      isOpenEditJob: false,
      jobEditData: {
        id: "",
        selectedSite: null,
        nearbySites: [] as INearestPointInfo[]
      }
    });
  };

  private onChangeSiteJob = (id: string) => {
    return (e: React.SyntheticEvent, checked: boolean) => {
      e.stopPropagation();
      const site = this.state.nearbySites.find(s => s.id === id);
      if (site) {
        if (!checked) {
          this.setState(
            {
              jobCreateData: {
                ...this.state.jobCreateData,
                checkedSites: this.state.jobCreateData.checkedSites.filter(
                  s => s.id !== site.id
                )
              }
            },
            () => {
              this.addLineToMap(
                this.state.jobCreateData.location[0],
                this.state.jobCreateData.location[1]
              );
            }
          );
        } else {
          this.setState(
            {
              jobCreateData: {
                ...this.state.jobCreateData,
                checkedSites: this.state.jobCreateData.checkedSites.concat([
                  site
                ]),
                selectedSite: site
              }
            },
            () => {
              this.addLineToMap(
                this.state.jobCreateData.location[0],
                this.state.jobCreateData.location[1]
              );
            }
          );
        }
      }
    };
  };

  private onChangeSiteEditJob = (
    lon: number,
    lat: number,
    checkedSites: INearestPointInfo[]
  ) => {
    this.removeLineOnMap();
    this.addLineToMap(lon, lat, checkedSites);
  };

  private onSelectSiteJob = (id: string) => {
    return (e: React.SyntheticEvent) => {
      const target = e.target as HTMLInputElement;
      if (target.type !== "checkbox") {
        const site = this.state.nearbySites.find(s => s.id === id);
        if (site) {
          this.setState(
            {
              jobCreateData: {
                ...this.state.jobCreateData,
                selectedSite: site
              }
            },
            () => {
              this.addLineToMap(
                this.state.jobCreateData.location[0],
                this.state.jobCreateData.location[1]
              );
            }
          );
        }
      }
    };
  };

  private onSubmitJob = (inputs: any, done: (r?: boolean) => void) => {
    if (this.state.jobCreateData.checkedSites.length === 0) {
      this.props.fireError("New job need at least one selected site");
      done();
    } else {
      this.props.saveJob(
        {
          jobName: inputs.name.value,
          address: this.state.jobCreateData.address,
          lon: this.state.jobCreateData.location[0],
          lat: this.state.jobCreateData.location[1],
          recommendHeight: inputs.height.value,
          frequency: inputs.frequency.value * 1000000,
          engineerId: inputs.engineer.value,
          candidates: this.state.jobCreateData.checkedSites.map(s => ({
            bendId: s.id,
            bendSiteName: s.name,
            lon: s.lng,
            lat: s.lat,
            displayOrder: 20,
            recommendHeight: 20
          }))
        },
        (r?: boolean) => {
          done(r);
          if (r) {
            this.props.getJob();
            this.onCloseJobSidebar();
          }
        }
      );
    }
  };
  private onSubmitEditJob = (
    inputs: {
      id: string;
      jobName: string;
      recommendHeight: number;
      frequency: number;
      engineerId: string;
    },
    candidates: IJobCandidate[],
    done: (r?: boolean) => void
  ) => {
    if (candidates.length === 0) {
      this.props.fireError("Job need at least one selected site");
      done();
    } else {
      this.props.editJobDetail(inputs, candidates, done);
    }
  };

  private getMapBoundCoor = () => {
    const canvas = this.map.getCanvas();
    const w = canvas.width;
    const h = canvas.height;
    const cUL = this.map.unproject([0, 0]).toArray();
    const cUR = this.map.unproject([w, 0]).toArray();
    const cLR = this.map.unproject([w, h]).toArray();
    const cLL = this.map.unproject([0, h]).toArray();
    return [[cUL, cUR, cLR, cLL, cUL]];
  };

  private onSelectJob = (id: string) => {
    const selectedJob = this.props.jobSearchResult.jobs.find(j => j.id === id);
    if (selectedJob) {
      this.map.setCenter([selectedJob.lon, selectedJob.lat]);
    }
  };

  private onViewDetailJob = (id: string) => {
    this.props.history.push(paths.jobDetail.replace(":id", id));
  };

  private reCheckResultData = () => {
    if (
      this.props.jobQuery.polygonCoordinates.length > 0 &&
      this.state.isOpenSearchSidebar
    ) {
      let isDiff = false;
      const bound = this.getMapBoundCoor()[0];
      this.props.jobQuery.polygonCoordinates.forEach((c, index) => {
        if (c.lon !== bound[index][0] || c.lat !== bound[index][1]) {
          isDiff = true;
        }
      });
      if (isDiff) {
        this.props.searchJobWithNewBound(
          bound.map(data => {
            return {
              lon: data[0],
              lat: data[1]
            };
          })
        );
      }
    }
  };

  private closeConfirmLimitCreateJob = (upgrade: boolean) => {
    if (upgrade) {
      const win = window.open(links.upgradePlan, "_blank");
      if (win) {
        win.focus();
      }
    }
    this.setState({
      isOpenLimitCreateJob: false
    });
  };

  public render() {
    const site = this.state.jobCreateData.selectedSite;
    return (
      <div className="map">
        <div className="map-inner" ref={this.mapContainer} />
        {this.state.portalContextMenu &&
          ["default", "openmaptiles"].indexOf(this.state.contextMenuSource) >=
            0 &&
          ReactDOM.createPortal(
            <ContextMenu onClickMenu={this.onClickMenu} />,
            this.state.portalContextMenu
          )}
        {this.state.portalContextMenu &&
          ["jobs", "jobs-process", "jobs-finish"].indexOf(
            this.state.contextMenuSource
          ) >= 0 &&
          ReactDOM.createPortal(
            <ContextMenuJob
              src={this.state.contextMenuSource}
              onClickMenu={this.onClickMenuJob}
            />,
            this.state.portalContextMenu
          )}

        {this.state.portalContextMenu &&
          ["sites"].indexOf(this.state.contextMenuSource) >= 0 &&
          ReactDOM.createPortal(
            <ContextMenuSite
              src={this.state.contextMenuSource}
              onClickMenu={this.onClickMenuSite}
            />,
            this.state.portalContextMenu
          )}
        <SearchLocation
          onClickResult={this.onClickFindPlace}
          onClear={this.onClearPlace}
          lat={this.state.lat}
          lng={this.state.lng}
          zoom={this.state.zoom}
        />
        <JobSearchSidebar
          onViewDetailJob={this.onViewDetailJob}
          onSelectJob={this.onSelectJob}
          getMapBound={this.getMapBoundCoor}
          onClickToggle={this.toggleSearchSidebar}
          isOpen={this.state.isOpenSearchSidebar}
        />
        <JobSidebar
          onSubmitJob={this.onSubmitJob}
          libUsers={this.props.libUsers}
          checkedSites={this.state.jobCreateData.checkedSites}
          selectedSite={this.state.jobCreateData.selectedSite}
          onSelectSite={this.onSelectSiteJob}
          onChangeSite={this.onChangeSiteJob}
          location={this.state.jobCreateData.location}
          address={this.state.jobCreateData.address}
          onClose={this.onCloseJobSidebar}
          isOpen={this.state.isOpenCreateJob}
          nearbySites={this.state.nearbySites}
          currentSite={site}
        />
        <JobEditSidebar
          id={this.state.isOpenEditJob ? this.state.jobEditData.id : ""}
          onSubmitJob={this.onSubmitEditJob}
          libUsers={this.props.libUsers}
          onChangeSite={this.onChangeSiteEditJob}
          onClose={this.onCloseJobEditSidebar}
          isOpen={this.state.isOpenEditJob}
          nearbySites={this.state.jobEditData.nearbySites}
        />
        <ConfirmDialog
          cancelText="Close"
          confirmText="Upgrade"
          title="Free trial"
          isOpen={this.state.isOpenLimitCreateJob}
          content={`Free trial library cannot create more than 2 jobs. Upgrade plan to remove the limit.`}
          onClose={this.closeConfirmLimitCreateJob}
        />
      </div>
    );
  }
}

export default withRouter(MapBox);
