import * as React from "react";
import { createStyles, withStyles } from "@material-ui/core/styles";
import IStyleProps from "../../../styles/utils";

const styles = createStyles({
  list: {
    listStyleType: "none",
    padding: "0",
    margin: "0",
    "& > li": {
      padding: "3px 8px"
    },
    "& > li:hover": {
      backgroundColor: "#dbdbdb"
    }
  }
});

export enum ContextMenuListJob {
  VIEW_JOB = "viewJob",
  EDIT_JOB = "editJob",
  DELETE_JOB = "deleteJob"
}

interface IProps {
  src: string;
  onClickMenu: (menu: ContextMenuListJob) => () => void;
}

const ContextMenu = ({ classes, onClickMenu, src }: IStyleProps & IProps) => {
  return (
    <ul className={classes.list}>
      <li onClick={onClickMenu(ContextMenuListJob.VIEW_JOB)}>View Job</li>
      {src === "jobs" && (
        <>
          <li onClick={onClickMenu(ContextMenuListJob.EDIT_JOB)}>Edit Job</li>
          <li onClick={onClickMenu(ContextMenuListJob.DELETE_JOB)}>
            Delete Job
          </li>
        </>
      )}
    </ul>
  );
};

export default withStyles(styles)(ContextMenu);
