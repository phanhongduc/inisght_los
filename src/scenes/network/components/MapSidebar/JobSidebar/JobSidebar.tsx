import * as React from "react";
import * as classNames from "classnames";
import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import SidebarCore from "../SidebarCore";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import { defaultRules } from "react-hoc-form-validatable";
import IStyleProps from "src/styles/utils";
import FormJob from "./FormJob";
import ListSite from "./ListSite";
import { Transition } from "react-transition-group";
import FresnelChart from "../../../../../components/FresnelChart/FresnelChart";
import { INearestPointInfo } from "../../MapBox";
import { ILibUser } from "../../../../lib/types/libs";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    backdrop: {
      zIndex: 1
    },
    searchSidebar: {
      position: "absolute",
      top: 10,
      right: -700,
      width: 700,
      bottom: 10,
      overflow: "visible",
      userSelect: "text"
    },
    chart: {
      position: "absolute",
      left: -400,
      height: 350,
      width: 400,
      bottom: 10,
      overflow: "visible"
    },
    searchSidebarOpen: {
      zIndex: 1,
      right: 10
    },
    chartSidebarOpen: {
      zIndex: 1,
      left: 10
    },
    info: {
      paddingTop: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2
    },
    tab: {
      color: theme.palette.common.white
    },
    tabs: {
      marginTop: theme.spacing.unit * 2,
      backgroundColor: "#0096DA",
      color: theme.palette.common.white
    },
    section: {
      width: "50%",
      borderRight: `1px solid ${theme.palette.divider}`
    },
    chartDisplay: {
      marginTop: theme.spacing.unit * 2,
      position: "absolute",
      bottom: "15px",
      width: "370px"
    },
    wrap: { display: "flex", height: "100%", flexDirection: "row" }
  });

function cutCoordinate(num: number): string {
  const part = num.toString().split(".");
  if (part[1]) {
    return `${part[0]}.${part[1].slice(0, 3)}`;
  } else {
    return part[0];
  }
}

interface IProps {
  onSubmitJob: (inputs: any, done: (r?: boolean) => void) => void;
  libUsers: ILibUser[];
  selectedSite: INearestPointInfo | null;
  isOpen: boolean;
  nearbySites: INearestPointInfo[];
  onClose: () => void;
  location: number[];
  address: string;
  currentSite: INearestPointInfo | null;
  onChangeSite: (id: string) => (e: any, checked: boolean) => void;
  onSelectSite: (id: string) => (e: React.SyntheticEvent) => void;
  checkedSites: INearestPointInfo[];
}

interface IStates {
  jobHeight: number;
  freq: number;
}

class JobSidebar extends React.Component<IStyleProps & IProps, IStates> {
  public state = {
    jobHeight: 20,
    freq: 500
  };

  private onChangeJobData = (data: string) => {
    return (e: React.SyntheticEvent) => {
      const target = e.target as HTMLInputElement;
      if (data === "height") {
        this.setState({
          jobHeight: Number.parseInt(target.value, 10)
        });
      } else {
        this.setState({
          freq: Number.parseInt(target.value, 10)
        });
      }
    };
  };

  public render() {
    const {
      classes,
      isOpen,
      nearbySites,
      onClose,
      location,
      address,
      onChangeSite,
      onSelectSite,
      selectedSite,
      checkedSites,
      currentSite,
      libUsers,
      onSubmitJob
    } = this.props;

    const listEngineer = libUsers.map(user => ({
      id: user.userId,
      name: `${user.firstName} ${user.lastName}`
    }));

    return (
      <Transition in={isOpen} timeout={250} unmountOnExit={true}>
        {(state: string) => (
          <SidebarCore
            className={classNames(classes.searchSidebar, {
              [classes.searchSidebarOpen]: isOpen && state === "entered"
            })}
          >
            <div className={classes.wrap}>
              <div className={classes.section}>
                {state !== "exiting" &&
                  nearbySites.length > 0 && (
                    <ListSite
                      checkedSites={checkedSites}
                      selectedSite={selectedSite}
                      onSelectSite={onSelectSite}
                      onChangeSite={onChangeSite}
                      sites={nearbySites}
                    />
                  )}
              </div>
              <div className={classes.top}>
                <div className={classes.info}>
                  {location.length === 2 && (
                    <Typography>
                      <b>Job location</b>: ({cutCoordinate(location[0])},{" "}
                      {cutCoordinate(location[1])})
                    </Typography>
                  )}
                  <Typography>
                    <b>Address</b>: {address ? address : "Loading..."}
                  </Typography>
                </div>
                <FormJob
                  submitCallback={onSubmitJob}
                  engineers={listEngineer}
                  onChangeJobData={this.onChangeJobData}
                  onCancel={onClose}
                  rules={defaultRules}
                />
                {currentSite &&
                  state !== "exiting" && (
                    <div className={classes.chartDisplay}>
                      <FresnelChart
                        id={currentSite.id}
                        frequency={this.state.freq * 1000000}
                        heightOfStart={this.state.jobHeight}
                        aEndLat={location[1]}
                        aEndLon={location[0]}
                        bEndLat={currentSite.lat}
                        bEndLon={currentSite.lng}
                        distanceFromAendToBend={currentSite.distance * 1000}
                      />
                    </div>
                  )}
              </div>
            </div>
          </SidebarCore>
        )}
      </Transition>
    );
  }
}

export default withStyles(styles)(JobSidebar);
