import * as React from "react";
import { List as ListV } from "react-virtualized";
import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import Divider from "@material-ui/core/Divider";
import List from "@material-ui/core/List";
import IStyleProps from "src/styles/utils";
import Loading from "src/components/SectionLoading";
import { INearestPointInfo } from "../../MapBox";
import Checkbox from "@material-ui/core/Checkbox";
import ListItemText from "@material-ui/core/ListItemText";
import ListItem from "@material-ui/core/ListItem";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    info: {
      paddingTop: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2
    },
    tab: {
      color: theme.palette.common.white
    },
    tabs: {
      marginTop: theme.spacing.unit * 2,
      backgroundColor: "#0096DA",
      color: theme.palette.common.white
    },
    selected: {
      backgroundColor: theme.palette.action.hover
    },
    checkbox: {
      width: 24,
      height: 24
    },
    container: {
      height: "100%"
    }
  });

interface IProps {
  isDisabled?: boolean;
  selectedSite: INearestPointInfo | null;
  checkedSites: INearestPointInfo[];
  sites: INearestPointInfo[];
  onChangeSite: (id: string) => (e: any, checked: boolean) => void;
  onSelectSite: (id: string) => (e: React.SyntheticEvent) => void;
}

interface IState {
  heightList: number;
  widthList: number;
}

class ListSite extends React.Component<IStyleProps & IProps, IState> {
  private container = React.createRef<HTMLDivElement>();
  private list = React.createRef<ListV>();
  public state = {
    heightList: 500,
    widthList: 300
  };
  public renderRow = (props: any) => {
    const {
      sites,
      classes,
      onChangeSite,
      onSelectSite,
      selectedSite,
      checkedSites,
      isDisabled
    } = this.props;

    const site = sites[props.index];

    const checked = Boolean(checkedSites.find(s => s.id === site.id));

    return (
      <div key={site.id} style={props.style}>
        <ListItem
          disabled={Boolean(isDisabled)}
          className={
            selectedSite && selectedSite.id === site.id ? classes.selected : ""
          }
          button={true}
          onClick={onSelectSite(site.id)}
        >
          <Checkbox
            disabled={Boolean(isDisabled)}
            className={classes.checkbox}
            checked={checked}
            onChange={onChangeSite(site.id)}
            tabIndex={-1}
            disableRipple={true}
          />
          <ListItemText
            primary={site.name}
            secondary={
              <span>
                <span>{site.distance.toFixed(2)} km</span>
              </span>
            }
          />
        </ListItem>
        <Divider />
      </div>
    );
  };

  private setSizeList = () => {
    if (this.container.current) {
      this.setState({
        widthList: this.container.current.scrollWidth,
        heightList: this.container.current.scrollHeight
      });
    }
  };

  public componentDidMount() {
    this.setSizeList();
    window.addEventListener("resize", this.setSizeList);
  }

  public componentWillUnmount() {
    window.removeEventListener("resize", this.setSizeList);
  }

  public componentWillReceiveProps() {
    if (this.list.current) {
      this.list.current.forceUpdateGrid();
    }
  }

  public render() {
    const { sites, classes } = this.props;
    return (
      <div ref={this.container} className={classes.container}>
        {sites.length === 0 && <Loading />}
        <List component="nav">
          <ListV
            ref={this.list}
            width={this.state.widthList}
            height={this.state.heightList - 16}
            rowHeight={68}
            rowCount={sites.length}
            rowRenderer={this.renderRow}
          />
        </List>
      </div>
    );
  }
}

export default withStyles(styles)(ListSite);
