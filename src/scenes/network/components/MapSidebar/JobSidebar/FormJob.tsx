import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import MenuItem from "@material-ui/core/MenuItem";
import Button from "@material-ui/core/Button";
import { HOCForm, FormValidateChildProps } from "react-hoc-form-validatable";
import Grid from "@material-ui/core/Grid";
import IStyleProps from "src/styles/utils";
import InputValidate from "src/components/InputValidate/index";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { createStyles, Theme } from "@material-ui/core/styles";
import { SyntheticEvent } from "react";

interface IState {
  selectTab: number;
}

interface IEngineer {
  id: string;
  name: string;
}

interface IProps {
  defaultValues?: {
    name?: string;
    height?: number;
    frequency?: number;
    engineer?: string;
  };
  engineers: IEngineer[];
  onCancel: () => void;
  onChangeJobData: (name: string) => (e: SyntheticEvent) => void;
}

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2
    },
    submit: {
      paddingTop: theme.spacing.unit,
      textAlign: "center"
    },
    cancel: {
      marginLeft: 20
    },
    height: {
      paddingRight: theme.spacing.unit
    },
    freq: {
      paddingLeft: theme.spacing.unit
    }
  });

class FormJob extends React.Component<
  IStyleProps & IProps & FormValidateChildProps,
  IState
> {
  public render() {
    const {
      submitted,
      classes,
      onSubmit,
      onCancel,
      onChangeJobData,
      engineers,
      defaultValues
    } = this.props;

    const defaultValueEngi = engineers.length > 0 ? engineers[0].id : "";

    return (
      <div>
        <form className={classes.wrap} noValidate={true} onSubmit={onSubmit}>
          <InputValidate
            defaultValue={
              defaultValues && defaultValues.name ? defaultValues.name : ""
            }
            rule="notEmpty"
            name="name"
            margin="normal"
            required={true}
            label="Job name"
            InputLabelProps={{
              shrink: true
            }}
            fullWidth={true}
          />
          <Grid container={true}>
            <Grid item={true} xs={6}>
              <InputValidate
                defaultValue={
                  defaultValues && defaultValues.height
                    ? defaultValues.height
                    : 20
                }
                onChange={onChangeJobData("height")}
                className={classes.height}
                type="number"
                rule="notEmpty"
                name="height"
                margin="normal"
                required={true}
                label="Job height"
                InputLabelProps={{
                  shrink: true
                }}
                fullWidth={true}
              />
            </Grid>
            <Grid item={true} xs={6}>
              <InputValidate
                onChange={onChangeJobData("frequency")}
                className={classes.freq}
                defaultValue={
                  defaultValues && defaultValues.frequency
                    ? defaultValues.frequency / 1000000
                    : 500
                }
                type="number"
                rule="notEmpty"
                name="frequency"
                margin="normal"
                required={true}
                label="Frequency (MHz)"
                InputLabelProps={{
                  shrink: true
                }}
                fullWidth={true}
              />
            </Grid>
          </Grid>
          <InputValidate
            defaultValue={
              defaultValues && defaultValues.engineer
                ? defaultValues.engineer
                : defaultValueEngi
            }
            fullWidth={true}
            name="engineer"
            label="Engineer"
            select={true}
            className={classes.select}
            SelectProps={{
              MenuProps: {
                className: classes.menu
              }
            }}
            margin="normal"
          >
            {engineers.map(e => (
              <MenuItem key={e.id} value={e.id}>
                {e.name}
              </MenuItem>
            ))}
          </InputValidate>
          <div className={classes.submit}>
            <Button
              type="submit"
              variant="raised"
              color="primary"
              disabled={submitted}
            >
              Save
            </Button>
            <Button
              disabled={submitted}
              className={classes.cancel}
              onClick={onCancel}
              type="button"
              variant="raised"
            >
              Cancel
            </Button>
          </div>
        </form>
      </div>
    );
  }
}
export default withStyles(styles)(HOCForm<IStyleProps & IProps>(FormJob));
