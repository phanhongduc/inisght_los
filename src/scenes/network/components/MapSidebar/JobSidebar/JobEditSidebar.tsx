import * as React from "react";
import * as classNames from "classnames";
import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import SidebarCore from "../SidebarCore";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import { defaultRules } from "react-hoc-form-validatable";
import IStyleProps from "src/styles/utils";
import FormJob from "./FormJob";
import ListSite from "./ListSite";
import { Transition } from "react-transition-group";
import FresnelChart from "../../../../../components/FresnelChart/FresnelChart";
import { INearestPointInfo } from "../../MapBox";
import { IJobCandidate, ILibUser } from "../../../../lib/types/libs";
import { IJobDetail } from "../../../types/Job";
import SectionLoading from "../../../../../components/SectionLoading";
import { IEditJobData } from "../../../actions/Job";
import cutCoordinate from "../../../../../helpers/cutCoordinate";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    backdrop: {
      zIndex: 1
    },
    searchSidebar: {
      position: "absolute",
      top: 10,
      right: -700,
      width: 700,
      bottom: 10,
      overflow: "visible",
      userSelect: "text"
    },
    chart: {
      position: "absolute",
      left: -400,
      height: 350,
      width: 400,
      bottom: 10,
      overflow: "visible"
    },
    searchSidebarOpen: {
      zIndex: 1,
      right: 10
    },
    chartSidebarOpen: {
      zIndex: 1,
      left: 10
    },
    info: {
      paddingTop: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2
    },
    tab: {
      color: theme.palette.common.white
    },
    tabs: {
      marginTop: theme.spacing.unit * 2,
      backgroundColor: "#0096DA",
      color: theme.palette.common.white
    },
    section: {
      width: "50%",
      borderRight: `1px solid ${theme.palette.divider}`
    },
    chartDisplay: {
      marginTop: theme.spacing.unit * 2,
      position: "absolute",
      bottom: "15px",
      width: "370px"
    },
    wrap: { display: "flex", height: "100%", flexDirection: "row" }
  });

export interface IProps {
  id: string;
  onSubmitJob: (
    inputs: IEditJobData,
    candidates: IJobCandidate[],
    done: (r?: boolean) => void
  ) => void;
  libUsers: ILibUser[];
  isOpen: boolean;
  nearbySites: INearestPointInfo[];
  onClose: () => void;
  onChangeSite: (
    lon: number,
    lat: number,
    checkedSites: INearestPointInfo[]
  ) => void;
  isLoading: boolean;
  data?: IJobDetail;
  getJobDetail: (id: string) => void;
}

interface IStates {
  isSubmitting: boolean;
  selectedSite: INearestPointInfo | null;
  jobHeight: number;
  freq: number;
  checkedSites: INearestPointInfo[];
}

class JobSidebar extends React.Component<IStyleProps & IProps, IStates> {
  public state = {
    isSubmitting: false,
    selectedSite: null,
    jobHeight: 20,
    freq: 500,
    checkedSites: [] as INearestPointInfo[]
  };

  public componentWillReceiveProps(nextProps: IProps) {
    if (nextProps.id && nextProps.id !== this.props.id) {
      this.props.getJobDetail(nextProps.id);
    }

    if (
      (nextProps.data && !this.props.data) ||
      (nextProps.data &&
        this.props.data &&
        nextProps.data.jobId !== this.props.data.jobId)
    ) {
      this.setState(
        {
          freq: nextProps.data.frequency / 1000000,
          jobHeight: nextProps.data.recommendHeight,
          checkedSites: nextProps.nearbySites.filter(n => {
            if (nextProps.data) {
              return nextProps.data.bends.find(b => {
                return b.bendId === n.id;
              });
            }
            return false;
          })
        },
        () => {
          if (this.props.data) {
            this.props.onChangeSite(
              this.props.data.lon,
              this.props.data.lat,
              this.state.checkedSites
            );
          }
        }
      );
    }
  }

  private onChangeJobData = (data: string) => {
    return (e: React.SyntheticEvent) => {
      const target = e.target as HTMLInputElement;
      if (data === "height") {
        this.setState({
          jobHeight: Number.parseInt(target.value, 10)
        });
      } else {
        this.setState({
          freq: Number.parseInt(target.value, 10)
        });
      }
    };
  };

  private onChangeSite = (id: string) => {
    return (e: React.SyntheticEvent, checked: boolean) => {
      e.stopPropagation();
      const site = this.props.nearbySites.find(s => s.id === id);
      if (site && this.props.data) {
        if (!checked) {
          this.setState(
            {
              checkedSites: this.state.checkedSites.filter(
                s => s.id !== site.id
              )
            },
            () => {
              if (this.props.data) {
                this.props.onChangeSite(
                  this.props.data.lon,
                  this.props.data.lat,
                  this.state.checkedSites
                );
              }
            }
          );
        } else {
          this.setState(
            {
              checkedSites: this.state.checkedSites.concat([site]),
              selectedSite: site
            },
            () => {
              if (this.props.data) {
                this.props.onChangeSite(
                  this.props.data.lon,
                  this.props.data.lat,
                  this.state.checkedSites
                );
              }
            }
          );
        }
      }
    };
  };

  private onSelectSiteJob = (id: string) => {
    return (e: React.SyntheticEvent) => {
      const target = e.target as HTMLInputElement;
      if (target.type !== "checkbox") {
        const site = this.props.nearbySites.find(s => s.id === id);
        if (site) {
          this.setState({
            selectedSite: site
          });
        }
      }
    };
  };

  private onSubmitJob = (inputs: any, done: (r?: boolean) => void) => {
    this.setState({
      isSubmitting: true
    });
    this.props.onSubmitJob(
      {
        id: this.props.id,
        jobName: inputs.name.value,
        recommendHeight: inputs.height.value,
        frequency: inputs.frequency.value * 1000000,
        engineerId: inputs.engineer.value
      },
      this.state.checkedSites.map(s => ({
        bendId: s.id,
        bendSiteName: s.name,
        lon: s.lng,
        lat: s.lat,
        displayOrder: 20,
        recommendHeight: 20
      })),
      (r?: boolean) => {
        done(r);
        this.setState({
          isSubmitting: false
        });
      }
    );
  };

  public render() {
    const {
      isLoading,
      classes,
      isOpen,
      nearbySites,
      onClose,
      libUsers,
      data
    } = this.props;

    const { selectedSite } = this.state;

    const listEngineer = libUsers.map(user => ({
      id: user.userId,
      name: `${user.firstName} ${user.lastName}`
    }));

    return (
      <Transition in={isOpen} timeout={250} unmountOnExit={true}>
        {(state: string) => (
          <SidebarCore
            className={classNames(classes.searchSidebar, {
              [classes.searchSidebarOpen]: isOpen && state === "entered"
            })}
          >
            {isLoading && <SectionLoading />}
            {!isLoading &&
              data && (
                <div className={classes.wrap}>
                  <div className={classes.section}>
                    {state !== "exiting" &&
                      nearbySites.length > 0 && (
                        <ListSite
                          isDisabled={this.state.isSubmitting}
                          checkedSites={this.state.checkedSites}
                          selectedSite={selectedSite}
                          onSelectSite={this.onSelectSiteJob}
                          onChangeSite={this.onChangeSite}
                          sites={nearbySites}
                        />
                      )}
                  </div>
                  <div className={classes.top}>
                    <div className={classes.info}>
                      <Typography>
                        <b>Job location</b>: ({cutCoordinate(data.lon)},{" "}
                        {cutCoordinate(data.lat)})
                      </Typography>
                      <Typography>
                        <b>Address</b>: {data.address}
                      </Typography>
                    </div>
                    <FormJob
                      defaultValues={{
                        name: data.jobName,
                        height: data.recommendHeight,
                        engineer: data.engineerId,
                        frequency: data.frequency
                      }}
                      submitCallback={this.onSubmitJob}
                      engineers={listEngineer}
                      onChangeJobData={this.onChangeJobData}
                      onCancel={onClose}
                      rules={defaultRules}
                    />
                    {selectedSite && (
                      <div className={classes.chartDisplay}>
                        <FresnelChart
                          // @ts-ignore
                          id={selectedSite.id}
                          frequency={this.state.freq * 1000000}
                          heightOfStart={this.state.jobHeight}
                          aEndLat={data.lat}
                          aEndLon={data.lon}
                          // @ts-ignore
                          bEndLat={selectedSite.lat}
                          // @ts-ignore
                          bEndLon={selectedSite.lng}
                          distanceFromAendToBend={
                            // @ts-ignore
                            selectedSite.distance * 1000
                          }
                        />
                      </div>
                    )}
                  </div>
                </div>
              )}
          </SidebarCore>
        )}
      </Transition>
    );
  }
}

export default withStyles(styles)(JobSidebar);
