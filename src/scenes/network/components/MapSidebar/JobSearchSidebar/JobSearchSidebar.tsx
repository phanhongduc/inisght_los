import * as React from "react";
import * as classNames from "classnames";
import SearchIcon from "@material-ui/icons/Search";
import LocationIcon from "@material-ui/icons/LocationOn";
import Divider from "@material-ui/core/Divider";
import Button from "@material-ui/core/Button";
import TextField from "@material-ui/core/TextField";
import MenuItem from "@material-ui/core/MenuItem";
import InputAdornment from "@material-ui/core/InputAdornment";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Checkbox from "@material-ui/core/Checkbox";
import { withStyles, createStyles, Theme } from "@material-ui/core/styles";
import SidebarCore from "../SidebarCore";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import IStyleProps from "src/styles/utils";
import SectionLoading from "src/components/SectionLoading";
import { Transition } from "react-transition-group";
import Pagination from "../../../../../components/Pagination/PaginationMUI";

import {
  ILibJobQuery,
  ILibQueryPoint,
  ILibUser,
  ISearchJobResult,
  JobStatus
} from "../../../../lib/types/libs";
import green from "@material-ui/core/colors/green";
import amber from "@material-ui/core/colors/amber";
import grey from "@material-ui/core/colors/grey";

export interface IProps {
  getMapBound: () => number[][][];
  users: ILibUser[];
  result: ISearchJobResult;
  query: ILibJobQuery;
  isOpen: boolean;
  onClickToggle: () => void;
  onChangeSkip: (skip: number) => void;
  onChangeKeyword: (keyword: string) => void;
  onChangeUser: (id: string) => void;
  onChangeStatus: (status: number) => void;
  onChangeSearchInMap: (data: ILibQueryPoint[]) => void;
  onSelectJob: (id: string) => void;
  onViewDetailJob: (id: string) => void;
}

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      height: "100%",
      display: "flex",
      flexDirection: "column"
    },
    searchSidebar: {
      position: "absolute",
      top: 10,
      right: -400,
      width: 400,
      bottom: 10,
      overflow: "visible",
      userSelect: "text"
    },
    searchSidebarOpen: {
      zIndex: 1,
      right: 10
    },
    toggle: {
      top: 0,
      left: -30,
      position: "absolute",
      borderTopLeftRadius: "4px",
      borderBottomLeftRadius: "4px",
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: theme.palette.common.white,
      width: 30,
      height: 50,
      borderRight: `1px solid ${theme.palette.divider}`,
      boxShadow: "-1px 0 8px rgba(0,0,0,.175)"
    },
    search: {
      display: "flex",
      alignItems: "flex-start",
      alignContent: "flex-start",
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      paddingTop: theme.spacing.unit * 2
    },
    filter: {
      position: "relative",
      zIndex: 1,
      paddingTop: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      display: "flex",
      alignItems: "flex-start",
      alignContent: "flex-start"
    },
    filterBox: {
      backgroundColor: theme.palette.common.white
    },
    sep: {
      height: theme.spacing.unit * 2,
      width: theme.spacing.unit * 2,
      display: "block"
    },
    name: {
      display: "flex",
      "& > span:nth-child(2)": {
        marginLeft: "auto"
      }
    },
    address: {
      "&  svg": {
        fontSize: 12
      }
    },
    jobStatus: {
      display: "inline-block",
      fontSize: "12px",
      padding: "5px",
      borderRadius: "5px",
      color: theme.palette.common.white,
      lineHeight: "1.1875em",
      marginRight: theme.spacing.unit
    },
    jobStatusCreated: {
      backgroundColor: grey[500]
    },
    jobStatusProcessing: {
      backgroundColor: amber[500]
    },
    jobStatusFinished: {
      backgroundColor: green[500]
    },
    list: {
      overflow: "auto"
    },
    pagination: {
      textAlign: "center",
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: 60
    },
    inMap: {
      position: "absolute",
      bottom: "10px",
      left: "8px",
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2
    }
  });

class JobSearchSidebar extends React.Component<IStyleProps & IProps> {
  private onChangePage = (page: number) => {
    this.props.onChangeSkip((page - 1) * this.props.query.take);
  };

  private onChangeKeywordHandler = (cb: (keyword: string) => void) => {
    return (e: React.ChangeEvent<HTMLInputElement>) => {
      const target = e.target as HTMLInputElement;
      cb(target.value);
    };
  };

  private onChangeUser = (cb: (keyword: string) => void) => {
    return (e: React.ChangeEvent<HTMLInputElement>) => {
      const target = e.target as HTMLInputElement;
      cb(target.value !== "all" ? target.value : "");
    };
  };

  private onChangeStatus = (cb: (status: number) => void) => {
    return (e: React.ChangeEvent<HTMLInputElement>) => {
      const target = e.target as HTMLInputElement;
      cb(target.value !== "all" ? Number.parseInt(target.value, 10) : -1);
    };
  };

  private onChangeSearchInMap = (e: React.ChangeEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    if (target.checked) {
      this.props.onChangeSearchInMap(
        this.props.getMapBound()[0].map(data => {
          return {
            lon: data[0],
            lat: data[1]
          };
        })
      );
    } else {
      this.props.onChangeSearchInMap([]);
    }
  };

  private onSelectJob = (id: string) => {
    return () => {
      this.props.onSelectJob(id);
    };
  };

  private onViewDetailJob = (id: string) => {
    return () => {
      this.props.onViewDetailJob(id);
    };
  };

  public render() {
    const {
      users,
      classes,
      isOpen,
      onClickToggle,
      result,
      query,
      onChangeKeyword,
      onChangeUser,
      onChangeStatus
    } = this.props;

    return (
      <Transition in={isOpen} timeout={250} unmountOnExit={false}>
        {(state: string) => (
          <SidebarCore
            className={classNames(classes.searchSidebar, {
              [classes.searchSidebarOpen]: isOpen && state === "entered"
            })}
          >
            <div className={classes.wrap}>
              <div className={classes.search}>
                <TextField
                  onChange={this.onChangeKeywordHandler(onChangeKeyword)}
                  InputProps={{
                    endAdornment: (
                      <InputAdornment position="end">
                        <SearchIcon />
                      </InputAdornment>
                    )
                  }}
                  fullWidth={true}
                  className={classes.searchInput}
                  placeholder="Search name or address"
                />
              </div>
              <div className={classes.filter}>
                <TextField
                  className={classes.filterBox}
                  value={query.assigneeId || "all"}
                  fullWidth={true}
                  label="Engineers"
                  select={true}
                  onChange={this.onChangeUser(onChangeUser)}
                >
                  <MenuItem value="all">All</MenuItem>
                  {users.map(u => (
                    <MenuItem key={u.userId} value={u.userId}>
                      {u.firstName} {u.lastName}
                    </MenuItem>
                  ))}
                </TextField>
                <span className={classes.sep} />
                <TextField
                  className={classes.filterBox}
                  value={query.jobStatus > -1 ? query.jobStatus : "all"}
                  fullWidth={true}
                  label="Status"
                  select={true}
                  onChange={this.onChangeStatus(onChangeStatus)}
                >
                  <MenuItem value="all">All</MenuItem>
                  <MenuItem value={JobStatus.Created}>
                    {JobStatus[JobStatus.Created]}
                  </MenuItem>
                  <MenuItem value={JobStatus.Processing}>
                    {JobStatus[JobStatus.Processing]}
                  </MenuItem>
                  <MenuItem value={JobStatus.Finished}>
                    {JobStatus[JobStatus.Finished]}
                  </MenuItem>
                </TextField>
              </div>
              {query.isLoading && <SectionLoading />}
              {!query.isLoading &&
                result.jobs.length > 0 && (
                  <>
                    <div className={classes.list}>
                      <List component="nav">
                        {result.jobs.map(job => (
                          <div key={job.id}>
                            <ListItem
                              onClick={this.onSelectJob(job.id)}
                              button={true}
                              className={classes.listItem}
                            >
                              <ListItemText
                                primary={
                                  <div className={classes.name}>
                                    <span>
                                      <span
                                        className={classNames(
                                          classes.jobStatus,
                                          {
                                            [classes.jobStatusFinished]:
                                              job.jobStatus ===
                                              JobStatus.Finished
                                          },
                                          {
                                            [classes.jobStatusProcessing]:
                                              job.jobStatus ===
                                              JobStatus.Processing
                                          },
                                          {
                                            [classes.jobStatusCreated]:
                                              job.jobStatus ===
                                              JobStatus.Created
                                          }
                                        )}
                                      >
                                        {JobStatus[job.jobStatus]}
                                      </span>
                                      {job.name}
                                    </span>{" "}
                                    <span>
                                      <Button
                                        size="small"
                                        color="primary"
                                        onClick={this.onViewDetailJob(job.id)}
                                      >
                                        View
                                      </Button>
                                    </span>
                                  </div>
                                }
                                secondary={
                                  <span className={classes.address}>
                                    <LocationIcon /> {job.address}
                                  </span>
                                }
                              />
                            </ListItem>
                            <Divider />
                          </div>
                        ))}
                      </List>
                    </div>
                  </>
                )}
              {result.total > 0 && (
                <div className={classes.pagination}>
                  <Pagination
                    onChangePage={this.onChangePage}
                    disabled={query.isLoading}
                    start={query.skip / query.take + 1}
                    display={5}
                    total={Math.ceil(result.total / query.take)}
                  />
                </div>
              )}
              <div className={classes.inMap}>
                <FormControlLabel
                  control={
                    <Checkbox
                      disabled={query.isLoading}
                      onChange={this.onChangeSearchInMap}
                      color="primary"
                      checked={query.polygonCoordinates.length > 0}
                      value="checkSearchBound"
                    />
                  }
                  label="Search within map"
                />
              </div>
            </div>

            <div className={classes.toggle} onClick={onClickToggle}>
              <SearchIcon />
            </div>
          </SidebarCore>
        )}
      </Transition>
    );
  }
}

export default withStyles(styles)(JobSearchSidebar);
