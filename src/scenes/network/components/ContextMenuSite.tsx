import * as React from "react";
import { createStyles, withStyles } from "@material-ui/core/styles";
import IStyleProps from "../../../styles/utils";

const styles = createStyles({
  list: {
    listStyleType: "none",
    padding: "0",
    margin: "0",
    "& > li": {
      padding: "3px 8px"
    },
    "& > li:hover": {
      backgroundColor: "#dbdbdb"
    }
  }
});

export enum ContextMenuListSite {
  CREATE_JOB = "createJob"
}

interface IProps {
  src: string;
  onClickMenu: (menu: ContextMenuListSite) => () => void;
}

const ContextMenuSite = ({
  classes,
  onClickMenu,
  src
}: IStyleProps & IProps) => {
  return (
    <ul className={classes.list}>
      <li onClick={onClickMenu(ContextMenuListSite.CREATE_JOB)}>
        Create new job
      </li>
    </ul>
  );
};

export default withStyles(styles)(ContextMenuSite);
