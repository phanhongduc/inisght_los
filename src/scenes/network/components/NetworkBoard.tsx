import * as React from "react";
import TabRoute from "../../../components/TabRoute";
import { Theme, createStyles, withStyles } from "@material-ui/core/styles";
import { IMyMixisOptions } from "../../../withRoot";
import { IStyleTypeProps } from "../../../styles/utils";
interface IProps {
  isLoading: boolean;
}
const styles = (theme: Theme) =>
  createStyles({
    content: {
      height:
        (theme.mixins as IMyMixisOptions).windowHeight -
        theme.spacing.unit * 14 -
        5,
      overflow: "auto"
    }
  });

const NetworkBoard = ({ classes }: IProps & IStyleTypeProps<typeof styles>) => {
  return <TabRoute routeKey="networkRoutes" extendsStyle={classes} />;
};

export default withStyles(styles, { withTheme: true })(NetworkBoard);
