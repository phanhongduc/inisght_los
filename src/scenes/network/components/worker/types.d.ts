// MyWorker/types.d.ts
import { Feature, Point } from "geojson";
import { IProperties } from "../../types/Site";
import { INearestPointInfo } from "../MapBox";
// Enumerate message types
export const enum MESSAGE_TYPE {
  READY = "ready",
  REQUEST = "request",
  RESULT = "result",
  ERROR = "error"
}

// Define expected properties for each message type
interface IReadyMessage {
  type: MESSAGE_TYPE.READY;
}

interface IRequestMessage {
  type: MESSAGE_TYPE.REQUEST;
  paramA: string;
  paramB: number;
}

interface IResultMessage {
  type: MESSAGE_TYPE.RESULT;
  data: Float32Array;
}

interface IErrorMessage {
  type: MESSAGE_TYPE.ERROR;
  error: string;
}
interface IPoint {
  lng: number;
  lat: number;
}
// Create a union type of all messages for convenience
type MyWorkerMessage = INearestPointInfo[];

// Extend MessageEvent to use our messages
interface IMyMessageEvent extends MessageEvent {
  listSite: Array<Feature<Point, IProperties>>;
  point: IPoint;
}

// Extend Worker to use our custom MessageEvent
export class MyWorker extends Worker {
  public onmessage: (this: MyWorker, ev: IMyMessageEvent) => any;

  public postMessage(
    this: MyWorker,
    msg: INearestPointInfo[],
    transferList?: ArrayBuffer[]
  ): any;
  public addEventListener(
    type: "message",
    listener: (this: MyWorker, ev: IMyMessageEvent) => any,
    useCapture?: boolean
  ): void;
  public addEventListener(
    type: "error",
    listener: (this: MyWorker, ev: ErrorEvent) => any,
    useCapture?: boolean
  ): void;
}
