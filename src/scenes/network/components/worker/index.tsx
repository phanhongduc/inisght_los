import { MyWorker } from "./types";
import MyWorkerImport from "worker-loader!./worker";

export default MyWorkerImport as typeof MyWorker;
