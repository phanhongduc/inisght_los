import { MyWorker } from "./types";
const ctx: MyWorker = self as any;
import { Feature, Point } from "geojson";
import { IProperties } from "../../types/Site";
import { INearestPointInfo } from "../MapBox";
import * as kdbush from "kdbush";
import * as geokdbush from "geokdbush";
interface IPoint {
  lng: number;
  lat: number;
}
// Respond to message from parent thread
ctx.onmessage = e => {
  const listSite: Array<Feature<Point, IProperties>> = e.data.listSite;
  const point: IPoint = e.data.point;
  // let message: string = ev.data;
  const points: number[][] = listSite.map((el: Feature<Point>) => {
    return el.geometry.coordinates;
  });
  const index = kdbush(points);
  const nearest = geokdbush.around(index, point.lng, point.lat, Infinity, 50);
  const result: INearestPointInfo[] = [];
  nearest.forEach((el: any) => {
    console.time("xx");
    const existedSite: INearestPointInfo | undefined = result.find(
      (siteEl: INearestPointInfo) => {
        return siteEl.lng === el[0] && siteEl.lat === el[1];
      }
    );
    const site: Feature<Point, IProperties> | undefined = listSite.find(
      (siteEl: Feature<Point, IProperties>) => {
        if (existedSite) {
          return (
            siteEl.geometry.coordinates[0] === el[0] &&
            siteEl.geometry.coordinates[1] === el[1] &&
            siteEl.properties.id !== existedSite.id
          );
        }
        return (
          siteEl.geometry.coordinates[0] === el[0] &&
          siteEl.geometry.coordinates[1] === el[1]
        );
      }
    );
    if (site) {
      const nearestSite: INearestPointInfo = {
        id: site.properties.id,
        name: site.properties.siteName,
        lng: site.geometry.coordinates[0],
        lat: site.geometry.coordinates[1],
        distance: geokdbush.distance(point.lng, point.lat, el[0], el[1])
      };
      result.push(nearestSite);
    }
  });
  console.timeEnd("xx");
  ctx.postMessage(result);
};
