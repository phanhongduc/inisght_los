import * as React from "react";

import MainMap from "../containers/MainMap";
import styles from "../styles/Hello";

// import { Link } from 'react-router-dom';
import { withStyles } from "@material-ui/core/styles";
import IStyleProps from "../../../styles/utils";
export interface IProps {
  updateBoundary?: (...args: any[]) => void;
}
// const Home = () => (<h1>Home <Link to="/about">About</Link></h1>);

function MapBoard({ updateBoundary, classes }: IStyleProps & IProps) {
  return (
    <div className={classes.hello} ref={updateBoundary}>
      <MainMap />
    </div>
  );
}

export default withStyles(styles)(MapBoard);
