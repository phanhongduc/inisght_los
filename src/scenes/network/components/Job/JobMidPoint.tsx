import * as React from "react";
import "mapbox-gl/dist/mapbox-gl.css";
import "react-image-lightbox/style.css";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import Divider from "@material-ui/core/Divider";
import EditIcon from "@material-ui/icons/Edit";
import IStyleProps from "../../../../styles/utils";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";
import { IBend, IJobDetail, IMidPoint } from "../../types/Job";
import * as mapboxgl from "mapbox-gl";
import Grid from "@material-ui/core/Grid";
import FresnelChart from "../../../../components/FresnelChart/FresnelChart";
import { Feature, Point } from "geojson";
import cutCoordinate from "../../../../helpers/cutCoordinate";
import { point } from "@turf/helpers";
import { distance } from "@turf/turf";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import IconButton from "@material-ui/core/IconButton";
import IconDelete from "@material-ui/icons/Delete";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Lightbox from "react-image-lightbox";
import ConfirmDialog from "../../../../components/Dialog/Confirm";
import { IGetSiteAddressData } from "../../actions/Site";
import VirtualLOS from "./VirtualLOS";
import DownloadFile from "src/components/LightBox/DownloadFile";
import MenuItem from "@material-ui/core/MenuItem/MenuItem";
import TextField from "@material-ui/core/TextField/TextField";
import * as moment from "moment";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      padding: theme.spacing.unit * 2
    },
    mapDisplay: {
      height: 300
    },
    listInfo: {
      height: 170
    },
    list: {
      display: "flex",
      flexWrap: "wrap",
      maxWidth: 600,

      "& > dt": {
        fontWeight: "bold",
        flexBasis: "50%",
        maxWidth: "50%",
        margin: "0",
        paddingBottom: "8px",
        paddingTop: "8px"
      },

      "& > dd": {
        flexBasis: "50%",
        maxWidth: "50%",
        textAlign: "left",
        margin: "0",
        paddingBottom: "8px",
        paddingTop: "8px"
      }
    },
    infoSection: {
      padding: theme.spacing.unit * 2
    },
    info: {
      marginTop: theme.spacing.unit * 2
    },
    iconEdit: {
      marginRight: theme.spacing.unit,
      fontSize: 16
    },
    capImg: {
      position: "relative",
      "& > img": {
        cursor: "pointer",
        width: "100%"
      }
    },
    capImgBackDrop: {
      zIndex: 1,
      position: "absolute"
    },
    capImgProcess: {
      color: theme.palette.common.white,
      zIndex: 1,
      position: "absolute",
      left: "50%",
      top: "50%",
      transform: "translate(-50%, -50%)"
    },
    capSection: {
      paddingTop: theme.spacing.unit,
      paddingLeft: theme.spacing.unit,
      paddingRight: theme.spacing.unit
    },
    delete: {
      color: theme.palette.error.main,
      marginLeft: "auto"
    },
    control: {
      display: "flex",
      alignItems: "center"
    },
    left: {
      marginLeft: "auto"
    },
    border: {
      borderRight: `1px solid ${theme.palette.divider}`
    },
    selected: {
      backgroundColor: "rgba(0, 0, 0, 0.08)"
    },
    title: {
      textAlign: "center",
      padding: theme.spacing.unit
    },
    midPoint: {
      padding: theme.spacing.unit * 2,
      marginTop: theme.spacing.unit * 3
    },
    progress: {
      top: 10
    },
    bendControl: {
      marginBottom: 32
    },
    bend: {
      padding: 16
    }
  });

interface IStates {
  deleteMedia: string;
  isOpenConfirm: boolean;
  confirmText: string;
  isEditing: boolean;
  selectedMidPoint: number;
  reviewImg: string;
  selectedBend: string;
  bends: string[];
}

interface IProps {
  id: string;
  deleteMidPointMedia: (
    mediaId: string,
    jobId: string,
    done?: (success: boolean) => void
  ) => void;
  data: IJobDetail;
  midPoints: IMidPoint[];
  siteAddress: {
    [k: string]: string;
  };
  getSiteAddress: (data: IGetSiteAddressData) => void;
  opVlos: (jobId: string, mediaId: string) => void;
}

const getAendFromMid = (mid: IMidPoint, data: IJobDetail): IBend => ({
  bendId: mid.aendSiteId,
  bendSiteName: data.jobName,
  address: mid.aendAddress,
  lon: mid.aendLon,
  lat: mid.aendLat,
  displayOrder: 0,
  recommendHeight: mid.aendHeight,
  distance: 0
});
const getBendFromMid = (mid: IMidPoint): IBend => ({
  bendId: mid.bendSiteId,
  bendSiteName: mid.bendName,
  address: mid.bendAddress,
  lon: mid.bendLon,
  lat: mid.bendLat,
  displayOrder: 0,
  recommendHeight: mid.bendHeight,
  distance: 0
});

class JobMidPoint extends React.Component<IStyleProps & IProps, IStates> {
  private map: mapboxgl.Map;
  private mapContainer: React.RefObject<HTMLDivElement>;

  constructor(props: IStyleProps & IProps) {
    super(props);
    this.mapContainer = React.createRef<HTMLDivElement>();

    const bends = this.props.midPoints.reduce(
      (current, m) => {
        if (current.indexOf(m.bendName) < 0) {
          current.push(m.bendName);
          return current;
        }

        return current;
      },
      [] as string[]
    );
    this.state = {
      bends,
      deleteMedia: "",
      isOpenConfirm: false,
      confirmText: "",
      reviewImg: "",
      selectedMidPoint: 0,
      selectedBend: bends[0],
      isEditing: false
    };
  }

  private onClickImgReview = (url: string) => {
    return () => {
      this.setState({
        reviewImg: url
      });
    };
  };

  private onCloseImgReview = () => {
    this.setState({
      reviewImg: ""
    });
  };

  private onClickDelete = (id: string) => {
    return () => {
      this.setState({
        confirmText: "Do you want to delete the media?",
        isOpenConfirm: true,
        deleteMedia: id
      });
    };
  };

  private handleCloseConfirm = (confirm: boolean) => {
    if (confirm) {
      this.setState(
        {
          isEditing: true
        },
        () => {
          this.props.deleteMidPointMedia(
            this.state.deleteMedia,
            this.props.id,
            success => {
              this.setState({
                isEditing: false,
                isOpenConfirm: !success,
                deleteMedia: ""
              });
            }
          );
        }
      );
    } else {
      this.setState({
        isOpenConfirm: false
      });
    }
  };

  private onChangeMidPoint = (index: number, selectedBendNew?: string) => {
    return () => {
      this.setState(
        {
          selectedMidPoint: index,
          selectedBend: selectedBendNew
            ? selectedBendNew
            : this.state.selectedBend
        },
        () => {
          const selectedBendMidPoint = this.props.midPoints.filter(m => {
            return m.bendName === this.state.selectedBend;
          })[this.state.selectedMidPoint];

          const selectedBend = this.props.data.bends.find(
            b => b.bendId === selectedBendMidPoint.bendSiteId
          ) as IBend;

          if (
            !selectedBend.address &&
            !this.props.siteAddress[selectedBend.bendId]
          ) {
            this.props.getSiteAddress({
              id: selectedBend.bendId,
              lat: selectedBend.lat,
              lon: selectedBend.lon
            });
          }

          if (!this.props.siteAddress[selectedBendMidPoint.id]) {
            this.props.getSiteAddress({
              id: selectedBendMidPoint.id,
              lat: selectedBendMidPoint.lat,
              lon: selectedBendMidPoint.lon
            });
          }

          (this.map.getSource(
            "measure-lines"
          ) as mapboxgl.GeoJSONSource).setData({
            type: "Feature",
            geometry: {
              type: "LineString",
              coordinates: [
                [selectedBend.lon, selectedBend.lat],
                [this.props.data.lon, this.props.data.lat]
              ]
            },
            properties: {}
          });

          (this.map.getSource("mid") as mapboxgl.GeoJSONSource).setData({
            type: "FeatureCollection",
            features: [
              {
                type: "Feature",
                geometry: {
                  type: "Point",
                  coordinates: [
                    selectedBendMidPoint.lon,
                    selectedBendMidPoint.lat
                  ]
                },
                properties: {}
              }
            ]
          });
        }
      );
    };
  };

  private onChangeBend = (e: React.ChangeEvent<HTMLInputElement>) => {
    this.onChangeMidPoint(0, e.target.value)();
  };

  public componentDidMount() {
    if (this.mapContainer.current && this.props.midPoints.length > 0) {
      const selectedBend = this.props.data.bends.find(
        b => b.bendId === this.props.midPoints[0].bendSiteId
      ) as IBend;

      if (
        !selectedBend.address &&
        !this.props.siteAddress[selectedBend.bendId]
      ) {
        this.props.getSiteAddress({
          id: selectedBend.bendId,
          lat: selectedBend.lat,
          lon: selectedBend.lon
        });
      }

      if (
        !this.props.siteAddress[
          this.props.midPoints[this.state.selectedMidPoint].id
        ]
      ) {
        this.props.getSiteAddress({
          id: this.props.midPoints[this.state.selectedMidPoint].id,
          lat: this.props.midPoints[this.state.selectedMidPoint].lat,
          lon: this.props.midPoints[this.state.selectedMidPoint].lon
        });
      }

      this.map = new mapboxgl.Map({
        container: this.mapContainer.current,
        style: "/3d_building_style.json",
        center: [this.props.data.lon, this.props.data.lat],
        zoom: 12
      });

      this.map.on("load", () => {
        this.map.loadImage(
          require("src/assets/images/marker/ic_job.png"),
          (error: any, image: HTMLImageElement) => {
            this.map.addImage("job", image);

            this.map.addSource("measure-lines", {
              type: "geojson",
              data: {
                type: "Feature",
                geometry: {
                  type: "LineString",
                  coordinates: [
                    [this.props.data.lon, this.props.data.lat],
                    [selectedBend.lon, selectedBend.lat]
                  ]
                },
                properties: {}
              }
            });

            this.map.addLayer({
              id: "measure-lines",
              type: "line",
              source: "measure-lines",
              layout: {
                "line-cap": "round",
                "line-join": "round"
              },
              paint: {
                "line-color": "#e25144",
                "line-width": 2.5,
                "line-opacity": 0.5
              },
              filter: ["in", "$type", "LineString"]
            });

            this.map.addSource("site", {
              type: "geojson",
              data: {
                type: "FeatureCollection",
                features: this.props.data.bends.map(b => ({
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: [b.lon, b.lat]
                  },
                  properties: {}
                })) as Array<Feature<Point>>
              }
            });

            this.map.addLayer({
              id: "site",
              type: "circle",
              source: "site",
              paint: {
                "circle-color": "#51bbd6",
                "circle-radius": 10
              }
            });

            this.map.addSource("job", {
              type: "geojson",
              data: {
                type: "FeatureCollection",
                features: [
                  {
                    type: "Feature",
                    geometry: {
                      type: "Point",
                      coordinates: [this.props.data.lon, this.props.data.lat]
                    },
                    properties: {}
                  }
                ]
              }
            });

            this.map.addLayer({
              id: "job",
              type: "symbol",
              source: "job",
              layout: {
                "icon-image": "job",
                "icon-size": 1,
                "icon-allow-overlap": true,
                "icon-rotation-alignment": "map"
              }
            });
          }
        );

        this.map.loadImage(
          require("src/assets/images/marker/gear_midpoint.png"),
          (err: any, img: HTMLImageElement) => {
            this.map.addImage("mid", img);

            this.map.addSource("mid", {
              type: "geojson",
              data: {
                type: "FeatureCollection",
                features:
                  this.props.midPoints.length > 0
                    ? ([
                        {
                          type: "Feature",
                          geometry: {
                            type: "Point",
                            coordinates: [
                              this.props.midPoints[0].lon,
                              this.props.midPoints[0].lat
                            ]
                          },
                          properties: {}
                        }
                      ] as Array<Feature<Point>>)
                    : ([] as Array<Feature<Point>>)
              }
            });

            this.map.addLayer({
              id: "mid",
              type: "symbol",
              source: "mid",
              layout: {
                "icon-image": "mid",
                "icon-size": 1,
                "icon-allow-overlap": true,
                "icon-rotation-alignment": "map"
              }
            });
          }
        );
      });
    }
  }

  private showVLOS = (jobId: string, mediaId: string) => () => {
    this.props.opVlos(jobId, mediaId);
  };

  public render() {
    const { classes, data, midPoints, siteAddress } = this.props;

    if (!midPoints || midPoints.length === 0) {
      return (
        <div className={classes.wrap}>
          <Typography>Not found any mid point.</Typography>
        </div>
      );
    }

    const selectedBendMidPoint = midPoints.filter(m => {
      return m.bendName === this.state.selectedBend;
    })[this.state.selectedMidPoint];

    const selectedBend = data.bends.find(
      b => b.bendId === selectedBendMidPoint.bendSiteId
    ) as IBend;

    return (
      <div className={classes.wrap}>
        <ConfirmDialog
          disabled={this.state.isEditing}
          isOpen={this.state.isOpenConfirm}
          content={this.state.confirmText}
          confirmText="Delete"
          onClose={this.handleCloseConfirm}
        />
        {this.state.reviewImg && (
          <Lightbox
            toolbarButtons={[
              <DownloadFile url={this.state.reviewImg} key="download" />
            ]}
            mainSrc={this.state.reviewImg}
            onCloseRequest={this.onCloseImgReview}
          />
        )}
        <Grid container={true} spacing={16}>
          <Grid item={true} md={3}>
            <Paper className={classes.bendControl}>
              <Typography variant="title" className={classes.title}>
                B-End
              </Typography>
              <Divider />
              <div className={classes.bend}>
                <TextField
                  fullWidth={true}
                  onChange={this.onChangeBend}
                  select={true}
                  value={this.state.selectedBend}
                >
                  {this.state.bends.map(m => {
                    return (
                      <MenuItem key={m} value={m}>
                        {m}
                      </MenuItem>
                    );
                  })}
                </TextField>
              </div>
            </Paper>
            <Paper>
              <Typography variant="title" className={classes.title}>
                Midpoints
              </Typography>
              <Divider />
              <List component="nav">
                {midPoints
                  .filter(m => {
                    return m.bendName === this.state.selectedBend;
                  })
                  .map((b, index) => (
                    <ListItem
                      className={
                        index === this.state.selectedMidPoint
                          ? classes.selected
                          : ""
                      }
                      key={b.id}
                      button={true}
                      onClick={this.onChangeMidPoint(index)}
                    >
                      {moment
                        .utc(b.createDate)
                        .local()
                        .format("DD/MM/YYYY - H:mm:ss")}
                    </ListItem>
                  ))}
              </List>
            </Paper>
          </Grid>
          <Grid item={true} md={9}>
            <Paper className={classes.infoSection}>
              <Grid container={true}>
                <Grid item={true} md={6}>
                  <div>
                    <div className={`map ${classes.mapDisplay}`}>
                      <div className="map-inner" ref={this.mapContainer} />
                    </div>
                  </div>
                </Grid>
                <Grid item={true} md={6}>
                  <div>
                    {selectedBendMidPoint ? (
                      <FresnelChart
                        classesOverride={{
                          progress: classes.progress
                        }}
                        key={selectedBendMidPoint.id}
                        terrainData={selectedBendMidPoint.terrainJson}
                        id={selectedBend.bendId}
                        frequency={data.frequency}
                        heightOfStart={selectedBendMidPoint.aendHeight}
                        heightOfEnd={selectedBendMidPoint.bendHeight}
                        distanceMidPoint={distance(
                          point([
                            selectedBendMidPoint.lon,
                            selectedBendMidPoint.lat
                          ]),
                          point([data.lon, data.lat]),
                          {
                            units: "meters"
                          }
                        )}
                        aEndLat={data.lat}
                        aEndLon={data.lon}
                        bEndLat={selectedBend.lat}
                        bEndLon={selectedBend.lon}
                        distanceFromAendToBend={selectedBend.distance}
                      />
                    ) : (
                      <FresnelChart
                        id={selectedBend.bendId}
                        frequency={data.frequency}
                        heightOfStart={data.recommendHeight}
                        aEndLat={data.lat}
                        aEndLon={data.lon}
                        bEndLat={selectedBend.lat}
                        bEndLon={selectedBend.lon}
                        distanceFromAendToBend={selectedBend.distance}
                      />
                    )}
                    <div className={classes.midPoint}>
                      <Typography variant="title">MidPoint</Typography>
                      <dl className={`${classes.list} ${classes.listInfo}`}>
                        <dt>Address:</dt>
                        <dd>{siteAddress[selectedBendMidPoint.id] || "..."}</dd>
                        <dt>Location:</dt>
                        <dd>
                          {cutCoordinate(selectedBendMidPoint.lon)},{" "}
                          {cutCoordinate(selectedBendMidPoint.lat)}
                        </dd>
                        <dt>Height:</dt>
                        <dd>{selectedBendMidPoint.height.toFixed(1)}</dd>
                      </dl>
                    </div>
                  </div>
                </Grid>
              </Grid>
              <Grid container={true} className={classes.info} spacing={32}>
                <Grid item={true} md={6} className={classes.border}>
                  <div>
                    <Typography variant="title">
                      A-END: {data.jobName}
                    </Typography>
                    <dl className={`${classes.list} ${classes.listInfo}`}>
                      <dt>Address:</dt>
                      <dd>{data.address}</dd>
                      <dt>Location:</dt>
                      <dd>
                        {cutCoordinate(data.lon)}, {cutCoordinate(data.lat)}
                      </dd>
                      <dt>Height:</dt>
                      <dd>{selectedBendMidPoint.aendHeight.toFixed(1)}</dd>
                    </dl>
                  </div>
                  {selectedBendMidPoint.aendMedias.length > 0 && (
                    <Grid container={true} spacing={8}>
                      {selectedBendMidPoint.aendMedias.map(m => (
                        <Grid item={true} xl={6} key={m.mediaId}>
                          <Paper className={classes.capSection}>
                            <div className={classes.capImg}>
                              <img
                                onClick={this.onClickImgReview(
                                  m.losUrl || m.url
                                )}
                                src={m.thumbnailUrl}
                              />
                            </div>
                            <div className={classes.control}>
                              <div>
                                <Button
                                  color="primary"
                                  variant="raised"
                                  size="small"
                                  disabled={this.state.isEditing}
                                  onClick={this.showVLOS(data.jobId, m.mediaId)}
                                >
                                  <EditIcon className={classes.iconEdit} />
                                  <span>Edit</span>
                                </Button>
                              </div>
                              <div className={classes.left}>
                                <IconButton
                                  onClick={this.onClickDelete(m.mediaId)}
                                >
                                  <IconDelete />
                                </IconButton>
                              </div>
                            </div>

                            <VirtualLOS
                              jobId={data.jobId}
                              bends={[
                                getAendFromMid(selectedBendMidPoint, data)
                              ]}
                              media={m}
                              selected={
                                getAendFromMid(selectedBendMidPoint, data)
                                  .bendId
                              }
                              frequency={selectedBendMidPoint.frequency}
                              isAend={true}
                            />
                          </Paper>
                        </Grid>
                      ))}
                    </Grid>
                  )}
                </Grid>
                <Grid item={true} md={6}>
                  <div>
                    <Typography variant="title">
                      B-END: {selectedBend.bendSiteName}
                    </Typography>
                    <dl className={`${classes.list} ${classes.listInfo}`}>
                      <dt>Address:</dt>
                      <dd>
                        {selectedBend.address ||
                          siteAddress[selectedBend.bendId] ||
                          "..."}
                      </dd>
                      <dt>Location:</dt>
                      <dd>
                        {cutCoordinate(selectedBend.lon)},{" "}
                        {cutCoordinate(selectedBend.lat)}
                      </dd>
                      <dt>Height:</dt>
                      <dd>{selectedBendMidPoint.bendHeight.toFixed(1)}</dd>
                    </dl>
                  </div>
                  {selectedBendMidPoint.bendMedias.length > 0 && (
                    <Grid container={true} spacing={8}>
                      {selectedBendMidPoint.bendMedias.map(m => (
                        <Grid item={true} xl={6} key={m.mediaId}>
                          <Paper className={classes.capSection}>
                            <div className={classes.capImg}>
                              <img
                                onClick={this.onClickImgReview(
                                  m.losUrl || m.url
                                )}
                                src={m.thumbnailUrl}
                              />
                            </div>
                            <div className={classes.control}>
                              <div>
                                <Button
                                  color="primary"
                                  variant="raised"
                                  size="small"
                                  disabled={this.state.isEditing}
                                  onClick={this.showVLOS(data.jobId, m.mediaId)}
                                >
                                  <EditIcon className={classes.iconEdit} />
                                  <span>Edit</span>
                                </Button>
                              </div>
                              <div className={classes.left}>
                                <IconButton
                                  onClick={this.onClickDelete(m.mediaId)}
                                >
                                  <IconDelete />
                                </IconButton>
                              </div>
                            </div>

                            <VirtualLOS
                              jobId={data.jobId}
                              bends={[getBendFromMid(selectedBendMidPoint)]}
                              media={m}
                              selected={
                                getBendFromMid(selectedBendMidPoint).bendId
                              }
                              frequency={selectedBendMidPoint.frequency}
                            />
                          </Paper>
                        </Grid>
                      ))}
                    </Grid>
                  )}
                </Grid>
              </Grid>
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(JobMidPoint);
