import * as React from "react";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import Typography from "@material-ui/core/Typography";
import SectionLoading from "src/components/SectionLoading";
import IStyleProps from "../../../../styles/utils";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { IJobLogFile, IJobLogResult } from "../../types/Job";
import TableHead from "@material-ui/core/TableHead/TableHead";
import TableRow from "@material-ui/core/TableRow/TableRow";
import TableCell from "@material-ui/core/TableCell/TableCell";
import TableBody from "@material-ui/core/TableBody/TableBody";
import Table from "@material-ui/core/Table/Table";
import PaginationMui from "../../../../components/Pagination/PaginationMUI";
import Grid from "@material-ui/core/Grid";
import Paper from "@material-ui/core/Paper";
import Divider from "@material-ui/core/Divider/Divider";
import List from "@material-ui/core/List/List";
import ListItem from "@material-ui/core/ListItem/ListItem";
import * as moment from "moment";
// import axios from "axios";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      padding: theme.spacing.unit * 2
    },
    row: {
      cursor: "pointer",
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.divider
      }
    },
    rowHeaded: {
      backgroundColor: "#181A1F",

      "& th": {
        color: theme.palette.common.white
      }
    },
    paging: {
      paddingTop: theme.spacing.unit * 2,
      textAlign: "center"
    },
    title: {
      textAlign: "center",
      padding: theme.spacing.unit
    },
    date: {
      width: 280
    },
    selected: {
      backgroundColor: "rgba(0, 0, 0, 0.08)"
    }
  });

export interface IJobLogOwnProps {
  id: string;
  selectedLib: string;
}

export interface IJobLogStateProps {
  data: null | IJobLogResult;
  logs: { [k: string]: IJobLogFile };
}

export interface IJobLogDispatchProps {
  getLog: () => void;
  getLogFile: (id: string, url: string) => void;
}

interface IJobLogState {
  select: number;
  take: number;
  skip: number;
}

class JobLog extends React.Component<
  IStyleProps & IJobLogStateProps & IJobLogOwnProps & IJobLogDispatchProps,
  IJobLogState
> {
  public state = {
    select: 0,
    take: 10,
    skip: 0
  };

  public componentDidMount() {
    this.props.getLog();
  }

  private onChangePagination = (page: number) => {
    this.setState({
      skip: (page - 1) * 10
    });
  };

  private changeLog(url: string, i: number) {
    return () => {
      if (this.props.data) {
        this.setState({
          select: i,
          take: 10,
          skip: 0
        });
        this.props.getLogFile(
          this.props.data.data[i].id,
          this.props.data.data[i].fileUrl
        );
      }
    };
  }

  public componentDidUpdate(
    prevProps: IJobLogStateProps & IJobLogOwnProps & IJobLogDispatchProps
  ) {
    if (
      (!prevProps.data && this.props.data) ||
      (prevProps.data &&
        this.props.data &&
        prevProps.data.lastModified !== this.props.data.lastModified)
    ) {
      if (this.props.data.data.length > 0) {
        this.setState({
          select: 0,
          take: 10,
          skip: 0
        });
        this.props.getLogFile(
          this.props.data.data[0].id,
          this.props.data.data[0].fileUrl
        );
      }
    }
  }

  public render() {
    if (!this.props.data) {
      return <SectionLoading />;
    }
    const {
      classes,
      data: { isFetching, data },
      logs
    } = this.props;

    const currentLogId = data[this.state.select]
      ? data[this.state.select].id
      : "";
    const currentLog =
      currentLogId && logs[currentLogId] ? logs[currentLogId] : null;

    return (
      <div className={classes.wrap}>
        {data.length === 0 && !isFetching && <Typography>No log</Typography>}
        {isFetching && <SectionLoading />}
        {data.length !== 0 && (
          <div>
            <Grid container={true} spacing={32}>
              <Grid item={true} md={3}>
                <Paper>
                  <Typography variant="title" className={classes.title}>
                    List Log
                  </Typography>
                  <Divider />
                  <List component="nav">
                    {data.map((d, i) => (
                      <ListItem
                        className={
                          i === this.state.select ? classes.selected : ""
                        }
                        key={d.id}
                        button={i !== this.state.select}
                        onClick={
                          i !== this.state.select
                            ? this.changeLog(d.fileUrl, i)
                            : undefined
                        }
                      >
                        {moment
                          .utc(d.createdDateUtc)
                          .local()
                          .format("DD/MM/YYYY - H:mm:ss")}
                      </ListItem>
                    ))}
                  </List>
                </Paper>
              </Grid>
              <Grid item={true} md={9}>
                {currentLog && currentLog.isFetching && <SectionLoading />}
                {currentLog &&
                  !currentLog.isFetching && (
                    <Paper>
                      <Table>
                        <TableHead>
                          <TableRow className={classes.rowHeaded}>
                            <TableCell className={classes.date}>Date</TableCell>
                            <TableCell>Action</TableCell>
                          </TableRow>
                        </TableHead>
                        <TableBody>
                          {currentLog.data
                            .slice(
                              this.state.skip,
                              this.state.skip === 0
                                ? this.state.take
                                : this.state.take + this.state.skip
                            )
                            .map((d, i) => (
                              <TableRow
                                className={classes.row}
                                key={i + d.join("|")}
                              >
                                <TableCell>
                                  {moment
                                    .utc(d[0])
                                    .local()
                                    .format("DD/MM/YYYY - H:mm:ss")}
                                </TableCell>
                                <TableCell>{d[d.length - 1]}</TableCell>
                              </TableRow>
                            ))}
                        </TableBody>
                      </Table>
                    </Paper>
                  )}

                {currentLog &&
                  !currentLog.isFetching && (
                    <div className={classes.paging}>
                      <PaginationMui
                        onChangePage={this.onChangePagination}
                        disabled={isFetching}
                        start={this.state.skip / this.state.take + 1}
                        display={this.state.take}
                        total={Math.ceil(
                          currentLog
                            ? currentLog.data.length / this.state.take
                            : 0
                        )}
                      />
                    </div>
                  )}
              </Grid>
            </Grid>
          </div>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(JobLog);
