import * as React from "react";
import Tabs from "@material-ui/core/Tabs";
import Button from "@material-ui/core/Button";
import Tab from "@material-ui/core/Tab";
import Paper from "@material-ui/core/Paper";
import Typography from "@material-ui/core/Typography";
import SectionLoading from "src/components/SectionLoading";
import {
  IJobDetail,
  IJobDetailBEnd,
  IMidPoint,
  MediaStatus
} from "../../types/Job";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import IStyleProps from "../../../../styles/utils";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import * as Loadable from "react-loadable";
import grey from "@material-ui/core/colors/grey";
import amber from "@material-ui/core/colors/amber";
import green from "@material-ui/core/colors/green";
import { IJobCandidate, ILibUser, JobStatus } from "../../../lib/types/libs";
import * as classNames from "classnames";
import {
  DeleteJobDetailBEndType,
  ICreateMidPointData,
  IDeleteJobDetailBEndData,
  IEditJobData
} from "../../actions/Job";
import { Omit } from "utility-types";
import DownloadIcon from "@material-ui/icons/FileDownload";
import JobExportReview from "./JobExportReview";
import Variant from "../../../../components/notification/types/variant";

const LoadableTabInfo = Loadable({
  loader: () => import("./JobInfo"),
  loading: SectionLoading
});

const LoadableTabBEnds = Loadable({
  loader: () => import("../../containers/JobBEnds"),
  loading: SectionLoading
});

const LoadableTabMidPoint = Loadable({
  loader: () => import("../../containers/JobMidPoint"),
  loading: SectionLoading
});

const LoadableTabLog = Loadable({
  loader: () => import("../../containers/JobLog"),
  loading: SectionLoading
});

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      padding: theme.spacing.unit * 2
    },
    title: {
      display: "flex",
      alignItems: "center",
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,

      "& > img": {
        marginRight: theme.spacing.unit * 2
      }
    },
    content: {
      paddingTop: theme.spacing.unit * 2
    },
    contentHead: {
      display: "flex"
    },
    export: {
      display: "flex",
      paddingRight: theme.spacing.unit * 2,
      color: theme.palette.common.white,
      marginLeft: "auto",

      "& button": {
        marginLeft: theme.spacing.unit
      }
    },
    finish: {
      color: theme.palette.common.white
    },
    tabs: {
      marginTop: theme.spacing.unit * 2,
      backgroundColor: "#0096DA",
      color: theme.palette.common.white
    },
    jobStatus: {
      display: "inline-block",
      fontSize: "12px",
      padding: "5px",
      borderRadius: "5px",
      color: theme.palette.common.white,
      lineHeight: "1.1875em",
      marginRight: theme.spacing.unit
    },
    jobStatusCreated: {
      backgroundColor: grey[500]
    },
    jobStatusProcessing: {
      backgroundColor: amber[500]
    },
    jobStatusFinished: {
      backgroundColor: green[500]
    }
  });

interface IStates {
  selectedTab: number;
  isSubmitExport: boolean;
  isSubmitFinish: boolean;
  isOpenExportReview: boolean;
  exportReviewUrl: string;
}

interface IProps {
  id: string;
  engineers: ILibUser[];
  selectedLib: string;
  getJobDetail: (id: string) => void;
  isLoading: boolean;
  data?: IJobDetail;
  bends: IJobDetailBEnd[];
  midPoints: IMidPoint[];
  editJobDetail: (
    data: IEditJobData,
    candidates: IJobCandidate[],
    done: (reset: boolean) => void
  ) => void;
  deleteJobDetailBEnd: (data: IDeleteJobDetailBEndData, cb: () => void) => void;
  createMidPoint: (
    data: ICreateMidPointData,
    done: (success: boolean) => void
  ) => void;
  exportJob: (
    id: string,
    done?: (success: boolean, url: string) => void
  ) => void;
  finishJob: (id: string, done?: (success: boolean) => void) => void;
  notify: (variant: Variant, message: string) => void;
}

class JobDetail extends React.Component<IStyleProps & IProps, IStates> {
  public state = {
    exportReviewUrl: "",
    selectedTab: 0,
    isSubmitExport: false,
    isSubmitFinish: false,
    isOpenExportReview: false
  };

  private onChangeTab = (event: React.ChangeEvent<{}>, value: any) => {
    this.setState({
      selectedTab: value
    });
  };

  private onCloseExportReview = () => {
    this.setState({
      isOpenExportReview: false
    });
  };

  private onChangeEngineer = (id: string, cb: () => void) => {
    const data = this.props.data;
    if (data) {
      this.props.editJobDetail(
        {
          id: data.jobId,
          engineerId: id,
          jobName: data.jobName,
          frequency: data.frequency,
          recommendHeight: data.recommendHeight
        },
        [],
        cb
      );
    }
  };

  private deleteMedia = (
    type: DeleteJobDetailBEndType,
    id: string,
    height: number,
    bendId: string,
    cb: () => void
  ) => {
    if (this.props.data) {
      this.props.deleteJobDetailBEnd(
        {
          type,
          id,
          height,
          bendId,
          jobId: this.props.data.jobId
        },
        cb
      );
    }
  };

  private createMidPoint = (
    data: Omit<ICreateMidPointData, "aendSiteId" | "aendAddress">,
    done: (success: true) => void
  ) => {
    if (this.props.data) {
      this.props.createMidPoint(
        {
          ...data,
          aendSiteId: this.props.data.jobId,
          aendAddress: this.props.data.address
        },
        done
      );
    }
  };

  private export = () => {
    if (this.props.data) {
      let haveMedia = false;
      this.props.bends.forEach(b => {
        b.media.forEach(m => {
          m.media.forEach(me => {
            if (me.mediaStatus === MediaStatus.Released) {
              haveMedia = true;
            }
          });
        });
      });

      if (haveMedia) {
        this.setState({
          isSubmitExport: true
        });
        this.props.exportJob(this.props.data.jobId, (success, url) => {
          if (success) {
            this.setState({
              isOpenExportReview: true,
              exportReviewUrl: url,
              isSubmitExport: false
            });
          } else {
            this.setState({
              isSubmitExport: false
            });
          }
        });
      } else {
        this.props.notify(
          Variant.ERROR,
          "Must have at least one released media"
        );
      }
    }
  };

  private finishJob = () => {
    if (this.props.data) {
      this.setState({
        isSubmitFinish: true
      });
      this.props.finishJob(this.props.data.jobId, () => {
        this.setState({
          isSubmitFinish: false
        });
      });
    }
  };

  public componentDidMount() {
    this.props.getJobDetail(this.props.id);
  }

  public render() {
    const {
      selectedLib,
      isLoading,
      classes,
      data,
      bends,
      engineers,
      midPoints
    } = this.props;
    const { selectedTab } = this.state;
    return (
      <div className={classes.wrap}>
        {this.state.isOpenExportReview && (
          <JobExportReview
            onClose={this.onCloseExportReview}
            url={this.state.exportReviewUrl}
          />
        )}
        {isLoading && <SectionLoading />}
        {data && (
          <Paper className={classes.content}>
            <div className={classes.contentHead}>
              <div>
                <div className={classes.title}>
                  <span
                    className={classNames(
                      classes.jobStatus,
                      {
                        [classes.jobStatusFinished]:
                          data.jobStatus === JobStatus.Finished
                      },
                      {
                        [classes.jobStatusProcessing]:
                          data.jobStatus === JobStatus.Processing
                      },
                      {
                        [classes.jobStatusCreated]:
                          data.jobStatus === JobStatus.Created
                      }
                    )}
                  >
                    {JobStatus[data.jobStatus]}
                  </span>
                  <Typography variant="title">
                    <b>Job {data.jobName}</b>
                  </Typography>
                </div>
              </div>
              <div className={classes.export}>
                <Button
                  onClick={this.export}
                  className={classes.upload}
                  variant={"raised"}
                  color="primary"
                  disabled={this.state.isSubmitExport}
                >
                  <DownloadIcon />
                  <span>Export</span>
                </Button>

                {data.jobStatus === JobStatus.Processing && (
                  <Button
                    disabled={this.state.isSubmitFinish}
                    onClick={this.finishJob}
                    variant="raised"
                    color="secondary"
                    className={classes.finish}
                  >
                    Finish Job
                  </Button>
                )}
              </div>
            </div>
            <Tabs
              onChange={this.onChangeTab}
              value={selectedTab}
              className={classes.tabs}
              scrollable={true}
              scrollButtons="auto"
            >
              <Tab label="Info" />
              <Tab label="B-Ends" />
              <Tab label="Mid-Points" />
              <Tab label="Log" />
            </Tabs>
            {selectedTab === 0 && (
              <LoadableTabInfo
                onChangeEngineer={this.onChangeEngineer}
                engineers={engineers}
                address={data.address}
                height={data.recommendHeight}
                engineer={data.engineerId}
                frequency={data.frequency}
              />
            )}
            {selectedTab === 1 &&
              bends && (
                <LoadableTabBEnds
                  id={this.props.id}
                  createMidPoint={this.createMidPoint}
                  isLoading={isLoading}
                  deleteMedia={this.deleteMedia}
                  detailBends={bends}
                  lon={data.lon}
                  lat={data.lat}
                  height={data.recommendHeight}
                  bends={data.bends}
                  frequency={data.frequency}
                />
              )}
            {selectedTab === 2 &&
              midPoints && (
                <LoadableTabMidPoint
                  id={this.props.id}
                  midPoints={midPoints}
                  data={data}
                />
              )}

            {selectedTab === 3 &&
              midPoints && (
                <LoadableTabLog selectedLib={selectedLib} id={this.props.id} />
              )}
          </Paper>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(JobDetail);
