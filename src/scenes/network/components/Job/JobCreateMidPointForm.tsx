import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { createStyles, Theme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import { HOCForm, FormValidateChildProps } from "react-hoc-form-validatable";
import IStyleProps from "src/styles/utils";
import InputValidate from "src/components/InputValidate";
import Grid from "@material-ui/core/Grid";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      padding: theme.spacing.unit,
      margin: "0 auto"
    },
    title: { display: "flex", alignItems: "center" },
    form: {
      paddingTop: theme.spacing.unit
    },
    actions: {
      marginTop: theme.spacing.unit * 2,
      textAlign: "center"
    },
    submit: {
      marginLeft: theme.spacing.unit
    }
  });

interface IProps {
  onChangeData: (name: string) => (e: React.ChangeEvent) => void;
  frequency: number;
  heightOfStart: number;
  heightOfEnd: number;
  onCancel: () => void;
}

class JobCreateMidPointForm extends React.Component<
  IStyleProps & IProps & FormValidateChildProps
> {
  public render() {
    const {
      classes,
      onSubmit,
      submitted,
      frequency,
      heightOfStart,
      heightOfEnd,
      onChangeData,
      onCancel
    } = this.props;
    return (
      <div className={classes.wrap}>
        <form className={classes.form} noValidate={true} onSubmit={onSubmit}>
          <Grid container={true} spacing={16}>
            <Grid item={true} md={6}>
              <InputValidate
                onChange={onChangeData("jobHeight")}
                defaultValue={heightOfStart || 0}
                type="number"
                rule="notEmpty"
                name="height"
                margin="normal"
                required={true}
                label="Job height (m)"
                InputLabelProps={{
                  shrink: true
                }}
                fullWidth={true}
              />

              <InputValidate
                onChange={onChangeData("frequency")}
                defaultValue={frequency || 0}
                type="number"
                rule="notEmpty"
                name="frequency"
                margin="normal"
                required={true}
                label="Frequency (MHz)"
                InputLabelProps={{
                  shrink: true
                }}
                fullWidth={true}
              />
            </Grid>
            <Grid item={true} md={6}>
              <InputValidate
                onChange={onChangeData("siteHeight")}
                defaultValue={heightOfEnd || 0}
                rule="notEmpty"
                disabled={submitted}
                name="siteHeight"
                type="number"
                margin="normal"
                label="Site height (m)"
                required={true}
                InputLabelProps={{
                  shrink: true
                }}
                fullWidth={true}
              />
            </Grid>
          </Grid>
          <div className={classes.actions}>
            <Button
              onClick={onCancel}
              variant="raised"
              type="button"
              disabled={submitted}
            >
              Cancel
            </Button>
            <Button
              className={classes.submit}
              variant="raised"
              color="primary"
              type="submit"
              disabled={submitted}
            >
              Submit
            </Button>
          </div>
        </form>
      </div>
    );
  }
}
export default withStyles(styles)(
  HOCForm<IStyleProps & IProps>(JobCreateMidPointForm)
);
