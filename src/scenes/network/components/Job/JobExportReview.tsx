import * as React from "react";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import IStyleProps from "../../../../styles/utils";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import DialogTitle from "@material-ui/core/DialogTitle";
import Dialog from "@material-ui/core/Dialog";
import IconButton from "@material-ui/core/IconButton";
import CloseIcon from "@material-ui/icons/Close";
import FileDownloadIcon from "@material-ui/icons/FileDownload";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      position: "relative",
      maxWidth: "1600px",
      width: "100%",
      height: "85vh"
    },
    docLoad: {
      width: "100%",
      height: "100%"
    },
    closeIcon: {
      position: "absolute",
      top: 10,
      right: 10
    },
    download: {
      position: "absolute",
      top: 10,
      right: 50
    }
  });

interface IProps {
  url: string;
  onClose: () => void;
}

class JobExportReview extends React.Component<IStyleProps & IProps> {
  private download = () => {
    window.open(this.props.url, "_blank");
  };

  public render() {
    const { classes, url, onClose } = this.props;
    return (
      <Dialog
        open={true}
        classes={{
          paper: classes.wrap
        }}
      >
        <IconButton className={classes.download} onClick={this.download}>
          <FileDownloadIcon />
        </IconButton>
        <IconButton className={classes.closeIcon} onClick={onClose}>
          <CloseIcon />
        </IconButton>
        <DialogTitle id="confirmation-dialog-title">
          Review document
        </DialogTitle>
        <iframe
          className={classes.docLoad}
          src={`https://docs.google.com/gview?url=${url}&embedded=true`}
        />
      </Dialog>
    );
  }
}

export default withStyles(styles)(JobExportReview);
