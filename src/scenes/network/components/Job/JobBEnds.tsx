import * as React from "react";
import * as mapboxgl from "mapbox-gl";
import "react-image-lightbox/style.css";
import Lightbox from "react-image-lightbox";
import "mapbox-gl/dist/mapbox-gl.css";
import IconDelete from "@material-ui/icons/Delete";
import Backdrop from "@material-ui/core/Backdrop";
import Paper from "@material-ui/core/Paper";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import Dialog from "@material-ui/core/Dialog";
import TextField from "@material-ui/core/TextField";
import IconButton from "@material-ui/core/IconButton";
import Select from "@material-ui/core/Select";
import InputAdornment from "@material-ui/core/InputAdornment";
import MenuItem from "@material-ui/core/MenuItem";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import IStyleProps from "../../../../styles/utils";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { IBend, IJobDetailBEnd, MediaStatus } from "../../types/Job";
import {
  DeleteJobDetailBEndType,
  ICreateMidPointData
} from "../../actions/Job";
import { Feature, Point } from "geojson";
import ConfirmDialog from "../../../../components/Dialog/Confirm";
import VirtualLOS from "./VirtualLOS";
import JobCreateMidPoint from "./JobCreateMidPoint";
import { Omit } from "utility-types";
import BackIcon from "@material-ui/icons/ArrowBack";
import { IGetSiteAddressData } from "../../actions/Site";
import DownloadFile from "src/components/LightBox/DownloadFile";
import Switch from "@material-ui/core/Switch";
import FormControlLabel from "@material-ui/core/FormControlLabel";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      position: "relative",
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2
    },
    list: {
      margin: 0,
      display: "flex",
      flexWrap: "wrap",

      "& > dt": {
        fontWeight: "bold",
        flexBasis: "50%",
        maxWidth: "50%",
        margin: "0",
        paddingBottom: "8px",
        paddingTop: "8px"
      },

      "& > dd": {
        flexBasis: "50%",
        maxWidth: "50%",
        textAlign: "right",
        margin: "0",
        paddingBottom: "8px",
        paddingTop: "8px",
        paddingRight: "16px"
      }
    },
    progress: {
      position: "static"
    },
    cap: {
      padding: theme.spacing.unit * 2,
      borderLeft: `1px solid ${theme.palette.divider}`
    },
    capHead: {
      paddingBottom: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2,
      display: "flex",
      borderBottom: `1px solid ${theme.palette.divider}`
    },
    capImg: {
      position: "relative",
      "& > img": {
        cursor: "pointer",
        width: "100%"
      }
    },
    capImgBackDrop: {
      zIndex: 1,
      position: "absolute"
    },
    capImgProcess: {
      color: theme.palette.common.white,
      zIndex: 1,
      position: "absolute",
      left: "50%",
      top: "50%",
      transform: "translate(-50%, -50%)"
    },
    capSection: {
      paddingTop: theme.spacing.unit,
      paddingLeft: theme.spacing.unit,
      paddingRight: theme.spacing.unit
    },
    delete: {
      color: theme.palette.error.main,
      marginLeft: "auto"
    },
    control: {
      display: "flex",
      alignItems: "center"
    },
    left: {
      marginLeft: "auto"
    },
    site: {
      paddingRight: theme.spacing.unit,
      height: "100%",
      display: "flex",
      flexDirection: "column"
    },
    mapDisplay: {
      marginTop: 8,
      height: 300
    },
    backdrop: {
      top: "0",
      left: "0",
      width: "100%",
      height: "100%",
      zIndex: 100,
      position: "absolute",
      backgroundColor: "rgba(0, 0, 0, 0.5)"
    },
    createMidPoint: {
      margin: "0 auto",
      padding: theme.spacing.unit
    },
    createMidPointDialog: {
      width: "100%",
      maxWidth: "calc(100% - 32px)",
      maxHeight: "calc(100% - 100px)",
      top: 40
    },
    draft: {
      marginLeft: -4
    }
  });

interface IDeleteData {
  type: DeleteJobDetailBEndType;
  id: string;
  height: number;
  bendId: string;
}

interface IStates {
  selectedHeight: number;
  selectedBend: string;
  isLoadedMap: boolean;
  reviewImg: string;
  isEditing: boolean;
  isOpenConfirm: boolean;
  deleteData: IDeleteData | null;
  confirmText: string;
  vLosId: string | null;
  isOpenCreateMidPoint: boolean;
  changingMediaStatus: string[];
}

interface IProps {
  id: string;
  isLoading: boolean;
  detailBends: IJobDetailBEnd[];
  bends: IBend[];
  frequency: number;
  height: number;
  lon: number;
  lat: number;
  deleteMedia: (
    type: DeleteJobDetailBEndType,
    id: string,
    height: number,
    bendId: string,
    cb: () => void
  ) => void;
  opVlos: (jobId: string, mediaId: string) => void;
  createMidPoint: (
    data: Omit<ICreateMidPointData, "aendSiteId" | "aendAddress">,
    done: (success: boolean) => void
  ) => void;
  siteAddress: {
    [k: string]: string;
  };
  getSiteAddress: (data: IGetSiteAddressData) => void;
  changeJobMediaStatus: (
    id: string,
    jobId: string,
    status: MediaStatus,
    done?: (success: boolean) => void
  ) => void;
}

class JobBEnds extends React.Component<IStyleProps & IProps, IStates> {
  private mapContainer: React.RefObject<HTMLDivElement>;
  private map: mapboxgl.Map;

  public state = {
    changingMediaStatus: [] as string[],
    isOpenCreateMidPoint: false,
    confirmText: "",
    deleteData: null,
    isEditing: false,
    isOpenConfirm: false,
    reviewImg: "",
    isLoadedMap: false,
    selectedBend: this.props.bends[0].bendId,
    selectedHeight:
      this.props.detailBends &&
      this.props.detailBends.length > 0 &&
      this.props.detailBends[0].media.length > 0
        ? this.props.detailBends[0].media[0].height
        : 0,
    vLosId: null
  };

  private onChangeHeight = (e: React.ChangeEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    this.setState({
      selectedHeight: parseFloat(target.value)
    });
  };

  private onChangeSite = (e: React.ChangeEvent<HTMLSelectElement>) => {
    const target = e.target as HTMLSelectElement;

    const selectedBend = this.props.bends.find(
      bend => bend.bendId === target.value
    ) as IBend;

    const selectedBendDetail = this.props.detailBends.find(
      b => b.bendId === selectedBend.bendId
    ) as IJobDetailBEnd;

    if (this.state.isLoadedMap) {
      this.setState(
        {
          selectedBend: target.value,
          selectedHeight: selectedBendDetail
            ? selectedBendDetail.media[0].height
            : this.state.selectedHeight
        },
        () => {
          const selectedSite = this.props.bends.find(
            bend => bend.bendId === this.state.selectedBend
          ) as IBend;

          if (
            !selectedSite.address &&
            !this.props.siteAddress[selectedSite.bendId]
          ) {
            this.props.getSiteAddress({
              id: selectedSite.bendId,
              lat: selectedSite.lat,
              lon: selectedSite.lon
            });
          }

          (this.map.getSource(
            "measure-lines"
          ) as mapboxgl.GeoJSONSource).setData({
            type: "Feature",
            geometry: {
              type: "LineString",
              coordinates: [
                [selectedSite.lon, selectedSite.lat],
                [this.props.lon, this.props.lat]
              ]
            },
            properties: {}
          });
        }
      );
    }
  };

  constructor(props: IStyleProps & IProps) {
    super(props);
    this.mapContainer = React.createRef<HTMLDivElement>();
  }

  public componentDidMount() {
    const selectedSite = this.props.bends.find(
      bend => bend.bendId === this.state.selectedBend
    ) as IBend;

    if (!selectedSite.address && !this.props.siteAddress[selectedSite.bendId]) {
      this.props.getSiteAddress({
        id: selectedSite.bendId,
        lat: selectedSite.lat,
        lon: selectedSite.lon
      });
    }

    if (this.mapContainer.current) {
      this.map = new mapboxgl.Map({
        container: this.mapContainer.current,
        style: "/3d_building_style.json",
        center: [this.props.lon, this.props.lat],
        zoom: 13
      });
    }

    this.map.on("load", () => {
      this.map.addControl(new mapboxgl.NavigationControl(), "top-left");

      this.setState({
        isLoadedMap: true
      });

      this.map.loadImage(
        require("src/assets/images/marker/ic_job.png"),
        (error: any, image: HTMLImageElement) => {
          this.map.addImage("job", image);

          this.map.addSource("measure-lines", {
            type: "geojson",
            data: {
              type: "Feature",
              geometry: {
                type: "LineString",
                coordinates: [
                  [this.props.lon, this.props.lat],
                  [this.props.bends[0].lon, this.props.bends[0].lat]
                ]
              },
              properties: {}
            }
          });

          this.map.addLayer({
            id: "measure-lines",
            type: "line",
            source: "measure-lines",
            layout: {
              "line-cap": "round",
              "line-join": "round"
            },
            paint: {
              "line-color": "#e25144",
              "line-width": 2.5,
              "line-opacity": 0.5
            },
            filter: ["in", "$type", "LineString"]
          });

          this.map.addSource("site", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: this.props.bends.map(b => ({
                type: "Feature",
                geometry: {
                  type: "Point",
                  coordinates: [b.lon, b.lat]
                },
                properties: {}
              })) as Array<Feature<Point>>
            }
          });

          this.map.addLayer({
            id: "site",
            type: "circle",
            source: "site",
            paint: {
              "circle-color": "#51bbd6",
              "circle-radius": 10
            }
          });

          this.map.addSource("job", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: [
                {
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: [this.props.lon, this.props.lat]
                  },
                  properties: {}
                }
              ]
            }
          });

          this.map.addLayer({
            id: "job",
            type: "symbol",
            source: "job",
            layout: {
              "icon-image": "job",
              "icon-size": 1,
              "icon-allow-overlap": true,
              "icon-rotation-alignment": "map"
            }
          });
        }
      );
    });
  }

  private onCloseImgReview = () => {
    this.setState({
      reviewImg: ""
    });
  };

  private onClickImgReview = (url: string) => {
    return () => {
      this.setState({
        reviewImg: url
      });
    };
  };

  private onClickDelete = (
    type: DeleteJobDetailBEndType,
    id: string,
    height: number,
    bendId: string
  ) => {
    return () => {
      this.setState({
        confirmText:
          type === DeleteJobDetailBEndType.Media
            ? "Do you want to delete this media?"
            : `Do you want to delete all media in ${height}m`,
        isOpenConfirm: true,
        deleteData: {
          type,
          id,
          height,
          bendId
        }
      });
    };
  };

  private handleCloseConfirm = (confirm: boolean) => {
    if (confirm) {
      this.setState(
        {
          isEditing: true
        },
        () => {
          if (this.state.deleteData) {
            // @ts-ignore
            const { type, bendId, height, id } = this.state.deleteData;

            if (typeof this.state.deleteData) {
              this.props.deleteMedia(type, id, height, bendId, () => {
                this.setState({
                  isEditing: false,
                  isOpenConfirm: false
                });
              });
            }
          }
        }
      );
    } else {
      this.setState({
        isOpenConfirm: false
      });
    }
  };

  private showVLOS = (jobId: string, mediaId: string) => () => {
    this.props.opVlos(jobId, mediaId);
  };

  private toggleCreateMidPoint = () => {
    this.setState({
      isOpenCreateMidPoint: !this.state.isOpenCreateMidPoint
    });
  };

  private onSubmit = (
    data: Omit<ICreateMidPointData, "aendSiteId" | "aendAddress">,
    done: (reset?: boolean) => void
  ) => {
    this.props.createMidPoint(data, (success: boolean) => {
      done(success);
      if (success) {
        this.setState({
          isOpenCreateMidPoint: false
        });
      }
    });
  };

  private handleChangeMediaStatus = (id: string, status: MediaStatus) => () => {
    const newState = this.state.changingMediaStatus.slice(0) as string[];

    if (newState.indexOf(id) < 0) {
      newState.push(id);
      this.setState({
        changingMediaStatus: newState
      });
      this.props.changeJobMediaStatus(id, this.props.id, status, () => {
        this.setState({
          changingMediaStatus: this.state.changingMediaStatus.filter(m => {
            return m !== id;
          })
        });
      });
    }
  };

  private renderValueHeight = (e: string | number) => {
    if (e === 0) {
      return "0";
    }
    return e;
  };

  private renderBendDetail = (detail: IJobDetailBEnd) => {
    const { classes } = this.props;

    const currentMedia = detail.media.find(
      m => m.height === this.state.selectedHeight
    );

    if (currentMedia) {
      return (
        <Grid item={true} md={7} className={classes.cap}>
          {this.state.reviewImg && (
            <Lightbox
              toolbarButtons={[
                <DownloadFile url={this.state.reviewImg} key="download" />
              ]}
              mainSrc={this.state.reviewImg}
              onCloseRequest={this.onCloseImgReview}
            />
          )}
          <div className={classes.capHead}>
            <TextField
              SelectProps={{
                renderValue: this.renderValueHeight
              }}
              onChange={this.onChangeHeight}
              label="height"
              select={true}
              InputProps={{
                endAdornment: <InputAdornment position="end">m</InputAdornment>
              }}
              value={
                this.state.selectedHeight === 0 ? 0 : this.state.selectedHeight
              }
            >
              {detail.media.map(m => {
                return (
                  <MenuItem key={m.height} value={m.height}>
                    {m.height}
                  </MenuItem>
                );
              })}
            </TextField>
            <IconButton
              className={classes.delete}
              onClick={this.onClickDelete(
                DeleteJobDetailBEndType.Height,
                "",
                this.state.selectedHeight,
                detail.bendId
              )}
              disabled={this.state.isEditing}
            >
              <IconDelete />
            </IconButton>
          </div>
          <Grid container={true} spacing={8}>
            {currentMedia.media.map(m => {
              let checkedDraft = false;
              let labelDraft = "";

              if (this.state.changingMediaStatus.indexOf(m.mediaId) > -1) {
                checkedDraft = m.mediaStatus === MediaStatus.Draft;
                labelDraft =
                  m.mediaStatus === MediaStatus.Draft ? "Released" : "Draft";
              } else {
                checkedDraft = m.mediaStatus === MediaStatus.Released;
                labelDraft =
                  m.mediaStatus === MediaStatus.Draft ? "Draft" : "Released";
              }

              return (
                <Grid key={m.mediaId} item={true} lg={6}>
                  <Paper className={classes.capSection}>
                    <div className={classes.capImg}>
                      {m.isProcessing && (
                        <div>
                          <Backdrop
                            open={true}
                            className={classes.capImgBackDrop}
                          />
                          <div className={classes.capImgProcess}>
                            Processing
                          </div>
                        </div>
                      )}
                      <img
                        onClick={this.onClickImgReview(
                          m.isLosProcessed ? m.losUrl : m.url
                        )}
                        src={m.thumbnailUrl}
                        alt={m.mediaId}
                      />
                    </div>
                    <div className={classes.control}>
                      <div>
                        <Button
                          color="primary"
                          variant="raised"
                          size="small"
                          disabled={this.state.isEditing}
                          onClick={this.showVLOS(detail.jobId, m.mediaId)}
                        >
                          Modify
                        </Button>
                        <VirtualLOS
                          jobId={detail.jobId}
                          bends={this.props.bends}
                          media={m}
                          selected={detail.bendId}
                          frequency={this.props.frequency}
                        />
                      </div>

                      <FormControlLabel
                        className={classes.draft}
                        disabled={
                          this.state.changingMediaStatus.indexOf(m.mediaId) > -1
                        }
                        control={
                          <Switch
                            onChange={this.handleChangeMediaStatus(
                              m.mediaId,
                              m.mediaStatus === MediaStatus.Released
                                ? MediaStatus.Draft
                                : MediaStatus.Released
                            )}
                            checked={checkedDraft}
                            value={m.mediaStatus.toString()}
                          />
                        }
                        label={labelDraft}
                      />

                      <div className={classes.left}>
                        <IconButton
                          disabled={this.state.isEditing}
                          className={classes.delete}
                          onClick={this.onClickDelete(
                            DeleteJobDetailBEndType.Media,
                            m.mediaId,
                            this.state.selectedHeight,
                            detail.bendId
                          )}
                        >
                          <IconDelete />
                        </IconButton>
                      </div>
                    </div>
                  </Paper>
                </Grid>
              );
            })}
          </Grid>
        </Grid>
      );
    }

    return null;
  };

  public render() {
    const {
      classes,
      bends,
      detailBends,
      isLoading,
      frequency,
      height,
      siteAddress
    } = this.props;
    const selectedBend = bends.find(
      bend => bend.bendId === this.state.selectedBend
    ) as IBend;

    const selectedBendDetail = detailBends.find(
      b => b.bendId === selectedBend.bendId
    );

    return (
      <div className={classes.wrap}>
        {isLoading ||
          (this.state.isEditing && (
            <Backdrop
              open={isLoading || this.state.isEditing}
              classes={{
                root: classes.backdrop
              }}
            />
          ))}
        <Grid container={true}>
          <Grid item={true} md={5}>
            <div className={classes.site}>
              <dl className={classes.list}>
                <dt>Site:</dt>
                <dd>
                  <Select
                    value={this.state.selectedBend}
                    onChange={this.onChangeSite}
                  >
                    {bends.map(bend => (
                      <MenuItem key={bend.bendId} value={bend.bendId}>
                        {bend.bendSiteName}
                      </MenuItem>
                    ))}
                  </Select>
                </dd>
                <dt>Address:</dt>
                <dd>
                  {selectedBend.address ||
                    siteAddress[selectedBend.bendId] ||
                    "..."}
                </dd>
                <dt>Distance:</dt>
                <dd>
                  {(selectedBend.distance / 1000).toFixed(2)}
                  km
                </dd>
              </dl>
              <div className={classes.createMidPoint}>
                <Button
                  variant="raised"
                  color="primary"
                  onClick={this.toggleCreateMidPoint}
                >
                  Create Mid Point
                </Button>
              </div>
              <div className={`map ${classes.mapDisplay}`}>
                <div className="map-inner" ref={this.mapContainer} />
              </div>
            </div>
          </Grid>
          {selectedBendDetail && this.renderBendDetail(selectedBendDetail)}
        </Grid>
        <ConfirmDialog
          disabled={this.state.isEditing}
          isOpen={this.state.isOpenConfirm}
          content={this.state.confirmText}
          confirmText="Delete"
          onClose={this.handleCloseConfirm}
        />
        <Dialog
          fullScreen={true}
          open={this.state.isOpenCreateMidPoint}
          fullWidth={true}
          classes={{
            paper: classes.createMidPointDialog
          }}
        >
          <DialogTitle>
            <IconButton onClick={this.toggleCreateMidPoint}>
              <BackIcon />
            </IconButton>
            <span>Create Mid-Point for site {selectedBend.bendSiteName}</span>
          </DialogTitle>
          <DialogContent>
            <JobCreateMidPoint
              onCancel={this.toggleCreateMidPoint}
              onSubmit={this.onSubmit}
              frequency={frequency}
              height={height}
              lon={this.props.lon}
              lat={this.props.lat}
              bend={selectedBend}
            />
          </DialogContent>
        </Dialog>
      </div>
    );
  }
}

export default withStyles(styles)(JobBEnds);
