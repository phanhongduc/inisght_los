import * as React from "react";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import IStyleProps from "../../../../styles/utils";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { ILibUser } from "../../../lib/types/libs";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      padding: theme.spacing.unit * 2
    },
    list: {
      display: "flex",
      flexWrap: "wrap",
      maxWidth: 440,

      "& > dt": {
        fontWeight: "bold",
        flexBasis: "50%",
        maxWidth: "50%",
        margin: "0",
        paddingBottom: "16px",
        paddingTop: "16px"
      },

      "& > dd": {
        flexBasis: "50%",
        maxWidth: "50%",
        textAlign: "left",
        margin: "0",
        paddingBottom: "16px",
        paddingTop: "16px"
      }
    }
  });

interface IStates {
  selectedEngineer: string;
  isEditing: boolean;
}

interface IProps {
  engineers: ILibUser[];
  address: string;
  height: number;
  frequency: number;
  engineer: string;
  onChangeEngineer: (id: string, cb: () => void) => void;
}

class JobInfo extends React.Component<IStyleProps & IProps, IStates> {
  public state = {
    selectedEngineer: this.props.engineer,
    isEditing: false
  };

  private onChangeEngineer = (e: React.SyntheticEvent<HTMLSelectElement>) => {
    const target = e.target as HTMLSelectElement;
    this.setState(
      {
        isEditing: true,
        selectedEngineer: target.value
      },
      () => {
        this.props.onChangeEngineer(target.value, () => {
          this.setState({
            isEditing: false
          });
        });
      }
    );
  };

  public render() {
    const { classes, address, height, frequency, engineers } = this.props;
    return (
      <div className={classes.wrap}>
        <dl className={classes.list}>
          <dt>Address:</dt>
          <dd>{address}</dd>
          <dt>Recommend Height :</dt>
          <dd>{height}m</dd>
          <dt>Frequency:</dt>
          <dd>{frequency / 1000000}MHz</dd>
          <dt>Engineer:</dt>
          <dd>
            <Select
              disabled={this.state.isEditing}
              value={this.state.selectedEngineer}
              onChange={this.onChangeEngineer}
            >
              {engineers.map(e => (
                <MenuItem value={e.userId} key={e.userId}>
                  {e.firstName} {e.lastName}
                </MenuItem>
              ))}
            </Select>
          </dd>
        </dl>
      </div>
    );
  }
}

export default withStyles(styles)(JobInfo);
