import { combineReducers } from "redux";
import { IBend } from "../../../../types/Job";
import {
  IVirtualLosSidebarSelectBEnd,
  IVirtualLosUpdateData
} from "../actions";
import {
  VIRTUAL_LOS_SIDEBAR_SELECT_B_END,
  VIRTUAL_LOS_SET_HEIGHT,
  VIRTUAL_LOS_SETUP_SAVE_DATA,
  VIRTUAL_LOS_UPDATE_SAVE_DATA
} from "../constants";
import {
  IVirtualLosDialogOpen,
  IVirtualLosSetHeight,
  IVirtualLosSetupData
} from "../actions/index";
import { VIRTUAL_LOS_DIALOG_OPEN } from "../constants/index";
import { ISetApiLoading, IUnsetApiLoading } from "src/actions/loading";
import { API_LOADING_SET, API_LOADING_UNSET } from "src/constants/loading";
import { REACT_APP_API_URL } from "src/environment";
import IVLOSMediaPost from "../types/VLOSMediaPost";

function selected(
  state: IBend | null = null,
  action: IVirtualLosSidebarSelectBEnd | IVirtualLosDialogOpen
) {
  switch (action.type) {
    case VIRTUAL_LOS_SIDEBAR_SELECT_B_END: {
      return action.payload;
    }
    case VIRTUAL_LOS_DIALOG_OPEN: {
      return null;
    }
  }

  return state;
}

function height(
  state: number | null = null,
  action: IVirtualLosSetHeight | IVirtualLosDialogOpen
) {
  switch (action.type) {
    case VIRTUAL_LOS_SET_HEIGHT: {
      return action.payload;
    }
    case VIRTUAL_LOS_DIALOG_OPEN: {
      return null;
    }
  }

  return state;
}

function mediaOpen(state: string | null = null, action: IVirtualLosDialogOpen) {
  switch (action.type) {
    case VIRTUAL_LOS_DIALOG_OPEN: {
      return action.payload;
    }
  }

  return state;
}

const userAPIs = [
  `${REACT_APP_API_URL}/api/Job/.*/virtuallos/.*/new/.*`,
  `${REACT_APP_API_URL}/api/Misc/media`
];

const isUserApi = (api: string) => {
  return !!userAPIs.find(pattern => !!api.match(new RegExp(pattern)));
};

const loading = (
  state: string[] = [],
  action: ISetApiLoading | IUnsetApiLoading
) => {
  switch (action.type) {
    case API_LOADING_SET: {
      if (!isUserApi(action.payload)) {
        return state;
      }
      return [...state, action.payload];
    }
    case API_LOADING_UNSET: {
      if (!isUserApi(action.payload)) {
        return state;
      }
      return state.filter(api => api !== action.payload);
    }
  }
  return state;
};

function saveData(
  state: IVLOSMediaPost | null = null,
  action:
    | IVirtualLosSetupData
    | IVirtualLosUpdateData
    | IVirtualLosDialogOpen
    | IVirtualLosSetHeight
): IVLOSMediaPost | null {
  switch (action.type) {
    case VIRTUAL_LOS_SETUP_SAVE_DATA: {
      return action.payload;
    }
    case VIRTUAL_LOS_UPDATE_SAVE_DATA: {
      return !state
        ? state
        : {
            ...state,
            ...action.payload
          };
    }
    case VIRTUAL_LOS_DIALOG_OPEN: {
      return null;
    }
    case VIRTUAL_LOS_SET_HEIGHT: {
      return !state
        ? state
        : {
            ...state,
            bendHeight: action.payload
          };
    }
  }

  return state;
}

export const virtualLos = combineReducers({
  selected,
  mediaOpen,
  loading,
  height,
  saveData
});
