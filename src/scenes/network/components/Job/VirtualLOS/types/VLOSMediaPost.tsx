export default interface IVLOSMediaPost {
  bendId: string;
  bendBearing: number;
  bendHeight: number;
  fresnelChartUrl: string | HTMLCanvasElement | null;
  bendDistance: number;
  currentBearing: number;
  sites: IVLOSMediaPostSite[];
}

export interface IVLOSMediaPostSite {
  siteId: string;
  siteName: string;
  distanceToLeft: number;
}

export interface IVLOSMediaPostAEnd {
  aendHeight: number;
  fresnelChartUrl: string | HTMLCanvasElement | null;
  sites: IVLOSMediaPostSite[];
}
