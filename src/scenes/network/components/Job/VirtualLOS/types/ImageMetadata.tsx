import * as moment from "moment";
export interface IImageMetadata {
  fovH: number;
  ratio: number;
  width: number;
  height: number;
  lon: number;
  lat: number;
  yaw: number;
  heightOfView: number;
  dateTime: moment.Moment;
}

export interface IImageExif {
  lenFocalLength: number;
  pixelXDimension: number;
  pixelYDimension: number;
  gpsLongitude: number;
  gpsLatitude: number;
  gimbalYawDegree: number;
  gpsAltitude: number;
  dateTime: string;
}

export interface IImageFov {
  horizontal: number;
  vertical: number;
  diagonal: number;
}

export interface IImageProcessDetail {
  image: HTMLImageElement;
  metadata: IImageMetadata;
}
