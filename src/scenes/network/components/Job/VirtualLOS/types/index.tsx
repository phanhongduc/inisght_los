import { IBend } from "../../../../types/Job";
import IVLOSMediaPost from "./VLOSMediaPost";

export default interface IVirtualLOS {
  selected: IBend | null;
  mediaOpen: string | null;
  loading: string[];
  height: number | null;
  saveData: IVLOSMediaPost | null;
}

export type Without<T, K> = Pick<T, Exclude<keyof T, K>>;
