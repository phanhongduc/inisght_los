import { Theme, createStyles } from "@material-ui/core/styles";
const styles = (theme: Theme) =>
  createStyles({
    root: {
      height: "100%",
      overflow: "hidden"
    },
    content: {
      position: "relative"
    },
    panAble: {
      cursor: "pointer"
    },
    chart: {
      position: "absolute",
      right: 10,
      width: 270,
      bottom: 60,
      transition: "background 1s",
      background: "#00000052",
      padding: 10
    },
    chartHidden: {
      position: "absolute",
      width: 1200,
      height: 800,
      background: "#00000052",
      opacity: 0
    }
  });

export default styles;
