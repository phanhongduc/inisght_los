import { Theme, createStyles } from "@material-ui/core/styles";
const styles = (theme: Theme) =>
  createStyles({
    content: {
      // padding: "0px !important"
    },
    button: {
      margin: theme.spacing.unit,
      width: theme.spacing.unit * 9
    },
    buttonGroup: {
      textAlign: "center"
    }
  });

export default styles;
