import { Theme, createStyles } from "@material-ui/core/styles";
const styles = (theme: Theme) =>
  createStyles({
    title: {
      paddingTop: theme.spacing.unit,
      paddingLeft: theme.spacing.unit * 2
    },
    textField: {
      paddingLeft: theme.spacing.unit * 3,
      paddingRight: theme.spacing.unit * 3
    },
    subTitle: {
      paddingLeft: theme.spacing.unit * 3,
      paddingTop: (theme.spacing.unit * 9) / 4
    },
    list: {
      maxHeight: 300,
      overflowY: "auto"
    },
    listItem: {
      padding: 0
    },
    itemInList: {
      paddingTop: 14
    }
  });

export default styles;
