import { Theme, createStyles } from "@material-ui/core/styles";
const styles = (theme: Theme) =>
  createStyles({
    root: {
      position: "absolute",
      top: 100,
      left: 20,
      padding: 0,
      zIndex: 10000
    },
    list: {
      padding: 0
    },
    listItem: {
      padding: 0
    },
    button: {
      padding: 0,
      minWidth: theme.spacing.unit * 6
    }
  });

export default styles;
