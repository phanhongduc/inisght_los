import { connect, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import VLContent from "../components/VLContent";
import { IBend } from "../../../../types/Job";
import {
  virtualLosSidebarSelectBEnd,
  virtualLosSetHeight,
  virtualLosUpdateData
} from "../actions/index";
import IVLOSMediaPost from "../types/VLOSMediaPost";
import { virtualLosSetupSaveData } from "../actions/index";
import { IImageMetadata } from "../types/ImageMetadata";
import * as turf from "@turf/turf";

function mapStateToProps({
  jobs: {
    virtualLos: { selected, height }
  }
}: IStoreState) {
  return {
    selected,
    height: height || 20
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    select: (selected: IBend, metadata: IImageMetadata) => {
      virtualLosSidebarSelectBEnd(dispatch)(selected);
      virtualLosUpdateData(dispatch)({
        bendId: selected.bendId,
        bendBearing: turf.bearing(
          [metadata.lon, metadata.lat],
          [selected.lon, selected.lat]
        ),
        bendDistance: turf.distance(
          [metadata.lon, metadata.lat],
          [selected.lon, selected.lat]
        )
      });
    },
    changeHeight: (height: number) => {
      virtualLosSetHeight(dispatch)(height);
    },

    setupSavedData: (data: IVLOSMediaPost) => {
      virtualLosSetupSaveData(dispatch)(data);
    },
    updateSavedData: (data: Partial<IVLOSMediaPost>) => {
      virtualLosUpdateData(dispatch)(data);
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(VLContent);
