import { connect, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import VirtualLos, { IProps, IDispatchs } from "../components";
import { virtualLosDialogClose, virtualLosSave } from "../actions/index";
import { mediaOpenKey } from "../helpers";
import { Without } from "../types";

type IVLOSDialogProps = Without<IProps, "isOpen" | "loading" | "canSave"> & {
  jobId: string;
};

function mapStateToProps(
  {
    jobs: {
      virtualLos: { mediaOpen, loading, saveData }
    }
  }: IStoreState,
  { jobId, media, ...props }: IVLOSDialogProps
) {
  const canSave = !saveData
    ? false
    : !saveData.fresnelChartUrl
      ? false
      : saveData.fresnelChartUrl instanceof HTMLCanvasElement;

  console.log("update canSave", canSave);
  return {
    ...props,
    media,
    canSave,
    loading: loading.length > 0,
    isOpen: mediaOpen === mediaOpenKey(jobId, media.mediaId)
  };
}

function mapDispatchToProps(
  dispatch: Dispatch,
  { jobId, media, isAend, ...props }: IVLOSDialogProps
) {
  return {
    onClose: () => {
      virtualLosDialogClose(dispatch)(jobId, media.mediaId);
    },
    onSave: () => {
      virtualLosSave(dispatch)(jobId, media.mediaId, {} as any, isAend);
    }
  };
}

export default connect<IProps, IDispatchs, IVLOSDialogProps>(
  mapStateToProps,
  mapDispatchToProps
)(VirtualLos);
