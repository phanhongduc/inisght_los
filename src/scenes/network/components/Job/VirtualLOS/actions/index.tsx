import { Action, Dispatch } from "redux";
import {
  VIRTUAL_LOS_SIDEBAR_SELECT_B_END,
  VIRTUAL_LOS_SAVE,
  VIRTUAL_LOS_SET_HEIGHT,
  VIRTUAL_LOS_UPDATE_SAVE_DATA,
  VIRTUAL_LOS_SETUP_SAVE_DATA
} from "../constants";
import { IBend } from "../../../../types/Job";
import { VIRTUAL_LOS_DIALOG_OPEN } from "../constants/index";
import { mediaOpenKey } from "../helpers";
import IVLOSMediaPost from "../types/VLOSMediaPost";

export interface IVirtualLosSidebarSelectBEnd
  extends Action<VIRTUAL_LOS_SIDEBAR_SELECT_B_END> {
  payload: IBend;
}

export const virtualLosSidebarSelectBEnd = (
  dispatch: Dispatch<IVirtualLosSidebarSelectBEnd>
) => (bEnd: IBend) => {
  dispatch({
    payload: bEnd,
    type: VIRTUAL_LOS_SIDEBAR_SELECT_B_END
  });
};

export interface IVirtualLosDialogOpen extends Action<VIRTUAL_LOS_DIALOG_OPEN> {
  payload: string | null;
}

export const virtualLosDialogOpen = (
  dispatch: Dispatch<IVirtualLosDialogOpen>
) => (jobId: string, mediaId: string) => {
  dispatch({
    payload: mediaOpenKey(jobId, mediaId),
    type: VIRTUAL_LOS_DIALOG_OPEN
  });
};

export const virtualLosDialogClose = (
  dispatch: Dispatch<IVirtualLosDialogOpen>
) => (jobId: string, mediaId: string) => {
  dispatch({
    payload: null,
    type: VIRTUAL_LOS_DIALOG_OPEN
  });
};

export interface IVirtualLosSave extends Action<VIRTUAL_LOS_SAVE> {
  payload: {
    jobId: string;
    mediaId: string;
    data: IVLOSMediaPost;
    isAend: boolean;
  };
}

export const virtualLosSave = (dispatch: Dispatch<IVirtualLosSave>) => (
  jobId: string,
  mediaId: string,
  data: IVLOSMediaPost,
  isAend: boolean = false
) => {
  dispatch({
    payload: { jobId, mediaId, data, isAend },
    type: VIRTUAL_LOS_SAVE
  });
};

export interface IVirtualLosSetHeight extends Action<VIRTUAL_LOS_SET_HEIGHT> {
  payload: number;
}

export const virtualLosSetHeight = (
  dispatch: Dispatch<IVirtualLosSetHeight>
) => (height: number) => {
  dispatch({
    payload: height,
    type: VIRTUAL_LOS_SET_HEIGHT
  });
};

export interface IVirtualLosUpdateData
  extends Action<VIRTUAL_LOS_UPDATE_SAVE_DATA> {
  payload: Partial<IVLOSMediaPost>;
}

export const virtualLosUpdateData = (
  dispatch: Dispatch<IVirtualLosUpdateData>
) => (bEnd: Partial<IVLOSMediaPost>) => {
  dispatch({
    payload: bEnd,
    type: VIRTUAL_LOS_UPDATE_SAVE_DATA
  });
};
export interface IVirtualLosSetupData
  extends Action<VIRTUAL_LOS_SETUP_SAVE_DATA> {
  payload: IVLOSMediaPost;
}

export const virtualLosSetupSaveData = (
  dispatch: Dispatch<IVirtualLosSetupData>
) => (bEnd: IVLOSMediaPost) => {
  dispatch({
    payload: bEnd,
    type: VIRTUAL_LOS_SETUP_SAVE_DATA
  });
};
