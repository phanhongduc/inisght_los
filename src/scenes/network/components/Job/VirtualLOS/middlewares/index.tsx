import { Store, Dispatch } from "react-redux";
import axios, { AxiosResponse } from "axios";
import { IStoreState } from "src/types";
import { IVirtualLosSave, virtualLosDialogClose } from "../actions/index";
import { VIRTUAL_LOS_SAVE } from "../constants/index";
import IVLOSMediaPost from "../types/VLOSMediaPost";
import { REACT_APP_API_URL } from "../../../../../../environment";
import { getJobDetail } from "src/scenes/network/actions/Job";
import { IVLOSMediaPostAEnd } from "../types/VLOSMediaPost";

export const vlosSave = (store: Store<IStoreState>) => (
  next: Dispatch<IVirtualLosSave>
) => (action: IVirtualLosSave) => {
  const currentState = store.getState();
  switch (action.type) {
    case VIRTUAL_LOS_SAVE: {
      if (currentState.libs.selectedLib) {
        const { jobId, mediaId, isAend } = action.payload;
        const saveData = currentState.jobs.virtualLos.saveData;
        if (saveData) {
          const saveFunc = !isAend
            ? saveBendMedia(saveData)
            : saveAendMedia({
                aendHeight: saveData.bendHeight,
                sites: saveData.sites,
                fresnelChartUrl: saveData.fresnelChartUrl
              });

          saveFunc(saveMediaData)(store.dispatch)(
            currentState.libs.selectedLib,
            jobId,
            mediaId
          );
        }
      }
      break;
    }
  }

  return next(action);
};

const saveMediaData = (
  postUrl: string,
  data: IVLOSMediaPost | IVLOSMediaPostAEnd
) => (dispatch: Dispatch) => (jobId: string, mediaId: string) => {
  saveChartImage(data.fresnelChartUrl as HTMLCanvasElement).then(url => {
    axios
      .post(postUrl, { ...data, fresnelChartUrl: url })
      .then((resp: AxiosResponse<any>) => {
        console.log(resp);
        virtualLosDialogClose(dispatch)(jobId, mediaId);
        dispatch(getJobDetail(jobId));
      });
  });
};
const toUint8Array = (s: string) => {
  return new Uint8Array(
    atob(s)
      .split("")
      .map(c => c.charCodeAt(0))
  );
};
const canvasToForm = (canvas: HTMLCanvasElement): FormData => {
  const b64Image = canvas.toDataURL("image/png").split(",")[1];
  const u8Image = toUint8Array(b64Image);

  const formData = new FormData();
  formData.append(
    "file",
    new Blob([u8Image], { type: "image/png" }),
    "xxxx.png"
  );
  return formData;
};

const saveChartImage = (canvas: HTMLCanvasElement): Promise<string> => {
  const formData = canvasToForm(canvas);

  return new Promise<string>((resolve, reject) => {
    axios
      .post(`${REACT_APP_API_URL}/api/Misc/media`, formData, {
        headers: {
          "Content-Type": "multipart/form-data"
        }
      })
      .then((resp: AxiosResponse<any>) => {
        console.log(resp);
        resolve(resp.data);
      });
  });
};

const saveAendMedia = (data: IVLOSMediaPostAEnd) => (
  saveFunc: typeof saveMediaData
) => (dispatch: Dispatch) => (
  libId: string,
  jobId: string,
  mediaId: string
) => {
  const url = `${REACT_APP_API_URL}/api/Job/${libId}/virtuallos/${jobId}/newaend/${mediaId}`;
  return saveFunc(url, data)(dispatch)(jobId, mediaId);
};

const saveBendMedia = (data: IVLOSMediaPost) => (
  saveFunc: typeof saveMediaData
) => (dispatch: Dispatch) => (
  libId: string,
  jobId: string,
  mediaId: string
) => {
  const url = `${REACT_APP_API_URL}/api/Job/${libId}/virtuallos/${jobId}/new/${mediaId}`;
  return saveFunc(url, data)(dispatch)(jobId, mediaId);
};
