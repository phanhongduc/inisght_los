import { IImageMetadata, IImageFov } from "./types/ImageMetadata";
import * as turf from "@turf/turf";
import { fieldOfView } from "field-of-view";
import * as moment from "moment";
import { IJobDetailMedia } from "../../../types/Job";

const computeFov = (
  lenFocusLenght: number,
  lenMult: number,
  ratio: number
): IImageFov => {
  let filmWidth35 = 36;
  let filmHeight35 = 24;
  const filmDiag35 = Math.sqrt(
    filmWidth35 * filmWidth35 + filmHeight35 * filmHeight35
  );

  const flen = lenFocusLenght;
  const flenMult = lenMult;
  const aspect = ratio;

  filmHeight35 = Math.sqrt((filmDiag35 * filmDiag35) / (aspect * aspect + 1));
  filmWidth35 = aspect * filmHeight35;

  const sensorWidth = filmWidth35 / flenMult;
  const sensorHeight = filmHeight35 / flenMult;
  const sensorDiag = Math.sqrt(
    sensorWidth * sensorWidth + sensorHeight * sensorHeight
  );

  const fovH = (2 * Math.atan(sensorWidth / (2 * flen)) * 180) / Math.PI;
  const fovV = (2 * Math.atan(sensorHeight / (2 * flen)) * 180) / Math.PI;
  const fovD = (2 * Math.atan(sensorDiag / (2 * flen)) * 180) / Math.PI;

  return {
    horizontal: fovH,
    vertical: fovV,
    diagonal: fovD
  };
};

const ratioCalc = (width: number, height: number) => width / height;

const LEN_MULT_FULLFRAME = 1;

export const exif2Metadata = (media: IJobDetailMedia): IImageMetadata => ({
  fovH: computeFov(
    media.focalLength35,
    LEN_MULT_FULLFRAME,
    ratioCalc(media.imageWidth, media.imageHeight)
  ).horizontal,
  ratio: ratioCalc(media.imageWidth, media.imageHeight),
  width: media.imageWidth,
  height: media.imageHeight,
  lon: media.currentLon,
  lat: media.currentLat,
  yaw: media.gimbalYaw,
  heightOfView: media.absoluteAltitude,
  dateTime: moment.utc(new Date())
});

const angleToShift = (bearing: number, heading: number): number => {
  const diff = bearing - heading;
  const d = Math.abs(diff) % 360;
  const r = d > 180 ? 360 - d : d;

  // calculate sign
  const sign =
    (diff >= 0 && diff <= 180) || (diff <= -180 && diff >= -360) ? 1 : -1;
  const result = r * sign;

  return result;
};

export const sitePosCalc = (
  droneLocation: turf.Coord,
  siteLocation: turf.Coord,
  metadata: IImageMetadata
): number => {
  // Find bearing between drone and site
  let bearing = turf.bearing(droneLocation, siteLocation);
  if (bearing < 0) {
    bearing = 360 + bearing;
  }

  // Read yaw from exif
  let yaw = metadata.yaw;
  if (yaw < 0) {
    yaw = 360 + yaw;
  }

  const degreetopixelWidth = metadata.width / metadata.fovH;
  const shift = angleToShift(bearing, yaw);
  const diffX = shift * degreetopixelWidth;
  const x = metadata.width / 2 + diffX;
  return x;
};

export const fovGeo = ({ lon, lat, yaw, fovH }: IImageMetadata) => {
  const camPoint = turf.point([lon, lat], {
    distance: 1000,
    bearing: yaw
  });
  const result = fieldOfView.fromFeature(camPoint, {
    angle: fovH
  });
  return [
    result.geometry.geometries[0].coordinates,
    ...result.geometry.geometries[1].coordinates
  ];

  // const polygon = turf.polygon([[result.geometry.geometries[0].coordinates, ...result.geometry.geometries[1].coordinates]]);
  // return turf.featureCollection([polygon]);
};

export const mediaOpenKey = (jobId: string, mediaId: string) =>
  `${jobId}$_${mediaId}`;
