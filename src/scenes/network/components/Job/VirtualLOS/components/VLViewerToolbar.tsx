import withStyles from "@material-ui/core/styles/withStyles";
import styles from "../styles/VLViewerToolbar";
import { IStyleTypeProps } from "src/styles/utils";
import * as React from "react";
import Toolbar from "@material-ui/core/Toolbar";
import ZoomOut from "@material-ui/icons/ZoomOut";
import ZoomIn from "@material-ui/icons/ZoomIn";
import Refresh from "@material-ui/icons/Refresh";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import Button from "@material-ui/core/Button";

export interface IDispatchs {
  onZoom: (zoomDetal: number) => void;
  onReset: () => void;
}

const ZOOM_DELTA = 1;

const onClickZoom = (
  zoom: (zoomDetal: number) => void,
  zoomDetal: number
) => () => {
  zoom(zoomDetal);
};

const VLViewerToolbar = ({
  onZoom,
  onReset,
  classes
}: IDispatchs & IStyleTypeProps<typeof styles>) => (
  <Toolbar className={classes.root}>
    <List className={classes.list}>
      <ListItem className={classes.listItem}>
        <Button
          variant="outlined"
          className={classes.button}
          mini={true}
          color="default"
          aria-label="ZoomOut"
          onClick={onClickZoom(onZoom, ZOOM_DELTA)}
        >
          <ZoomOut />
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          variant="outlined"
          className={classes.button}
          mini={true}
          color="default"
          aria-label="ZoomIn"
          onClick={onClickZoom(onZoom, -ZOOM_DELTA)}
        >
          <ZoomIn />
        </Button>
      </ListItem>
      <ListItem className={classes.listItem}>
        <Button
          variant="outlined"
          className={classes.button}
          mini={true}
          color="default"
          aria-label="Refresh"
          onClick={onReset}
        >
          <Refresh />
        </Button>
      </ListItem>
    </List>
  </Toolbar>
);

export default withStyles(styles, { withTheme: true })(VLViewerToolbar);
