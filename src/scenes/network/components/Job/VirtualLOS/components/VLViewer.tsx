import withStyles from "@material-ui/core/styles/withStyles";
import styles from "../styles/VLViewer";
import { IStyleTypeProps } from "src/styles/utils";
import * as React from "react";
import { IJobDetailMedia, IBend } from "../../../../types/Job";
import {
  Stage,
  Layer,
  Text as KonvaText,
  Group,
  Rect,
  Line
} from "react-konva";
import CanvasImage from "./CanvasImage";
import Paper from "@material-ui/core/Paper";
import antennaImage from "src/assets/images/bts_antenna.png";
import logo from "src/assets/images/logo_los.png";
import { IImageMetadata } from "../types/ImageMetadata";
import { sitePosCalc } from "../helpers";
import * as turf from "@turf/turf";
import FresnelChart from "src/components/FresnelChart";
import IVLOSMediaPost from "../types/VLOSMediaPost";
import TryAble from "src/helpers/Tryable";
import { Vector2d } from "konva";
import * as Konva from "konva";
import * as classNames from "classnames";
import VLViewerToolbar from "./VLViewerToolbar";

export interface IProps {
  frequency: number;
  media: IJobDetailMedia;
  image: HTMLImageElement;
  metadata: IImageMetadata;
  bends: IBend[];
  selected: IBend;
  bendHeight: number;
  isAend: boolean;
}

export interface IDispatchs {
  setupSavedData: (data: IVLOSMediaPost) => void;
  updateSavedData: (data: Partial<IVLOSMediaPost>) => void;
}

interface IAntenna {
  x: number;
  bend: IBend;
}

interface IState {
  width: number;
  height: number;
  antennas: IAntenna[];
  antenaDiff: number;
  zoomLevel: number;
  layerPosition: Vector2d;
}

const DEFAULT_ZOOM_LEVEL = 1;
const SCALE_BY = 1.05;
const ZOOM_MIN = 1;
const ZOOM_MAX = 10;

const ANTENNA_WIDTH = 20;
const ANTENNA_HEIGHT = (ANTENNA_WIDTH / 235) * 380 + 20;

const KONVA_STYLE = {
  fontSize: 16,
  fontColorNormal: "#ffffff",
  fontColor: "#f3f20d",
  INFO: {
    width: 1000,
    height: 60,
    borderColor: "red",
    borderSize: 2,
    borderRadius: 10,
    margin: 10,
    colCount: 4,
    rowCount: 2
  },
  LOGO: {
    height: 70,
    width: 50
  }
};

const decFloor = (target: number, dec: number) => {
  const res = Math.floor(target * Math.pow(10, dec)) / Math.pow(10, dec);
  const ress = (res + "").split(".");
  ress[1] = ((ress[1] || "00") + "00000000000000").slice(0, dec);
  return ress.join(".");
};

const grid = (size: number, cols: number, margin: number) => (
  idx: number
): number => {
  const colSize = size / cols - margin;

  return (colSize + margin) * idx + margin;
};

const gridCol = grid(
  KONVA_STYLE.INFO.width,
  KONVA_STYLE.INFO.colCount,
  KONVA_STYLE.INFO.margin
);
const gridRow = grid(
  KONVA_STYLE.INFO.height,
  KONVA_STYLE.INFO.rowCount,
  KONVA_STYLE.INFO.margin
);

interface IInfoTextProps {
  text: string;
  row: number;
  col: number;
  colSpan?: number;
  color?: string;
}
const InfoText = ({ text, row, col, colSpan = 1, color }: IInfoTextProps) => (
  <KonvaText
    text={text}
    fontSize={KONVA_STYLE.fontSize}
    y={gridRow(row)}
    width={(KONVA_STYLE.INFO.width / KONVA_STYLE.INFO.colCount) * colSpan}
    fill={color || KONVA_STYLE.fontColorNormal}
    x={gridCol(col)}
  />
);

const isZoomed = (zoom: number) => zoom !== DEFAULT_ZOOM_LEVEL;

const getVLOSTypeName = (isAend: boolean) => (isAend ? "Aend" : "'B'end");

class VLViewer extends React.Component<
  IProps & IStyleTypeProps<typeof styles> & IDispatchs,
  IState
> {
  private pageContainer: React.RefObject<HTMLDivElement>;
  private antenaImage: any;
  private canvasTry: TryAble<HTMLCanvasElement>;

  constructor(props: IProps & IStyleTypeProps<typeof styles> & IDispatchs) {
    super(props);
    this.pageContainer = React.createRef<HTMLDivElement>();
    const width = this.props.image.width;
    const height = this.props.image.height;

    const { metadata } = props;

    const dronePos = [metadata.lon, metadata.lat];

    this.state = {
      width: 500,
      height: (500 * height) / width,
      antennas: props.bends.map(bend => ({
        x: sitePosCalc(dronePos, [bend.lon, bend.lat], metadata),
        bend
      })),
      antenaDiff: 0,
      zoomLevel: DEFAULT_ZOOM_LEVEL,
      layerPosition: { x: 0, y: 0 }
    };

    this.antenaImage = antennaImage;
  }
  public componentDidMount() {
    const width = this.props.image.width;
    const height = this.props.image.height;

    if (this.pageContainer.current) {
      console.log(
        this.pageContainer.current.clientWidth,
        this.pageContainer.current.clientHeight
      );
      this.setState({
        width: this.pageContainer.current.clientWidth,
        height: (this.pageContainer.current.clientWidth * height) / width
      });
    }

    const { metadata, selected } = this.props;

    this.props.setupSavedData({
      bendId: selected.bendId,
      bendBearing: turf.bearing(
        [metadata.lon, metadata.lat],
        [selected.lon, selected.lat]
      ),
      bendHeight: metadata.heightOfView,
      fresnelChartUrl: "",
      bendDistance: turf.distance(
        [metadata.lon, metadata.lat],
        [selected.lon, selected.lat]
      ),
      currentBearing: metadata.yaw,
      sites: this.state.antennas
        .map(({ x, bend }) => ({
          siteId: bend.bendId,
          siteName: bend.bendSiteName,
          distanceToLeft: x / metadata.width
        }))
        .filter(site => site.distanceToLeft <= 1 && site.distanceToLeft >= 0)
    });
  }

  get scaleRatio(): number {
    return this.state.width / this.props.metadata.width;
  }

  private dragAntenna = (anten: IAntenna) => (e: any) => {
    const newPos = {
      x: e.target.x()
    };
    console.log(newPos);

    const diff = newPos.x + ANTENNA_WIDTH / 2 - anten.x * this.scaleRatio;

    this.setState({
      antenaDiff: diff
    });
    this.props.updateSavedData({
      sites: this.state.antennas
        .map(({ x, bend }) => ({
          siteId: bend.bendId,
          siteName: bend.bendSiteName,
          distanceToLeft: (x * this.scaleRatio + diff) / this.state.width
        }))
        .filter(site => site.distanceToLeft <= 1 && site.distanceToLeft >= 0)
    });
  };

  private chartLoading = (isComplete: boolean) => {
    if (this.canvasTry) {
      this.canvasTry.cancel();
    }

    const { updateSavedData } = this.props;
    if (!isComplete) {
      updateSavedData({
        fresnelChartUrl: null
      });
    } else {
      this.canvasTry = new TryAble(() => {
        return document.getElementsByClassName(
          "chartjs-render-monitor"
        )[1] as HTMLCanvasElement;
      });
      this.canvasTry.then(chartCanvas => {
        console.log("update canvas");
        updateSavedData({
          fresnelChartUrl: chartCanvas
        });
      });
    }
  };

  private validLayerPos = (
    { x, y }: Vector2d,
    { width, height }: { width: number; height: number },
    scale: number
  ): Vector2d => {
    let validX = x;
    let validY = y;
    if (validX > 0) {
      validX = 0;
    }
    if (validY > 0) {
      validY = 0;
    }
    if (validX < -width * (scale - 1)) {
      validX = -width * (scale - 1);
    }
    if (validY < -height * (scale - 1)) {
      validY = -height * (scale - 1);
    }
    return {
      x: validX,
      y: validY
    };
  };

  private toolbarZoom = (zoomDelta: number) => {
    console.log(zoomDelta);

    this.zoom(zoomDelta, { x: this.state.width / 2, y: this.state.height / 2 });
  };

  private zoomImage = (e: {
    evt: React.WheelEvent<HTMLDivElement>;
    currentTarget: Konva.Stage;
  }) => {
    const stage = e.currentTarget;
    this.zoom(e.evt.deltaY, stage.getPointerPosition());
  };

  private zoom = (delta: number, zoomPoint: Vector2d) => {
    const oldScale = this.state.zoomLevel;
    const layerPos = this.state.layerPosition;

    const mousePointTo = {
      x: zoomPoint.x / oldScale - layerPos.x / oldScale,
      y: zoomPoint.y / oldScale - layerPos.y / oldScale
    };

    const newScale = delta > 0 ? oldScale / SCALE_BY : oldScale * SCALE_BY;

    const newPos = {
      x: -(mousePointTo.x - zoomPoint.x / newScale) * newScale,
      y: -(mousePointTo.y - zoomPoint.y / newScale) * newScale
    };
    const validPos = this.validLayerPos(newPos, this.state, newScale);
    if (newScale < ZOOM_MIN || newScale > ZOOM_MAX) {
      return;
    }
    this.setState({
      zoomLevel: newScale,
      layerPosition: validPos
    });
  };

  private dragImage = (e: { target: Konva.Layer }) => {
    const newPos = {
      x: e.target.x(),
      y: e.target.y()
    };

    const validPos = this.validLayerPos(
      newPos,
      this.state,
      this.state.zoomLevel
    );
    this.setState({
      layerPosition: validPos
    });
  };

  private resetZoom = () => {
    this.setState({
      zoomLevel: DEFAULT_ZOOM_LEVEL,
      layerPosition: { x: 0, y: 0 }
    });
  };

  public render() {
    const { image, metadata, selected, classes, bendHeight } = this.props;
    const scale = this.scaleRatio;
    const {
      antennas,
      antenaDiff,
      width,
      zoomLevel,
      layerPosition
    } = this.state;

    const bearing = selected
      ? turf.bearing([metadata.lon, metadata.lat], [selected.lon, selected.lat])
      : 0;
    const distance = selected
      ? turf.distance(
          [metadata.lon, metadata.lat],
          [selected.lon, selected.lat]
        )
      : 0;

    const canDragImage = isZoomed(zoomLevel);

    const selectedAnten = antennas.find(
      anten => anten.bend.bendId === selected.bendId
    );
    if (!selectedAnten) {
      return;
    }

    const typeName = getVLOSTypeName(this.props.isAend);

    return (
      <Paper className={this.props.classes.root}>
        <div
          ref={this.pageContainer}
          className={
            !canDragImage
              ? classes.content
              : classNames(classes.content, classes.panAble)
          }
        >
          <VLViewerToolbar onZoom={this.toolbarZoom} onReset={this.resetZoom} />
          <Stage
            width={this.state.width}
            height={this.state.height}
            onContentWheel={this.zoomImage}
          >
            <Layer
              scale={{ x: zoomLevel, y: zoomLevel }}
              x={layerPosition.x}
              y={layerPosition.y}
              draggable={canDragImage}
              onDragMove={this.dragImage}
            >
              <CanvasImage src={image.src} width={this.state.width} />
              <Line
                x={selectedAnten.x * scale + antenaDiff}
                y={0}
                points={[0, 0, 0, this.state.height]}
                stroke="yellow"
                strokeWidth={0.5 / zoomLevel}
              />
            </Layer>

            <Layer
              x={KONVA_STYLE.INFO.margin * 2}
              y={KONVA_STYLE.INFO.margin * 2}
            >
              <Rect
                width={KONVA_STYLE.INFO.width}
                height={KONVA_STYLE.INFO.height}
                opacity={0.3}
                fill="#000000"
              />
              <InfoText
                row={0}
                col={0}
                text={`Height: ${decFloor(metadata.heightOfView, 2)} (m)`}
              />
              <InfoText
                row={0}
                col={1}
                text={`${typeName}: ${selected.bendSiteName}`}
                color={KONVA_STYLE.fontColor}
              />
              <InfoText
                row={0}
                col={2}
                text={`Bearing to ${typeName}: ${decFloor(bearing, 2)}˚`}
                color={KONVA_STYLE.fontColor}
              />
              <InfoText
                row={0}
                col={3}
                text={`${typeName} Height: ${decFloor(bendHeight, 2)} (m)`}
              />
              {metadata.yaw > 0 && (
                <InfoText
                  row={1}
                  col={0}
                  text={`Bearing: ${decFloor(metadata.yaw, 2)}˚`}
                />
              )}
              <InfoText
                row={1}
                col={1}
                text={`${decFloor(selected.lat, 5)},${decFloor(
                  selected.lon,
                  5
                )}`}
                color={KONVA_STYLE.fontColor}
              />
              <InfoText
                row={1}
                col={2}
                text={`Distance to ${typeName}: ${decFloor(
                  distance * 1000,
                  2
                )} (m)`}
                color={KONVA_STYLE.fontColor}
              />

              <InfoText
                row={1}
                col={3}
                text={metadata.dateTime.toLocaleString()}
              />

              <CanvasImage
                src={logo}
                width={KONVA_STYLE.LOGO.width}
                x={width - KONVA_STYLE.LOGO.width - 40}
                y={0}
              />
            </Layer>

            <Layer
              y={
                (this.state.height - ANTENNA_HEIGHT) * zoomLevel +
                layerPosition.y
              }
              scale={{ x: zoomLevel, y: zoomLevel }}
              x={layerPosition.x}
            >
              {antennas.map(antenna => (
                <Group key={antenna.bend.bendId}>
                  <CanvasImage
                    src={this.antenaImage}
                    width={ANTENNA_WIDTH}
                    x={antenna.x * scale + antenaDiff - ANTENNA_WIDTH / 2}
                    y={0}
                    draggable={true}
                    onDragMove={this.dragAntenna(antenna)}
                  />
                  <KonvaText
                    text={antenna.bend.bendSiteName}
                    fontSize={KONVA_STYLE.fontSize}
                    y={ANTENNA_HEIGHT - 20}
                    align="center"
                    width={200}
                    fill="#f3f20d"
                    x={antenna.x * scale + antenaDiff - 200 / 2}
                  />
                </Group>
              ))}
            </Layer>
          </Stage>
          {selected && (
            <div className={this.props.classes.chart}>
              <FresnelChart
                id={selected.bendId}
                frequency={this.props.frequency}
                heightOfStart={metadata.heightOfView}
                heightOfEnd={bendHeight}
                aEndLat={metadata.lat}
                aEndLon={metadata.lon}
                bEndLat={selected.lat}
                bEndLon={selected.lon}
                distanceFromAendToBend={distance * 1000}
                distanceMidPoint={1}
              />
            </div>
          )}
          {selected && (
            <div className={this.props.classes.chartHidden}>
              <FresnelChart
                id={selected.bendId}
                frequency={this.props.frequency}
                heightOfStart={metadata.heightOfView}
                heightOfEnd={bendHeight}
                aEndLat={metadata.lat}
                aEndLon={metadata.lon}
                bEndLat={selected.lat}
                bEndLon={selected.lon}
                distanceFromAendToBend={distance * 1000}
                onComplete={this.chartLoading}
                distanceMidPoint={1}
              />
            </div>
          )}
        </div>
      </Paper>
    );
  }
}

export default withStyles(styles, { withTheme: true })(VLViewer);
