import * as React from "react";
import * as Konva from "konva";
import { Image as KonvaImage, KonvaNodeProps } from "react-konva";
import { Without } from "../types";

type IProps = Without<
  Konva.ImageConfig & KonvaNodeProps,
  "image" | "height" | "width"
> & {
  src: string;
  width: number;
};

interface IState {
  image: any;
  height: number;
}

export default class CanvasImage extends React.Component<IProps, IState> {
  constructor(props: IProps) {
    super(props);

    this.state = {
      image: null,
      height: 300
    };
  }
  public componentDidMount() {
    const image = new Image();
    image.src = this.props.src;
    image.onload = () => {
      const ratio = image.height / image.width;

      this.setState({
        image,
        height: this.props.width * ratio
      });
    };
  }

  public render() {
    return (
      <KonvaImage
        image={this.state.image}
        width={this.props.width}
        height={this.state.height}
        {...this.props}
      />
    );
  }
}
