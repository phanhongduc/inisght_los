import withStyles from "@material-ui/core/styles/withStyles";
import styles from "../styles/VLContent";
import { IStyleTypeProps } from "src/styles/utils";
import * as React from "react";
import Grid from "@material-ui/core/Grid";
import VLSideBar from "./VLSideBar";
import VLViewer from "./VLViewer";
import { IBend, IJobDetailMedia } from "../../../../types/Job";
import VLImageProcess from "./VLImageProcess";
import { IImageProcessDetail, IImageMetadata } from "../types/ImageMetadata";
import IVLOSMediaPost from "../types/VLOSMediaPost";

export interface IProps {
  frequency: number;
  bends: IBend[];
  selected: IBend | null;
  selectedId: string;
  media: IJobDetailMedia;
  height: number;
  children: React.ReactElement<any>;
  isAend: boolean;
}
export interface IDispatchs {
  select: (bend: IBend, meta: IImageMetadata) => void;
  changeHeight: (height: number) => void;
  setupSavedData: (data: IVLOSMediaPost) => void;
  updateSavedData: (data: Partial<IVLOSMediaPost>) => void;
}

const combineMeta = (
  metadata: IImageMetadata,
  height: number | null
): IImageMetadata => {
  return metadata;
};

const onSelect = (
  meta: IImageMetadata,
  select: (bend: IBend, meta: IImageMetadata) => void
) => (bend: IBend) => {
  select(bend, meta);
};

const VLContent = ({
  bends,
  selected,
  select,
  media,
  image,
  metadata,
  selectedId,
  frequency,
  changeHeight,
  height,
  setupSavedData,
  updateSavedData,
  children,
  isAend
}: IProps &
  IDispatchs &
  IStyleTypeProps<typeof styles> &
  IImageProcessDetail) => (
  <Grid container={true} spacing={16}>
    <Grid item={true} xs={3}>
      <VLSideBar
        bends={bends}
        selected={
          selected
            ? selected
            : bends.find(bend => bend.bendId === selectedId) || null
        }
        select={onSelect(metadata, select)}
        media={media}
        metadata={combineMeta(metadata, height)}
        changeHeight={changeHeight}
        bendHeight={height || 20}
      >
        {children}
      </VLSideBar>
    </Grid>
    <Grid item={true} xs={9}>
      <VLViewer
        frequency={frequency}
        media={media}
        image={image}
        metadata={combineMeta(metadata, height)}
        bends={bends}
        selected={
          selected
            ? selected
            : (bends.find(bend => bend.bendId === selectedId) as IBend)
        }
        setupSavedData={setupSavedData}
        updateSavedData={updateSavedData}
        bendHeight={height}
        isAend={isAend}
      />
    </Grid>
  </Grid>
);

const imageDisplay = ({
  ...props
}: IProps & IDispatchs & IStyleTypeProps<typeof styles>) => ({
  image,
  metadata
}: IImageProcessDetail) => (
  <VLContent {...props} image={image} metadata={metadata} />
);

const VLContentWrap = (
  props: IProps & IDispatchs & IStyleTypeProps<typeof styles>
) => <VLImageProcess media={props.media} display={imageDisplay(props)} />;

export default withStyles(styles, { withTheme: true })(VLContentWrap);
