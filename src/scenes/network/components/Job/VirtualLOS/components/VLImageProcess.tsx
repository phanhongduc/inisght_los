import withStyles from "@material-ui/core/styles/withStyles";
import styles from "../styles/VLViewer";
import { IStyleTypeProps } from "src/styles/utils";
import * as React from "react";
import { IJobDetailMedia } from "../../../../types/Job";
import { IImageMetadata, IImageProcessDetail } from "../types/ImageMetadata";
import { exif2Metadata } from "../helpers";
import CircularProgress from "@material-ui/core/CircularProgress";

export interface IProps {
  media: IJobDetailMedia;
  display: (imageDetail: IImageProcessDetail) => React.ReactElement<any>;
}

interface IState {
  image: HTMLImageElement | null;
  metadata: IImageMetadata | null;
}

class VLImageProcess extends React.Component<
  IProps & IStyleTypeProps<typeof styles>,
  IState
> {
  constructor(props: IProps & IStyleTypeProps<typeof styles>) {
    super(props);

    this.state = {
      image: null,
      metadata: null
    };
  }
  public componentDidMount() {
    this.process(this.props.media.url).then(
      ({ image, metadata }: IImageProcessDetail) => {
        this.setState({
          image,
          metadata
        });
      }
    );
  }

  private process = (src: string): Promise<IImageProcessDetail> => {
    return new Promise((resolve, reject) => {
      Promise.all([this.getDataUri(src), this.getMetaData(src)]).then(
        ([image, metadata]) => {
          resolve({ image, metadata });
        }
      );
    });
  };

  private getDataUri = (url: string) => {
    return new Promise<HTMLImageElement>((resolve, reject) => {
      const image = new Image();

      image.onload = () => {
        resolve(image);
      };

      image.src = url;
    });
  };

  private getMetaData = (url: string): Promise<IImageMetadata> => {
    return new Promise<IImageMetadata>((resolve, reject) => {
      const metadata = exif2Metadata(this.props.media);
      resolve(metadata);
    });
  };

  public render() {
    if (!this.state.image || !this.state.metadata) {
      return (
        <CircularProgress
          size={50}
          style={{
            display: "block",
            margin: "auto"
          }}
        />
      );
    }
    return this.props.display({
      image: this.state.image,
      metadata: this.state.metadata
    });
  }
}

export default withStyles(styles, { withTheme: true })(VLImageProcess);
