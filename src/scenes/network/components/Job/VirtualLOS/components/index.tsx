import withStyles from "@material-ui/core/styles/withStyles";
import styles from "../styles";
import { IStyleTypeProps } from "src/styles/utils";
import Dialog from "@material-ui/core/Dialog";
import * as React from "react";
// import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import VLContent from "../containers/VLContent";
import { IBend, IJobDetailMedia } from "../../../../types/Job";
import Button from "@material-ui/core/Button";
import SectionLoading from "src/components/SectionLoading";

export interface IProps {
  frequency: number;
  isOpen: boolean;
  bends: IBend[];
  media: IJobDetailMedia;
  selected: string;
  loading: boolean;
  canSave: boolean;
  isAend?: boolean;
}

export interface IDispatchs {
  onClose: () => void;
  onSave: () => void;
}
const onCancel = (close: () => void) => () => {
  close();
};

const onOK = (onSave: () => void) => () => {
  onSave();
};

const VirtualLOS = ({
  isOpen,
  bends,
  media,
  selected,
  frequency,
  onClose,
  onSave,
  loading,
  classes,
  canSave,
  isAend
}: IProps & IDispatchs & IStyleTypeProps<typeof styles>) => (
  <Dialog
    disableBackdropClick={true}
    disableEscapeKeyDown={true}
    open={isOpen}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
    fullScreen={true}
    maxWidth={false}
  >
    {/* <DialogTitle id="alert-dialog-title">{"Virtual LOS"}</DialogTitle> */}
    <DialogContent className={classes.content}>
      {loading && <SectionLoading />}
      <VLContent
        bends={bends}
        media={media}
        selectedId={selected}
        frequency={frequency}
        isAend={isAend || false}
      >
        <div className={classes.buttonGroup}>
          <Button
            disabled={loading}
            onClick={onCancel(onClose)}
            color="primary"
            className={classes.button}
          >
            {"Cancel"}
          </Button>
          <Button
            disabled={loading || !canSave}
            onClick={onOK(onSave)}
            color="primary"
            autoFocus={true}
            className={classes.button}
          >
            {"Save"}
          </Button>
        </div>
      </VLContent>
    </DialogContent>
  </Dialog>
);

export default withStyles(styles, { withTheme: true })(VirtualLOS);
