import withStyles from "@material-ui/core/styles/withStyles";
import styles from "../styles/VLMap";
import { IStyleTypeProps } from "src/styles/utils";
import * as React from "react";
import Mapbox, { IMapBoxOptions } from "src/components/Mapbox/Mapbox";
import { IBend, IJobDetailMedia } from "../../../../types/Job";
import { FeatureCollection } from "geojson";
import * as turf from "@turf/turf";
import { IImageMetadata } from "../types/ImageMetadata";
import { fovGeo } from "../helpers";
import { createFeatureCollection } from "../../../../../../helpers/geojson";

export interface IProps {
  bends: IBend[];
  selected: IBend | null;
  media: IJobDetailMedia;
  metadata: IImageMetadata;
}

const VLMap = ({
  media,
  bends,
  selected,
  classes,
  metadata
}: IProps & IStyleTypeProps<typeof styles>) => {
  console.log(metadata);
  const fov = fovGeo(metadata);

  const bbox = turf.bbox(
    turf.lineString([
      ...bends.map(bend => [bend.lon, bend.lat]),
      [metadata.lon, metadata.lat]
    ])
  );

  const lines = !selected
    ? []
    : [[[metadata.lon, metadata.lat], [selected.lon, selected.lat]]];

  const freeSites = bends
    .filter(bend => !selected || bend.bendId !== selected.bendId)
    .map(bend => [bend.lon, bend.lat]);
  const selectSite = !selected ? [] : [[selected.lon, selected.lat]];

  const mapOptions: IMapBoxOptions = {
    center: {
      lng: media.currentLon,
      lat: media.currentLat
    },
    zoom: 16,
    bbox,
    data: {
      "measure-lines": turf.lineStrings(lines) as FeatureCollection,
      job: turf.points([[metadata.lon, metadata.lat]]) as FeatureCollection,
      freeSites: turf.points(freeSites) as FeatureCollection,
      selectSite: turf.points(selectSite) as FeatureCollection,
      fov: createFeatureCollection([
        {
          type: "Feature",
          geometry: {
            type: "Polygon",
            coordinates: [fov]
          },
          properties: {}
        }
      ])
    },
    images: [
      {
        name: "job",
        value: require("src/assets/images/marker/ic_job.png")
      }
    ],
    layers: [
      {
        id: "measure-lines",
        type: "line",
        layout: {
          "line-cap": "round",
          "line-join": "round"
        },
        paint: {
          "line-color": "#e25144",
          "line-width": 2.5,
          "line-opacity": 0.5
        }
      },
      {
        id: "freeSites",
        type: "circle",
        paint: {
          "circle-color": "#51bbd6",
          "circle-radius": 10
        }
      },
      {
        id: "selectSite",
        type: "circle",
        paint: {
          "circle-color": "#e25144",
          "circle-radius": 15
        }
      },
      {
        id: "job",
        type: "symbol",
        layout: {
          "icon-image": "job",
          "icon-size": 1,
          "icon-allow-overlap": true,
          "icon-rotation-alignment": "map"
        }
      },
      {
        id: "fov",
        type: "fill",
        layout: {},
        paint: {
          "fill-color": "#3388ff",
          "fill-opacity": 0.2
        }
      }
    ]
  };

  return (
    <div className={classes.root}>
      <Mapbox {...mapOptions} />
    </div>
  );
};

export default withStyles(styles, { withTheme: true })(VLMap);
