import withStyles from "@material-ui/core/styles/withStyles";
import styles from "../styles/VLSidebar";
import { IStyleTypeProps } from "src/styles/utils";
import * as React from "react";
import Paper from "@material-ui/core/Paper";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import { IBend, IJobDetailMedia } from "../../../../types/Job";
import ListItemText from "@material-ui/core/ListItemText";
// import Radio from "@material-ui/core/Radio";
import VLMap from "./VLMap";
import { IImageMetadata } from "../types/ImageMetadata";
import Typography from "@material-ui/core/Typography";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";

export interface IProps {
  bends: IBend[];
  selected: IBend | null;
  media: IJobDetailMedia;
  metadata: IImageMetadata;
  children: React.ReactElement<any>;
  bendHeight: number;
}

export interface IDispatchs {
  select: (bend: IBend) => void;
  changeHeight: (height: number) => void;
}

const selectBend = (bend: IBend, callback: (bend: IBend) => void) => () => {
  callback(bend);
};

const isCheck = (bend: IBend, selected: IBend | null) => {
  return !!selected && bend.bendId === selected.bendId;
};

const onChangeHeight = (changeHeight: (height: number) => void) => (
  event: React.ChangeEvent<HTMLInputElement>
) => {
  changeHeight(parseFloat(event.target.value));
};

const VLSideBar = ({
  bends,
  selected,
  select,
  media,
  metadata,
  changeHeight,
  classes,
  children,
  bendHeight
}: IProps & IDispatchs & IStyleTypeProps<typeof styles>) => (
  <Paper>
    <Typography variant="headline" component="h3" className={classes.title}>
      Virtual LOS
    </Typography>
    <VLMap
      bends={bends}
      media={media}
      selected={selected}
      metadata={metadata}
    />
    <Grid container={true} spacing={16}>
      <Grid item={true} xs={3}>
        <Typography
          variant="subheading"
          component="h5"
          className={classes.subTitle}
        >
          'B'end:
        </Typography>
      </Grid>
      <Grid item={true} xs={9}>
        <List
          component="nav"
          // subheader={<ListSubheader>B-Ends</ListSubheader>}
          className={classes.list}
        >
          {bends.filter(bend => isCheck(bend, selected)).map(bend => (
            <ListItem
              key={bend.bendId}
              role={undefined}
              dense={true}
              button={true}
              onClick={selectBend(bend, select)}
              className={classes.listItem}
            >
              {/* <Radio checked={isCheck(bend, selected)} /> */}
              <ListItemText
                primary={bend.bendSiteName}
                className={classes.itemInList}
              />
            </ListItem>
          ))}
        </List>
      </Grid>
    </Grid>

    <Grid container={true} spacing={16}>
      <Grid item={true} xs={3}>
        <Typography
          variant="subheading"
          component="h5"
          className={classes.subTitle}
        >
          Height(m):
        </Typography>
      </Grid>
      <Grid item={true} xs={9}>
        <div className={classes.textField}>
          <TextField
            id="height"
            value={bendHeight}
            onChange={onChangeHeight(changeHeight)}
            type="number"
            InputLabelProps={{
              shrink: true
            }}
            margin="normal"
            fullWidth={true}
          />
        </div>
      </Grid>
    </Grid>
    {children}
  </Paper>
);

export default withStyles(styles, { withTheme: true })(VLSideBar);
