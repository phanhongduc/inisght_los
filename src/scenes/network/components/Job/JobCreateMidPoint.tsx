import * as React from "react";
import throttle from "lodash-es/throttle";
import { defaultRules } from "react-hoc-form-validatable";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import IStyleProps from "../../../../styles/utils";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import Slider from "@material-ui/lab/Slider";
import * as mapboxgl from "mapbox-gl";
import "mapbox-gl/dist/mapbox-gl.css";
import { IBend } from "../../types/Job";
import Grid from "@material-ui/core/Grid";
import along from "@turf/along";
import near from "@turf/nearest-point-on-line";
import distance from "@turf/distance";
import { lineString, point } from "@turf/helpers";
import FresnelChart, {
  ITerrainDataChange
} from "../../../../components/FresnelChart/FresnelChart";
import { Feature, Point } from "geojson";
import cutCoordinate from "../../../../helpers/cutCoordinate";
import JobCreateMidPointForm from "./JobCreateMidPointForm";
import { Omit } from "utility-types";
import { ICreateMidPointData } from "../../actions/Job";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    mapDisplay: {
      height: "100%",
      maxHeight: 450
    },
    chartTop: {
      display: "flex",
      marginBottom: theme.spacing.unit * 2
    },
    end: {
      textAlign: "center",
      width: 120,
      "& > img": {
        display: "block",
        margin: "0 auto"
      }
    },
    chart: {
      height: 200
    },
    point: {
      height: "27px",
      width: "27px",
      backgroundColor: "#51bbd6",
      borderRadius: "50%",
      margin: "0 auto"
    },
    pointName: {
      textOverflow: "ellipsis",
      whiteSpace: "nowrap",
      overflow: "hidden"
    },
    list: {
      display: "flex",
      flexWrap: "wrap",
      maxWidth: 300,
      paddingLeft: 32,

      "& > dt": {
        fontWeight: "bold",
        flexBasis: "50%",
        maxWidth: "50%",
        margin: "0",
        paddingBottom: theme.spacing.unit,
        paddingTop: theme.spacing.unit
      },

      "& > dd": {
        flexBasis: "50%",
        maxWidth: "50%",
        margin: "0",
        paddingBottom: theme.spacing.unit,
        paddingTop: theme.spacing.unit
      }
    },
    loading: {
      left: "calc(50% - 25px)",
      bottom: 0
    },
    fChart: {
      maxHeight: 200
      // maxWidth: 600
    }
  });

interface IProps {
  bend: IBend;
  lon: number;
  lat: number;
  frequency: number;
  height: number;
  onSubmit: (
    data: Omit<ICreateMidPointData, "aendSiteId" | "aendAddress">,
    done: (reset?: boolean) => void
  ) => void;
  onCancel: () => void;
}

interface IStates {
  terrainData: string;
  heightOfStart: number;
  heightOfEnd: number;
  frequency: number;
  heightMidPoint: number;
  currentSlide: number;
  distanceMidPoint: number;
  coorMidPoint: number[];
}

class JobCreateMidPoint extends React.Component<IStyleProps & IProps, IStates> {
  private mapContainer: React.RefObject<HTMLDivElement> = React.createRef<
    HTMLDivElement
  >();
  private map: mapboxgl.Map;

  public state = {
    terrainData: "",
    frequency: this.props.frequency / 1000000,
    heightOfStart: this.props.height,
    heightOfEnd: this.props.bend.recommendHeight,
    heightMidPoint: 0,
    currentSlide: 50,
    distanceMidPoint: 0,
    coorMidPoint: []
  };

  private onChangeSlide = (e: React.ChangeEvent, value: number) => {
    this.updateMidPoint(value);
  };

  private updateMidPoint = (currentSlide?: number) => {
    const line = lineString([
      [this.props.lon, this.props.lat],
      [this.props.bend.lon, this.props.bend.lat]
    ]);

    const midPoint = along(
      line,
      (this.props.bend.distance * (currentSlide || this.state.currentSlide)) /
        100,
      {
        units: "meters"
      }
    );

    (this.map.getSource("mid") as mapboxgl.GeoJSONSource).setData({
      type: "FeatureCollection",
      features: midPoint
        ? ([midPoint] as Array<Feature<Point>>)
        : ([] as Array<Feature<Point>>)
    });

    this.setState({
      currentSlide: currentSlide || this.state.currentSlide,
      coorMidPoint: midPoint.geometry ? midPoint.geometry.coordinates : [],
      distanceMidPoint: distance(
        midPoint,
        point([this.props.lon, this.props.lat]),
        {
          units: "meters"
        }
      )
    });
  };

  private onMove = (e: any) => {
    const line = lineString([
      [this.props.lon, this.props.lat],
      [this.props.bend.lon, this.props.bend.lat]
    ]);

    const pointD = point([e.lngLat.lng, e.lngLat.lat]);

    const newPoint = near(line, pointD);

    this.updateMidPoint(
      (distance(
        newPoint as Feature<Point>,
        point([this.props.lon, this.props.lat]),
        {
          units: "meters"
        }
      ) /
        this.props.bend.distance) *
        100
    );
  };

  private onMoveThrottle = throttle(this.onMove, 15);

  private onUp = () => {
    if (this.mapContainer && this.mapContainer.current) {
      this.map.off("mousemove", this.onMoveThrottle);
      this.mapContainer.current.style.cursor = "";
    }
  };

  private onChangeMidPointHeight = (height: number) => {
    this.setState({
      heightMidPoint: height
    });
  };

  private onChangeTerrain = (data: ITerrainDataChange) => {
    this.setState({
      terrainData: JSON.stringify(data)
    });
  };

  private onChangeData = (field: string) => {
    return (e: React.ChangeEvent<HTMLInputElement>) => {
      switch (field) {
        case "jobHeight": {
          this.setState({
            heightOfStart: parseFloat(e.target.value)
          });
          break;
        }
        case "frequency": {
          this.setState({
            frequency: parseFloat(e.target.value)
          });
          break;
        }
        case "siteHeight": {
          this.setState({
            heightOfEnd: parseFloat(e.target.value)
          });
          break;
        }
      }
    };
  };

  private onSubmit = (inputs: any, done: (reset: boolean) => void) => {
    const { bend, lat, lon } = this.props;

    const data = {
      aendLon: lon,
      aendLat: lat,
      aendHeight: this.state.heightOfStart,
      bendSiteId: bend.bendId,
      bendAddress: bend.address,
      bendLon: bend.lon,
      bendLat: bend.lat,
      bendName: bend.bendSiteName,
      bendHeight: this.state.heightOfEnd,
      lat: this.state.coorMidPoint[1],
      lon: this.state.coorMidPoint[0],
      height: this.state.heightMidPoint,
      frequency: this.state.frequency * 1000000,
      terrainJson: this.state.terrainData
    };

    this.props.onSubmit(data, done);
  };

  public componentDidMount() {
    if (this.mapContainer.current) {
      this.map = new mapboxgl.Map({
        container: this.mapContainer.current,
        style: "/3d_building_style.json",
        center: [this.props.lon, this.props.lat],
        zoom: 13
      });

      this.map.addControl(new mapboxgl.NavigationControl(), "top-left");

      this.map.loadImage(
        require("src/assets/images/marker/ic_job.png"),
        (error: any, image: HTMLImageElement) => {
          this.map.addImage("job", image);

          this.map.addSource("measure-lines", {
            type: "geojson",
            data: {
              type: "Feature",
              geometry: {
                type: "LineString",
                coordinates: [
                  [this.props.lon, this.props.lat],
                  [this.props.bend.lon, this.props.bend.lat]
                ]
              },
              properties: {}
            }
          });

          this.map.addLayer({
            id: "measure-lines",
            type: "line",
            source: "measure-lines",
            layout: {
              "line-cap": "round",
              "line-join": "round"
            },
            paint: {
              "line-color": "#e25144",
              "line-width": 2.5,
              "line-opacity": 0.5
            },
            filter: ["in", "$type", "LineString"]
          });

          this.map.addSource("site", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: [
                {
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: [this.props.bend.lon, this.props.bend.lat]
                  },
                  properties: {}
                }
              ]
            }
          });

          this.map.addLayer({
            id: "site",
            type: "circle",
            source: "site",
            paint: {
              "circle-color": "#51bbd6",
              "circle-radius": 10
            }
          });

          this.map.addSource("job", {
            type: "geojson",
            data: {
              type: "FeatureCollection",
              features: [
                {
                  type: "Feature",
                  geometry: {
                    type: "Point",
                    coordinates: [this.props.lon, this.props.lat]
                  },
                  properties: {}
                }
              ]
            }
          });

          this.map.addLayer({
            id: "job",
            type: "symbol",
            source: "job",
            layout: {
              "icon-image": "job",
              "icon-size": 1,
              "icon-allow-overlap": true,
              "icon-rotation-alignment": "map"
            }
          });

          this.map.loadImage(
            require("src/assets/images/marker/gear_midpoint.png"),
            (err: any, img: HTMLImageElement) => {
              this.map.addImage("mid", img);

              this.map.addSource("mid", {
                type: "geojson",
                data: {
                  type: "FeatureCollection",
                  features: [] as Array<Feature<Point>>
                }
              });

              this.map.addLayer({
                id: "mid",
                type: "symbol",
                source: "mid",
                layout: {
                  "icon-image": "mid",
                  "icon-size": 1,
                  "icon-allow-overlap": true,
                  "icon-rotation-alignment": "map"
                }
              });

              this.map.on("mousedown", "mid", (e: any) => {
                e.preventDefault();

                if (this.mapContainer && this.mapContainer.current) {
                  this.mapContainer.current.style.cursor = "grab";
                }

                this.map.on("mousemove", this.onMoveThrottle);
                this.map.once("mouseup", this.onUp);
              });

              this.map.on("touchstart", "mid", (e: any) => {
                e.preventDefault();

                if (this.mapContainer && this.mapContainer.current) {
                  this.mapContainer.current.style.cursor = "grab";
                }

                this.map.on("touchmove", this.onMoveThrottle);
                this.map.once("touchend", this.onUp);
              });

              this.updateMidPoint();
            }
          );
        }
      );
    }
  }

  public render() {
    const { classes, bend, lat, lon } = this.props;
    return (
      <div className={classes.wrap}>
        <Grid container={true} spacing={16}>
          <Grid item={true} md={5}>
            <div className={`map ${classes.mapDisplay}`}>
              <div className="map-inner" ref={this.mapContainer} />
            </div>

            <div>
              <JobCreateMidPointForm
                onCancel={this.props.onCancel}
                submitCallback={this.onSubmit}
                onChangeData={this.onChangeData}
                frequency={this.state.frequency}
                heightOfStart={this.state.heightOfStart}
                heightOfEnd={this.state.heightOfEnd}
                rules={defaultRules}
              />
            </div>
          </Grid>
          <Grid item={true} md={7}>
            <div>
              <div className={classes.chartTop}>
                <div className={classes.end}>
                  <img
                    src={require("src/assets/images/marker/ic_job.png")}
                    alt="job"
                  />
                  <div>Job</div>
                  <div>
                    ({(this.state.distanceMidPoint / 1000).toFixed(2)} km)
                  </div>
                </div>
                <Slider
                  max={100}
                  value={this.state.currentSlide}
                  aria-labelledby="label"
                  onChange={this.onChangeSlide}
                />
                <div className={classes.end}>
                  <div className={classes.point} />
                  <div className={classes.pointName}>B-End</div>
                  <div>
                    ({(
                      (bend.distance - this.state.distanceMidPoint) /
                      1000
                    ).toFixed(2)}{" "}
                    km)
                  </div>
                </div>
              </div>
              <div>
                <dl className={classes.list}>
                  <dt>Height: </dt>
                  <dd>{this.state.heightMidPoint.toFixed(1)} m</dd>
                  <dt>Location: </dt>
                  <dd>
                    {cutCoordinate(this.state.coorMidPoint[0])} ,{" "}
                    {cutCoordinate(this.state.coorMidPoint[1])}
                  </dd>
                </dl>
              </div>
              <div className={classes.chart}>
                <FresnelChart
                  classesOverride={{
                    progress: classes.loading,
                    chart: classes.fChart
                  }}
                  onChangeMidPointHeight={this.onChangeMidPointHeight}
                  onChangeTerrain={this.onChangeTerrain}
                  distanceMidPoint={this.state.distanceMidPoint}
                  id={bend.bendId + "midPoint"}
                  frequency={this.state.frequency * 1000000}
                  heightOfStart={this.state.heightOfStart}
                  heightOfEnd={this.state.heightOfEnd}
                  aEndLat={lat}
                  aEndLon={lon}
                  bEndLat={bend.lat}
                  bEndLon={bend.lon}
                  distanceFromAendToBend={bend.distance}
                />
              </div>
            </div>
          </Grid>
        </Grid>
      </div>
    );
  }
}

export default withStyles(styles)(JobCreateMidPoint);
