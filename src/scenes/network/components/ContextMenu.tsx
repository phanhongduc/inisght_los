import * as React from "react";
import { createStyles, withStyles } from "@material-ui/core/styles";
import IStyleProps from "../../../styles/utils";

const styles = createStyles({
  list: {
    listStyleType: "none",
    padding: "0",
    margin: "0",
    "& > li": {
      padding: "3px 8px"
    },
    "& > li:hover": {
      backgroundColor: "#dbdbdb"
    }
  }
});

export enum ContextMenuList {
  HIDE_BTS = "hideBTS",
  NEW_JOB = "newJob"
}

interface IProps {
  onClickMenu: (menu: ContextMenuList) => () => void;
}

const ContextMenu = ({ classes, onClickMenu }: IStyleProps & IProps) => {
  return (
    <ul className={classes.list}>
      <li onClick={onClickMenu(ContextMenuList.NEW_JOB)}>Create new job</li>
    </ul>
  );
};

export default withStyles(styles)(ContextMenu);
