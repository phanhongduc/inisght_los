import * as Loadable from "react-loadable";
import SectionLoading from "../../components/SectionLoading";
import * as React from "react";
import {
  IStartBoardComponent,
  IStartIncidentComponent
} from "./actions/MainMap";
import * as actions from "./actions";
import { IRoutes } from "src/components/TabRoute/types/Routes";
import paths from "src/paths";

const LoadableMapBoard = Loadable({
  loader: () => import("./containers/MapBoard"),
  loading: SectionLoading
});

const LoadableJobDetail = Loadable({
  loader: () => import("./containers/JobDetail"),
  loading: SectionLoading
});

const networkRoutes: IRoutes<IStartBoardComponent | IStartIncidentComponent> = [
  {
    isAlways: true,
    exact: true,
    tabName: "Network",
    uri: paths.board,
    component: () => <LoadableMapBoard />,
    startAction: actions.startBoardComponent
  },
  {
    exact: true,
    tabName: "Job",
    uri: paths.jobDetail,
    component: ({ match }: { match: { params: { id: string } } }) => (
      <LoadableJobDetail key={match.params.id} id={match.params.id} />
    ),
    startAction: actions.startBoardComponent
  }
];

export default networkRoutes;
