import IVirtualLOS from "../components/Job/VirtualLOS/types";

export interface IJobDetail {
  jobName: string;
  address: string;
  lon: number;
  lat: number;
  recommendHeight: number;
  frequency: number;
  jobStatus: number;
  jobId: string;
  engineerId: string;
  bends: IBend[];
}

export interface IJobInfo {
  name: string;
  address: string;
  lon: number;
  lat: number;
  jobStatus: number;
  id: string;
}

export interface IJobDetailBEnd {
  id: string;
  bendId: string;
  bendName: string;
  address: string;
  jobId: string;
  latitude: number;
  longitude: number;
  media: IJobDetailBEndMedia[];
  isMidPoint: boolean;
}

export interface IJobDetailBEndMedia {
  height: number;
  media: IJobDetailMedia[];
}

export enum MediaStatus {
  Draft = 0,
  Released = 1
}

export interface IJobDetailMedia {
  url: string;
  mediaId: string;
  mediaStatus: MediaStatus;
  currentDroneHeight: number;
  currentDroneBearing: number;
  bendLat: number;
  bendLon: number;
  currentLat: number;
  currentLon: number;
  thumbnailUrl: string;
  thumbnailAvailable: boolean;
  isProcessing: boolean;
  isLosProcessed: boolean;
  losUrl: string;
  gimbalRoll: number;
  gimbalPitch: number;
  gimbalYaw: number;
  flightRoll: number;
  flightPitch: number;
  flightYaw: number;
  createDate: string;
  focalLength35: number;
  focalLength: number;
  imageWidth: number;
  imageHeight: number;
  absoluteAltitude: number;
  relativeAltitude: number;
}

export interface IBend {
  bendId: string;
  bendSiteName: string;
  address: string;
  lon: number;
  lat: number;
  displayOrder: number;
  recommendHeight: number;
  distance: number;
}

export interface IMidPoint {
  aendSiteId: string;
  aendAddress: string;
  aendLon: number;
  aendLat: number;
  aendHeight: number;
  bendSiteId: string;
  bendAddress: string;
  bendLon: number;
  bendLat: number;
  bendName: string;
  bendHeight: number;
  frequency: number;
  lat: number;
  lon: number;
  height: number;
  id: string;
  createDate: string;
  terrainJson: string;
  aendMedias: IMidPointMedia[];
  bendMedias: IMidPointMedia[];
}

export interface IMidPointMedia {
  url: string;
  mediaId: string;
  mediaStatus: number;
  currentDroneHeight: number;
  currentDroneBearing: number;
  bendLat: number;
  bendLon: number;
  currentLat: number;
  currentLon: number;
  thumbnailUrl: string;
  thumbnailAvailable: boolean;
  isProcessing: boolean;
  isLosProcessed: boolean;
  losUrl: string;
  gimbalRoll: number;
  gimbalPitch: number;
  gimbalYaw: number;
  flightRoll: number;
  flightPitch: number;
  flightYaw: number;
  createDate: string;
  focalLength35: number;
  focalLength: number;
  imageWidth: number;
  imageHeight: number;
  absoluteAltitude: number;
  relativeAltitude: number;
}

export interface IJobDetailState {
  isFetching: boolean;
  isFailed: boolean;
}

export interface IJobLog {
  id: string;
  createdDateUtc: string;
  fileUrl: string;
}

export interface IJobLogResult {
  isFetching: boolean;
  isFailed: boolean;
  data: IJobLog[];
  lastModified: number;
}

export interface IJobLogFile {
  id: string;
  url: string;
  data: string[][];
  lastModified: number;
  isFetching: boolean;
  isFailed: boolean;
}

export interface IStoreJobs {
  listJob: {
    [k: string]: IJobDetail;
  };
  jobState: {
    [k: string]: IJobDetailState;
  };
  jobBend: {
    [k: string]: IJobDetailBEnd[];
  };
  jobMidPoint: {
    [k: string]: IMidPoint[];
  };
  jobLog: {
    [k: string]: IJobLogResult;
  };
  logs: {
    [k: string]: IJobLogFile;
  };
  virtualLos: IVirtualLOS;
  recentJobs: IJobInfo[];
}
