import { DeckGLProperties } from "deck.gl";
import { ISite } from "./Site";
import IGeoProperties from "./GeoProperties";
import IJsonVersion from "./JsonVersion";
import IMapState from "./MapState";

export default interface INetwork {
  viewState: DeckGLProperties;
  siteData: ISite[];
  hoverItem: IGeoProperties | null;
  jsonVersion: IJsonVersion;
  mapState: IMapState;
  // tabs: ITabsInfo;
}
