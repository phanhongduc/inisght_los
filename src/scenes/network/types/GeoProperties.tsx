export default interface IGeoProperties<T = {}> {
  siteId: string;
  name: string;
  id: string;
  type: string;
  data?: T;
}

export interface IPointGeoJsonProperties {
  id: string;
  Altitude: number;
  IsDrone: boolean;
  address: string;
  bearing: number;
  businessname: string;
  contactphonenumber: string;
  fieldofview: number;
  heightofcamera: number;
  isavailable: boolean;
  lengthoffov: number;
  ownername: string;
  pointid: string;
  pointname: string;
  timetosavevideo: number;
  timetosavevideotype: string;
  type: number;
}

export interface ICoverageGeoJsonProperties {
  id: string;
  pointid: string;
  pointname: string;
  type: number;
}

export interface ICoverageProperties {
  pointid: string;
  pointname: string;
  type: 0;
  id: string;
}
