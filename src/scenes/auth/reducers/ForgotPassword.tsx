import { IChangeFormForgotPassword, IAuthenticationFail } from "../actions";
import {
  CHANGE_FORM_FORGOT_PASSWORD,
  AUTHENTICATION_FAIL
} from "../../../constants";
import {
  IFormForgotPassword,
  IErrorForgotPassword
} from "../components/ForgotPassword";
import { combineReducers } from "redux";

const form = (
  state: IFormForgotPassword = { email: "" },
  action: IChangeFormForgotPassword
): IFormForgotPassword => {
  switch (action.type) {
    case CHANGE_FORM_FORGOT_PASSWORD:
      return action.payload;
  }
  return state;
};

function validateEmail(email: string) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(String(email).toLowerCase());
}

const error = (
  state: IErrorForgotPassword = { email: "" },
  action: IChangeFormForgotPassword | IAuthenticationFail
): IErrorForgotPassword => {
  switch (action.type) {
    case CHANGE_FORM_FORGOT_PASSWORD: {
      const value = action.payload;
      if (!value.email) {
        return {
          email: "can't empty"
        };
      }
      if (!validateEmail(value.email)) {
        return {
          email: "is not in the correct format"
        };
      }

      return {
        ...state,
        email: ""
      };
    }
    case AUTHENTICATION_FAIL:
      return {
        ...state,
        server: action.payload
      };
  }
  return state;
};

export const forgot = combineReducers({
  form,
  error
});
