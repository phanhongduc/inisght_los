import * as React from "react";
import { RouteComponentProps, Link } from "react-router-dom";
import FormGroup from "@material-ui/core/FormGroup";
import TextField from "@material-ui/core/TextField";
import {
  Theme,
  Card,
  CardContent,
  CardActions,
  Button
} from "@material-ui/core";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import IStyleProps from "../../../styles/utils";
import Typography from "@material-ui/core/Typography";
import logo from "src/assets/images/logo-main.png";

const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  card: {
    width: 550,
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  },
  logo: {
    position: "absolute",
    top: "-88px",
    left: "50%",
    transform: "translate(-50%)"
  },
  button: {
    margin: "auto"
  },
  bottom: {
    display: "block",
    textAlign: "right"
  },
  forgot: {
    textDecoration: "none",
    fontSize: "small"
  }
});

export interface IFormForgotPassword {
  email: string;
}
export interface IErrorForgotPassword {
  email: string;
  server?: string;
}
export interface IProps {
  form: IFormForgotPassword;
  error?: IErrorForgotPassword;
  onChange: (form: IFormForgotPassword) => void;
  onReset?: (form: IFormForgotPassword) => void;
}
export interface IRouteProps extends RouteComponentProps<any>, IProps {}

const ForgotPasswordForm = ({
  classes,
  form,
  error = {
    email: null
  },
  onChange
}: IStyleProps & IProps) => {
  const isErrors = {
    email: error ? !!error.email : false
  };

  const onChangeEvent = (name: string) => (event: any) => {
    onChange({
      ...form,
      [name]: event.target.value
    });
  };
  return (
    <FormGroup>
      <TextField
        required={true}
        error={isErrors.email}
        id="required"
        label={!isErrors.email ? `email` : `email ${error.email}`}
        className={classes.textField}
        margin="normal"
        fullWidth={true}
        value={form.email}
        onChange={onChangeEvent("email")}
      />
    </FormGroup>
  );
};
// const ForgotLink = (props: any) => <Link to="/auth/forgot" {...props} />;

const ForgotPassword = ({
  classes,
  form,
  error,
  onChange,
  onReset
}: IStyleProps & IProps) => {
  const submitFnc = () => {
    if (onReset) {
      onReset(form);
    }
  };
  return (
    <div className={classes.card}>
      <img src={logo} className={classes.logo} />
      <Card>
        <CardContent>
          <Typography variant="headline">Forgot Password</Typography>
          {error &&
            error.server && (
              <Typography variant="caption" color="error">
                {error.server}
              </Typography>
            )}
          <ForgotPasswordForm {...{ classes, form, error, onChange }} />
        </CardContent>
        <CardActions>
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            onClick={submitFnc}
          >
            Reset Password
          </Button>
          {/* <Button size="small" component={ForgotLink}>
            Forgot password
          </Button> */}
        </CardActions>
        <CardActions className={classes.bottom}>
          <Link to="/auth/signin" className={classes.forgot}>
            Sign In
          </Link>
        </CardActions>
      </Card>
    </div>
  );
};

export default withStyles(styles)(ForgotPassword);
