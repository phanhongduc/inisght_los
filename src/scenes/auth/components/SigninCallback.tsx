import * as React from "react";
import { CallbackComponent } from "redux-oidc";
import { withRouter, RouteComponentProps } from "react-router-dom";
import { mgr } from "../middlewares/userManager";
import { User } from "oidc-client";
import LinearProgress from "@material-ui/core/LinearProgress";
import Redirecting from "src/components/Redirecting";

export interface IProps {
  setupAuthentication: (user: User) => void;
}
export interface IState {
  hasError: boolean;
}
export interface IRouteMenuProps extends RouteComponentProps<any>, IProps {}

class CallbackPage extends React.Component<IRouteMenuProps, IState> {
  constructor(props: IRouteMenuProps) {
    super(props);
    this.state = {
      hasError: false
    };
  }
  public callback = () => {
    console.log("CallbackPage: getUser");
    mgr.getUser().then(user => {
      console.log("CallbackPage: end", user);
      if (user) {
        this.props.setupAuthentication(user);
      } else {
        console.log("User not logged in");
      }
    });
  };
  private errorCallback = () => {
    // Display fallback UI
    this.setState({ hasError: true });
  };
  public render() {
    // just redirect to '/' in both cases
    const { hasError } = this.state;
    if (hasError) {
      mgr.signinRedirect();
      return <Redirecting />;
    }

    console.log("CallbackPage: start");
    return (
      <CallbackComponent
        userManager={mgr}
        successCallback={this.callback}
        errorCallback={this.errorCallback}
      >
        <LinearProgress />
      </CallbackComponent>
    );
  }
}

export default withRouter<IRouteMenuProps>(CallbackPage);
