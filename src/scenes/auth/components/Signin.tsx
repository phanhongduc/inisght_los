import * as React from "react";
import { Theme } from "@material-ui/core/styles";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import IStyleProps from "../../../styles/utils";
import SectionLoading from "src/components/SectionLoading";
const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit
  },
  card: {
    width: 550,
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  },
  logo: {
    position: "absolute",
    top: "-88px",
    left: "50%",
    transform: "translate(-50%)"
  },
  button: {
    margin: "auto"
  },
  bottom: {
    display: "block",
    textAlign: "right"
  },
  forgot: {
    textDecoration: "none",
    fontSize: "small"
  }
});

export interface IFormLogin {
  username: string;
  password: string;
}
export interface IErrorLogin {
  username: string;
  password: string;
  server?: string;
}
export interface IProps {
  form: IFormLogin;
  error?: IErrorLogin;
  onChange: (form: IFormLogin, keys: string[]) => void;
  onLogin?: (form: IFormLogin) => void;
}

class Signin extends React.Component<IStyleProps & IProps> {
  public render() {
    return <SectionLoading />;
  }
}

export default withStyles(styles)(Signin);
