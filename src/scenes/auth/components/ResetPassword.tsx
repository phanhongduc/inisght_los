import * as React from "react";
import { Theme, Card, CardContent, CardActions } from "@material-ui/core";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import IStyleProps from "../../../styles/utils";
import Typography from "@material-ui/core/Typography";
import logo from "src/assets/images/logo-main.png";
import ResetPasswordForm from "./ResetPasswordForm";
import { defaultRules } from "react-hoc-form-validatable";
import { IResetPasswordPayload } from "../actions/ResetPassword";
import { RouteComponentProps, withRouter, Link } from "react-router-dom";

const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  container: {
    display: "flex",
    flexWrap: "wrap"
  },
  card: {
    width: 550,
    position: "absolute",
    left: "50%",
    top: "50%",
    transform: "translate(-50%, -50%)"
  },
  logo: {
    position: "absolute",
    top: "-88px",
    left: "50%",
    transform: "translate(-50%)"
  },
  bottom: {
    display: "block",
    textAlign: "right"
  },
  forgot: {
    textDecoration: "none",
    fontSize: "small"
  }
});

export interface IProps {
  resetPassword: (
    data: IResetPasswordPayload,
    cb: (reset: boolean) => void
  ) => void;
}

export interface IRouteMenuProps extends RouteComponentProps<any>, IProps {}

class ResetPassword extends React.Component<IStyleProps & IRouteMenuProps> {
  private onSubmit = (inputs: any, reset: (should: boolean) => void) => {
    this.props.resetPassword(
      {
        code: this.props.match.params.token,
        userId: this.props.match.params.id,
        confirmPassword: inputs.confirmPassword.value,
        password: inputs.password.value
      },
      (should: boolean) => {
        reset(should);
        if (should) {
          setTimeout(() => {
            window.location.href = "";
          }, 2500);
        }
      }
    );
  };

  public render() {
    const { classes } = this.props;
    return (
      <div className={classes.card}>
        <img src={logo} className={classes.logo} />
        <Card>
          <CardContent>
            <Typography variant="headline">Reset password</Typography>
            <ResetPasswordForm
              submitCallback={this.onSubmit}
              validateLang="en"
              rules={defaultRules}
            />
          </CardContent>
          <CardActions className={classes.bottom}>
            <Link to="/auth/signin" className={classes.forgot}>
              Back to sign in
            </Link>
          </CardActions>
        </Card>
      </div>
    );
  }
}

export default withRouter<IRouteMenuProps>(withStyles(styles)(ResetPassword));
