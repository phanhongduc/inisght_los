import { connect, Dispatch } from "react-redux";
import * as actions from "../actions";
import { IStoreState } from "../../../types";
import Signin from "../components/Signin";
import { IFormLogin } from "../components/Signin";

export function mapStateToProps({ login }: IStoreState) {
  return {
    ...login
  };
}

export function mapDispatchToProps(
  dispatch: Dispatch<actions.IChangeFormLogin | actions.ISubmitFormLogin>
): {
  onChange: (form: IFormLogin, keys: string[]) => void;
  onLogin: (form: IFormLogin) => void;
} {
  return {
    onChange: (form: IFormLogin, keys: string[]) => {
      dispatch(actions.changeFormLogin(form, keys));
    },
    onLogin: (form: IFormLogin) => {
      dispatch(actions.submitFormLogin(form));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)<{}>(Signin);
