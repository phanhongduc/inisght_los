import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import ResetPassword from "../components/ResetPassword";
import {
  IResetPassword,
  IResetPasswordPayload,
  resetPassword
} from "../actions/ResetPassword";

export function mapStateToProps({ login }: IStoreState) {
  return {
    ...login
  };
}

export function mapDispatchToProps(dispatch: Dispatch<IResetPassword>) {
  return {
    resetPassword: (
      data: IResetPasswordPayload,
      cb: (reset: boolean) => void
    ) => {
      dispatch(resetPassword(data, cb));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)<{}>(ResetPassword);
