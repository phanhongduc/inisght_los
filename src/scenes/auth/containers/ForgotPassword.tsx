import { connect, Dispatch } from "react-redux";
import * as actions from "../actions";
import { IStoreState } from "../../../types";
import ForgotPassword from "../components/ForgotPassword";
import { IFormForgotPassword } from "../components/ForgotPassword";

export function mapStateToProps({ forgot }: IStoreState) {
  return {
    ...forgot
  };
}

export function mapDispatchToProps(
  dispatch: Dispatch<
    actions.IChangeFormForgotPassword | actions.ISubmitFormForgotPassword
  >
): {
  onChange: (form: IFormForgotPassword) => void;
  onReset: (form: IFormForgotPassword) => void;
} {
  return {
    onChange: (form: IFormForgotPassword) => {
      dispatch(actions.changeFormForgotPassword(form));
    },
    onReset: (form: IFormForgotPassword) => {
      dispatch(actions.submitFormForgotPassword(form));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)<{}>(ForgotPassword);
