export default interface IAccountRequest {
  grant_type: string;
  username: string;
  password: string;
  client_id: string;
  client_secret: string;
}

export const createRequest = ({
  username,
  password
}: {
  username: string;
  password: string;
}): IAccountRequest => {
  return {
    grant_type: "password",
    username,
    password,
    client_id: "0d82b092-1c10-4390-b822-f73f010e6228",
    client_secret: "f32ac06c-c56d-4fb9-aba8-54ce347ac2a8"
  };
};
