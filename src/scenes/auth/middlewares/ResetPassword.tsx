import axios, { AxiosError, AxiosResponse } from "axios";
import { Dispatch, Store } from "react-redux";
import Variant from "../../../components/notification/types/variant";
import { REACT_APP_API_URL } from "../../../environment";
import { IStoreState } from "../../../types";
import { common } from "../../../actions";
import getErrorMessage from "src/helpers/getErrorMessage";
import { IResetPassword } from "../actions/ResetPassword";
import { USER_RESET_PASSWORD } from "../constants/ResetPassword";

export const resetPasswordMiddleware = (store: Store<IStoreState>) => (
  next: Dispatch<IResetPassword>
) => (action: IResetPassword) => {
  switch (action.type) {
    case USER_RESET_PASSWORD: {
      axios
        .put(
          `${REACT_APP_API_URL}/api/v2/user/password/reset/confirm`,
          action.payload
        )
        .then((data: AxiosResponse<any>) => {
          common.fireNotification(store.dispatch)({
            message: "Reset password success.",
            variant: Variant.SUCCESS
          });
          action.meta.cb(true);
        })
        .catch((e: AxiosError) => {
          console.error(e);
          const response = e.response as AxiosResponse;
          common.fireNotification(store.dispatch)({
            message:
              response && response.data
                ? getErrorMessage(response.data, "Reset password failed.")
                : "Reset password failed.",
            variant: Variant.ERROR
          });
          action.meta.cb(false);
        });
      break;
    }
  }
  return next(action);
};
