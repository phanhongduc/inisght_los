import { Store } from "react-redux";
import { IStoreState } from "src/types";
import { Dispatch } from "redux";
import { common } from "src/actions";
import { ISubmitFormForgotPassword } from "../actions";
import { SUBMIT_FORM_FORGOT_PASSWORD } from "src/constants";
import Variant from "src/components/notification/types/variant";
import axios, { AxiosResponse, AxiosError } from "axios";
import { REACT_APP_API_URL } from "src/environment";
import * as actions from "../actions";
import { IFormForgotPassword } from "../components/ForgotPassword";

const validate = (form: IFormForgotPassword) => {
  return form.email;
};
interface IErrorMessage {
  ErrorMessage: string;
}

export const reset = (store: Store<IStoreState>) => (
  next: Dispatch<ISubmitFormForgotPassword>
) => (action: ISubmitFormForgotPassword) => {
  switch (action.type) {
    case SUBMIT_FORM_FORGOT_PASSWORD: {
      if (!validate(action.payload)) {
        store.dispatch(actions.changeFormForgotPassword(action.payload));
        break;
      }

      axios({
        method: "post",
        url: `${REACT_APP_API_URL}/api/v2/user/password/forgot`,
        data: action.payload
      })
        .then((data: AxiosResponse<any | IErrorMessage>) => {
          common.fireNotification(store.dispatch)({
            message: "Please check email to reset password!",
            variant: Variant.SUCCESS
          });
        })
        .catch((e: AxiosError) => {
          if (e.response && e.response.status === 400) {
            store.dispatch(
              actions.authenticationFail(e.response.data.ErrorMessage)
            );
          }
        });
    }
  }

  return next(action);
};
