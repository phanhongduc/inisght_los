import { Store } from "react-redux";
import {} from "react-router";
// import axios, { AxiosResponse } from "axios";
import { IStoreState } from "src/types";
import { Dispatch } from "redux";
import {
  IRecheckAuthentication,
  ISetupAuthentication,
  ILogout
} from "../actions";
import {
  SETUP_AUTHENTICATION,
  APPLICATION_INIT,
  LOGOUT,
  SIGN_IN_SILENT
} from "src/constants";
import { IApplicationInit, ISignInSilent, signInSilent } from "src/actions";
import * as actions from "../actions";

// import IAccount from "../../../types/Account";
import { selectLib, setListLibs, getListLibs } from "../../lib/actions/libs";
import { mgr } from "./userManager";
import clientStorage, { ClientStorageKey } from "src/helpers/clientStorage";
const SILENT_TIME_OUT: number = 300000; // 10 minutes
export const saveAccount2LocalStorage = (store: Store<IStoreState>) => (
  next: Dispatch<ISetupAuthentication>
) => (action: ISetupAuthentication) => {
  switch (action.type) {
    case SETUP_AUTHENTICATION: {
      clientStorage.setItem(
        ClientStorageKey.ACCOUNT,
        JSON.stringify(action.payload || {})
      );
    }
  }

  return next(action);
};

export const setupSignIn = (store: Store<IStoreState>) => (
  next: Dispatch<IApplicationInit | ISignInSilent>
) => (action: IApplicationInit | ISignInSilent) => {
  switch (action.type) {
    case APPLICATION_INIT: {
      console.log("APPLICATION_INIT: start");
      mgr.getUser().then(user => {
        console.log("APPLICATION_INIT: end", user);
        if (user) {
          const time = new Date().getTime();
          if (time < user.expires_at * 1000) {
            store.dispatch(actions.setupAuthentication(user));
            store.dispatch(signInSilent(user.expires_at * 1000 - time));
          }
        } else {
          if (window.location.href.indexOf("#/callback#") === -1) {
            console.log("User not logged in");
          }
        }
        store.dispatch(actions.checkingAuthentication(false));
      });
    }
  }

  return next(action);
};

export const setupSignInSilent = (store: Store<IStoreState>) => (
  next: Dispatch<ISignInSilent>
) => (action: ISignInSilent) => {
  switch (action.type) {
    case SIGN_IN_SILENT: {
      let timeout: number = action.payload - SILENT_TIME_OUT;
      if (timeout < 0) {
        timeout = 100;
      }
      console.log(timeout);
      setTimeout(() => {
        mgr.startSilentRenew();
        mgr.signinSilentCallback();
        mgr.signinSilent().then(el => {
          store.dispatch(actions.setupAuthentication(el));
        });
      }, timeout);
    }
  }

  return next(action);
};

export const logout = (store: Store<IStoreState>) => (
  next: Dispatch<ILogout>
) => (action: ILogout) => {
  switch (action.type) {
    case LOGOUT: {
      mgr.signoutRedirect();
    }
  }
  return next(action);
};

export const recheckAuthentication = (store: Store<IStoreState>) => (
  next: Dispatch<ISetupAuthentication | IRecheckAuthentication>
) => (action: ISetupAuthentication | IRecheckAuthentication) => {
  switch (action.type) {
    case SETUP_AUTHENTICATION: {
      console.log(action);
      if (!action.payload) {
        clientStorage.removeItem(ClientStorageKey.LIBS);
        clientStorage.removeItem(ClientStorageKey.CURRENT_LIB);
      } else {
        const currentLib = clientStorage.getItem(
          ClientStorageKey.CURRENT_LIB,
          action.payload
        );
        const libs = clientStorage.getItem(
          ClientStorageKey.LIBS,
          action.payload
        );
        if (currentLib && libs) {
          store.dispatch(setListLibs(JSON.parse(libs)));
          store.dispatch(selectLib(currentLib));
          store.dispatch(getListLibs());
        }
      }
      break;
    }
  }

  return next(action);
};
