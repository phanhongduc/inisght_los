import * as constants from "src/constants";
import { Action } from "redux";
import IAccount from "../../../types/Account";
import { Dispatch } from "react-redux";
import { User } from "oidc-client";

export interface ISetupAuthentication
  extends Action<constants.SETUP_AUTHENTICATION> {
  payload?: User;
}

export interface ICheckingAuthentication
  extends Action<constants.CHECKING_AUTHENTICATION> {
  payload: boolean;
}

export interface ILogout extends Action<constants.LOGOUT> {}

export function setupAuthentication(account?: User): ISetupAuthentication {
  return {
    payload: account,
    type: constants.SETUP_AUTHENTICATION
  };
}
export interface IRecheckAuthentication extends Action<constants.AUTH_RECHECK> {
  payload?: IAccount;
}

export function logout(): ILogout {
  return {
    type: constants.LOGOUT
  };
}

export function checkingAuthentication(
  isChecking: boolean
): ICheckingAuthentication {
  return {
    type: constants.CHECKING_AUTHENTICATION,
    payload: isChecking
  };
}

export interface IRecheckAuthentication extends Action<constants.AUTH_RECHECK> {
  payload?: IAccount;
}

export const recheckAuthentication = (
  dispatch: Dispatch<IRecheckAuthentication>
) => (account: IAccount) => {
  return dispatch({
    payload: account,
    type: constants.AUTH_RECHECK
  });
};
