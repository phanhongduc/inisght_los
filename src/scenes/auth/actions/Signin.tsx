import * as constants from "src/constants";
import { Action } from "redux";
import { IFormLogin } from "../components/Signin";

export interface IChangeFormLogin extends Action<constants.CHANGE_FORM_LOGIN> {
  payload: { value: IFormLogin; keys: string[] };
}
export interface IAuthenticationFail
  extends Action<constants.AUTHEN_TICATION_FAIL> {
  payload: string;
}

export interface ISubmitFormLogin extends Action<constants.SUBMIT_FORM_LOGIN> {
  payload: IFormLogin;
}

export function changeFormLogin(
  form: IFormLogin,
  keys: string[]
): IChangeFormLogin {
  return {
    payload: { value: form, keys },
    type: constants.CHANGE_FORM_LOGIN
  };
}
export function authenticationFail(message: string): IAuthenticationFail {
  return {
    payload: message,
    type: constants.AUTHENTICATION_FAIL
  };
}

export function submitFormLogin(form: IFormLogin): ISubmitFormLogin {
  return {
    payload: form,
    type: constants.SUBMIT_FORM_LOGIN
  };
}
