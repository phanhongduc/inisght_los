import { Action } from "redux";
import { USER_RESET_PASSWORD } from "../constants/ResetPassword";

export interface IResetPasswordPayload {
  userId: string;
  password: string;
  confirmPassword: string;
  code: string;
}

export interface IResetPassword extends Action<USER_RESET_PASSWORD> {
  payload: IResetPasswordPayload;
  meta: {
    cb: (reset: boolean) => void;
  };
}

export function resetPassword(
  payload: IResetPasswordPayload,
  cb: (reset: boolean) => void
): IResetPassword {
  return {
    type: USER_RESET_PASSWORD,
    payload,
    meta: {
      cb
    }
  };
}
