import * as constants from "src/constants";
import { Action } from "redux";
import { IFormForgotPassword } from "../components/ForgotPassword";

export interface IChangeFormForgotPassword
  extends Action<constants.CHANGE_FORM_FORGOT_PASSWORD> {
  payload: IFormForgotPassword;
}

export interface ISubmitFormForgotPassword
  extends Action<constants.SUBMIT_FORM_FORGOT_PASSWORD> {
  payload: IFormForgotPassword;
}

export function submitFormForgotPassword(
  form: IFormForgotPassword
): ISubmitFormForgotPassword {
  return {
    payload: form,
    type: constants.SUBMIT_FORM_FORGOT_PASSWORD
  };
}

export function changeFormForgotPassword(
  form: IFormForgotPassword
): IChangeFormForgotPassword {
  return {
    payload: form,
    type: constants.CHANGE_FORM_FORGOT_PASSWORD
  };
}
