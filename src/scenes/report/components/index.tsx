import withStyles from "@material-ui/core/styles/withStyles";
import styles from "../styles";
import { IStyleTypeProps } from "src/styles/utils";
import * as React from "react";
import Paper from "@material-ui/core/Paper";
import { Report } from "react-powerbi-client";
import { TokenType, Permissions, FilterType } from "powerbi-models";
import { Embed } from "powerbi-client";
import CircularProgress from "@material-ui/core/CircularProgress";

export interface IProps {
  libId: string;
  accessToken: string;
  embedUrl: string;
  id: string;
}

export interface IDispatchs {
  onClose?: () => void;
}

const libraryFilter = (libId: string) => ({
  $schema: "http://powerbi.com/product/schema#advanced",
  target: {
    table: "Jobs",
    column: "LibraryId"
  },
  logicalOperator: "And",
  conditions: [
    {
      operator: "Is",
      value: libId
    }
  ],
  filterType: FilterType.Advanced
});
const PBReport = ({
  libId,
  accessToken,
  embedUrl,
  id,
  classes
}: IProps & IDispatchs & IStyleTypeProps<typeof styles>) => {
  if (!embedUrl) {
    return (
      <CircularProgress
        size={50}
        style={{
          display: "block",
          margin: "auto"
        }}
      />
    );
  }

  const onEmbedded = (e: Embed) => {
    e.on("error", event => {
      console.log(event);
    });
  };
  return (
    <Paper className={classes.root}>
      <Report
        id={id}
        accessToken={accessToken}
        embedUrl={embedUrl}
        tokenType={TokenType.Embed}
        permissions={Permissions.All}
        onEmbedded={onEmbedded}
        className={classes.report}
        filterPaneEnabled={false}
        navContentPaneEnabled={false}
        filters={[libraryFilter(libId)]}
      />
    </Paper>
  );
};

export default withStyles(styles, { withTheme: true })(PBReport);
