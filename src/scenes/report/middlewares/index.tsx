import { Store, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import { IReportLoadApi, reportLoaded } from "../actions";
import { REPORT_LOAD_API } from "../constants";
import axios, { AxiosResponse } from "axios";
import { REACT_APP_API_URL } from "../../../environment";
import { IReportDetail } from "../types";

export const reportInit = (store: Store<IStoreState>) => (
  next: Dispatch<IReportLoadApi>
) => (action: IReportLoadApi) => {
  switch (action.type) {
    case REPORT_LOAD_API: {
      axios
        .get(`${REACT_APP_API_URL}/api/Misc/${action.payload}/bi`)
        .then((reps: AxiosResponse<IReportDetail>) => {
          console.log(reps.data);
          reportLoaded(store.dispatch)(reps.data);
        });
      break;
    }
  }

  return next(action);
};
