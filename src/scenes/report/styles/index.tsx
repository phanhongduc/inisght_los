import { Theme, createStyles } from "@material-ui/core/styles";
const styles = (theme: Theme) =>
  createStyles({
    root: {
      height: "calc(100% - 4px)",
      boxShadow: "none"
    },
    report: {
      height: "100%"
    }
  });

export default styles;
