export const REPORT_LOADED = "REPORT_LOADED";
export type REPORT_LOADED = typeof REPORT_LOADED;

export const REPORT_LOAD_API = "REPORT_LOAD_API";
export type REPORT_LOAD_API = typeof REPORT_LOAD_API;
