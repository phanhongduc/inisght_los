import { Action } from "redux";
import { REPORT_LOADED, REPORT_LOAD_API } from "../constants";
import { Dispatch } from "react-redux";
import { IReportDetail } from "../types";

export interface IReportLoaded extends Action<REPORT_LOADED> {
  payload: IReportDetail;
}

export const reportLoaded = (dispatch: Dispatch<IReportLoaded>) => (
  flag: IReportDetail
) => {
  dispatch({
    payload: flag,
    type: REPORT_LOADED
  });
};

export interface IReportLoadApi extends Action<REPORT_LOAD_API> {
  payload: string;
}

export const reportLoadApi = (dispatch: Dispatch<IReportLoadApi>) => (
  libId: string
) => {
  dispatch({
    payload: libId,
    type: REPORT_LOAD_API
  });
};
