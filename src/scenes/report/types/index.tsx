export default interface IReport {
  detail: IReportDetail | null;
}

export interface IReportDetail {
  accessToken: string;
  embedUrl: string;
  id: string;
}
