import { IReportLoaded } from "../actions";
import { REPORT_LOADED } from "../constants";
import { combineReducers } from "redux";
import IGuideTour from "../types";
import { IReportDetail } from "../types/index";

function detail(state: IReportDetail | null = null, action: IReportLoaded) {
  switch (action.type) {
    case REPORT_LOADED:
      return action.payload;
  }
  return state;
}

export const report = combineReducers<IGuideTour>({
  detail
});
