import { connect, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import PBReport, { IProps, IDispatchs } from "../components";
import { reportLoadApi } from "../actions/index";

function mapStateToPropsWrap(
  { report: { detail } }: IStoreState,
  props: { libId: string }
): IProps {
  if (!detail) {
    return {
      ...props,
      accessToken: "",
      embedUrl: "",
      id: ""
    };
  }
  return {
    ...props,
    ...detail
  };
}

function mapDispatchToPropsWrap(dispatch: Dispatch, props: { libId: string }) {
  console.log("mapDispatchToPropsWrap", props);
  reportLoadApi(dispatch)(props.libId);
  return {};
}

const Wrap = connect<IProps, IDispatchs>(
  mapStateToPropsWrap,
  mapDispatchToPropsWrap
)(PBReport);

function mapStateToProps({ libs: { selectedLib } }: IStoreState) {
  return {
    libId: selectedLib
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onClose: () => {
      console.log("close");
    }
  };
}

export default connect<{}, IDispatchs, IProps>(
  mapStateToProps,
  mapDispatchToProps
)(Wrap);
