import * as React from "react";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import Paper from "@material-ui/core/Paper";
import IStyleProps from "../../../styles/utils";
import PaginationMui from "src/components/Pagination";
import SectionLoading from "src/components/SectionLoading";
import { ISetSearchQueryPayload } from "../actions/users";
import { IInvitation } from "../types/invitation";
import InvitationsTable from "./InvitationsTable";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import { ILib } from "../../lib/types/libs";

interface IProps {
  currentLib: ILib;
  isLoading: boolean;
  isFailed: boolean;
  users: IInvitation[];
  total: number;
  searchKeyword: string;
  searchRole: string;
  paging: {
    limit: number;
    page: number;
  };
  searchUser: () => void;
  updateQuery: (query: ISetSearchQueryPayload) => void;
  resend: (id: IInvitation) => void;
  resendAll: () => void;
  delete: (id: IInvitation) => void;
}

interface IStates {
  isOpenUserAdd: boolean;
}

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    backdrop: {
      zIndex: 1
    },
    wrap: {
      position: "relative",
      height: "100%",
      paddingTop: theme.spacing.unit,
      paddingBottom: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      overflowX: "hidden"
    },
    control: {
      display: "flex",
      paddingRight: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2
    },
    add: {
      marginLeft: "auto"
    },
    paging: {
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      textAlign: "center"
    }
  });

class UserList extends React.Component<IStyleProps & IProps, IStates> {
  public state = {
    isOpenUserAdd: false
  };

  private onChangePagination = (page: number) => {
    this.props.updateQuery({
      page
    });
  };

  private onResend = (id: IInvitation) => {
    return () => {
      this.props.resend(id);
    };
  };
  private onDelete = (id: IInvitation) => {
    return () => {
      this.props.delete(id);
    };
  };

  public componentDidUpdate(prevProps: IProps) {
    if (prevProps.currentLib.id !== this.props.currentLib.id) {
      this.props.searchUser();
    }
  }

  public render() {
    const { users, classes, paging, total, isLoading, resendAll } = this.props;
    return (
      <div className={classes.wrap}>
        {users.length > 0 && isLoading && <SectionLoading />}
        {users.length > 0 && (
          <Paper>
            <Toolbar>
              <Typography variant="title" id="tableTitle">
                Invitations
              </Typography>
            </Toolbar>
            <InvitationsTable
              resend={this.onResend}
              users={users}
              onDelete={this.onDelete}
              resendAll={resendAll}
            />
            <div className={classes.paging}>
              <PaginationMui
                onChangePage={this.onChangePagination}
                disabled={isLoading}
                start={paging.page}
                display={5}
                total={Math.ceil(total / paging.limit)}
              />
            </div>
          </Paper>
        )}
      </div>
    );
  }
}

export default withStyles(styles)(UserList);
