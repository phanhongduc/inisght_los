import * as React from "react";
import UserList from "../containers/UserList";
import UserPendingList from "../containers/UserPendingList";
import { UserLibRole } from "../../../helpers/permission";
import { allow } from "src/helpers/permission";

const UserManagement = ({ currentRole }: { currentRole: UserLibRole }) => {
  const allowUserList = allow("userList", currentRole);

  return (
    <div
      style={{
        position: "relative",
        overflow: "hidden"
      }}
    >
      {allowUserList && <UserList />}
      {allowUserList && <UserPendingList />}
    </div>
  );
};

export default UserManagement;
