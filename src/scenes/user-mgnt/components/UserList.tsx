import * as React from "react";
import { IUser } from "../types/users";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import Paper from "@material-ui/core/Paper";
import BackDrop from "@material-ui/core/Backdrop";
import IStyleProps from "../../../styles/utils";
import PaginationMui from "src/components/Pagination";
import SectionLoading from "src/components/SectionLoading";
import Sidebar from "../components/Sidebar";
import ControlBar from "../components/ControlBar";
import FormAddUser from "../components/FormAddUser";
import DetailUser from "../components/DetailUser";
import UsersTable from "./UsersTable";
import {
  IRequestEditUser,
  IRequestInviteUser,
  ISetSearchQueryPayload
} from "../actions/users";
import { defaultRules } from "react-hoc-form-validatable";
import { UserLibRole } from "../../../helpers/permission";
import Typography from "@material-ui/core/Typography";
import Toolbar from "@material-ui/core/Toolbar";
import ConfirmDialog from "../../../components/Dialog/Confirm";
import { ILib, LibType } from "../../lib/types/libs";

interface IProps {
  currentLib: ILib;
  currentRole: UserLibRole;
  isLoading: boolean;
  isFailed: boolean;
  users: IUser[];
  total: number;
  totalInvi: number;
  searchKeyword: string;
  searchRole: string;
  paging: {
    limit: number;
    page: number;
  };
  searchUser: () => void;
  updateQuery: (query: ISetSearchQueryPayload) => void;
  updatePagingQuery: (query: ISetSearchQueryPayload) => void;
  addUser: (data: IRequestInviteUser, cb: (reset: boolean) => void) => void;
  editUser: (
    data: {
      id: string | null;
      data: IRequestEditUser;
    },
    cb: (reset: boolean) => void
  ) => void;
  changeStatusUser: (status: boolean, user: IUser, cb: () => void) => void;
}

interface IStates {
  dataInvite: IRequestInviteUser | null;
  isOpenConfirmInvite: boolean;
  isOpenConfirmFreeTrial: boolean;
  isOpenUserAdd: boolean;
  cbFormInvite: ((should: boolean) => void);
  openUserDetail: string | null;
}

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    backdrop: {
      zIndex: 1
    },
    wrap: {
      paddingTop: theme.spacing.unit,
      paddingBottom: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      overflowX: "hidden"
    },
    control: {
      display: "flex",
      paddingRight: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2
    },
    add: {
      marginLeft: "auto"
    },
    paging: {
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      textAlign: "center"
    }
  });

class UserList extends React.Component<IStyleProps & IProps, IStates> {
  public state = {
    cbFormInvite: (should: boolean) => {
      // empty
    },
    dataInvite: null,
    isOpenUserAdd: false,
    isOpenConfirmInvite: false,
    isOpenConfirmFreeTrial: false,
    openUserDetail: null
  };

  private onChangePagination = (page: number) => {
    this.props.updatePagingQuery({
      page
    });
  };

  private onChangeKeyword = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    this.props.updateQuery({
      page: 1,
      keyword: target.value
    });
  };
  private onChangeRole = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    this.props.updateQuery({
      page: 1,
      role: target.value === "all" ? "" : target.value
    });
  };

  private createSubmit = (inputs: any, reset: (should: boolean) => void) => {
    if (this.props.currentLib.type === LibType.PayAsYouGo) {
      this.setState({
        isOpenConfirmInvite: true,
        cbFormInvite: reset,
        dataInvite: {
          email: inputs.emailAddress.value,
          firstName: inputs.firstName.value,
          lastName: inputs.lastName.value,
          role: inputs.roleSystemName.value
        }
      });
    } else if (
      this.props.currentLib.type === LibType.FreeTrial &&
      this.props.total + this.props.totalInvi > 4
    ) {
      this.setState({
        isOpenConfirmFreeTrial: true
      });
    } else {
      this.props.addUser(
        {
          email: inputs.emailAddress.value,
          firstName: inputs.firstName.value,
          lastName: inputs.lastName.value,
          role: inputs.roleSystemName.value
        },
        reset
      );
    }
  };

  private editSubmit = (inputs: any, reset: (should: boolean) => void) => {
    if (this.state.openUserDetail !== null) {
      this.props.editUser(
        {
          id: this.state.openUserDetail,
          data: {
            roleSystemName: inputs.roleSystemName.value
          }
        },
        reset
      );
    }
  };

  private toggleAddUser = () => {
    if (
      this.state.isOpenUserAdd === false &&
      this.props.currentLib.type === LibType.FreeTrial &&
      this.props.total + this.props.totalInvi > 4
    ) {
      this.setState({
        isOpenConfirmFreeTrial: true
      });
    } else {
      this.setState({
        isOpenUserAdd: !this.state.isOpenUserAdd
      });
    }
  };

  private onClickRowUser = (id: string) => {
    return () => {
      console.log(id);
      this.setState({
        openUserDetail: id
      });
    };
  };
  private closeDetailUser = () => {
    this.setState({
      openUserDetail: null
    });
  };

  private changeStatusUser = (status: boolean, user: IUser) => {
    this.props.changeStatusUser(status, user, () => {
      this.setState({
        openUserDetail: ""
      });
    });
  };

  private closeConfirmInvite = (result: boolean) => {
    if (result) {
      const { dataInvite, cbFormInvite } = this.state;
      if (dataInvite) {
        this.props.addUser(dataInvite, (success: boolean) => {
          if (success) {
            this.setState({
              isOpenConfirmInvite: false,
              dataInvite: null
            });
            cbFormInvite(true);
          }
        });
      }
    } else {
      this.state.cbFormInvite(false);
      this.setState({
        isOpenConfirmInvite: false,
        dataInvite: null
      });
    }
  };

  private closeConfirmInviteFreeTrial = () => {
    this.setState({
      isOpenConfirmFreeTrial: false
    });
  };

  public componentDidUpdate(prevProps: IProps) {
    if (prevProps.currentLib.id !== this.props.currentLib.id) {
      this.setState({
        isOpenConfirmFreeTrial: false,
        isOpenConfirmInvite: false,
        isOpenUserAdd: false
      });
      this.props.searchUser();
    }
  }

  public render() {
    const {
      users,
      classes,
      paging,
      total,
      isLoading,
      searchKeyword,
      searchRole,
      currentRole
    } = this.props;
    return (
      <div className={classes.wrap}>
        {isLoading && <SectionLoading />}
        <ControlBar
          searchRole={searchRole || "all"}
          onChangeRole={this.onChangeRole}
          onChangeKeyword={this.onChangeKeyword}
          searchKeyword={searchKeyword}
          toggleAddUser={this.toggleAddUser}
        />
        {users.length > 0 && (
          <Paper>
            <Toolbar>
              <Typography variant="title" id="tableTitle">
                Users
              </Typography>
            </Toolbar>
            <UsersTable onClickRow={this.onClickRowUser} users={users} />
            <div className={classes.paging}>
              <PaginationMui
                onChangePage={this.onChangePagination}
                disabled={isLoading}
                start={paging.page}
                display={5}
                total={Math.ceil(total / paging.limit)}
              />
            </div>
          </Paper>
        )}
        {(this.state.isOpenUserAdd || Boolean(this.state.openUserDetail)) && (
          <BackDrop
            className={classes.backdrop}
            open={
              this.state.isOpenUserAdd || Boolean(this.state.openUserDetail)
            }
          />
        )}
        <Sidebar isOpen={this.state.isOpenUserAdd}>
          <FormAddUser
            onClickBack={this.toggleAddUser}
            submitCallback={this.createSubmit}
            validateLang="en"
            rules={defaultRules}
          />
        </Sidebar>
        <Sidebar isOpen={Boolean(this.state.openUserDetail)}>
          <DetailUser
            user={users.find(user => user.id === this.state.openUserDetail)}
            onClickBack={this.closeDetailUser}
            editCallback={this.editSubmit}
            changeStatusUser={this.changeStatusUser}
            currentRole={currentRole}
          />
        </Sidebar>
        <ConfirmDialog
          confirmText="Invite"
          title="Adding user will cost 25$/user (First 4 users is free)"
          isOpen={this.state.isOpenConfirmInvite}
          content={`Do you want to invite the user?`}
          onClose={this.closeConfirmInvite}
        />
        <ConfirmDialog
          confirmText="Ok"
          isConfirm={true}
          title="Free trial"
          isOpen={this.state.isOpenConfirmFreeTrial}
          content={`Free trial library cannot invite more than 4 users.`}
          onClose={this.closeConfirmInviteFreeTrial}
        />
      </div>
    );
  }
}

export default withStyles(styles)(UserList);
