import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { createStyles, Theme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import MenuItem from "@material-ui/core/MenuItem";
import { HOCForm, FormValidateChildProps } from "react-hoc-form-validatable";
import IStyleProps from "../../../styles/utils";
import InputValidate from "src/components/InputValidate";
import { IUser } from "../types/users";
import * as classNames from "classnames";
import ConfirmDialog from "src/components/Dialog/Confirm";
import { allow, UserLibRole } from "src/helpers/permission";
import { inviteSelectBox } from "../types/invitation";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    submit: {
      marginTop: theme.spacing.unit * 2,
      textAlign: "center",
      display: "block",
      marginLeft: "auto",
      marginRight: "auto"
    },
    button: {
      margin: theme.spacing.unit
    },
    actived: {},
    warning: {
      backgroundColor: "#ff0000"
    }
  });
interface IProps {
  user: IUser;
  currentRole: UserLibRole;
  changeStatusUser: (status: boolean, user: IUser) => void;
}

interface IState {
  isStatusChange: boolean;
  isDelete: boolean;
}
class FormAddUser extends React.Component<
  IStyleProps & IProps & FormValidateChildProps,
  IState
> {
  constructor(props: IStyleProps & IProps & FormValidateChildProps) {
    super(props);
    this.state = {
      isStatusChange: false,
      isDelete: false
    };
  }

  private closeStatusDialog = (value: boolean) => {
    this.setState({
      isStatusChange: false
    });
    if (value) {
      const newStatus = !this.props.user.isBan;
      this.props.changeStatusUser(newStatus, this.props.user);
    }
  };

  private onChangeStatus = () => {
    this.setState({
      isStatusChange: true
    });
  };
  // private onDelteStatus = () => {
  //   this.setState({
  //     isDelete: true
  //   });
  // };

  public render() {
    const { classes, onSubmit, submitted, user, currentRole } = this.props;
    const statusAction = user.isBan ? "Unban" : "Ban";
    return (
      <div className={classes.wrap}>
        <form className={classes.form} noValidate={true} onSubmit={onSubmit}>
          <InputValidate
            defaultValue={user.roleName}
            label="Role"
            fullWidth={true}
            rule="notEmpty"
            name="roleSystemName"
            select={true}
            className={classes.textField}
            InputLabelProps={{
              shrink: true
            }}
            margin="normal"
          >
            {inviteSelectBox.map(item => (
              <MenuItem value={item.value} key={item.value}>
                {item.label}
              </MenuItem>
            ))}
          </InputValidate>
          {allow("ban", currentRole, user.roleName) && (
            <div className={classes.submit}>
              <Button
                className={classes.button}
                variant="raised"
                color="primary"
                type="submit"
                disabled={submitted}
              >
                Update
              </Button>
              <Button
                className={classNames(classes.button, {
                  [classes.actived]: user.isBan,
                  [classes.warning]: !user.isBan
                })}
                variant="contained"
                color="secondary"
                onClick={this.onChangeStatus}
              >
                {statusAction}
              </Button>
              {/* <Button
                className={classNames(classes.button, classes.warning)}
                variant="contained"
                color="secondary"
                onClick={this.onDelteStatus}
              >
                Delete
              </Button> */}
            </div>
          )}
        </form>
        <ConfirmDialog
          isOpen={this.state.isStatusChange}
          content={`Do you want to ${statusAction} this user`}
          onClose={this.closeStatusDialog}
        />
      </div>
    );
  }
}
export default withStyles(styles)(HOCForm<IStyleProps & IProps>(FormAddUser));
