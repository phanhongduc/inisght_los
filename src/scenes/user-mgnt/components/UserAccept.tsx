import * as React from "react";
import { IInvitationDetail } from "../types/invitation";
import { User } from "oidc-client";
import UserAcceptNotMatchEmail from "./UserAcceptNotMatchEmail";
import UserAcceptDialog from "./UserAcceptDialog";

export interface IProps {
  invitationId: string;
  invitationDetail: IInvitationDetail;
  account: User;
  logout: () => void;
  backToHome: () => void;
  accept: () => void;
  decline: () => void;
}

const UserAccept = ({ invitationDetail, account, ...props }: IProps) => {
  if (!invitationDetail) {
    return <div> Loading invitation detail ... </div>;
  }

  if (invitationDetail.email !== account.profile.email) {
    return (
      <UserAcceptNotMatchEmail
        invitationDetail={invitationDetail}
        account={account}
        {...props}
      />
    );
  }
  return <UserAcceptDialog invitationDetail={invitationDetail} {...props} />;
};

export default UserAccept;
