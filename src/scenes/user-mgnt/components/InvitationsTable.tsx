import * as React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import IStyleProps from "../../../styles/utils";
import { IInvitation } from "../types/invitation";
import Button from "@material-ui/core/Button";

interface IProps {
  users: IInvitation[];
  resend: (id: IInvitation) => () => void;
  onDelete: (id: IInvitation) => () => void;
  resendAll: () => void;
}

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    row: {
      cursor: "pointer",
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.divider
      }
    },
    rowHeaded: {
      backgroundColor: "#181A1F",

      "& th": {
        color: theme.palette.common.white
      }
    },
    paging: {
      paddingTop: theme.spacing.unit * 2,
      textAlign: "center"
    },
    button: {
      margin: theme.spacing.unit
    }
  });

const InvitationsTable = ({
  users,
  classes,
  resend,
  onDelete,
  resendAll
}: IProps & IStyleProps) => (
  <Table>
    <TableHead>
      <TableRow className={classes.rowHeaded}>
        <TableCell variant="head">Email</TableCell>
        <TableCell variant="head">First name</TableCell>
        <TableCell variant="head">Last name</TableCell>
        <TableCell variant="head">Role name</TableCell>
        <TableCell variant="head" numeric={true}>
          <Button
            onClick={resendAll}
            color="primary"
            variant="raised"
            className={classes.add}
          >
            Resend All
          </Button>
        </TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {users.map(invitation => (
        <TableRow className={classes.row} key={invitation.id}>
          <TableCell>{invitation.email}</TableCell>
          <TableCell>{invitation.firstName}</TableCell>
          <TableCell>{invitation.lastName}</TableCell>
          <TableCell>{invitation.roleName}</TableCell>
          <TableCell numeric={true}>
            <Button
              className={classes.button}
              variant="raised"
              color="primary"
              onClick={resend(invitation)}
              size="small"
            >
              Resend
            </Button>
            <Button
              className={classes.button}
              variant="raised"
              color="primary"
              onClick={onDelete(invitation)}
              size="small"
            >
              Delete
            </Button>
          </TableCell>
        </TableRow>
      ))}
    </TableBody>
  </Table>
);

export default withStyles(styles)(InvitationsTable);
