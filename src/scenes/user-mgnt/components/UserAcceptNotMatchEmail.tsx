import * as React from "react";
import { IInvitationDetail } from "../types/invitation";
import { User } from "oidc-client";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";

export interface IProps {
  invitationId: string;
  invitationDetail: IInvitationDetail;
  account: User;
  logout: () => void;
  backToHome: () => void;
}

const UserAcceptNotMatchEmail = ({
  invitationDetail,
  account,
  logout,
  backToHome
}: IProps) => {
  const currentEmail = account.profile.email;
  return (
    <Dialog
      disableBackdropClick={true}
      disableEscapeKeyDown={true}
      open={true}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"Email not match"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {`Login user is ${currentEmail} but invited email is ${
            invitationDetail.email
          }. Relogin to accept invitation.`}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={backToHome} color="primary">
          Back to Home
        </Button>
        <Button onClick={logout} color="primary" autoFocus={true}>
          Logout
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default UserAcceptNotMatchEmail;
