import * as React from "react";
import { IInvitationDetail } from "../types/invitation";
import Dialog from "@material-ui/core/Dialog";
import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import DialogContentText from "@material-ui/core/DialogContentText";
import DialogActions from "@material-ui/core/DialogActions";
import Button from "@material-ui/core/Button";

export interface IProps {
  invitationDetail: IInvitationDetail;
  accept: () => void;
  decline: () => void;
}

const UserAcceptDialog = ({ invitationDetail, accept, decline }: IProps) => {
  return (
    <Dialog
      disableBackdropClick={true}
      disableEscapeKeyDown={true}
      open={true}
      aria-labelledby="alert-dialog-title"
      aria-describedby="alert-dialog-description"
    >
      <DialogTitle id="alert-dialog-title">{"You are invited"}</DialogTitle>
      <DialogContent>
        <DialogContentText id="alert-dialog-description">
          {`Owner of ${
            invitationDetail.libraryName
          } has invite to join his library.`}
        </DialogContentText>
      </DialogContent>
      <DialogActions>
        <Button onClick={accept} color="primary" autoFocus={true}>
          Accept
        </Button>
        <Button onClick={decline} color="primary">
          Decline
        </Button>
      </DialogActions>
    </Dialog>
  );
};

export default UserAcceptDialog;
