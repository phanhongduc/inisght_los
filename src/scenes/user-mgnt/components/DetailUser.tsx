import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { createStyles, Theme } from "@material-ui/core/styles";
import BackIcon from "@material-ui/icons/ArrowBack";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IStyleProps from "../../../styles/utils";
import { IUser } from "../types/users";
import FormUser from "./FormEditUser";
import { defaultRules } from "react-hoc-form-validatable";
import Grid from "@material-ui/core/Grid";
import { UserLibRole } from "../../../helpers/permission";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2
    },
    info: {
      paddingTop: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      paddingRight: theme.spacing.unit * 2
    },
    title: {
      padding: theme.spacing.unit * 2,
      display: "flex",
      alignItems: "center"
    },
    submit: {
      marginTop: theme.spacing.unit * 2,
      textAlign: "center",
      display: "block",
      marginLeft: "auto",
      marginRight: "auto"
    },
    tabs: {
      marginTop: theme.spacing.unit * 2,
      backgroundColor: "#0096DA",
      color: theme.palette.common.white
    },
    banButton: {
      margin: theme.spacing.unit
    }
  });
interface IProps {
  user: IUser | undefined;
  onClickBack: () => void;
  editCallback: (inputs: any, reset: (should: boolean) => void) => void;
  changeStatusUser: (status: boolean, user: IUser) => void;
  currentRole: UserLibRole;
}

interface IStates {
  openTab: number;
}

class FormAddUser extends React.Component<IStyleProps & IProps, IStates> {
  public state = {
    openTab: 0
  };

  public render() {
    const {
      classes,
      user,
      editCallback,
      onClickBack,
      changeStatusUser,
      currentRole
    } = this.props;
    return user ? (
      <div>
        <div className={classes.title}>
          <IconButton onClick={onClickBack}>
            <BackIcon />
          </IconButton>
          <Typography variant="title">User: {user.firstName}</Typography>
        </div>
        <Divider />
        <Grid container={true} spacing={16} className={classes.info}>
          <Grid item={true} xs={4}>
            <Typography>
              <b>Email</b>:
            </Typography>
          </Grid>
          <Grid item={true} xs={8}>
            <Typography>{user.email}</Typography>
          </Grid>
        </Grid>
        <Grid container={true} spacing={16} className={classes.info}>
          <Grid item={true} xs={4}>
            <Typography>
              <b>First name</b>:
            </Typography>
          </Grid>
          <Grid item={true} xs={8}>
            <Typography>{user.firstName}</Typography>
          </Grid>
        </Grid>
        <Grid container={true} spacing={16} className={classes.info}>
          <Grid item={true} xs={4}>
            <Typography>
              <b>Last name</b>:
            </Typography>
          </Grid>
          <Grid item={true} xs={8}>
            <Typography>{user.lastName}</Typography>
          </Grid>
        </Grid>
        <Grid container={true} spacing={16} className={classes.info}>
          <Grid item={true} xs={4}>
            <Typography>
              <b>Status</b>:
            </Typography>
          </Grid>
          <Grid item={true} xs={8}>
            <Typography>{user.isBan ? "Banned" : "Active"}</Typography>
          </Grid>
        </Grid>

        <div className={classes.wrap}>
          <FormUser
            validateLang="en"
            rules={defaultRules}
            submitCallback={editCallback}
            user={user}
            changeStatusUser={changeStatusUser}
            currentRole={currentRole}
          />
        </div>
      </div>
    ) : (
      ""
    );
  }
}
export default withStyles(styles)(FormAddUser);
