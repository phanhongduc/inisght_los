import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import UserList from "../components/UserList";
import { IUser } from "../types/users";
import {
  inviteUser,
  editUser,
  getListUser,
  IRequestInviteUser,
  IRequestEditUser,
  ISetSearchQueryPayload,
  setSearchUserQuery,
  setSearchUserQueryInvitation
} from "../actions/users";

function mapStateToProps({
  usersManagement,
  libs: { currentRole, selectedLib, list }
}: IStoreState) {
  return {
    currentLib: list.find(l => l.id === selectedLib),
    isLoading: usersManagement.loading.length > 0,
    isFailed: usersManagement.searchUserQuery.isLoading,
    users: usersManagement.listUser.users,
    total: usersManagement.listUser.total,
    totalInvi: usersManagement.listUserInvitation.total,
    searchKeyword: usersManagement.searchUserQuery.keyword,
    searchRole: usersManagement.searchUserQuery.role,
    paging: {
      limit: usersManagement.searchUserQuery.limit,
      page: usersManagement.searchUserQuery.page
    },
    currentRole
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    searchUser: () => {
      dispatch(getListUser());
    },
    updateQuery: (query: ISetSearchQueryPayload) => {
      dispatch(setSearchUserQuery(query));
      dispatch(setSearchUserQueryInvitation(query));
    },

    updatePagingQuery: (query: ISetSearchQueryPayload) => {
      dispatch(setSearchUserQuery(query));
    },
    addUser: (data: IRequestInviteUser, cb: (reset: boolean) => void) => {
      dispatch(inviteUser(data, cb));
    },
    editUser: (
      data: {
        id: string;
        data: IRequestEditUser;
      },
      cb: (reset: boolean) => void
    ) => {
      dispatch(editUser(data, cb));
    },
    changeStatusUser: (status: boolean, user: IUser, cb: () => void) => {
      dispatch(
        editUser(
          {
            id: user.id,
            data: {
              isBan: status
            }
          },
          () => {
            dispatch(getListUser());
            cb();
          }
        )
      );
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserList);
