import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import UserManagement from "../components/UserManagement";
import { getListUser, getListUserInvitation } from "../actions/users";

function mapStateToProps({ libs: { currentRole } }: IStoreState) {
  return {
    currentRole
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  dispatch(getListUser());
  dispatch(getListUserInvitation());

  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserManagement);
