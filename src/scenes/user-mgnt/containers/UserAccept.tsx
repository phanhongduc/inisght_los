import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import UserAccept from "../components/UserAccept";
import {
  getInvitationIdDetail,
  updateInvitationId,
  invitationAccept,
  invitationDecline
} from "../actions/accept";
import { logout, setupAuthentication } from "../../auth/actions";
import { push } from "react-router-redux";
import paths from "src/paths";
import { IInvitationDetail } from "../types/invitation";

function mapStateToPropsWrap({
  usersManagement: { invitationId }
}: IStoreState) {
  return {
    invitationId
  };
}

function mapDispatchToPropsWrap(dispatch: Dispatch) {
  return {};
}

function mapStateToProps({
  usersManagement: { invitationDetail },
  auth: { account }
}: IStoreState) {
  return {
    invitationDetail,
    account
  };
}

function mapDispatchToProps(
  dispatch: Dispatch,
  { invitationId }: { invitationId: string }
) {
  if (invitationId) {
    dispatch(getInvitationIdDetail(invitationId));
  }
  return {
    logout: () => {
      dispatch(logout());
      dispatch(setupAuthentication());
    },
    backToHome: () => {
      dispatch(updateInvitationId(""));
      dispatch(push(paths.home));
    }
  };
}

function mapStateToPropsDetail({  }: IStoreState) {
  return {};
}

function mapDispatchToPropsDetail(
  dispatch: Dispatch,
  {
    invitationId,
    invitationDetail
  }: { invitationId: string; invitationDetail: IInvitationDetail }
) {
  return {
    accept: () => {
      dispatch(invitationAccept(invitationId, invitationDetail));
    },
    decline: () => {
      dispatch(invitationDecline(invitationId, invitationDetail));
    }
  };
}
const UserAcceptDetail = connect(
  mapStateToPropsDetail,
  mapDispatchToPropsDetail
)(UserAccept);

const UserAcceptWrap = connect(
  mapStateToProps,
  mapDispatchToProps
)(UserAcceptDetail);

export default connect(
  mapStateToPropsWrap,
  mapDispatchToPropsWrap
)(UserAcceptWrap);
