import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import UserPendingList from "../components/UserPendingList";
import {
  getListUserInvitation,
  setSearchUserQueryInvitation
} from "../actions/users";
import { ISetSearchQueryPayload } from "../actions/users";
import {
  resendInvitation,
  resendAllInvitation,
  deleteInvitation
} from "../actions/invitation";
import { IInvitation } from "../types/invitation";

function mapStateToProps({
  usersManagement,
  libs: { list, selectedLib }
}: IStoreState) {
  return {
    currentLib: list.find(l => l.id === selectedLib),
    isLoading: usersManagement.loading.length > 0,
    isFailed: usersManagement.searchUserQueryInvitation.isLoading,
    users: usersManagement.listUserInvitation.invitations,
    total: usersManagement.listUserInvitation.total,
    searchKeyword: usersManagement.searchUserQueryInvitation.keyword,
    searchRole: usersManagement.searchUserQueryInvitation.role,
    paging: {
      limit: usersManagement.searchUserQueryInvitation.limit,
      page: usersManagement.searchUserQueryInvitation.page
    }
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    searchUser: () => {
      dispatch(getListUserInvitation());
    },
    updateQuery: (query: ISetSearchQueryPayload) => {
      dispatch(setSearchUserQueryInvitation(query));
    },
    resend: (invitation: IInvitation) => {
      dispatch(resendInvitation(invitation));
    },
    delete: (invitation: IInvitation) => {
      dispatch(deleteInvitation(invitation));
    },
    resendAll: () => {
      dispatch(resendAllInvitation());
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserPendingList);
