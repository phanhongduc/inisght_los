import { Action } from "redux";
import {
  UPDATE_INVITATION_ID,
  GET_INVITATION_DETAIL,
  SET_INVITATION_DETAIL,
  INVITATION_ACCEPT
} from "../constants/accept";
import { IInvitationDetail } from "../types/invitation";
import { INVITATION_DECLINE } from "../constants/accept";

export interface IUpdateInvitationId extends Action<UPDATE_INVITATION_ID> {
  payload: string;
}

export function updateInvitationId(data: string): IUpdateInvitationId {
  return {
    type: UPDATE_INVITATION_ID,
    payload: data
  };
}

export interface IGetInvitationDetail extends Action<GET_INVITATION_DETAIL> {
  payload: string;
}

export function getInvitationIdDetail(id: string): IGetInvitationDetail {
  return {
    type: GET_INVITATION_DETAIL,
    payload: id
  };
}

export interface ISetInvitationDetail extends Action<SET_INVITATION_DETAIL> {
  payload: IInvitationDetail;
}

export function setInvitationIdDetail(
  data: IInvitationDetail
): ISetInvitationDetail {
  return {
    type: SET_INVITATION_DETAIL,
    payload: data
  };
}

export interface IInvitationAccept extends Action<INVITATION_ACCEPT> {
  payload: {
    id: string;
    detail: IInvitationDetail;
  };
}

export function invitationAccept(
  id: string,
  detail: IInvitationDetail
): IInvitationAccept {
  return {
    type: INVITATION_ACCEPT,
    payload: { id, detail }
  };
}

export interface IInvitationDecline extends Action<INVITATION_DECLINE> {
  payload: {
    id: string;
    detail: IInvitationDetail;
  };
}

export function invitationDecline(
  id: string,
  detail: IInvitationDetail
): IInvitationDecline {
  return {
    type: INVITATION_DECLINE,
    payload: { id, detail }
  };
}
