import { Action } from "redux";
import {
  RESEND_INVITATION,
  RESEND_ALL_INVITATION
} from "../constants/invitation";
import { IInvitation } from "../types/invitation";
import { DELETE_INVITATION } from "../constants/invitation";

export interface IResendInvitation extends Action<RESEND_INVITATION> {
  payload: IInvitation;
}

export function resendInvitation(data: IInvitation): IResendInvitation {
  return {
    type: RESEND_INVITATION,
    payload: data
  };
}

export interface IResendAllInvitation extends Action<RESEND_ALL_INVITATION> {}

export function resendAllInvitation(): IResendAllInvitation {
  return {
    type: RESEND_ALL_INVITATION
  };
}

export interface IDeleteInvitation extends Action<DELETE_INVITATION> {
  payload: IInvitation;
}

export function deleteInvitation(data: IInvitation): IDeleteInvitation {
  return {
    type: DELETE_INVITATION,
    payload: data
  };
}
