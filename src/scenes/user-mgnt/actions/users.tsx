import {
  INVITE_USER,
  EDIT_USER,
  EDIT_USER_PASSWORD,
  GET_LIST_USER,
  SET_LIST_USER,
  SET_SEARCH_QUERY_USER,
  SET_USER,
  SET_LIST_USER_INVITATION
} from "../constants/users";
import { Action } from "redux";
import { IResponseListUser } from "../types/users";
import { UserLibRole } from "../../../helpers/permission";
import {
  GET_LIST_USER_INVITATION,
  SET_SEARCH_QUERY_USER_INVITATION
} from "../constants/users";
import { IResponseListInvitation } from "../types/invitation";

export interface IGetListUser extends Action<GET_LIST_USER> {}

export function getListUser(): IGetListUser {
  return {
    type: GET_LIST_USER
  };
}

export interface ISetListUser extends Action<SET_LIST_USER> {
  payload: IResponseListUser;
}

export function setListUser(data: IResponseListUser): ISetListUser {
  return {
    type: SET_LIST_USER,
    payload: data
  };
}

export interface ISetSearchQueryPayload {
  keyword?: string;
  role?: string;
  page?: number;
  limit?: number;
  isLoading?: boolean;
  isFailed?: boolean;
}

export interface ISetSearchUserQuery extends Action<SET_SEARCH_QUERY_USER> {
  payload: ISetSearchQueryPayload;
}

export function setSearchUserQuery(
  query: ISetSearchQueryPayload
): ISetSearchUserQuery {
  return {
    type: SET_SEARCH_QUERY_USER,
    payload: query
  };
}

export interface IInviteUser extends Action<INVITE_USER> {
  payload: IRequestInviteUser;
  meta: {
    cb: (reset: boolean) => void;
  };
}

export interface IRequestInviteUser {
  email: string;
  firstName: string;
  lastName: string;
  role: string;
}

export function inviteUser(
  data: IRequestInviteUser,
  cb: (success: boolean) => void
): IInviteUser {
  return {
    type: INVITE_USER,
    payload: data,
    meta: {
      cb
    }
  };
}

export interface IEditUser extends Action<EDIT_USER> {
  payload: {
    id: string;
    data: IRequestEditUser;
  };
  meta: {
    cb: (reset: boolean) => void;
  };
}

export interface IRequestEditUser {
  isBan?: boolean;
  roleSystemName?: UserLibRole;
}

export function editUser(
  data: {
    id: string;
    data: IRequestEditUser;
  },
  cb: (success: boolean) => void
): IEditUser {
  return {
    type: EDIT_USER,
    payload: data,
    meta: {
      cb
    }
  };
}

export interface IResponseEditUser {
  firstName?: string;
  lastName?: string;
  roleSystemName?: UserLibRole;
}

export interface ISetUser extends Action<SET_USER> {
  payload: {
    id: string;
    data: IResponseEditUser;
  };
}

export function setUser(data: {
  id: string;
  data: IResponseEditUser;
}): ISetUser {
  return {
    type: SET_USER,
    payload: data
  };
}

export interface IResponseEditUser {
  firstName?: string;
  lastName?: string;
  roleSystemName?: UserLibRole;
}

export interface IEditUserPassword extends Action<EDIT_USER_PASSWORD> {
  payload: IEditUserPasswordPayload;
  meta: {
    cb: (success: boolean) => void;
  };
}

export interface IEditUserPasswordPayload {
  id: string;
  newPassword: string;
  confirmPassword: string;
}

export function editUserPassword(
  data: {
    id: string;
    newPassword: string;
    confirmPassword: string;
  },
  cb: (success: boolean) => void
): IEditUserPassword {
  return {
    type: EDIT_USER_PASSWORD,
    payload: data,
    meta: {
      cb
    }
  };
}

export interface IGetListUserInvitation
  extends Action<GET_LIST_USER_INVITATION> {}

export function getListUserInvitation(): IGetListUserInvitation {
  return {
    type: GET_LIST_USER_INVITATION
  };
}

export interface ISetListUserInvitation
  extends Action<SET_LIST_USER_INVITATION> {
  payload: IResponseListInvitation;
}

export function setListUserInvitation(
  data: IResponseListInvitation
): ISetListUserInvitation {
  return {
    type: SET_LIST_USER_INVITATION,
    payload: data
  };
}

export interface ISetSearchUserQueryInvitation
  extends Action<SET_SEARCH_QUERY_USER_INVITATION> {
  payload: ISetSearchQueryPayload;
}

export function setSearchUserQueryInvitation(
  query: ISetSearchQueryPayload
): ISetSearchUserQueryInvitation {
  return {
    type: SET_SEARCH_QUERY_USER_INVITATION,
    payload: query
  };
}
