import { UserLibRole } from "../../../helpers/permission";

export interface IResponseList<T extends any> {
  total: number;
}
export interface IResponseListUser extends IResponseList<IUser> {
  users: IUser[];
}

export interface IUser {
  roleName: UserLibRole;
  id: string;
  userName: string;
  email: string;
  firstName: string;
  lastName: string;
  isBan: boolean;
}

export interface ISearchUserQuery {
  keyword: string;
  role: string;
  page: number;
  limit: number;
  isLoading: boolean;
  isFailed: boolean;
}
