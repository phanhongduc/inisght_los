import { IResponseList } from "./users";
import { UserLibRole } from "../../../helpers/permission";

export interface IInvitation {
  email: string;
  firstName: string;
  lastName: string;
  roleName: UserLibRole;
  createdOnUtc: string;
  id: string;
  libraryId: string;
  updatedOnUtc: string;
}
export interface IInvitationDetail {
  email: string;
  firstname: string;
  lastName: string;
  role: UserLibRole;
  libraryId: string;
  libraryName: string;
}

export interface IResponseListInvitation extends IResponseList<IInvitation> {
  invitations: IInvitation[];
}

export interface ISelectItem {
  value: string;
  label: string;
}
export const inviteSelectBox: ISelectItem[] = [
  { value: "Admin", label: "Admin" },
  { value: "User", label: "User" }
];
