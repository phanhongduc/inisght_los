import { IResponseListUser, ISearchUserQuery } from "./users";
import { IResponseListInvitation, IInvitationDetail } from "./invitation";

export interface IUsersManagement {
  listUser: IResponseListUser;
  searchUserQuery: ISearchUserQuery;

  listUserInvitation: IResponseListInvitation;
  searchUserQueryInvitation: ISearchUserQuery;
  invitationId: string;
  invitationDetail: IInvitationDetail | null;
  loading: string[];
}
