import { combineReducers } from "redux";
import { IResponseListUser, ISearchUserQuery, IUser } from "../types/users";
import {
  ISetListUser,
  ISetSearchUserQuery,
  ISetUser,
  ISetListUserInvitation,
  ISetSearchUserQueryInvitation
} from "../actions/users";
import {
  SET_LIST_USER_INVITATION,
  SET_SEARCH_QUERY_USER_INVITATION
} from "../constants/users";
import {
  SET_LIST_USER,
  SET_SEARCH_QUERY_USER,
  SET_USER
} from "../constants/users";
import {
  IResponseListInvitation,
  IInvitationDetail
} from "../types/invitation";
import { IUpdateInvitationId, ISetInvitationDetail } from "../actions/accept";
import {
  UPDATE_INVITATION_ID,
  SET_INVITATION_DETAIL
} from "../constants/accept";
import { ISetApiLoading, IUnsetApiLoading } from "src/actions/loading";
import { API_LOADING_SET, API_LOADING_UNSET } from "src/constants/loading";
import { REACT_APP_API_URL } from "src/environment";

function listUser(
  state: IResponseListUser = {
    total: 0,
    users: []
  },
  action: ISetListUser | ISetUser
) {
  switch (action.type) {
    case SET_LIST_USER: {
      return action.payload;
    }

    case SET_USER: {
      return {
        total: state.total,
        users: state.users.reduce(
          (current, user) => {
            if (user.id === action.payload.id) {
              current.push({
                ...user,
                ...{
                  lastName: action.payload.data.lastName || user.lastName,
                  firstName: action.payload.data.firstName || user.firstName,
                  roleName: action.payload.data.roleSystemName || user.roleName
                }
              });
            } else {
              current.push(user);
            }
            return current;
          },
          [] as IUser[]
        )
      };
    }
  }

  return state;
}

function searchUserQuery(
  state: ISearchUserQuery = {
    isFailed: false,
    isLoading: false,
    keyword: "",
    role: "",
    limit: 10,
    page: 1
  },
  action: ISetSearchUserQuery
) {
  if (action.type === SET_SEARCH_QUERY_USER) {
    return {
      ...state,
      ...action.payload
    };
  }
  return state;
}

function listUserInvitation(
  state: IResponseListInvitation = {
    total: 0,
    invitations: []
  },
  action: ISetListUserInvitation
) {
  switch (action.type) {
    case SET_LIST_USER_INVITATION: {
      return action.payload;
    }
  }

  return state;
}

function searchUserQueryInvitation(
  state: ISearchUserQuery = {
    isFailed: false,
    isLoading: false,
    keyword: "",
    role: "",
    limit: 10,
    page: 1
  },
  action: ISetSearchUserQueryInvitation
) {
  if (action.type === SET_SEARCH_QUERY_USER_INVITATION) {
    return {
      ...state,
      ...action.payload
    };
  }
  return state;
}

function invitationId(state: string = "", action: IUpdateInvitationId) {
  if (action.type === UPDATE_INVITATION_ID) {
    return action.payload;
  }
  return state;
}
function invitationDetail(
  state: IInvitationDetail | null = null,
  action: ISetInvitationDetail
) {
  if (action.type === SET_INVITATION_DETAIL) {
    return action.payload;
  }
  return state;
}

const userAPIs = [`${REACT_APP_API_URL}/api/User/.*`];

const isUserApi = (api: string) => {
  return !!userAPIs.find(pattern => !!api.match(new RegExp(pattern)));
};

const loading = (
  state: string[] = [],
  action: ISetApiLoading | IUnsetApiLoading
) => {
  switch (action.type) {
    case API_LOADING_SET: {
      if (!isUserApi(action.payload)) {
        return state;
      }
      return [...state, action.payload];
    }
    case API_LOADING_UNSET: {
      if (!isUserApi(action.payload)) {
        return state;
      }
      return state.filter(api => api !== action.payload);
    }
  }
  return state;
};

export const usersManagement = combineReducers({
  listUser,
  searchUserQuery,
  listUserInvitation,
  searchUserQueryInvitation,
  invitationId,
  invitationDetail,
  loading
});
