import { Store, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import { Action } from "redux";
import { LOCATION_CHANGE, LocationChangeAction } from "react-router-redux";
import RouteUri from "src/helpers/routeUri";
import paths from "src/paths";
import { getMatch } from "src/helpers/url";
import {
  updateInvitationId,
  IUpdateInvitationId,
  IGetInvitationDetail,
  setInvitationIdDetail
} from "../actions/accept";
import {
  UPDATE_INVITATION_ID,
  GET_INVITATION_DETAIL,
  INVITATION_ACCEPT
} from "../constants/accept";
import { SETUP_AUTHENTICATION } from "src/constants";
import { ISetupAuthentication } from "../../auth/actions/Auth";
import axios, { AxiosResponse, AxiosError } from "axios";
import { REACT_APP_API_URL } from "../../../environment";
import { IInvitationDetail } from "../types/invitation";
import { selectLib, getListLibs } from "../../lib/actions/libs";
import { IInvitationAccept, IInvitationDecline } from "../actions/accept";
import { common } from "src/actions";
import Variant from "src/components/notification/types/variant";
import { INVITATION_DECLINE } from "../constants/accept";
import clientStorage, { ClientStorageKey } from "src/helpers/clientStorage";

type LOCATION_CHANGE = typeof LOCATION_CHANGE;

interface IPathInit extends Action<LOCATION_CHANGE> {}

export const initInvitationAccept = (store: Store<IStoreState>) => (
  next: Dispatch<IPathInit | IUpdateInvitationId | ISetupAuthentication>
) => (
  action: LocationChangeAction | IUpdateInvitationId | ISetupAuthentication
) => {
  switch (action.type) {
    case LOCATION_CHANGE:
      {
        const match = getMatch(
          action.payload,
          new RouteUri(paths.invitationAccept)
        );
        if (match) {
          const invitationId = match.params.id;
          store.dispatch(updateInvitationId(invitationId));
        }
      }
      break;
    case UPDATE_INVITATION_ID:
      {
        clientStorage.setItem(
          ClientStorageKey.INVITATION_ID_LOCAL_KEY,
          action.payload
        );
      }
      break;
    case SETUP_AUTHENTICATION:
      {
        const invitationId =
          clientStorage.getItem(ClientStorageKey.INVITATION_ID_LOCAL_KEY) || "";
        store.dispatch(updateInvitationId(invitationId));
      }
      break;
  }
  return next(action);
};

export const initInvitationAcceptAction = (store: Store<IStoreState>) => (
  next: Dispatch<IGetInvitationDetail | IInvitationAccept | IInvitationDecline>
) => (
  action: IGetInvitationDetail | IInvitationAccept | IInvitationDecline
) => {
  switch (action.type) {
    case GET_INVITATION_DETAIL:
      {
        getInvitationDetail(store.dispatch)(action.payload);
      }
      break;
    case INVITATION_ACCEPT:
      {
        invitationAccept(store.dispatch)(
          action.payload.id,
          action.payload.detail
        );
      }
      break;
    case INVITATION_DECLINE:
      {
        invitationDecline(store.dispatch)(
          action.payload.id,
          action.payload.detail
        );
      }
      break;
  }
  return next(action);
};

const getInvitationDetail = (dispatch: Dispatch) => (id: string) => {
  axios
    .get(`${REACT_APP_API_URL}/api/User/invitation/detail/${id}`)
    .then((resp: AxiosResponse<IInvitationDetail>) => {
      dispatch(setInvitationIdDetail(resp.data));
    })
    .catch((error: AxiosError) => {
      if (error.response && error.response.status === 400) {
        common.fireNotification(dispatch)({
          message: error.response.data,
          variant: Variant.ERROR
        });
        dispatch(updateInvitationId(""));
      }
    });
};

const invitationAccept = (dispatch: Dispatch) => (
  id: string,
  inv: IInvitationDetail
) => {
  axios
    .post(`${REACT_APP_API_URL}/api/User/invitation/accept/${id}`, {})
    .then((resp: AxiosResponse<any>) => {
      dispatch(updateInvitationId(""));
      dispatch(getListLibs());
      dispatch(selectLib(inv.libraryId));
    });
};
const invitationDecline = (dispatch: Dispatch) => (
  id: string,
  inv: IInvitationDetail
) => {
  axios
    .post(`${REACT_APP_API_URL}/api/User/invitation/decline/${id}`, {})
    .then((resp: AxiosResponse<any>) => {
      dispatch(updateInvitationId(""));
    });
};
