import { Dispatch, Store } from "react-redux";
import { IStoreState } from "../../../types";
import {
  IInviteUser,
  IEditUser,
  IEditUserPassword,
  IGetListUser,
  setListUser,
  setUser
} from "../actions/users";
import {
  INVITE_USER,
  EDIT_USER,
  GET_LIST_USER,
  SET_SEARCH_QUERY_USER,
  SET_SEARCH_QUERY_USER_INVITATION
} from "../constants/users";
import axios, { AxiosError, AxiosResponse } from "axios";
import { REACT_APP_API_URL } from "../../../environment";
import { IResponseListUser, ISearchUserQuery } from "../types/users";
import { common } from "../../../actions";
import Variant from "../../../components/notification/types/variant";
import {
  IGetListUserInvitation,
  setListUserInvitation
} from "../actions/users";
import { GET_LIST_USER_INVITATION } from "../constants/users";
import { IResponseListInvitation } from "../types/invitation";
import { getListUserInvitation } from "../actions/users";
import {
  ISetSearchUserQuery,
  ISetSearchUserQueryInvitation
} from "../actions/users";

export const usersMiddleware = (store: Store<IStoreState>) => (
  next: Dispatch<
    | IGetListUser
    | IInviteUser
    | IEditUser
    | IEditUserPassword
    | IGetListUserInvitation
    | ISetSearchUserQuery
    | ISetSearchUserQueryInvitation
  >
) => (
  action:
    | IGetListUser
    | IInviteUser
    | IEditUser
    | IEditUserPassword
    | IGetListUserInvitation
    | ISetSearchUserQuery
    | ISetSearchUserQueryInvitation
) => {
  const currentState = store.getState() as IStoreState;
  const libId = currentState.libs.selectedLib;
  switch (action.type) {
    case GET_LIST_USER: {
      getListUser(store.dispatch)(
        libId,
        action,
        currentState.usersManagement.searchUserQuery
      );
      break;
    }
    case SET_SEARCH_QUERY_USER:
      {
        getListUser(store.dispatch)(libId, action, {
          ...currentState.usersManagement.searchUserQuery,
          ...action.payload
        });
      }
      break;
    case GET_LIST_USER_INVITATION: {
      getListUserInviation(store.dispatch)(
        libId,
        action,
        currentState.usersManagement.searchUserQueryInvitation
      );
      break;
    }
    case SET_SEARCH_QUERY_USER_INVITATION:
      {
        getListUserInviation(store.dispatch)(libId, action, {
          ...currentState.usersManagement.searchUserQueryInvitation,
          ...action.payload
        });
      }
      break;
    case INVITE_USER: {
      inviteUser(store.dispatch)(libId, action);
      break;
    }
    case EDIT_USER: {
      if (action.payload.data.roleSystemName) {
        updateUser(store.dispatch)(libId, action);
      } else if (action.payload.data.isBan !== undefined) {
        if (action.payload.data.isBan) {
          banUser(store.dispatch)(libId, action);
        } else {
          unbanUser(store.dispatch)(libId, action);
        }
      }

      break;
    }
  }
  return next(action);
};

const getListUser = (dispatch: Dispatch) => (
  libId: string,
  action: IGetListUser | ISetSearchUserQuery,
  searchUserQuery: ISearchUserQuery
) => {
  axios
    .post(
      `${REACT_APP_API_URL}/api/User/${libId}/search/${
        searchUserQuery.limit
      }/${(searchUserQuery.page - 1) * searchUserQuery.limit}`,
      {
        keyword: searchUserQuery.keyword,
        role: searchUserQuery.role
      }
    )
    .then((data: AxiosResponse<IResponseListUser>) => {
      dispatch(setListUser(data.data));
    })
    .catch((e: AxiosError) => {
      console.error(e);
    });
};

const updateUser = (dispatch: Dispatch) => (
  libId: string,
  action: IEditUser
) => {
  axios
    .put(`${REACT_APP_API_URL}/api/User/${libId}/update/${action.payload.id}`, {
      role: action.payload.data.roleSystemName
    })
    .then((data: AxiosResponse) => {
      common.fireNotification(dispatch)({
        message: "Update user role success.",
        variant: Variant.SUCCESS
      });
      dispatch(
        setUser({
          id: action.payload.id,
          data: action.payload.data
        })
      );
      action.meta.cb(true);
    })
    .catch((e: AxiosError) => {
      const response = e.response as AxiosResponse;

      common.fireNotification(dispatch)({
        message:
          response && response.data && response.data.ErrorMessage
            ? response.data.ErrorMessage
            : response.data || "Update user role failed.",
        variant: Variant.ERROR
      });

      action.meta.cb(false);
    });
};

const unbanUser = (dispatch: Dispatch) => (
  libId: string,
  action: IEditUser
) => {
  axios
    .put(
      `${REACT_APP_API_URL}/api/User/${libId}/unban/${action.payload.id}`,
      {}
    )
    .then((data: AxiosResponse) => {
      common.fireNotification(dispatch)({
        message: "Unban user success.",
        variant: Variant.SUCCESS
      });
      dispatch(
        setUser({
          id: action.payload.id,
          data: action.payload.data
        })
      );
      action.meta.cb(true);
    })
    .catch((e: AxiosError) => {
      const response = e.response as AxiosResponse;

      common.fireNotification(dispatch)({
        message:
          response && response.data && response.data.ErrorMessage
            ? response.data.ErrorMessage
            : response.data || "Unban user failed.",
        variant: Variant.ERROR
      });

      action.meta.cb(false);
    });
};
const banUser = (dispatch: Dispatch) => (libId: string, action: IEditUser) => {
  axios
    .put(`${REACT_APP_API_URL}/api/User/${libId}/ban/${action.payload.id}`, {})
    .then((data: AxiosResponse) => {
      common.fireNotification(dispatch)({
        message: "Ban user success.",
        variant: Variant.SUCCESS
      });
      dispatch(
        setUser({
          id: action.payload.id,
          data: action.payload.data
        })
      );
      action.meta.cb(true);
    })
    .catch((e: AxiosError) => {
      const response = e.response as AxiosResponse;

      common.fireNotification(dispatch)({
        message:
          response && response.data && response.data.ErrorMessage
            ? response.data.ErrorMessage
            : response.data || "Ban user failed.",
        variant: Variant.ERROR
      });

      action.meta.cb(false);
    });
};

const getListUserInviation = (dispatch: Dispatch) => (
  libId: string,
  action: IGetListUserInvitation | ISetSearchUserQueryInvitation,
  searchUserQuery: ISearchUserQuery
) => {
  axios
    .post(
      `${REACT_APP_API_URL}/api/User/${libId}/invitation/search/${
        searchUserQuery.limit
      }/${(searchUserQuery.page - 1) * searchUserQuery.limit}`,
      {
        keyword: searchUserQuery.keyword,
        role: searchUserQuery.role
      }
    )
    .then((data: AxiosResponse<IResponseListInvitation>) => {
      dispatch(setListUserInvitation(data.data));
    })
    .catch((e: AxiosError) => {
      console.error(e);
    });
};

const inviteUser = (dispatch: Dispatch) => (
  libId: string,
  action: IInviteUser
) => {
  axios
    .post(
      `${REACT_APP_API_URL}/api/User/${libId}/invitation/new`,
      action.payload
    )
    .then((data: AxiosResponse) => {
      common.fireNotification(dispatch)({
        message: "Invite user success.",
        variant: Variant.SUCCESS
      });
      action.meta.cb(true);

      dispatch(getListUserInvitation());
    })
    .catch((e: AxiosError) => {
      const response = e.response as AxiosResponse;

      common.fireNotification(dispatch)({
        message: response.data.ErrorMessage
          ? response.data.ErrorMessage
          : response.data,
        variant: Variant.ERROR
      });

      action.meta.cb(false);
    });
};
