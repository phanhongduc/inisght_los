import { Dispatch, Store } from "react-redux";
import { IStoreState } from "../../../types";
import axios, { AxiosError, AxiosResponse } from "axios";
import { REACT_APP_API_URL } from "../../../environment";
import { common } from "../../../actions";
import Variant from "../../../components/notification/types/variant";
import { IInvitation } from "../types/invitation";
import {
  IResendInvitation,
  IResendAllInvitation,
  IDeleteInvitation
} from "../actions/invitation";
import { DELETE_INVITATION } from "../constants/invitation";
import {
  RESEND_INVITATION,
  RESEND_ALL_INVITATION
} from "../constants/invitation";
import { getListUserInvitation } from "../actions/users";

export const resendMiddleware = (store: Store<IStoreState>) => (
  next: Dispatch<IResendInvitation | IResendAllInvitation | IDeleteInvitation>
) => (action: IResendInvitation | IResendAllInvitation | IDeleteInvitation) => {
  const currentState = store.getState() as IStoreState;
  const libId = currentState.libs.selectedLib;
  switch (action.type) {
    case RESEND_INVITATION: {
      resendInvitation(store.dispatch)(libId, action.payload);
      break;
    }
    case RESEND_ALL_INVITATION: {
      resendAllInvitation(store.dispatch)(libId);
      break;
    }
    case DELETE_INVITATION: {
      deleteInvitation(store.dispatch)(libId, action.payload);
      break;
    }
  }
  return next(action);
};

const resendInvitation = (dispatch: Dispatch) => (
  libId: string,
  invitation: IInvitation
) => {
  axios
    .post(
      `${REACT_APP_API_URL}/api/User/${libId}/invitation/resend/${
        invitation.id
      }`,
      {}
    )
    .then((data: AxiosResponse<any>) => {
      common.fireNotification(dispatch)({
        message: `Resend invitation to ${invitation.email} success.`,
        variant: Variant.SUCCESS
      });
    })
    .catch((e: AxiosError) => {
      common.fireNotification(dispatch)({
        message: `Resend invitation to ${invitation.email} fail.`,
        variant: Variant.ERROR
      });
    });
};

const resendAllInvitation = (dispatch: Dispatch) => (libId: string) => {
  axios
    .post(`${REACT_APP_API_URL}/api/User/${libId}/invitation/resendall`, {})
    .then((data: AxiosResponse<any>) => {
      common.fireNotification(dispatch)({
        message: `Resend invitation to all emails success.`,
        variant: Variant.SUCCESS
      });
    })
    .catch((e: AxiosError) => {
      common.fireNotification(dispatch)({
        message: `Resend invitation to  all emails fail.`,
        variant: Variant.ERROR
      });
    });
};

const deleteInvitation = (dispatch: Dispatch) => (
  libId: string,
  invitation: IInvitation
) => {
  axios
    .delete(
      `${REACT_APP_API_URL}/api/User/${libId}/invitation/${invitation.id}`
    )
    .then((data: AxiosResponse<any>) => {
      common.fireNotification(dispatch)({
        message: `Delete invitation to ${invitation.email} success.`,
        variant: Variant.SUCCESS
      });
      dispatch(getListUserInvitation());
    })
    .catch((e: AxiosError) => {
      common.fireNotification(dispatch)({
        message: `Delete invitation to ${invitation.email} failed.`,
        variant: Variant.ERROR
      });
    });
};
