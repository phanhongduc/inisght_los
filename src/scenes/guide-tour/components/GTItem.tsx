import * as React from "react";

interface IPropsOrigin {
  children: React.ReactElement<any>;
}

export interface IPropsText extends IPropsOrigin {
  title: string;
  icon: React.ReactElement<any>;
}
export interface IPropsElement extends IPropsOrigin {
  title: React.ReactElement<any>;
}

export type IProps = IPropsText | IPropsElement;

const GTItem = ({ children }: IProps) => children;

export default GTItem;
