import { Theme, createStyles } from "@material-ui/core/styles";
import { IStyleTypeProps } from "../../../../styles/utils";
import * as React from "react";
import withStyles from "@material-ui/core/styles/withStyles";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing.unit * 2
    },
    video: {
      width: "100%",
      height: "auto"
    },
    text: {
      marginBottom: theme.spacing.unit * 2
    }
  });

const StepCreateJob = ({ classes }: IStyleTypeProps<typeof styles>) => (
  <div className={classes.root}>
    <video
      controls={true}
      className={classes.video}
      src={require("src/assets/guide/create-job.mp4")}
      autoPlay={true}
    />
  </div>
);

export default withStyles(styles, { withTheme: true })(StepCreateJob);
