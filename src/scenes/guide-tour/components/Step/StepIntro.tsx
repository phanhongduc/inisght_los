import { Theme, createStyles } from "@material-ui/core/styles";
import { IStyleTypeProps } from "../../../../styles/utils";
import * as React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing.unit * 2
    },
    video: {
      display: "block",
      margin: "0 auto"
    },
    text: {
      marginBottom: theme.spacing.unit * 2
    }
  });

const StepIntro = ({ classes }: IStyleTypeProps<typeof styles>) => (
  <div className={classes.root}>
    <Typography className={classes.text}>
      We will show you just how easy it is to create a job, fly the mission, and
      see the results. Watch the video below for an overview:
    </Typography>
    <iframe
      className={classes.video}
      width="560"
      height="315"
      src="https://www.youtube.com/embed/WrtBn1WEQzU"
      frameBorder="0"
      allowFullScreen={true}
    />
  </div>
);

export default withStyles(styles, { withTheme: true })(StepIntro);
