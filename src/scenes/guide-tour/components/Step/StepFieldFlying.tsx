import { createStyles, Theme } from "@material-ui/core/styles";
import { IStyleTypeProps } from "../../../../styles/utils";
import * as React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing.unit * 2
    },
    video: {
      display: "block",
      margin: "0 auto"
    },
    text: {
      marginBottom: theme.spacing.unit
    }
  });

const StepFieldFlying = ({ classes }: IStyleTypeProps<typeof styles>) => (
  <div className={classes.root}>
    <Typography className={classes.text}>
      Use our <b>Line of Sight</b> app and follow below steps.
    </Typography>
    <ul>
      <li>
        <Typography className={classes.text}>
          Login to the Line of Sight mobile app from the App Store, and navigate
          to the location of the job
        </Typography>
      </li>
      <li>
        <Typography className={classes.text}>
          Click the job and select 'Launch'
        </Typography>
      </li>
      <li>
        <Typography className={classes.text}>
          Press the take-off button and fly the UAV safely to approx. 20m
        </Typography>
      </li>
      <li>
        <Typography className={classes.text}>
          On the top-right corner, click on 'Select Site'. A dropdown will
          appear with the candidates sites chosen
        </Typography>
      </li>
      <li>
        <Typography className={classes.text}>
          Select on a site and watch the UAV rotate towards it. Use the camera
          button to take a photo
        </Typography>
      </li>
      <li>
        <Typography className={classes.text}>
          Repeat until all candidate sites are completed, and then select to
          land the UAV. You can navigate the UAV during the landing process for
          safety
        </Typography>
      </li>
      <li>
        <Typography className={classes.text}>
          Click the back arrow to exit UAV control
        </Typography>
      </li>
      <li>
        <Typography className={classes.text}>
          The app will ask whether you would like to process now. Select 'Yes'.
        </Typography>
      </li>
      <li>
        <Typography className={classes.text}>
          Watch the photos download, process and then upload to the cloud.
        </Typography>
      </li>
    </ul>
  </div>
);

export default withStyles(styles, { withTheme: true })(StepFieldFlying);
