import { createStyles, Theme } from "@material-ui/core/styles";
import { IStyleTypeProps } from "../../../../styles/utils";
import * as React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import Typography from "@material-ui/core/Typography";

const styles = (theme: Theme) =>
  createStyles({
    root: {
      padding: theme.spacing.unit * 2
    },
    video: {
      display: "block",
      margin: "0 auto"
    },
    text: {
      marginBottom: theme.spacing.unit
    }
  });

const StepFinish = ({ classes }: IStyleTypeProps<typeof styles>) => (
  <div className={classes.root}>
    <Typography className={classes.text}>
      Now you got the basics down. If you have any question, please contact us
      directly.
    </Typography>
  </div>
);

export default withStyles(styles, { withTheme: true })(StepFinish);
