import * as React from "react";
import styles from "../styles/GTFrame";
import withStyles from "@material-ui/core/styles/withStyles";
import Grid from "@material-ui/core/Grid";
import { IProps as IItemProps, IPropsText } from "./GTItem";
import List from "@material-ui/core/List";
import { IStyleTypeProps } from "src/styles/utils";
import ListItem from "@material-ui/core/ListItem";
import ListItemText from "@material-ui/core/ListItemText";
import * as classNames from "classnames";
import GTToolbar from "./GTToolbar";

export interface IProps {
  children: Array<React.ReactElement<IItemProps>>;
  selected: number;
  dontShowNextTime: boolean;
}

export interface IDispatchs {
  onSelect: (idx: number) => void;
  close: () => void;
  toggleShow: (flg: boolean) => void;
}
const onClickItem = (idx: number, onSelect: (idx: number) => void) => () => {
  onSelect(idx);
};

const isTextTitle = (props: IItemProps): props is IPropsText => {
  return (props as IPropsText).icon !== undefined;
};
const GTFrame = ({
  children,
  classes,
  selected,
  onSelect,
  close,
  dontShowNextTime,
  toggleShow
}: IProps & IDispatchs & IStyleTypeProps<typeof styles>) => (
  <Grid container={true}>
    <Grid item={true} xs={4} className={classes.sideBar}>
      <List component="nav" className={classes.list}>
        {children.map((child, idx) => {
          const CItemList = (props: {
            children: React.ReactElement<any> | Array<React.ReactElement<any>>;
          }) => (
            <ListItem
              key={child.key || idx}
              role={undefined}
              dense={true}
              button={true}
              className={
                selected !== idx
                  ? classes.listItem
                  : classNames(classes.listItem, classes.listItemSelected)
              }
              onClick={onClickItem(idx, onSelect)}
            >
              {props.children}
            </ListItem>
          );
          const itemProps = child.props;
          if (isTextTitle(itemProps)) {
            return (
              <CItemList key={child.key || idx}>
                {itemProps.icon}
                <ListItemText
                  className={classes.listItemText}
                  classes={{
                    primary: classes.listItemText
                  }}
                >
                  {itemProps.title}
                </ListItemText>
              </CItemList>
            );
          }
          return (
            <CItemList key={child.key || idx}>{itemProps.title}</CItemList>
          );
        })}
      </List>
    </Grid>

    <Grid item={true} xs={8} className={classes.content}>
      <div className={classes.contentStep}>{children[selected]}</div>
      <GTToolbar
        move={onSelect}
        close={close}
        finish={close}
        step={selected}
        steps={children.length}
        dontShowNextTime={dontShowNextTime}
        toggleShow={toggleShow}
      />
    </Grid>
  </Grid>
);

export default withStyles(styles, { withTheme: true })(GTFrame);
