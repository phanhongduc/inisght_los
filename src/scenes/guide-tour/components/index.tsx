import withStyles from "@material-ui/core/styles/withStyles";
import styles from "../styles";
import { IStyleTypeProps } from "src/styles/utils";
import Dialog from "@material-ui/core/Dialog";
import * as React from "react";
// import DialogTitle from "@material-ui/core/DialogTitle";
import DialogContent from "@material-ui/core/DialogContent";
import GTContent from "../containers/GTContent";

export interface IProps {
  isOpen: boolean;
}

export interface IDispatchs {
  onClose: () => void;
}

const VirtualLOS = ({
  isOpen,
  onClose,
  classes
}: IProps & IDispatchs & IStyleTypeProps<typeof styles>) => (
  <Dialog
    disableBackdropClick={true}
    disableEscapeKeyDown={true}
    open={isOpen}
    aria-labelledby="alert-dialog-title"
    aria-describedby="alert-dialog-description"
    maxWidth={false}
    className={classes.root}
  >
    <DialogContent className={classes.content}>
      <GTContent />
    </DialogContent>
  </Dialog>
);

export default withStyles(styles, { withTheme: true })(VirtualLOS);
