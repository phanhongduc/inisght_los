import * as React from "react";
import styles from "../styles";
import withStyles from "@material-ui/core/styles/withStyles";
import GTItem from "./GTItem";
import { IStyleTypeProps } from "src/styles/utils";
import GTFrame from "../containers/GTFrame";
import Label from "@material-ui/icons/Label";
import ArrowForward from "@material-ui/icons/ArrowForward";
import Done from "@material-ui/icons/Done";
import * as Loadable from "react-loadable";
import SectionLoading from "../../../components/SectionLoading";

const LoadableStepIntro = Loadable({
  loader: () => import("./Step/StepIntro"),
  loading: SectionLoading
});

const LoadableStepCreateJob = Loadable({
  loader: () => import("./Step/StepCreateJob"),
  loading: SectionLoading
});

const LoadableStepFieldFlying = Loadable({
  loader: () => import("./Step/StepFieldFlying"),
  loading: SectionLoading
});

const LoadableStepResult = Loadable({
  loader: () => import("./Step/StepResult"),
  loading: SectionLoading
});

const LoadableStepLOS = Loadable({
  loader: () => import("./Step/StepLOS"),
  loading: SectionLoading
});

const LoadableStepFinish = Loadable({
  loader: () => import("./Step/StepFinish"),
  loading: SectionLoading
});

export interface IProps {
  selected: number;
}

const GTContent = ({ classes }: IProps & IStyleTypeProps<typeof styles>) => (
  <GTFrame>
    <GTItem
      title={"Hi and welcome to Line of Sight"}
      icon={<Label fontSize="inherit" />}
    >
      <LoadableStepIntro />
    </GTItem>
    <GTItem
      title={"Step 1: Create a job"}
      icon={<ArrowForward fontSize="inherit" />}
    >
      <LoadableStepCreateJob />
    </GTItem>
    <GTItem
      title={"Step 2: Field Flying"}
      icon={<ArrowForward fontSize="inherit" />}
    >
      <LoadableStepFieldFlying />
    </GTItem>
    <GTItem
      title={"Step 3: Review Results"}
      icon={<ArrowForward fontSize="inherit" />}
    >
      <LoadableStepResult />
    </GTItem>
    <GTItem
      title={"Step 4: Edit Virtual LOS"}
      icon={<ArrowForward fontSize="inherit" />}
    >
      <LoadableStepLOS />
    </GTItem>
    <GTItem title={"Finish"} icon={<Done fontSize="inherit" />}>
      <LoadableStepFinish />
    </GTItem>
  </GTFrame>
);

export default withStyles(styles, { withTheme: true })(GTContent);
