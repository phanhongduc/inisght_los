import * as React from "react";
import styles from "../styles/GTToolbar";
import withStyles from "@material-ui/core/styles/withStyles";
import { IStyleTypeProps } from "src/styles/utils";
import Toolbar from "@material-ui/core/Toolbar";
import Button from "@material-ui/core/Button";
import ArrowBack from "@material-ui/icons/ArrowBack";
import ArrowForward from "@material-ui/icons/ArrowForward";
import Close from "@material-ui/icons/Close";
import Done from "@material-ui/icons/Done";
import Checkbox from "@material-ui/core/Checkbox";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import * as classNames from "classnames";

export interface IProps {
  step: number;
  steps: number;
  dontShowNextTime: boolean;
}

export interface IDispatchs {
  move: (step: number) => void;
  close: () => void;
  finish: () => void;
  toggleShow: (flg: boolean) => void;
}

const onClickMove = (step: number, move: (step: number) => void) => () => {
  move(step);
};

const onClickToggle = (flg: boolean, toggle: (flg: boolean) => void) => () => {
  toggle(flg);
};

const GTToolbar = ({
  step,
  steps,
  move,
  close,
  finish,
  classes,
  toggleShow,
  dontShowNextTime
}: IProps & IDispatchs & IStyleTypeProps<typeof styles>) => (
  <Toolbar className={classes.root}>
    <div className={classes.buttonGroupTop}>
      <Button
        variant="fab"
        className={classNames(classes.button, classes.closeButton)}
        mini={true}
        color="default"
        aria-label="ZoomOut"
        onClick={close}
      >
        <Close />
      </Button>
    </div>
    <div className={classes.buttonGroupBottom}>
      {step < steps - 1 && (
        <Button
          variant="fab"
          className={classNames(classes.button, classes.actionButton)}
          mini={true}
          color="default"
          aria-label="ZoomOut"
          onClick={onClickMove(step + 1, move)}
        >
          <ArrowForward />
        </Button>
      )}
      {step === steps - 1 && (
        <Button
          variant="fab"
          className={classNames(classes.button, classes.actionButton)}
          mini={true}
          color="default"
          aria-label="ZoomOut"
          onClick={finish}
        >
          <Done />
        </Button>
      )}
      {step > 0 && (
        <Button
          variant="fab"
          className={classNames(classes.button, classes.actionButton)}
          mini={true}
          color="default"
          aria-label="ZoomOut"
          onClick={onClickMove(step - 1, move)}
        >
          <ArrowBack />
        </Button>
      )}

      <FormControlLabel
        control={
          <Checkbox
            checked={!dontShowNextTime}
            onChange={onClickToggle(!dontShowNextTime, toggleShow)}
            className={classes.checkbox}
          />
        }
        label="Show tutorial next time"
      />
    </div>
  </Toolbar>
);

export default withStyles(styles, { withTheme: true })(GTToolbar);
