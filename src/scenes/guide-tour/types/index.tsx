export default interface IGuideTour {
  isOpen: boolean;
  selected: number;
  dontShowNextTime: boolean;
}
