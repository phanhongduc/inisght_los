import { Store, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import { IApplicationInit } from "../../../actions";
import { APPLICATION_INIT } from "../../../constants";
import {
  guideTourDontShowNext,
  guideTourDialogControl,
  IGuideTourDontShowNext
} from "../actions/index";
import { GUILD_TOUR_DONT_SHOW_NEXT } from "../constants/index";
import clientStorage, { ClientStorageKey } from "src/helpers/clientStorage";

export const guideTourInit = (store: Store<IStoreState>) => (
  next: Dispatch<IApplicationInit | IGuideTourDontShowNext>
) => (action: IApplicationInit | IGuideTourDontShowNext) => {
  switch (action.type) {
    case APPLICATION_INIT: {
      const dontShowFlg =
        clientStorage.getItem(ClientStorageKey.DONT_SHOW_NEXT_KEY) === "true";
      guideTourDontShowNext(store.dispatch)(dontShowFlg);
      guideTourDialogControl(store.dispatch)(!dontShowFlg);
      break;
    }
    case GUILD_TOUR_DONT_SHOW_NEXT: {
      clientStorage.setItem(
        ClientStorageKey.DONT_SHOW_NEXT_KEY,
        action.payload ? "true" : "false"
      );
      break;
    }
  }

  return next(action);
};
