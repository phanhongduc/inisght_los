import { connect, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import GTFrame, { IProps, IDispatchs } from "../components/GTFrame";
import { Without } from "src/helpers/types";
import {
  guideTourSelectItem,
  guideTourDialogControl,
  guideTourDontShowNext
} from "../actions";

type IExProps = Without<IProps, "selected" | "dontShowNextTime">;

function mapStateToProps(
  { guideTour: { selected, dontShowNextTime } }: IStoreState,
  props: IExProps
) {
  return { ...props, selected, dontShowNextTime };
}

function mapDispatchToProps(dispatch: Dispatch): IDispatchs {
  return {
    onSelect: (idx: number) => {
      guideTourSelectItem(dispatch)(idx);
    },
    close: () => {
      guideTourDialogControl(dispatch)(false);
    },
    toggleShow: (flg: boolean) => {
      guideTourDontShowNext(dispatch)(flg);
    }
  };
}

export default connect<IProps, IDispatchs, IExProps>(
  mapStateToProps,
  mapDispatchToProps
)(GTFrame);
