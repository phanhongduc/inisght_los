import { connect, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import GuideTour, { IProps, IDispatchs } from "../components";

function mapStateToProps({ guideTour: { isOpen }, auth, libs }: IStoreState) {
  return {
    isOpen: !!(isOpen && auth.isAuth && libs.selectedLib)
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onClose: () => {
      console.log("close");
    }
  };
}

export default connect<IProps, IDispatchs>(
  mapStateToProps,
  mapDispatchToProps
)(GuideTour);
