import { connect, Dispatch } from "react-redux";
import { IStoreState } from "src/types";
import GTContent from "../components/GTContent";

function mapStateToProps({ guideTour }: IStoreState) {
  return {
    selected: guideTour.selected
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {};
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(GTContent);
