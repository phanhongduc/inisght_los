import { IGuideTourDialogControl } from "../actions";
import {
  GUILD_TOUR_OPEN_CONTROL,
  GUILD_TOUR_SELECT_ITEM
} from "../constants/index";
import { combineReducers } from "redux";
import IGuideTour from "../types";
import { IGuideTourSelectItem, IGuideTourDontShowNext } from "../actions/index";
import { GUILD_TOUR_DONT_SHOW_NEXT } from "../constants/index";

function isOpen(state: boolean = true, action: IGuideTourDialogControl) {
  switch (action.type) {
    case GUILD_TOUR_OPEN_CONTROL:
      return action.payload;
  }
  return state;
}

function selected(state: number = 0, action: IGuideTourSelectItem) {
  switch (action.type) {
    case GUILD_TOUR_SELECT_ITEM:
      return action.payload;
  }
  return state;
}

function dontShowNextTime(
  state: boolean = false,
  action: IGuideTourDontShowNext
) {
  switch (action.type) {
    case GUILD_TOUR_DONT_SHOW_NEXT:
      return action.payload;
  }
  return state;
}

export const guideTour = combineReducers<IGuideTour>({
  isOpen,
  selected,
  dontShowNextTime
});
