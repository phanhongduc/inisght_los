import { Action } from "redux";
import {
  GUILD_TOUR_OPEN_CONTROL,
  GUILD_TOUR_SELECT_ITEM,
  GUILD_TOUR_DONT_SHOW_NEXT
} from "../constants";
import { Dispatch } from "react-redux";

export interface IGuideTourDialogControl
  extends Action<GUILD_TOUR_OPEN_CONTROL> {
  payload: boolean;
}

export const guideTourDialogControl = (
  dispatch: Dispatch<IGuideTourDialogControl>
) => (flag: boolean) => {
  dispatch({
    payload: flag,
    type: GUILD_TOUR_OPEN_CONTROL
  });
};

export interface IGuideTourSelectItem extends Action<GUILD_TOUR_SELECT_ITEM> {
  payload: number;
}

export const guideTourSelectItem = (
  dispatch: Dispatch<IGuideTourSelectItem>
) => (flag: number) => {
  dispatch({
    payload: flag,
    type: GUILD_TOUR_SELECT_ITEM
  });
};

export interface IGuideTourDontShowNext
  extends Action<GUILD_TOUR_DONT_SHOW_NEXT> {
  payload: boolean;
}

export const guideTourDontShowNext = (
  dispatch: Dispatch<IGuideTourDontShowNext>
) => (flag: boolean) => {
  dispatch({
    payload: flag,
    type: GUILD_TOUR_DONT_SHOW_NEXT
  });
};
