import { Theme, createStyles } from "@material-ui/core/styles";
const styles = (theme: Theme) =>
  createStyles({
    root: {},
    content: {
      width: 900,
      height: 600,
      padding: "0 !important"
    },
    button: {
      margin: theme.spacing.unit,
      width: theme.spacing.unit * 9
    },
    buttonGroup: {
      textAlign: "center"
    }
  });

export default styles;
