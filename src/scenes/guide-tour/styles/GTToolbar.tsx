import { Theme, createStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";

const buttonGroup = (theme: Theme): CSSProperties => ({
  position: "absolute",
  left: 0,
  width: "100%",
  paddingLeft: theme.spacing.unit * 2,
  paddingRight: theme.spacing.unit * 2
});

const styles = (theme: Theme) =>
  createStyles({
    root: {
      position: "absolute",
      right: 0,
      top: 0,
      height: "100%",
      width: "100%"
    },
    button: {
      marginLeft: theme.spacing.unit * 2,
      float: "right"
    },
    actionButton: {
      background: "#106ebe",
      color: "#ffffff",
      "&:hover": {
        background: "#106ebeb5"
      }
    },
    closeButton: {
      background: "none",
      boxShadow: "none"
    },
    buttonGroupTop: {
      ...buttonGroup(theme),
      top: theme.spacing.unit * 2
    },
    buttonGroupBottom: {
      ...buttonGroup(theme),
      bottom: theme.spacing.unit * 2
    },
    checkbox: {
      color: "#106ebe"
    }
  });

export default styles;
