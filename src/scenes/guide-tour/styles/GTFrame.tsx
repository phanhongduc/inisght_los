import { Theme, createStyles } from "@material-ui/core/styles";

const BORDER_STYLE = "1px solid #c8c8c8";

const styles = (theme: Theme) =>
  createStyles({
    root: {},
    content: {
      position: "relative"
    },
    list: {
      padding: 0
    },
    listItem: {
      height: 100,
      borderBottom: BORDER_STYLE,
      color: "#106ebe !important",
      fontSize: theme.spacing.unit * 4
    },
    listItemSelected: {
      backgroundColor: "#bdbdbda1 !important"
    },
    listItemText: {
      fontSize: 18,
      color: "#106ebe",
      paddingRight: 0
    },
    sideBar: {
      borderRight: BORDER_STYLE
    },
    contentStep: {
      maxHeight: 520,
      overflow: "auto",
      marginTop: 40,
      position: "relative",
      zIndex: 1
    }
  });

export default styles;
