import { UserLibRole } from "../../../helpers/permission";

export interface ILib {
  id: string;
  name: string;
  ownerId: string;
  status: LibStatus;
  role: UserLibRole;
  type: LibType;
  active: boolean;
}

export enum LibType {
  FreeTrial = 0,
  PayAsYouGo = 10,
  Enterprise = 100
}

export enum LibTypeText {
  FreeTrial = "Free trial",
  PayAsYouGo = "Pay as you go",
  Enterprise = "Enterprise"
}

export interface ILibsState {
  isLoading: boolean;
  isFailed: boolean;
}

export interface ILibVersion {
  createdDatetimeUtc: string;
  versionId: number;
  siteJsonUrl: string;
}

export interface ILibUser {
  userId: string;
  email: string;
  firstName: string;
  lastName: string;
}

export interface ILibQueryPoint {
  lat: number;
  lon: number;
}

export interface ILibJobQuery {
  keyword: string;
  jobStatus: number;
  assigneeId: string;
  take: number;
  skip: number;
  isLoading: boolean;
  isFailed: boolean;
  polygonCoordinates: ILibQueryPoint[];
}

export interface ILibsStore {
  list: ILib[];
  listState: ILibsState;
  selectedLib: string;
  selectedLibVersion: ILibVersion | null;
  users: ILibUser[];
  jobQuery: ILibJobQuery;
  jobSearchResult: ISearchJobResult;
  currentRole: UserLibRole;
  template: string;
}

export enum LibStatus {
  NOT_IMPORT = 0,
  IMPORTING,
  SUCCESS,
  FAILED,
  PROCESSING
}

export enum LibActiveStatusText {
  ACTIVE = "Active",
  INACTIVE = "Inactive"
}

export enum LibStatusText {
  NOT_IMPORT = "Not import",
  IMPORTING = "Importing",
  SUCCESS = "Imported",
  FAILED = "Failed import",
  PROCESSING = "Processing"
}

export interface ISiteData {
  latitude: string;
  longitude: string;
  networkname: string;
  siteid: string;
  sitename: string;
}

export interface ISaveJobData {
  jobName: string;
  address: string;
  lon: number;
  lat: number;
  recommendHeight: number;
  frequency: number;
  candidates: IJobCandidate[];
  engineerId: string;
}

export interface IJobCandidate {
  bendId: string;
  bendSiteName: string;
  lon: number;
  lat: number;
  displayOrder: number;
  recommendHeight: number;
}

export interface IResponseSearchJob {
  total: number;
  jobs: IJob[];
}

export interface ISearchJobResult extends IResponseSearchJob {
  lastUpdated: number;
}

export interface IJob {
  name: string;
  jobStatus: number;
  lat: number;
  lon: number;
  id: string;
  address: string;
}

export enum JobStatus {
  Created = 0,
  Processing = 10,
  Finished = 20
}
