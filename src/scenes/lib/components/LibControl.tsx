import * as React from "react";
import { Redirect, Route, RouteProps, Switch } from "react-router";
import * as Loadable from "react-loadable";
import SectionLoading from "src/components/SectionLoading";
import { ILib, LibStatus } from "../types/libs";
import paths from "../../../paths";
import Variant from "../../../components/notification/types/variant";

const LoadAbleSelectLib = Loadable({
  loading: SectionLoading,
  loader: () => import("../containers/StepSelectLib")
});

const LoadAbleStepImport = Loadable({
  loading: SectionLoading,
  loader: () => import("../containers/StepImport")
});

export interface IProps {
  libs: ILib[];
  selectLib: string;
  showError: (msg: string, v?: Variant) => void;
}

class LibControlCompoent extends React.Component<IProps & RouteProps> {
  constructor(props: IProps & RouteProps) {
    super(props);
  }

  public componentWillMount() {
    const { libs, selectLib, showError } = this.props;
    const selectedLib = libs.find(lib => lib.id === selectLib);

    if (selectedLib && selectedLib.status !== 0) {
      if (selectedLib.status !== LibStatus.SUCCESS) {
        const importing =
          selectedLib.status === LibStatus.IMPORTING ||
          selectedLib.status === LibStatus.PROCESSING;
        showError(
          importing
            ? "Data is processing with the selected library."
            : "Failed import library.",
          importing ? Variant.INFO : Variant.ERROR
        );
      }
    }
  }

  public render() {
    return <LibControl {...this.props} />;
  }
}

function LibControl(props: IProps & RouteProps) {
  const { libs, selectLib, location } = props;
  const selectedLib = libs.find(lib => lib.id === selectLib);

  if (selectedLib && selectedLib.status !== 0) {
    if (selectedLib.status !== LibStatus.FAILED) {
      return <Redirect to={paths.board} />;
    }
  }

  if (
    selectedLib &&
    selectedLib.status === LibStatus.NOT_IMPORT &&
    location &&
    location.pathname !== paths.libImport
  ) {
    return <Redirect to={paths.libImport} />;
  }

  return (
    <Switch>
      <Route path={paths.libSelect} component={LoadAbleSelectLib} />
      <Route path={paths.libImport} component={LoadAbleStepImport} />
    </Switch>
  );
}

export default LibControlCompoent;
