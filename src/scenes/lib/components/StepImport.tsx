import * as React from "react";
import ReactTable, { Column, RowInfo } from "react-table";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import "react-table/react-table.css";
import IStyleProps from "../../../styles/utils";
import ReactFileReader from "react-file-reader";
import * as Papaparse from "papaparse";
import Button from "@material-ui/core/Button";
// import CloudUploadIcon from "@material-ui/icons/CloudUpload";
// import Stepper from '@material-ui/core/Stepper';
// import Step from '@material-ui/core/Step';
// import StepLabel from '@material-ui/core/StepLabel';
import red from "@material-ui/core/colors/red";
import blue from "@material-ui/core/colors/blue";
import Dropzone from "react-dropzone";
import logo from "src/assets/images/csv.png";
import { Theme, Typography } from "@material-ui/core";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import SaveIcon from "@material-ui/icons/Save";
import { ISiteData } from "../types/libs";
import { Redirect } from "react-router";
import paths from "../../../paths";

// const data = [{
//   name: 'Tanner Linsley',
//   age: 26,
//   friend: {
//     name: 'Jason Maurer',
//     age: 23,
//   }
// }]

const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  wrapper: {
    display: "inline"
  },
  // wrapperButton: {
  //   display: "flex",
  //   background: "linear-gradient(90deg,#28b1fc,#3a7eea)",
  //   height: '150px'
  // },
  wrapperInside: {
    margin: "auto",
    display: "flex",
    paddingTop: "30px"
  },
  title: {
    margin: "auto"
  },
  header: {
    margin: "auto"
  },
  content: {
    margin: "auto",
    width: "600px",
    color: "#666",
    fontSize: "15px"
    // span: {

    // }
  },
  invalidInform: {
    margin: "auto"
  },
  backButton: {
    margin: "auto",
    marginRight: "30px"
  },
  uploadButton: {
    position: "absolute",
    right: "20px",
    top: "25px"
  },
  importButton: {
    margin: "auto"
  },
  templateButton: {
    margin: "auto"
  },
  process: {
    height: "70px",
    width: "700px",
    margin: "auto",
    paddingLeft: "40px",
    paddingRight: "40px",
    borderRadius: "40px"
  },
  table: {
    width: "65%",
    marginLeft: "auto",
    marginRight: "auto"
  },
  csv: {
    width: "60px",
    height: "60px"
  },
  csvText: {
    marginBottom: "auto",
    marginTop: "auto"
  },
  importZone: {
    display: "flex",
    margin: "auto"
  },
  dropzone: {
    width: "600px",
    display: "flex",
    height: "90px",
    border: "1px dashed #18bfd0",
    padding: "10px 20px 15px 10px",
    borderRadius: "4px"
  }
});

interface IState {
  doneUpload: boolean;
  fileName: string;
  activeStep: number;
  skipped: Set<number>;
  validData: any[];
  invalidData: any[];
  validColumns: Column[];
  invalidColumns: Column[];
}

interface IProps {
  selectLib: string;
  libName: string;
  logout: () => void;
  uploadLib: (
    libId: string,
    data: ISiteData[],
    done: (reset: boolean) => void
  ) => void;
}

interface ICell {
  index: number;
  value: string;
  original: ISiteData;
  column: {
    id: string;
  };
}

// const steps = ['Network Site-Based CSV', 'Invite User'];

class StepImport extends React.Component<IStyleProps & IProps, IState> {
  constructor(props: IStyleProps & IProps) {
    super(props);
    this.state = {
      doneUpload: false,
      fileName: "",
      activeStep: 0,
      skipped: new Set(),
      validData: [],
      invalidData: [],
      validColumns: [
        {
          Header: "Valid Sites",
          columns: []
        }
      ],
      invalidColumns: [
        {
          Header: "Invalid Sites",
          columns: []
        }
      ]
    };
  }
  private validData(data: ISiteData) {
    return (
      data.latitude &&
      data.longitude &&
      Number(data.latitude) <= 90 &&
      Number(data.latitude) >= -90 &&
      Number(data.longitude) >= -180 &&
      Number(data.longitude) <= 180 &&
      data.networkname &&
      data.siteid &&
      data.sitename
    );
  }
  public handleFile = (files: any) => {
    const self = this;
    if (files[0]) {
      this.setState({
        fileName: files[0].name
      });
      Papaparse.parse(files[0], {
        complete: (results: any) => {
          console.log(results);
          const headers: string[] = results.data[0];
          const validColumns: any[] = [];
          const invalidColumns: any[] = [];
          const validData: ISiteData[] = [];
          const invalidData: ISiteData[] = [];
          const keys: string[] = [];
          headers.forEach(element => {
            const key = element.toLowerCase().replace(/\s/g, "");
            const validColumn: Column = {
              Header: element,
              accessor: key,
              Cell: this.renderValidEditable
            };
            const invalidColumn: Column = {
              Header: element,
              accessor: key,
              Cell: this.renderInvalidEditable
            };
            keys.push(key);
            validColumns.push(validColumn);
            invalidColumns.push(invalidColumn);
          });
          const validActionColumn: Column = {
            Header: "Action",
            accessor: "siteid",
            Cell: this.renderValidAction,
            Filter: () => <div />
          };

          const inValidActionColumn: Column = {
            Header: "Action",
            accessor: "siteid",
            Cell: this.renderInvalidAction,
            Filter: () => <div />
          };
          validColumns.push(validActionColumn);
          invalidColumns.push(inValidActionColumn);
          const listData: string[][] = results.data.slice(1);
          listData.forEach((element, dataIndex) => {
            const dataEl: ISiteData = {
              latitude: "",
              longitude: "",
              networkname: "",
              siteid: "",
              sitename: ""
            };
            let isEmpty = true;
            element.forEach((el, index) => {
              dataEl[keys[index]] = el;
              if (el) {
                isEmpty = false;
              }
            });
            if (this.validData(dataEl)) {
              validData.push(dataEl);
            } else if (!isEmpty) {
              invalidData.push(dataEl);
            }
          });

          self.setState({
            validColumns: [
              {
                ...self.state.validColumns[0],
                columns: validColumns
              }
            ],
            invalidColumns: [
              {
                ...self.state.invalidColumns[0],
                columns: invalidColumns
              }
            ],
            validData,
            invalidData
          });
        }
      });
    }
  };
  public renderInvalidAction = (cellInfo: ICell) => {
    return (
      <div style={{ display: "flex" }}>
        <IconButton
          style={{ height: "20px", width: "20px", marginLeft: "auto" }}
          onClick={this.checkInvalidSite.bind(this, cellInfo.original)}
          aria-label="Save"
          color="secondary"
        >
          <SaveIcon style={{ fontSize: 16 }} />
        </IconButton>
        <IconButton
          style={{
            height: "20px",
            width: "20px",
            marginLeft: "20px",
            marginRight: "auto",
            color: red[500]
          }}
          onClick={this.removeInvalidSite.bind(this, cellInfo.value)}
          aria-label="Delete"
        >
          <DeleteIcon style={{ fontSize: 16 }} />
        </IconButton>
      </div>
    );
  };
  public renderValidAction = (cellInfo: ICell) => {
    return (
      <div style={{ display: "flex" }}>
        <IconButton
          style={{
            height: "20px",
            width: "20px",
            margin: "auto",
            color: red[500]
          }}
          onClick={this.removeValidSite.bind(this, cellInfo.value)}
          aria-label="Delete"
        >
          <DeleteIcon style={{ fontSize: 16 }} />
        </IconButton>
      </div>
    );
  };
  public removeValidSite = (index: string) => {
    const validData = this.state.validData.filter(
      (row: ISiteData) => row.siteid !== index
    );
    this.setState({
      validData
    });
  };
  public checkInvalidSite = (siteData: ISiteData) => {
    if (this.validData(siteData)) {
      const invalidData = this.state.invalidData.filter(
        (row: ISiteData) => row.siteid !== siteData.siteid
      );
      this.state.validData.unshift(siteData);
      this.setState({
        validData: this.state.validData,
        invalidData
      });
    }
  };
  public removeInvalidSite = (index: string) => {
    const invalidData = this.state.invalidData.filter(
      (row: ISiteData) => row.siteid !== index
    );
    this.setState({
      invalidData
    });
  };
  public renderValidEditable = (cellInfo: ICell) => {
    const { validData } = this.state;
    if (!validData[cellInfo.index]) {
      return <div />;
    }
    return (
      <div
        contentEditable={true}
        suppressContentEditableWarning={true}
        onBlur={this.onBlurValidData(cellInfo)}
      >
        {validData[cellInfo.index][cellInfo.column.id]}
      </div>
    );
  };
  public renderInvalidEditable = (cellInfo: ICell) => {
    if (!this.state.invalidData[cellInfo.index]) {
      return <div />;
    }
    return (
      <div
        contentEditable={true}
        suppressContentEditableWarning={true}
        onBlur={this.onBlurInvalidData(cellInfo)}
      >
        {this.state.invalidData[cellInfo.index][cellInfo.column.id]}
      </div>
    );
  };
  private onBlurValidData = (cellInfo: ICell) => (e: any) => {
    const validData: ISiteData[] = [...this.state.validData];
    const oldCell = { ...validData[cellInfo.index] };
    validData[cellInfo.index][cellInfo.column.id] = e.target.innerHTML;
    if (this.validData(validData[cellInfo.index])) {
      this.setState({ validData });
    } else {
      document.execCommand("undo");
      setTimeout(() => {
        validData[cellInfo.index] = oldCell;
        this.setState({ validData });
      }, 500);
    }
  };

  private onBlurInvalidData = (cellInfo: ICell) => (e: any) => {
    const invalidData = [...this.state.invalidData];
    invalidData[cellInfo.index][cellInfo.column.id] = e.target.innerHTML;
    this.setState({ invalidData });
  };

  private handleUploadLib = () => {
    const { uploadLib, selectLib } = this.props;
    setTimeout(() => {
      uploadLib(selectLib, this.state.validData, status => {
        console.log(status);
        setTimeout(() => {
          this.setState({
            doneUpload: status
          });
        }, 1000);
      });
    }, 500);
  };
  // private isStepSkipped(step: number) {
  //   return this.state.skipped.has(step);
  // }
  private styleHeader = () => {
    return {
      style: {
        background: "#18bfd0",
        color: "white"
      }
    };
  };
  private styleTr = (state: any, rowInfo: RowInfo) => {
    return {
      style: {
        color: "#222",
        fontSize: "13px",
        fontWeight: 400,
        background: rowInfo.index % 2 === 1 ? "#f4f4f4" : "#fff"
      }
    };
  };
  private styleTd = (state: any, rowInfo: RowInfo, column: Column) => {
    return {
      style: {
        textAlign: column.id === "sitename" ? "left" : "center"
      }
    };
  };

  private downloadCSV(aStyles: { [key: string]: any }, text: string) {
    let csv = "Site Name,Network Name,Latitude,Longitude,Site Id\n";
    const data = [
      ["200025_BroadwayCentre", "VHA", "-33.88372", "151.19387", 200025],
      ["200027_MLCCentre", "VHA", "-33.8686", "151.2093", 200027]
    ];
    data.forEach(row => {
      csv += row.join(",");
      csv += "\n";
    });
    // const hiddenElement = document.createElement("a");
    // hiddenElement.href = "data:text/csv;charset=utf-8," + encodeURI(csv);
    // hiddenElement.target = "_blank";
    // hiddenElement.download = "sites.csv";
    // hiddenElement.click();
    return (
      <a
        style={aStyles}
        href={"data:text/csv;charset=utf-8," + encodeURI(csv)}
        target="_blank"
        download="sites.csv"
      >
        {text}
      </a>
    );
  }
  public render() {
    if (this.state.doneUpload) {
      return <Redirect to={paths.board} />;
    }

    const { classes, libName } = this.props;
    return (
      <div className={classes.wrapper}>
        <div className={classes.wrapperInside}>
          <div className={classes.header}>
            <Typography variant="headline" color="secondary">
              {`Upload Network Site-Based CSV for library ${libName}`}
            </Typography>
          </div>
        </div>
        <div className={classes.wrapperInside}>
          <div className={classes.content}>
            Tell us about your network. Please upload a site CSV in the
            following format: Site Name, Network Name, Latitude, Longitude, Site
            Id or download CSV template
            {this.downloadCSV(
              {
                marginLeft: "5px",
                cursor: "pointer",
                textDecoration: "underline",
                color: blue[500]
              },
              "here"
            )}
          </div>
        </div>
        <div className={classes.wrapperInside}>
          <div className={classes.importZone}>
            <Dropzone
              onDrop={this.handleFile}
              disableClick={true}
              className={classes.dropzone}
            >
              <img src={logo} className={classes.csv} />
              <Typography variant="subheading" className={classes.csvText}>
                {this.state.fileName.length > 0
                  ? this.state.fileName
                  : "Please upload a network site-based CSV"}
              </Typography>
              <div className={classes.uploadButton}>
                <ReactFileReader
                  handleFiles={this.handleFile}
                  fileTypes={[".csv"]}
                >
                  <Button variant="contained" color="primary">
                    {this.state.invalidData &&
                    this.state.invalidData.length > 0 &&
                    this.state.invalidColumns
                      ? "Re-upload"
                      : "Upload"}
                  </Button>
                </ReactFileReader>
              </div>
            </Dropzone>
          </div>
        </div>
        {this.state.invalidData &&
          this.state.invalidData.length > 0 &&
          this.state.invalidColumns && (
            <div>
              <div className={classes.wrapperInside}>
                <div className={classes.invalidInform}>
                  <Typography color="error">
                    <b>
                      Please correct these invalid sites to import into library
                    </b>
                  </Typography>
                </div>
              </div>
              <div className={classes.wrapperInside}>
                <div className={classes.table}>
                  <ReactTable
                    getTheadProps={this.styleHeader}
                    getTrProps={this.styleTr}
                    getTdProps={this.styleTd}
                    data={this.state.invalidData}
                    columns={this.state.invalidColumns}
                    defaultPageSize={10}
                    minRows={0}
                  />
                </div>
              </div>
            </div>
          )}
        <div className={classes.wrapperInside}>
          {this.state.validData &&
            this.state.validData.length > 0 &&
            this.state.validColumns && (
              <div className={classes.table}>
                <ReactTable
                  data={this.state.validData}
                  columns={this.state.validColumns}
                  getTrProps={this.styleTr}
                  getTheadProps={this.styleHeader}
                  getTdProps={this.styleTd}
                  defaultPageSize={10}
                  minRows={0}
                />
              </div>
            )}
        </div>

        <div className={classes.wrapperInside}>
          {this.state.validData.length > 0 && (
            <div className={classes.importButton}>
              <Button
                variant="contained"
                disabled={
                  this.state.validData.length === 0 ||
                  this.state.invalidData.length > 0
                }
                color="primary"
                onClick={this.handleUploadLib}
              >
                Import
              </Button>
            </div>
          )}
        </div>
        <div className={classes.wrapperInside} />
      </div>
    );
  }
}

export default withStyles(styles)(StepImport);
