import { Dispatch, Store } from "react-redux";
import { debounce } from "lodash-es";
import { IStoreState } from "../../../types";
import {
  getJob,
  IGetJob,
  IGetLibVersion,
  IGetListLibs,
  ISaveJob,
  ISelectLib,
  setJobQuery,
  setLibUsers,
  setLibVersion,
  setListJob,
  setListLibs,
  setListLibsState,
  ISetListLibs,
  IUploadLib,
  getListLibs,
  IGetLibTemplate,
  getLibTemplate,
  setLibTemplate
} from "../actions/libs";
import axios, { AxiosError, AxiosResponse, Canceler } from "axios";
import { REACT_APP_API_URL } from "../../../environment";
import {
  GET_JOB,
  GET_LIB_TEMPLATE,
  GET_LIB_VERSION,
  GET_LIST_LIBS,
  SAVE_JOB,
  SELECT_LIB,
  SET_LIST_LIBS,
  UPLOAD_LIB
} from "../constants/libs";
import { getMatch } from "src/helpers/url";
import RouteUri from "src/helpers/routeUri";
import paths from "../../../paths";
import {
  ILib,
  ILibUser,
  ILibVersion,
  IResponseSearchJob,
  ISiteData
} from "../types/libs";
import { common } from "../../../actions";
import getErrorMessage from "../../../helpers/getErrorMessage";
import Variant from "../../../components/notification/types/variant";
import { setCurrentLibRole } from "../actions/libs";
import { LOCATION_CHANGE, LocationChangeAction } from "react-router-redux";
import { Action } from "redux";
import { getRecentJobs } from "../../network/actions/Job";
import clientStorage, { ClientStorageKey } from "src/helpers/clientStorage";

let cancelerGetPointApi: Canceler;

const debounceCallSearchKeyword = debounce((cb: () => void) => {
  cb();
}, 250);

type LOCATION_CHANGE = typeof LOCATION_CHANGE;
interface IPathInit extends Action<LOCATION_CHANGE> {}

const convertArrayOfObjectsToCSV = (data: ISiteData[]) => {
  if (data == null || !data.length) {
    return null;
  }

  const columnDelimiter = ",";
  const lineDelimiter = "\n";

  const keys = Object.keys(data[0]);

  let result = "";
  result += keys.join(columnDelimiter);
  result += lineDelimiter;

  data.forEach(item => {
    let ctr = 0;
    keys.forEach(key => {
      if (ctr > 0) {
        result += columnDelimiter;
      }
      result += item[key];
      ctr++;
    });
    result += lineDelimiter;
  });

  return result;
};

export const initPathSelectLibs = (store: Store<IStoreState>) => (
  next: Dispatch<IPathInit>
) => (action: LocationChangeAction) => {
  const currentState = store.getState();
  switch (action.type) {
    case LOCATION_CHANGE: {
      if (currentState.auth.isAuth) {
        const match = getMatch(action.payload, new RouteUri(paths.lib));
        if (match) {
          store.dispatch(getListLibs());
        }
      }
    }
  }

  return next(action);
};

export const libsMiddleware = (store: Store<IStoreState>) => (
  next: Dispatch<
    IGetListLibs | ISelectLib | IGetLibVersion | ISaveJob | IGetJob | IUploadLib
  >
) => (
  action:
    | IGetListLibs
    | ISelectLib
    | IGetLibVersion
    | ISaveJob
    | IGetJob
    | IUploadLib
) => {
  const currentState = store.getState();
  switch (action.type) {
    case GET_LIST_LIBS: {
      store.dispatch(
        setListLibsState({
          isFailed: false,
          isLoading: true
        })
      );
      axios
        .get(`${REACT_APP_API_URL}/api/Library/list`)
        .then((data: AxiosResponse<ILib[]>) => {
          store.dispatch(setListLibs(data.data));
          clientStorage.setItem(
            ClientStorageKey.LIBS,
            JSON.stringify(data.data),
            currentState.auth.account
          );
          store.dispatch(
            setListLibsState({
              isFailed: false,
              isLoading: false
            })
          );
        })
        .catch((e: AxiosError) => {
          console.error(e);
          store.dispatch(
            setListLibsState({
              isFailed: true,
              isLoading: false
            })
          );
        });
      break;
    }
    case UPLOAD_LIB: {
      const csv = convertArrayOfObjectsToCSV(action.payload.data);
      if (!csv) {
        break;
      }
      const blob = new Blob([csv], {
        type: "text/csv"
      });
      const data = new FormData();
      data.append("file", blob, "sample.csv");
      axios
        .post(
          `${REACT_APP_API_URL}/api/library/${
            action.payload.libId
          }/upload/site`,
          data,
          {
            headers: {
              "Content-Type":
                "multipart/form-data; boundary=----WebKitFormBoundaryKsetHroO4LDWPs4Y"
            }
          }
        )
        .then((dataRes: AxiosResponse<any[]>) => {
          // action.meta.done(true);
          store.dispatch(getListLibs());
          common.fireNotification(store.dispatch)({
            message: "update lib success.",
            variant: Variant.SUCCESS
          });
        })
        .catch((e: AxiosError) => {
          console.error(e);
          action.meta.done(false);
          common.fireNotification(store.dispatch)({
            message:
              e.response && e.response.data
                ? getErrorMessage(e.response.data, "Failed update lib")
                : "Failed update lib",
            variant: Variant.ERROR
          });
        });
      break;
    }
    case SELECT_LIB: {
      store.dispatch(
        setJobQuery({
          assigneeId: "",
          jobStatus: -1,
          keyword: "",
          take: 10,
          skip: 0,
          isLoading: false,
          isFailed: false,
          polygonCoordinates: []
        })
      );
      clientStorage.setItem(
        ClientStorageKey.CURRENT_LIB,
        action.payload,
        currentState.auth.account
      );
      store.dispatch(getJob(action.payload));
      axios
        .get(`${REACT_APP_API_URL}/api/Library/${action.payload}/users`)
        .then((data: AxiosResponse<ILibUser[]>) => {
          store.dispatch(setLibUsers(data.data));
        })
        .catch((e: AxiosError) => {
          console.error(e);
        });
      store.dispatch(getRecentJobs(action.payload));
      store.dispatch(getLibTemplate(action.payload));
      break;
    }
    case GET_LIB_VERSION: {
      axios
        .get(
          `${REACT_APP_API_URL}/api/Site/${
            currentState.libs.selectedLib
          }/latest`
        )
        .then((data: AxiosResponse<ILibVersion>) => {
          store.dispatch(setLibVersion(data.data));
        })
        .catch((e: AxiosError) => {
          console.error(e);
          common.fireNotification(store.dispatch)({
            message:
              e.response && e.response.data
                ? getErrorMessage(
                    e.response.data,
                    "Failed get latest library version."
                  )
                : "Failed get latest library version.",
            variant: Variant.ERROR
          });
        });
      break;
    }
    case SAVE_JOB: {
      axios
        .post(
          `${REACT_APP_API_URL}/api/Job/${currentState.libs.selectedLib}/new`,
          action.payload
        )
        .then((data: AxiosResponse<ILibVersion>) => {
          store.dispatch(
            setJobQuery({
              assigneeId: "",
              jobStatus: -1,
              keyword: "",
              take: 10,
              skip: 0,
              isLoading: false,
              isFailed: false,
              polygonCoordinates: []
            })
          );
          action.meta.done(true);
          common.fireNotification(store.dispatch)({
            message: "Save job success.",
            variant: Variant.SUCCESS
          });
          store.dispatch(getRecentJobs(currentState.libs.selectedLib));
        })
        .catch((e: AxiosError) => {
          console.error(e);
          action.meta.done(false);
          common.fireNotification(store.dispatch)({
            message:
              e.response && e.response.data
                ? getErrorMessage(e.response.data, "Failed save job.")
                : "Failed save job.",
            variant: Variant.ERROR
          });
        });
      break;
    }
    case GET_JOB: {
      debounceCallSearchKeyword(() => {
        if (cancelerGetPointApi) {
          cancelerGetPointApi();
        }
        const {
          keyword,
          skip,
          take,
          jobStatus,
          assigneeId,
          polygonCoordinates
        } = currentState.libs.jobQuery;
        store.dispatch(
          setJobQuery({
            isLoading: true,
            isFailed: false
          })
        );
        axios
          .post(
            `${REACT_APP_API_URL}/api/Job/${action.payload ||
              currentState.libs.selectedLib}/search/${take}/${skip}`,
            {
              keyword,
              jobStatus,
              assigneeId,
              polygonCoordinates
            },
            {
              cancelToken: new axios.CancelToken(c => {
                cancelerGetPointApi = c;
              })
            }
          )
          .then((data: AxiosResponse<IResponseSearchJob>) => {
            store.dispatch(
              setJobQuery({
                isLoading: false,
                isFailed: false
              })
            );
            store.dispatch(setListJob(data.data));
          })
          .catch((e: AxiosError) => {
            store.dispatch(
              setJobQuery({
                isLoading: false,
                isFailed: true
              })
            );
            console.error(e);
          });
      });
      break;
    }
  }
  return next(action);
};

export const libsRoleMiddleware = (store: Store<IStoreState>) => (
  next: Dispatch<ISetListLibs | ISelectLib>
) => (action: ISetListLibs | ISelectLib) => {
  const currentState = store.getState() as IStoreState;
  switch (action.type) {
    case SET_LIST_LIBS: {
      updateLibRole(store.dispatch)(
        currentState.libs.selectedLib,
        action.payload
      );
      break;
    }
    case SELECT_LIB: {
      updateLibRole(store.dispatch)(action.payload, currentState.libs.list);
      break;
    }
  }
  return next(action);
};

const updateLibRole = (dispatch: Dispatch) => (libId: string, libs: ILib[]) => {
  const currentLib = libs.find(lib => lib.id === libId);
  if (currentLib) {
    dispatch(setCurrentLibRole(currentLib.role));
  }
};

export const libsTemplateMiddleware = (store: Store<IStoreState>) => (
  next: Dispatch<IGetLibTemplate>
) => (action: IGetLibTemplate) => {
  // const currentState = store.getState() as IStoreState;
  switch (action.type) {
    case GET_LIB_TEMPLATE: {
      axios
        .get(`${REACT_APP_API_URL}/api/Misc/${action.payload}/template`)
        .then((data: AxiosResponse) => {
          store.dispatch(setLibTemplate(data.data));
        })
        .catch((e: AxiosError) => {
          console.error(e);
        });
      break;
    }
  }
  return next(action);
};
