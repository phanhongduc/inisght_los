import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import LibControl from "../components/LibControl";
import { common } from "../../../actions";
import Variant from "../../../components/notification/types/variant";

function mapStateToProps({ libs }: IStoreState) {
  return {
    libs: libs.list,
    selectLib: libs.selectedLib,
    isLoading: libs.listState.isLoading,
    selectedLibVersion: libs.selectedLibVersion
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    showError: (message: string, v?: Variant) => {
      common.fireNotification(dispatch)({
        message,
        variant: v ? v : Variant.ERROR
      });
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LibControl);
