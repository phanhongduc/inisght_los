import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import StepImport from "../components/StepImport";
import { setupAuthentication, logout } from "../../auth/actions/Auth";
import { uploadLib } from "../actions/libs";
import { ISiteData } from "../types/libs";

function mapStateToProps({ libs }: IStoreState) {
  let libName = "";
  const currentLib = libs.list.find(el => el.id === libs.selectedLib);
  if (currentLib) {
    libName = currentLib.name;
  }
  return {
    libs: libs.list,
    selectLib: libs.selectedLib,
    libName,
    isLoading: libs.listState.isLoading
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    logout: () => {
      dispatch(logout());
      dispatch(setupAuthentication());
    },
    uploadLib: (
      libId: string,
      data: ISiteData[],
      done: (reset: boolean) => void
    ) => {
      dispatch(uploadLib({ libId, data }, done));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(StepImport);
