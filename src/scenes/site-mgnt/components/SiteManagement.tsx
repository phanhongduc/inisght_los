import * as React from "react";
import withStyles, { CSSProperties } from "@material-ui/core/styles/withStyles";
import "react-table/react-table.css";
import IStyleProps from "../../../styles/utils";
import { Theme } from "@material-ui/core";
import Paper from "@material-ui/core/Paper";
import BackDrop from "@material-ui/core/Backdrop";
import { defaultRules } from "react-hoc-form-validatable";
import SectionLoading from "src/components/SectionLoading";
import ControlBar from "./ControlBar";
import SitesTable from "./SitesTable";
import SideBar from "./SideBar";
import FormAddSite from "./FormAddSite";
import FormEditSite from "./FormEditSite";
import { debounce } from "lodash-es";
import { ISite, ISiteData } from "../types/sites";
import PaginationMui from "src/components/Pagination";
import { ISetSearchQueryPayload } from "../actions/sites";
import { ILib, LibStatus } from "../../lib/types/libs";

interface IProps {
  getListSite: () => void;
  selectedLib: ILib;
  isLoading: boolean;
  isFailed: boolean;
  sites: ISite[];
  total: number;
  searchKeyword: string;
  paging: {
    limit: number;
    page: number;
  };
  updateQuery: (query: ISetSearchQueryPayload) => void;
  searchSite: () => void;
  addSite: (data: ISiteData, cb: (reset: boolean) => void) => void;
  editSite: (
    data: {
      id: string;
      data: ISiteData;
    },
    cb: (reset: boolean) => void
  ) => void;
  deleteSite: (site: ISite, cb: (reset: boolean) => void) => void;
}
const customRule = {
  ...defaultRules,
  validLon: {
    rule(value: any) {
      return value && Number(value) >= -180 && Number(value) <= 180;
    },

    message: {
      error: "It should be in range (-180, 180)"
    }
  },
  validLat: {
    rule(value: any) {
      return value && Number(value) >= -90 && Number(value) <= 90;
    },

    message: {
      error: "It should be in range (-90, 90)"
    }
  }
};
const styles = (theme: Theme): { [k: string]: CSSProperties } => ({
  backdrop: {
    zIndex: 1
  },
  wrap: {
    position: "relative",
    height: "100%",
    overflow: "hidden"
  },
  loading: {
    height: "5px"
  },
  wrapper: {
    height: "100%",
    position: "relative",
    overflowX: "hidden",
    paddingTop: "8px",
    paddingLeft: "16px",
    paddingRight: "16px",
    paddingBottom: "16px"
  },
  paging: {
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
    textAlign: "center"
  }
});

interface IState {
  isOpenSiteAdd: boolean;
  openSiteDetail: string | null;
}

class StepImport extends React.Component<IStyleProps & IProps, IState> {
  public state: IState = {
    isOpenSiteAdd: false,
    openSiteDetail: null
  };

  private debounceSearchKeyword = debounce((cb: () => void) => {
    cb();
  }, 250);

  private onChangeKeyword = (e: React.SyntheticEvent<HTMLInputElement>) => {
    const target = e.target as HTMLInputElement;
    this.props.updateQuery({
      page: 1,
      keyword: target.value
    });
    this.debounceSearchKeyword(() => {
      this.props.searchSite();
    });
  };

  private toggleAddSite = () => {
    this.setState({
      isOpenSiteAdd: !this.state.isOpenSiteAdd
    });
  };
  private onClickRowSite = (id: string) => {
    return () => {
      console.log(id);
      this.setState({
        openSiteDetail: id
      });
    };
  };

  private onChangePagination = (page: number) => {
    this.props.updateQuery({
      page
    });
    this.props.searchSite();
  };

  private createSubmit = (inputs: any, reset: (should: boolean) => void) => {
    this.props.addSite(
      {
        siteId: inputs.siteId.value,
        siteName: inputs.siteName.value,
        networkName: inputs.networkName.value,
        lon: inputs.lon.value,
        lat: inputs.lat.value
      },
      reset
    );
  };

  private editSubmit = (inputs: any, reset: (should: boolean) => void) => {
    if (this.state.openSiteDetail !== null) {
      this.props.editSite(
        {
          id: this.state.openSiteDetail,
          data: {
            siteId: inputs.siteId.value,
            siteName: inputs.siteName.value,
            networkName: inputs.networkName.value,
            lon: inputs.lon.value,
            lat: inputs.lat.value
          }
        },
        reset
      );
    }
  };

  private closeEditSite = () => {
    this.setState({
      openSiteDetail: null
    });
  };

  public componentDidMount() {
    this.props.getListSite();
  }

  public componentDidUpdate(prevProps: IProps) {
    if (prevProps.selectedLib.id !== this.props.selectedLib.id) {
      this.props.getListSite();
    }
  }

  public render() {
    const {
      isLoading,
      sites,
      paging,
      total,
      deleteSite,
      searchKeyword,
      classes,
      selectedLib
    } = this.props;
    const currentSite = sites.find(
      site => site.id === this.state.openSiteDetail
    );
    return (
      <div className={classes.wrap}>
        <div className={classes.loading}>{isLoading && <SectionLoading />}</div>
        <div className={classes.wrapper}>
          <ControlBar
            linkToImport={Boolean(
              selectedLib &&
                (selectedLib.status === LibStatus.FAILED ||
                  LibStatus.NOT_IMPORT)
            )}
            onChangeKeyword={this.onChangeKeyword}
            searchKeyword={searchKeyword}
            toggleAddSite={this.toggleAddSite}
          />
          {sites.length > 0 && (
            <Paper>
              <SitesTable onClickRow={this.onClickRowSite} sites={sites} />
              <div className={classes.paging}>
                <PaginationMui
                  onChangePage={this.onChangePagination}
                  disabled={isLoading}
                  start={paging.page}
                  display={5}
                  total={Math.ceil(total / paging.limit)}
                />
              </div>
            </Paper>
          )}
          {(this.state.isOpenSiteAdd || Boolean(this.state.openSiteDetail)) && (
            <BackDrop
              className={classes.backdrop}
              open={
                this.state.isOpenSiteAdd || Boolean(this.state.openSiteDetail)
              }
            />
          )}
          <SideBar isOpen={this.state.isOpenSiteAdd}>
            <FormAddSite
              onClickBack={this.toggleAddSite}
              submitCallback={this.createSubmit}
              validateLang="en"
              rules={customRule}
            />
          </SideBar>
          <SideBar isOpen={Boolean(this.state.openSiteDetail)}>
            {currentSite && (
              <FormEditSite
                onClickBack={this.closeEditSite}
                validateLang="en"
                rules={customRule}
                submitCallback={this.editSubmit}
                deleteSite={deleteSite}
                site={currentSite}
              />
            )}
          </SideBar>
        </div>
      </div>
    );
  }
}

export default withStyles(styles)(StepImport);
