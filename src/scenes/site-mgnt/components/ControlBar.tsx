import * as React from "react";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import Button from "@material-ui/core/Button";
import InputAdornment from "@material-ui/core/InputAdornment";
import TextField from "@material-ui/core/TextField";
import SearchIcon from "@material-ui/icons/Search";
import Paper from "@material-ui/core/Paper";
import { Link } from "react-router-dom";
import IStyleProps from "../../../styles/utils";
import paths from "../../../paths";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    control: {
      display: "flex",
      paddingRight: theme.spacing.unit * 2,
      paddingLeft: theme.spacing.unit * 2,
      paddingTop: theme.spacing.unit * 2,
      paddingBottom: theme.spacing.unit * 2,
      marginBottom: theme.spacing.unit * 2
    },
    add: {
      marginLeft: "auto",

      "& a": {
        textDecoration: "none"
      },
      "& button:last-child": {
        marginLeft: 10
      }
    },
    role: {
      display: "flex",
      alignItems: "center",
      marginLeft: theme.spacing.unit * 2
    }
  });

interface IProps {
  linkToImport: boolean;
  onChangeKeyword: (e: React.ChangeEvent<HTMLInputElement>) => void;
  searchKeyword: string;
  toggleAddSite: () => void;
}

class ControlBar extends React.Component<IStyleProps & IProps> {
  public render() {
    const {
      linkToImport,
      searchKeyword,
      classes,
      onChangeKeyword,
      toggleAddSite
    } = this.props;
    return (
      <Paper className={classes.control}>
        <TextField
          onChange={onChangeKeyword}
          value={searchKeyword}
          placeholder="Search site"
          InputProps={{
            endAdornment: (
              <InputAdornment position="end">
                <SearchIcon />
              </InputAdornment>
            )
          }}
        />

        <div className={classes.add}>
          {linkToImport && (
            <Link to={paths.libImport}>
              <Button color="primary" variant="raised" className={classes.add}>
                Import site by file
              </Button>
            </Link>
          )}

          <Button onClick={toggleAddSite} color="primary" variant="raised">
            Add Site
          </Button>
        </div>
      </Paper>
    );
  }
}

export default withStyles(styles)(ControlBar);
