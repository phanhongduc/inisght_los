import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { createStyles, Theme } from "@material-ui/core/styles";
// import * as classNames from "classnames";
import Button from "@material-ui/core/Button";
import BackIcon from "@material-ui/icons/ArrowBack";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { HOCForm, FormValidateChildProps } from "react-hoc-form-validatable";
import IStyleProps from "../../../styles/utils";
import InputValidate from "src/components/InputValidate";
import ConfirmDialog from "src/components/Dialog/Confirm";
import { ISite } from "../types/sites";
const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      padding: theme.spacing.unit * 2
    },
    title: { display: "flex", alignItems: "center" },
    submit: {
      marginTop: theme.spacing.unit * 2,
      textAlign: "center",
      display: "block",
      marginLeft: "auto",
      marginRight: "auto"
    },
    deleteButton: {
      marginLeft: "10px"
    }
  });
interface IProps {
  onClickBack: () => void;
  deleteSite: (site: ISite, cb: (reset: boolean) => void) => void;
  site: ISite;
}
interface IState {
  isDelete: boolean;
}
class FormEditSite extends React.Component<
  IStyleProps & IProps & FormValidateChildProps,
  IState
> {
  constructor(props: IStyleProps & IProps & FormValidateChildProps) {
    super(props);
    this.state = {
      isDelete: false
    };
  }
  private onDelteStatus = () => {
    this.setState({
      isDelete: true
    });
  };
  private closeDeleteDialog = (value: boolean) => {
    const { deleteSite, onClickBack } = this.props;
    this.setState({
      isDelete: false
    });
    if (value) {
      deleteSite(this.props.site, (status: boolean) => {
        if (status) {
          onClickBack();
        }
      });
    }
  };
  public render() {
    const { classes, site, onSubmit, submitted, onClickBack } = this.props;
    return (
      <div className={classes.wrap}>
        <div className={classes.title}>
          <IconButton onClick={onClickBack}>
            <BackIcon />
          </IconButton>
          <Typography variant="title">Edit Site</Typography>
        </div>
        <form className={classes.form} noValidate={true} onSubmit={onSubmit}>
          <InputValidate
            defaultValue={site.siteId}
            rule="notEmpty"
            name="siteId"
            type="text"
            margin="normal"
            label="Site Id"
            InputLabelProps={{
              shrink: true
            }}
            className={classes.textField}
            fullWidth={true}
          />
          <InputValidate
            defaultValue={site.siteName}
            rule="notEmpty"
            name="siteName"
            type="text"
            margin="normal"
            label="Site name"
            InputLabelProps={{
              shrink: true
            }}
            fullWidth={true}
          />
          <InputValidate
            defaultValue={site.networkName}
            rule="notEmpty"
            name="networkName"
            type="text"
            margin="normal"
            label="Network Name"
            InputLabelProps={{
              shrink: true
            }}
            fullWidth={true}
          />
          <InputValidate
            defaultValue={site.lon}
            rule="notEmpty|validLon"
            name="lon"
            type="number"
            margin="normal"
            label="Longitude"
            InputLabelProps={{
              shrink: true
            }}
            fullWidth={true}
          />
          <InputValidate
            defaultValue={site.lat}
            rule="notEmpty|validLat"
            name="lat"
            type="number"
            margin="normal"
            label="Latitude"
            InputLabelProps={{
              shrink: true
            }}
            fullWidth={true}
          />
          <div className={classes.submit}>
            <Button
              variant="raised"
              color="primary"
              type="submit"
              disabled={submitted}
            >
              Update
            </Button>
            <Button
              className={classes.deleteButton}
              variant="contained"
              color="secondary"
              onClick={this.onDelteStatus}
            >
              Delete
            </Button>
          </div>
        </form>
        <ConfirmDialog
          isOpen={this.state.isDelete}
          content={`Do you want to delete this site?`}
          onClose={this.closeDeleteDialog}
        />
      </div>
    );
  }
}
export default withStyles(styles)(HOCForm<IStyleProps & IProps>(FormEditSite));
