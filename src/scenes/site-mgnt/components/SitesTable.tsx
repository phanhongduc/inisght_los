import * as React from "react";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import { ISite } from "../types/sites";
import { createStyles, Theme, withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import IStyleProps from "../../../styles/utils";

interface IProps {
  sites: ISite[];
  onClickRow: (id: string) => () => void;
}

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    row: {
      cursor: "pointer",
      "&:nth-of-type(odd)": {
        backgroundColor: theme.palette.divider
      }
    },
    rowHeaded: {
      backgroundColor: "#181A1F",

      "& th": {
        color: theme.palette.common.white
      }
    },
    paging: {
      paddingTop: theme.spacing.unit * 2,
      textAlign: "center"
    }
  });

const UsersTable = ({ sites, classes, onClickRow }: IProps & IStyleProps) => (
  <Table>
    <TableHead>
      <TableRow className={classes.rowHeaded}>
        <TableCell>Site Id</TableCell>
        <TableCell>Site Name</TableCell>
        <TableCell>Network Name</TableCell>
        <TableCell>Longitude</TableCell>
        <TableCell>Latitude</TableCell>
      </TableRow>
    </TableHead>
    <TableBody>
      {sites.map(site => (
        <TableRow
          className={classes.row}
          key={site.id}
          onClick={onClickRow(site.id)}
        >
          <TableCell>{site.siteId}</TableCell>
          <TableCell>{site.siteName}</TableCell>
          <TableCell>{site.networkName}</TableCell>
          <TableCell>{site.lon}</TableCell>
          <TableCell>{site.lat}</TableCell>
        </TableRow>
      ))}
    </TableBody>
  </Table>
);

export default withStyles(styles)(UsersTable);
