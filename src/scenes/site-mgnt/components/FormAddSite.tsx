import * as React from "react";
import { withStyles } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import { createStyles, Theme } from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";
import BackIcon from "@material-ui/icons/ArrowBack";
import IconButton from "@material-ui/core/IconButton";
import Typography from "@material-ui/core/Typography";
import { HOCForm, FormValidateChildProps } from "react-hoc-form-validatable";
import IStyleProps from "../../../styles/utils";
import InputValidate from "src/components/InputValidate";

const styles = (theme: Theme): { [k: string]: CSSProperties } =>
  createStyles({
    wrap: {
      padding: theme.spacing.unit * 2
    },
    title: { display: "flex", alignItems: "center" },
    submit: {
      marginTop: theme.spacing.unit * 2,
      textAlign: "center",
      display: "block",
      marginLeft: "auto",
      marginRight: "auto"
    }
  });
interface IProps {
  onClickBack: () => void;
}
class FormAddSite extends React.Component<
  IStyleProps & IProps & FormValidateChildProps
> {
  public render() {
    const { classes, onSubmit, submitted, onClickBack } = this.props;
    return (
      <div className={classes.wrap}>
        <div className={classes.title}>
          <IconButton onClick={onClickBack}>
            <BackIcon />
          </IconButton>
          <Typography variant="title">Add Site</Typography>
        </div>
        <form className={classes.form} noValidate={true} onSubmit={onSubmit}>
          <InputValidate
            rule="notEmpty"
            name="siteId"
            type="text"
            margin="normal"
            label="Site Id"
            InputLabelProps={{
              shrink: true
            }}
            className={classes.textField}
            fullWidth={true}
          />
          <InputValidate
            rule="notEmpty"
            name="siteName"
            type="text"
            margin="normal"
            label="Site name"
            InputLabelProps={{
              shrink: true
            }}
            fullWidth={true}
          />
          <InputValidate
            rule="notEmpty"
            name="networkName"
            type="text"
            margin="normal"
            label="Network Name"
            InputLabelProps={{
              shrink: true
            }}
            fullWidth={true}
          />
          <InputValidate
            rule="notEmpty|validLon"
            name="lon"
            type="number"
            margin="normal"
            label="Longitude"
            InputLabelProps={{
              shrink: true
            }}
            fullWidth={true}
          />
          <InputValidate
            rule="notEmpty|validLat"
            name="lat"
            type="number"
            margin="normal"
            label="Latitude"
            InputLabelProps={{
              shrink: true
            }}
            fullWidth={true}
          />
          <Button
            className={classes.submit}
            variant="raised"
            color="primary"
            type="submit"
            disabled={submitted}
          >
            Add
          </Button>
        </form>
      </div>
    );
  }
}
export default withStyles(styles)(HOCForm<IStyleProps & IProps>(FormAddSite));
