export const GET_LIST_SITE = "SITE_MGNT/GET_LIST_SITE";
export type GET_LIST_SITE = typeof GET_LIST_SITE;

export const SET_LIST_SITE = "SITE_MGNT/SET_LIST_SITE";
export type SET_LIST_SITE = typeof SET_LIST_SITE;

export const SET_SEARCH_QUERY_SITE = "SITE_MGNT/SET_SEARCH_QUERY_SITE";
export type SET_SEARCH_QUERY_SITE = typeof SET_SEARCH_QUERY_SITE;

export const ADD_SITE = "SITE_MGNT/ADD_SITE";
export type ADD_SITE = typeof ADD_SITE;

export const EDIT_SITE = "SITE_MGNT/EDIT_SITE";
export type EDIT_SITE = typeof EDIT_SITE;

export const DELETE_SITE = "SITE_MGNT/DELETE_SITE";
export type DELETE_SITE = typeof DELETE_SITE;

export const SET_SITE = "SITE_MGNT/SET_SITE";
export type SET_SITE = typeof SET_SITE;
