import { connect, Dispatch } from "react-redux";
import { IStoreState } from "../../../types";
import SiteManagement from "../components/SiteManagement";
import {
  ISetSearchQueryPayload,
  setSearchSiteQuery,
  getListSite,
  addSite,
  editSite,
  deleteSite
} from "../actions/sites";
import { ISiteData, ISite } from "../types/sites";

function mapStateToProps({ sitesManagement, libs }: IStoreState) {
  return {
    selectedLib: libs.list.find(l => l.id === libs.selectedLib),
    isLoading: sitesManagement.searchSiteQuery.isLoading,
    isFailed: sitesManagement.searchSiteQuery.isLoading,
    sites: sitesManagement.listSite.sites,
    total: sitesManagement.listSite.total,
    searchKeyword: sitesManagement.searchSiteQuery.keyword,
    paging: {
      limit: sitesManagement.searchSiteQuery.limit,
      page: sitesManagement.searchSiteQuery.page
    }
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    getListSite: () => {
      dispatch(getListSite());
    },
    updateQuery: (query: ISetSearchQueryPayload) => {
      dispatch(setSearchSiteQuery(query));
    },
    searchSite: () => {
      dispatch(getListSite());
    },
    addSite: (data: ISiteData, cb: (reset: boolean) => void) => {
      dispatch(addSite(data, cb));
    },
    editSite: (
      data: {
        id: string;
        data: ISiteData;
      },
      cb: (reset: boolean) => void
    ) => {
      dispatch(editSite(data, cb));
    },
    deleteSite: (site: ISite, cb: (reset: boolean) => void) => {
      dispatch(deleteSite(site, cb));
    }
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(SiteManagement);
