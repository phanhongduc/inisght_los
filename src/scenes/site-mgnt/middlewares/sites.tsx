import { Dispatch, Store } from "react-redux";
import { IStoreState } from "../../../types";
import axios, { AxiosError, AxiosResponse } from "axios";
import { REACT_APP_API_URL } from "../../../environment";
import { common } from "../../../actions";
import Variant from "../../../components/notification/types/variant";
import {
  GET_LIST_SITE,
  ADD_SITE,
  EDIT_SITE,
  DELETE_SITE
} from "../constants/sites";
import {
  IGetListSite,
  setSearchSiteQuery,
  getListSite,
  setListSite,
  IEditSite,
  IAddSite,
  IDeleteSite
} from "../actions/sites";
import { IResponseListSite, ISearchSiteQuery, ISiteData } from "../types/sites";
import getErrorMessage from "../../../helpers/getErrorMessage";

export const sitesMiddleware = (store: Store<IStoreState>) => (
  next: Dispatch<IGetListSite | IAddSite | IEditSite | IDeleteSite>
) => (action: IGetListSite | IAddSite | IEditSite | IDeleteSite) => {
  const currentState = store.getState() as IStoreState;
  const libId = currentState.libs.selectedLib;
  switch (action.type) {
    case GET_LIST_SITE: {
      getListSiteData(store.dispatch)(
        libId,
        action,
        currentState.sitesManagement.searchSiteQuery
      );
      break;
    }
    case ADD_SITE: {
      addSite(store.dispatch)(libId, action);
      break;
    }
    case EDIT_SITE: {
      updateSite(store.dispatch)(libId, action);
      break;
    }
    case DELETE_SITE: {
      deleteSite(store.dispatch)(libId, action);
      break;
    }
  }
  return next(action);
};

const getListSiteData = (dispatch: Dispatch) => (
  libId: string,
  action: IGetListSite,
  searchSiteQuery: ISearchSiteQuery
) => {
  dispatch(
    setSearchSiteQuery({
      isFailed: false,
      isLoading: true
    })
  );
  axios
    .post(
      `${REACT_APP_API_URL}/api/Site/${libId}/search/${
        searchSiteQuery.limit
      }/${(searchSiteQuery.page - 1) * searchSiteQuery.limit}`,
      {
        keyword: searchSiteQuery.keyword
      }
    )
    .then((data: AxiosResponse<IResponseListSite>) => {
      dispatch(
        setSearchSiteQuery({
          isFailed: false,
          isLoading: false
        })
      );
      dispatch(setListSite(data.data));
    })
    .catch((e: AxiosError) => {
      dispatch(
        setSearchSiteQuery({
          isFailed: true,
          isLoading: false
        })
      );
    });
};
const validData = (data: ISiteData) => {
  return (
    data.lat &&
    data.lon &&
    Number(data.lat) <= 90 &&
    Number(data.lat) >= -90 &&
    Number(data.lon) >= -180 &&
    Number(data.lon) <= 180 &&
    data.networkName &&
    data.siteId &&
    data.siteName
  );
};
const updateSite = (dispatch: Dispatch) => (
  libId: string,
  action: IEditSite
) => {
  if (!validData(action.payload.data)) {
    common.fireNotification(dispatch)({
      message: "Site is invalid",
      variant: Variant.ERROR
    });
    action.meta.cb(false);
    return;
  }
  axios
    .put(`${REACT_APP_API_URL}/api/Site/${libId}/update/${action.payload.id}`, {
      ...action.payload.data
    })
    .then((data: AxiosResponse) => {
      common.fireNotification(dispatch)({
        message: "Update site success.",
        variant: Variant.SUCCESS
      });
      // dispatch(
      //   setSite({
      //     id: action.payload.id,
      //     data: action.payload.data
      //   })
      // );
      dispatch(getListSite());
      setTimeout(() => {
        action.meta.cb(true);
      }, 1000);
    })
    .catch((e: AxiosError) => {
      const response = e.response as AxiosResponse;

      common.fireNotification(dispatch)({
        message:
          response && response.data && response.data.ErrorMessage
            ? response.data.ErrorMessage
            : response.data || "Update site failed.",
        variant: Variant.ERROR
      });

      action.meta.cb(false);
    });
};

const addSite = (dispatch: Dispatch) => (libId: string, action: IAddSite) => {
  if (!validData(action.payload)) {
    common.fireNotification(dispatch)({
      message: "Site is invalid",
      variant: Variant.ERROR
    });
    action.meta.cb(false);
    return;
  }
  axios
    .post(`${REACT_APP_API_URL}/api/Site/${libId}/new`, action.payload)
    .then((data: AxiosResponse) => {
      common.fireNotification(dispatch)({
        message: "Add site success.",
        variant: Variant.SUCCESS
      });
      action.meta.cb(true);
      dispatch(getListSite());
    })
    .catch((e: AxiosError) => {
      const response = e.response as AxiosResponse;
      common.fireNotification(dispatch)({
        message:
          response && response.data
            ? getErrorMessage(response.data, "Add site failed.")
            : "Add site failed.",
        variant: Variant.ERROR
      });

      action.meta.cb(false);
    });
};

const deleteSite = (dispatch: Dispatch) => (
  libId: string,
  action: IDeleteSite
) => {
  axios
    .post(`${REACT_APP_API_URL}/api/Site/${libId}/delete/${action.payload.id}`)
    .then((data: AxiosResponse) => {
      common.fireNotification(dispatch)({
        message: "Delete site success.",
        variant: Variant.SUCCESS
      });
      dispatch(getListSite());
      action.meta.cb(true);
    })
    .catch((e: AxiosError) => {
      const response = e.response as AxiosResponse;
      common.fireNotification(dispatch)({
        message:
          response && response.data
            ? getErrorMessage(response.data, "Delete site failed.")
            : "Delete site failed.",
        variant: Variant.ERROR
      });

      action.meta.cb(false);
    });
};
