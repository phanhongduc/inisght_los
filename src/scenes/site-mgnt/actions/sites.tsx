import {
  GET_LIST_SITE,
  SET_LIST_SITE,
  SET_SEARCH_QUERY_SITE,
  ADD_SITE,
  EDIT_SITE,
  SET_SITE,
  DELETE_SITE
} from "../constants/sites";
import { Action } from "redux";
import { ISite, ISiteData, IResponseListSite } from "../types/sites";
import {} from "../../../constants";

export interface IGetListSite extends Action<GET_LIST_SITE> {}

export function getListSite(): IGetListSite {
  return {
    type: GET_LIST_SITE
  };
}

export interface ISetListSite extends Action<SET_LIST_SITE> {
  payload: IResponseListSite;
}

export function setListSite(data: IResponseListSite): ISetListSite {
  return {
    type: SET_LIST_SITE,
    payload: data
  };
}

export interface ISetSearchQueryPayload {
  keyword?: string;
  page?: number;
  limit?: number;
  isLoading?: boolean;
  isFailed?: boolean;
}

export interface ISetSearchSiteQuery extends Action<SET_SEARCH_QUERY_SITE> {
  payload: ISetSearchQueryPayload;
}

export function setSearchSiteQuery(
  query: ISetSearchQueryPayload
): ISetSearchSiteQuery {
  return {
    type: SET_SEARCH_QUERY_SITE,
    payload: query
  };
}

export interface IAddSite extends Action<ADD_SITE> {
  payload: ISiteData;
  meta: {
    cb: (reset: boolean) => void;
  };
}

export function addSite(
  data: ISiteData,
  cb: (success: boolean) => void
): IAddSite {
  return {
    type: ADD_SITE,
    payload: data,
    meta: {
      cb
    }
  };
}

export interface IDeleteSite extends Action<DELETE_SITE> {
  payload: ISite;
  meta: {
    cb: (reset: boolean) => void;
  };
}

export function deleteSite(
  data: ISite,
  cb: (success: boolean) => void
): IDeleteSite {
  return {
    type: DELETE_SITE,
    payload: data,
    meta: {
      cb
    }
  };
}

export interface IEditSite extends Action<EDIT_SITE> {
  payload: {
    id: string;
    data: ISiteData;
  };
  meta: {
    cb: (reset: boolean) => void;
  };
}

export function editSite(
  data: {
    id: string;
    data: ISiteData;
  },
  cb: (success: boolean) => void
): IEditSite {
  return {
    type: EDIT_SITE,
    payload: data,
    meta: {
      cb
    }
  };
}

export interface ISetSite extends Action<SET_SITE> {
  payload: {
    id: string;
    data: ISiteData;
  };
}

export function setSite(data: { id: string; data: ISiteData }): ISetSite {
  return {
    type: SET_SITE,
    payload: data
  };
}
