export interface ISite {
  lat: string;
  lon: string;
  networkName: string;
  siteId: string;
  siteName: string;
  id: string;
  libraryId: string;
  library: any;
  createdOnUtc: string;
  updatedOnUtc: string;
}

export interface ISiteData {
  lat: string;
  lon: string;
  networkName: string;
  siteId: string;
  siteName: string;
}

export interface IResponseListSite {
  total: number;
  sites: ISite[];
}

export interface ISearchSiteQuery {
  keyword: string;
  page: number;
  limit: number;
  isLoading: boolean;
  isFailed: boolean;
}
