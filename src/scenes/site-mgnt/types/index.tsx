import { IResponseListSite, ISearchSiteQuery } from "./sites";

export interface ISitesManagement {
  listSite: IResponseListSite;
  searchSiteQuery: ISearchSiteQuery;
}
