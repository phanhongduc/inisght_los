import { combineReducers } from "redux";
import { ISetListSite, ISetSearchSiteQuery } from "../actions/sites";
import { IResponseListSite, ISearchSiteQuery } from "../types/sites";
import { SET_SEARCH_QUERY_SITE, SET_LIST_SITE } from "../constants/sites";

function listSite(
  state: IResponseListSite = {
    total: 0,
    sites: []
  },
  action: ISetListSite
) {
  switch (action.type) {
    case SET_LIST_SITE: {
      return action.payload;
    }
  }

  return state;
}

function searchSiteQuery(
  state: ISearchSiteQuery = {
    isFailed: false,
    isLoading: false,
    keyword: "",
    limit: 10,
    page: 1
  },
  action: ISetSearchSiteQuery
) {
  if (action.type === SET_SEARCH_QUERY_SITE) {
    return {
      ...state,
      ...action.payload
    };
  }
  return state;
}

export const sitesManagement = combineReducers({
  listSite,
  searchSiteQuery
});
