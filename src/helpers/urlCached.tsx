// import IAccount from "src/types/Account";
import { UNIQUE_ID } from "./sessionId";
import { User } from "oidc-client";

export default class UrlCached {
  private dics: { [key: string]: { value: string; time: Date } };

  constructor(private acc: User, private cachedTime: number) {
    this.dics = {};
  }

  private keyCreate(shareKey: string) {
    return `${UNIQUE_ID}_${this.acc.profile.sid}_${shareKey}`;
  }

  public setUrl(shareKey: string, value: string) {
    const key = this.keyCreate(shareKey);
    this.dics[key] = {
      value,
      time: new Date()
    };
  }

  public getUrl(shareKey: string): string | null {
    const key = this.keyCreate(shareKey);
    const storeValue = this.dics[key];
    if (!storeValue) {
      return null;
    }
    if (new Date().getTime() - storeValue.time.getTime() > this.cachedTime) {
      return null;
    }
    return storeValue.value;
  }
}
