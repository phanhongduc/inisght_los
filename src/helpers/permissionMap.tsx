import { UserLibRole } from "./permission";

export const PEMISSION_MAP = {
  ban: (role: UserLibRole, targetRole: UserLibRole) => {
    if (role === UserLibRole.User) {
      return false;
    }
    if (targetRole === UserLibRole.Owner) {
      return false;
    }
    return true;
  },

  userList: (role: UserLibRole) => {
    if (role === UserLibRole.User) {
      return false;
    }
    return true;
  }
};
