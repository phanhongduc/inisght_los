import { User } from "oidc-client";

export enum ClientStorageKey {
  ACCOUNT = "account",
  LIBS = "libs",
  CURRENT_LIB = "currentLib",
  DONT_SHOW_NEXT_KEY = "DONT_SHOW_NEXT_KEY",
  MAIN_MAP_STATE = "mainMapState",
  INVITATION_ID_LOCAL_KEY = "INVITATION_ID"
}

class ClientStorage {
  // private account: User | null;
  constructor(private lStore: Storage) {}

  // public setAccount(user: User | null) {
  //     this.account = user;
  //     this.setItem(ClientStorageKey.ACCOUNT, JSON.stringify(user || {}), true);
  // }

  // public getAccount() {
  //     const user = this.getItem(ClientStorageKey.ACCOUNT, true);
  //     this.account = !user ? null : JSON.parse(user);
  //     return this.account;
  // }

  public getItem(key: ClientStorageKey, context: User | null = null) {
    return this.lStore.getItem(this.calKey(key, context));
  }

  public setItem(
    key: ClientStorageKey,
    value: string,
    context: User | null = null
  ) {
    return this.lStore.setItem(this.calKey(key, context), value);
  }

  public removeItem(key: ClientStorageKey, context: User | null = null) {
    return this.lStore.removeItem(this.calKey(key, context));
  }

  private calKey(key: ClientStorageKey, context: User | null): string {
    if (!context) {
      return key.toString();
    }
    return `${key.toString()}_${context.profile.email}`;
  }
}

export default new ClientStorage(localStorage);
