import { Action } from "redux";
import { Dispatch } from "react-redux";

export const NEW_KEY = "NEW_KEY";

export interface IMappingType<T> {
  [key: string]: T;
}

export function mappingValue<T>(value: T): IMappingType<T> {
  return {
    [NEW_KEY]: value
  };
}

export type IMappingAction<T extends Action> = T & {
  key: string;
};

export function mappingDispatch<T extends Action>(
  dispatch: Dispatch<T>,
  key: string = NEW_KEY
) {
  return (action: T) =>
    dispatch({
      ...(action as any),
      key
    });
}

type Reducer<T, U extends Action> = (state: T | undefined, action: U) => T;

export const mappingReducer = <T, U extends Action>(reducer: Reducer<T, U>) => (
  state: IMappingType<T> = {},
  action: IMappingAction<U>
): IMappingType<T> => {
  const key = action.key || NEW_KEY;
  return {
    ...state,
    [key]: reducer(state[key], action)
  };
};

export type CLONE<K> = any;
export type EXTEND<K> = any;

export const cloneKey = <K extends any>(extendKey: EXTEND<K>) => (
  key: K
): CLONE<K> => `${key}_EXTEND_${extendKey}`;
export const revertKey = <T extends CLONE<K>, K extends any>(
  extendKey: EXTEND<K>
) => (key: CLONE<K>): K => {
  return (key as string).replace(`_EXTEND_${extendKey}`, "") as any;
};

export type CLONE_ACTION<T extends Action<K>, K extends any> = {
  [Key in keyof T]: T[Key]
} & {
  type: CLONE<K>;
};

export const cloneAction = <T extends Action<K>, K extends any>(
  key: EXTEND<K>
) => (action: T): CLONE_ACTION<T, K> => ({
  ...(action as any),
  type: cloneKey(key)(action.type)
});

const revertAction = <
  T extends CLONE_ACTION<U, K>,
  K extends any,
  U extends Action<K>
>(
  key: EXTEND<K>
) => (action: T): U => ({
  ...(action as any),
  type: revertKey(key)(action.type)
});

export const cloneReducer = <
  T,
  U extends Action<K> = Action<K>,
  K extends any = any
>(
  key: EXTEND<K>
) => (reducer: Reducer<T, U>) => (state: T, action: CLONE_ACTION<U, K>): T =>
  reducer(state, revertAction<CLONE_ACTION<U, K>, K, U>(key)(action));

export const cloneDispatch = <T extends Action<K>, K extends any = any>(
  key: EXTEND<K>
) => (dispatch: Dispatch<T>): Dispatch<T> => (action: T) =>
  dispatch(cloneAction<T, K>(key)(action) as any);
