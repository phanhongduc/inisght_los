export default function getErrorMessage(
  res: { ErrorMessage?: string } | string[] | string,
  defaultMsg: string
): string {
  if (Array.isArray(res) && res.length > 0) {
    return res[0];
  } else if (
    typeof res === "object" &&
    !Array.isArray(res) &&
    res.ErrorMessage
  ) {
    return res.ErrorMessage;
  } else if (typeof res === "string") {
    return res;
  }
  return defaultMsg;
}
