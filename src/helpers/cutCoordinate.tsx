export default function cutCoordinate(num: number): string {
  if (!num) {
    return "";
  }
  const part = num.toString().split(".");
  if (part[1]) {
    return `${part[0]}.${part[1].slice(0, 4)}`;
  } else {
    return part[0];
  }
}
