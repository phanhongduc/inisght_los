type ResolveFunc<T> = (value: T) => any;
type RejectFunc = (reason: any) => any;

export default class TryAble<R extends any = any> implements Promise<R> {
  public [Symbol.toStringTag]: "Promise";
  private isCancel: boolean;
  private promise: Promise<R>;

  constructor(private tryFunc: () => R) {
    this.promise = new Promise<R>((resolve, reject) => {
      return this.executor(resolve, reject);
    });
    this.isCancel = false;
  }

  private executor(resolve: ResolveFunc<R>, reject: RejectFunc) {
    try {
      const result = this.tryFunc();
      if (result) {
        resolve(result);
      } else {
        setTimeout(() => {
          if (!this.isCancel) {
            this.executor(resolve, reject);
          } else {
            reject({ cancel: this.isCancel });
          }
        });
      }
    } catch (e) {
      reject(e);
    }
  }

  public cancel() {
    this.isCancel = true;
  }
  public then<TResult1 = R, TResult2 = never>(
    onfulfilled?:
      | ((value: R) => TResult1 | PromiseLike<TResult1>)
      | null
      | undefined,
    onrejected?:
      | ((reason: any) => TResult2 | PromiseLike<TResult2>)
      | null
      | undefined
  ): Promise<TResult1 | TResult2> {
    return this.promise.then(onfulfilled, onrejected);
  }
  public catch<TResult = never>(
    onrejected?:
      | ((reason: any) => TResult | PromiseLike<TResult>)
      | null
      | undefined
  ): Promise<R | TResult> {
    return this.promise.catch(onrejected);
  }
}
