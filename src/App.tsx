import * as React from "react";
import { withRouter, HashRouter } from "react-router-dom";
// import { Redirect } from "react-router";
import * as Loadable from "react-loadable";
import withRoot from "./withRoot";
import { withStyles, Theme } from "@material-ui/core/styles";
import { CSSProperties } from "@material-ui/core/styles/withStyles";
import Layout from "./containers/layouts";
import SectionLoading from "./components/SectionLoading";
import paths from "./paths";
import { REACT_APP_ROUTE_HASH } from "./environment";
import Route from "./components/Route";
import { ProtectLevel } from "./components/Route";
import NotiSnackbar from "./components/notification";
import Switch from "./components/Switch";
import "plyr/dist/plyr.css";

const LoadableResetPassword = Loadable({
  loader: () => import("./scenes/auth/containers/ResetPassword"),
  loading: SectionLoading
});

const LoadableSigninCallback = Loadable({
  loader: () => import("./scenes/auth/containers/SigninCallback"),
  loading: SectionLoading
});

const LoadableForgotPassword = Loadable({
  loader: () => import("./scenes/auth/containers/ForgotPassword"),
  loading: SectionLoading
});

const LoadableNetworkBoard = Loadable({
  loader: () => import("./scenes/network/containers/NetworkBoard"),
  loading: SectionLoading
});

const LoadableSelectLib = Loadable({
  loader: () => import("./scenes/lib"),
  loading: SectionLoading
});

const LoadableUserManagement = Loadable({
  loader: () => import("./scenes/user-mgnt"),
  loading: SectionLoading
});

const LoadableSiteManagement = Loadable({
  loader: () => import("./scenes/site-mgnt"),
  loading: SectionLoading
});

const LoadableUserAccept = Loadable({
  loader: () => import("./scenes/user-mgnt/containers/UserAccept"),
  loading: SectionLoading
});

const LoadableGuideTour = Loadable({
  loader: () => import("./scenes/guide-tour"),
  loading: SectionLoading
});

const LoadableReport = Loadable({
  loader: () => import("./scenes/report"),
  loading: SectionLoading
});

const LoadableTemplate = Loadable({
  loader: () => import("./scenes/template/containers/Template"),
  loading: SectionLoading
});

const styles = (theme: Theme): { root: CSSProperties } => ({
  root: {
    paddingTop: theme.spacing.unit * 20,
    textAlign: "center"
  }
});

// const ConnectedSwitch = connect<SwitchProps>(({ routing }: IStoreState) => ({
//   location: routing.location || undefined
// }))(Switch);

const Home = () => <div />;

const AppContainer = ({
  classes,
  theme
}: {
  classes: { root: string };
  theme: Theme;
}) => {
  const routerApp = (
    <Switch>
      <Layout>
        <Route
          protect={ProtectLevel.public}
          exact={true}
          path={paths.callback}
          component={LoadableSigninCallback}
        />
        <Route
          protect={ProtectLevel.public}
          exact={true}
          path={paths.forgot}
          component={LoadableForgotPassword}
        />
        <Route
          protect={ProtectLevel.public}
          exact={true}
          path={paths.resetPassword}
          component={LoadableResetPassword}
        />
        <Route
          protect={ProtectLevel.private}
          path={paths.lib}
          component={LoadableSelectLib}
        />
        <Route
          protect={ProtectLevel.private}
          exact={true}
          path="/"
          component={Home}
        />
        <Route
          protect={ProtectLevel.private}
          path={paths.board}
          component={LoadableNetworkBoard}
        />
        <Route
          protect={ProtectLevel.private}
          path={paths.users}
          component={LoadableUserManagement}
        />
        <Route
          protect={ProtectLevel.private}
          path={paths.siteManagement}
          component={LoadableSiteManagement}
        />
        <Route
          protect={ProtectLevel.private}
          path={paths.invitation}
          component={LoadableUserAccept}
        />
        <Route
          protect={ProtectLevel.private}
          path={paths.report}
          component={LoadableReport}
        />

        <Route
          protect={ProtectLevel.private}
          path={paths.template}
          component={LoadableTemplate}
        />
        <NotiSnackbar />
        <LoadableGuideTour />
      </Layout>
    </Switch>
  );
  if (!REACT_APP_ROUTE_HASH) {
    return routerApp;
  }
  return <HashRouter>{routerApp}</HashRouter>;
};

// Use withRouter to fix problem redirect no render
// https://github.com/ReactTraining/react-router/issues/4924
// https://github.com/ReactTraining/react-router/blob/master/packages/react-router/docs/guides/redux.md
const App = withRouter(
  withRoot(withStyles(styles, { withTheme: true })(AppContainer))
);

export default App;
